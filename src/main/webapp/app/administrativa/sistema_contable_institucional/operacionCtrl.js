app.requires.push('angularModalService');
app.requires.push('ngAnimate');


app.controller("operacionCtrl",["$scope","NgTableParams","crud","modal","ModalService", function ($scope,NgTableParams,crud,modal,ModalService){
   
   //variable para almacenar las operaciones    
   $scope.operaciones=[];
   //variable objeto operacion
   $scope.operacion={operacionID:'',descripcion:"",tipo:"",estado:''};
          
   //arreglo donde estan todas las cuentas operacion asociada
    $scope.cuentaOperaciones = [];
    //variable objeto cuenta contable
    $scope.cuentaOperacion = {cuentaContableID:'',numero:"",nombre:"",estado:"",naturaleza:''};
    //variable que asigna detalle de cuentas a la operacion
    $scope.operacion.cuentaOperaciones=$scope.cuentaOperaciones;
    //variable temporal, para la seleccion de una operacion
    $scope.operacionSel = {};
      
    //Variables para manejo de la tabla
    var paramsOperaciones= {count: 10,sorting: { tipo: "desc" }};
    var settingOperaciones = { counts: [] , filterOptions: { filterComparator: _.startsWith }};
    $scope.tablaOperaciones = new NgTableParams(paramsOperaciones, settingOperaciones);
 
   
  
   $scope.showNuevaOperacion = function() {

    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/agregarOperacion.html",
      controller: "agregarNuevaOperacionCtrl",
      inputs: {
        title: "Nueva Operación"
      }
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
             $scope.operacion  = result.operacion;
                //insertamos el elemento a la lista
                insertarElemento(settingOperaciones.dataset,$scope.operacion);
                $scope.tablaOperaciones.reload();
                console.log($scope.operacion);
            }           
                              
      });
    });
   
  };  
  
   $scope.listarOperaciones = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('operacion',1,'listarOperacion');        
        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
        crud.listar("/sistemaContable",request,function(response){
            console.log(response.toString());
            settingOperaciones.dataset = response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingOperaciones.dataset);
            $scope.tablaOperaciones.settings(settingOperaciones);
            console.log( $scope.tablaOperaciones);
        },function(data){
            console.info(data);
        });
    };  
  
  $scope.showNuevaCuenta = function() {

    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/agregarCuentaContable.html",
      controller: "agregarNuevaCuentaCtrl",
      inputs: {
        title: "Nueva Cuenta Contable"
      }
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
        $scope.cuentaContable  = result.cuenta;
      });
    });
    console.log($scope.cuentaContable);
  };
  
   $scope.prepararEditar = function(t){
        $scope.operacionSel = JSON.parse(JSON.stringify(t));
        
      ModalService.showModal({
        templateUrl: "administrativa/sistema_contable_institucional/agregarOperacion.html",
      controller: "editarOperacionCtrl",
      inputs: {
        operacion:$scope.operacionSel,
        title: "Editar Operación"
      }
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
             $scope.operacion  = result.operacion;
                //insertamos el elemento a la lista
                settingOperaciones.dataset[$scope.operacionSel.i] = $scope.operacion;
                $scope.tablaOperaciones.reload();
                console.log($scope.operacion);
            }           
                              
      });
    });
       
    };
  
  $scope.eliminarOperacion = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('operacion',1,'eliminarOperacion');
            request.setData({operacionID:idDato});

            crud.eliminar("/sistemaContable",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingOperaciones.dataset,i);
                    $scope.tablaOperaciones.reload();
                }

            },function(data){
                console.info(data);
            });
            
        })};
    
   $scope.tipo  = function(t){
        if(t==='E')
            return "Egreso";
        return "Ingreso";
    };
    
  
        
}]);



app.controller('agregarNuevaOperacionCtrl', [
    '$scope', '$element', 'title', 'close','crud','modal','ModalService','NgTableParams',
    function($scope, $element, title, close,crud,modal,ModalService,NgTableParams) {
             
//Variable que servira para crear una nueva operacion
 $scope.operacion={operacionID:0,descripcion:"",tipo:'',estado:'A'}
             
 //variable que servira para crear una nueva cuenta contable
   $scope.cuenta = {cuentaContableID:'',numero:"",nombre:"",naturaleza:""};
   $scope.title=title;
 //Variables parala naturaleza de la cuenta  
    $scope.tipoNaturaleza ={tipo:'',descripcion:""} 
    $scope.listaNaturaleza = [{tipo:true,descripcion:"Debe"},{tipo:false,descripcion:"Haber"}];      
//variables de la lista de cuentas asociadas
    $scope.listaCuentas=[];        
    
    //arreglo donde estan todas las cuentas contables
    $scope.cuentasContables = [];
    //variable que servira para objeto de la cuenta contable
    $scope.cuentaContable = {cuentaContableID:0,tipo:"0",nombre:""};
    //variable temporal, para la seleccion deuna cuenta contable
    $scope.cuentaContableSel = {};
      
    //Variables para manejo de la tabla
    var paramsCuenta= {count: 10};
    var settingCuenta = { counts: []};
    $scope.tablaCuenta = new NgTableParams(paramsCuenta, settingCuenta);
  
  
     function  listarTabla (){
        //preparamos un objeto request
        var request = crud.crearRequest('cuentaContable',1,'listarCuentaContable');        
        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistemaContable",request,function(response){
            console.log(response.toString());
            settingCuenta.dataset = response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingCuenta.dataset);
            $scope.tablaCuenta.settings(settingCuenta);
            console.log( $scope.tablaCuenta);
        },function(data){
            console.info(data);
        });
    };  

   
  
  
 $scope.agregarCuenta=function (){
     $scope.listaCuentas.push($scope.cuenta);
     $scope.cuenta = {cuentaContableID:'',numero:"",nombre:"",naturaleza:""};
     
 };
 
 $scope.eliminarCuentaOperacion =function (i){   
        $scope.listaCuentas.splice(i, 1);                              
 };
 
 $scope.desNat=function (t){
     if(t)
         return "Debe";
     return "Haber";
 };
     
 listarTabla();
 
 $scope.showBuscarCuenta = function() {

    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/buscarCuenta.html",
      controller: "buscarCuentaCtrl",
      inputs: {
        title: "Seleccionar Cuenta Contable",
        tabla: $scope.tablaCuenta
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
              $scope.cuenta=result.data;
            
          }
        
      });
    });
    console.log($scope.cuenta);
  };
  
  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
 	  close({
      cuenta: $scope.cuentaContable,
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Manually hide the modal.
    $element.modal('hide');
    
    //  Now call close, returning control to the caller.
    close({
      cuenta: $scope.cuentaContable,
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
 $scope.agregarOperacion = function(){
        
        var request = crud.crearRequest('operacion',1,'insertarOperacion');                            
        
        $scope.operacion.listaCuentas=$scope.listaCuentas;
        request.setData($scope.operacion);
        
        if($scope.operacion.descripcion==="" ){
            modal.mensaje("CONFIRMACION","Ingrese nombre de la Cuenta");
            return;
        }
         if($scope.operacion.tipo==="" ){
            modal.mensaje("CONFIRMACION","Seleccion el tipo de operación");
            return;
        }
         if($scope.operacion.listaCuentas.length===0 || $scope.operacion.listaCuentas.length>1 ){
            modal.mensaje("CONFIRMACION","Seleccione solo una cuenta contable");
            return;
        }
      
        crud.insertar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                  //recuperamos las variables que nos envio el servidor
                $scope.operacion.operacionID = response.data.operacionID;
                $scope.operacion.descripcion = response.data.descripcion;
                $scope.operacion.estado = response.data.estado;
                $scope.operacion.cuentaOperaciones = response.data.cuentaOperaciones;
                $scope.operacion.listaCuentas=[];
             
                 
               //  Manually hide the modal.
             $element.modal('hide');
    
                 //  Now call close, returning control to the caller.
                 close({
                     operacion:$scope.operacion,
                       flag:true
                }, 500); // close, but give 500ms for bootstrap to animate
            }  
           
        },function(data){
            console.info(data);
        });               
          console.log($scope.operacion);   
    };  
  
  
  

}]);




app.controller('editarOperacionCtrl', [
    '$scope', '$element', 'title','operacion' ,'close','crud','modal','ModalService','NgTableParams',
    function($scope, $element, title,operacion, close,crud,modal,ModalService,NgTableParams) {
             
 //Variable que servira para crear una nueva operacion
 $scope.operacion={operacionID:0,descripcion:"",tipo:'',estado:'A'}
             
 //variable que servira para crear una nueva cuenta contable
   $scope.cuenta = {cuentaContableID:'',nombre:"",naturaleza:""};
   $scope.title=title;
 //Variables parala naturaleza de la cuenta  
    $scope.tipoNaturaleza ={tipo:'',descripcion:""} 
    $scope.listaNaturaleza = [{tipo:true,descripcion:"Debe"},{tipo:false,descripcion:"Haber"}];      
//variables de la lista de cuentas asociadas
    $scope.listaCuentas=[];        
    
    //arreglo donde estan todas las cuentas contables
    $scope.cuentasContables = [];
    //variable que servira para objeto de la cuenta contable
    $scope.cuentaContable = {cuentaContableID:0,tipo:"0",nombre:""};
    //variable temporal, para la seleccion deuna cuenta contable
    $scope.cuentaContableSel = {};
      
    //Variables para manejo de la tabla
    var paramsCuenta= {count: 10};
    var settingCuenta = { counts: []};
    $scope.tablaCuenta = new NgTableParams(paramsCuenta, settingCuenta);
  
    $scope.operacion=operacion;
    $scope.listaCuentas =operacion.cuentaOperaciones;
    
     function  listarTabla (){
        //preparamos un objeto request
        var request = crud.crearRequest('cuentaContable',1,'listarCuentaContable');        
        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistemaContable",request,function(response){
            console.log(response.toString());
            settingCuenta.dataset = response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingCuenta.dataset);
            $scope.tablaCuenta.settings(settingCuenta);
            console.log( $scope.tablaCuenta);
        },function(data){
            console.info(data);
        });
    };  

   
  
  
 $scope.agregarCuenta=function (){
     $scope.listaCuentas.push($scope.cuenta);
     $scope.cuenta = {cuentaContableID:'',nombre:"",naturaleza:""};
     
 };
 
 $scope.eliminarCuentaOperacion =function (i){   
        $scope.listaCuentas.splice(i, 1);                              
 };
 
 
 $scope.desNat=function (t){
     if( t==='true')
         return "Debe";
     return "Haber";
 };
     
 listarTabla();
 
 $scope.showBuscarCuenta = function() {

    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/buscarCuenta.html",
      controller: "buscarCuentaCtrl",
      inputs: {
        title: "Seleccionar Cuenta Contable",
        tabla: $scope.tablaCuenta
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
              $scope.cuenta=result.data;
            
          }
        
      });
    });
    console.log($scope.cuenta);
  };
  
  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
 	  close({
      cuenta: $scope.cuentaContable,
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Manually hide the modal.
    $element.modal('hide');
    
    //  Now call close, returning control to the caller.
    close({
      cuenta: $scope.cuentaContable,
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
 $scope.actualizarOperacion = function(){
        
        var request = crud.crearRequest('operacion',1,'actualizarOperacion');                            
        
        $scope.operacion.listaCuentas=$scope.listaCuentas;
        request.setData($scope.operacion);
     
      if($scope.operacion.descripcion==="" ){
            modal.mensaje("CONFIRMACION","Ingrese nombre de la Cuenta");
            return;
        }
         if($scope.operacion.tipo==="" ){
            modal.mensaje("CONFIRMACION","Seleccion el tipo de operación");
            return;
        }
         if($scope.operacion.listaCuentas.length===0 || $scope.operacion.listaCuentas.length>1 ){
            modal.mensaje("CONFIRMACION","Seleccione solo una cuenta contable");
            return;
        }
        crud.actualizar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                               
                 
               //  Manually hide the modal.
             $element.modal('hide');
    
                 //  Now call close, returning control to the caller.
                 close({
                     operacion:$scope.operacion,
                       flag:true
                }, 500); // close, but give 500ms for bootstrap to animate
            }  
           
        },function(data){
            console.info(data);
        });               
          console.log($scope.operacion);   
    };  
  
  
  

}]);



app.controller('buscarCuentaCtrl', [
  '$scope', '$element', 'title','tabla', 'close','crud','modal','NgTableParams',
  function($scope, $element, title,tabla, close,crud,modal,NgTableParams) {
   
    
  $scope.title=title;
  $scope.tablaCuenta=tabla;
  
  

  $scope.setClickedRow=function (d){
        
       //  Manually hide the modal.
             $element.modal('hide');
    
                 //  Now call close, returning control to the caller.
                 close({
                     data: d,
                     flag:true
                }, 500); // close, but give 500ms for bootstrap to animate
                
                console.log(d);
  }
 
  $scope.close = function() {
      $element.modal('hide');
 	  close({
     
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Manually hide the modal.
    $element.modal('hide');
    
    //  Now call close, returning control to the caller.
    close({
    
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
 

}]);




        

