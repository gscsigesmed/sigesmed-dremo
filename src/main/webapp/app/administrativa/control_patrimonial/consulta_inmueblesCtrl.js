app.controller("consulta_inmueblesCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){


    /*Tabla de Bienes Inmuebles*/
    
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_bienes_inmuebles = new NgTableParams(params, setting); 


    $scope.listar_inmuebles = function(orgId){
        var request = crud.crearRequest('ingresos',1,'listar_bienes_inmuebles');
        request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_bienes_inmuebles.settings(setting);
                $scope.tabla_bienes_inmuebles.reload();
            }
        },function(data){
            console.info(data);
        });
    }

    $scope.editarBienInmueble = function(bien_inm,orgId){
            $scope.verificar_bien = {
                id_bien:0,
                org_id:0,
                interfaz:""
              };
        $scope.verificar_bien.id_bien = bien_inm.cod_bie_inm;
        $scope.verificar_bien.org_id = orgId;
        $scope.verificar_bien.interfaz = "consulta_inmuebles";

        localStorage.setItem('update_bien_inmueble', window.btoa(JSON.stringify($scope.verificar_bien)) );
        $location.path('bienes_inmuebles');

    }

    $scope.eliminarBienInmueble = function(bien_inm){
        
        modal.mensajeConfirmacion($scope,"Seguro que desea Eliminar el Bien Inmueble ?",function(){
       
        var request = crud.crearRequest('ingresos',1,'eliminar_bien_inmueble');
           request.setData(bien_inm);
           
           crud.eliminar("/controlPatrimonial",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    /*Eliminamos el Elemento en la Tabla y Actualizamos*/
                    eliminarElemento(setting.dataset, bien_inm.i);
                    iniciarPosiciones(setting.dataset);
                    $scope.tabla_bienes_inmuebles.settings(setting);
                    $scope.tabla_bienes_inmuebles.reload();
                }

            },function(data){
            });

        });

    }

    /*Edicion y Verificacion de Bienes*/
    $scope.verificar_registro_bien = function(bien,user){
    
    
    $scope.verificar_bien = {
      id_bien:0,
      org_id:0,
      interfaz:""
    };
    $scope.verificar_bien.id_bien = bien.cod_bie;
    $scope.verificar_bien.org_id = bien.org_id;
    $scope.verificar_bien.interfaz = "consulta_inmuebles";
    
    localStorage.setItem('update_bien_inmueble', window.btoa(JSON.stringify($scope.verificar_bien)) );
    $location.path('bienes_inmuebles');

};


}]);