app.controller("bienes_inmueblesCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){

    /*Datos Catalogo*/
    $scope.item_catalogo= {
        gru_gen_id:0,
        cla_gen_id:0,
        fam_gen_id:0,
        uni_med_id:0,
        cod:0,
        den_bie:""
    };
    
    $scope.ambiente = {
        descripcion:"",
        ubicacion:"",
        estado:"",
        area:0,
        inst_cons:"",
        fec_cons:new Date(),
        user:0,
        orgid:0
    };
    
    /*Condicion*/
    $scope.condicion = [
        {cond_id:"B",cond_nom:"BUENO"},
        {cond_id:"M",cond_nom:"MALO"},
        {cond_id:"R",cond_nom:"REGULAR"}
    ];
    
    $scope.lista = {
        direccion :"",
        propiedad:"",
        tipo_terreno:"",
        um:"",
        condicion:""
    }
    
    
    /*Datos Bien Inmueble*/
    $scope.bien_inmueble = {
        /*Cabecera*/
        descripcion:"",
        tip_dire:"",
        direccion:"",
        nro:0,
        mz:"",
        lt:"",
        departamento:"",
        provincia:"",
        distrito:"",
        user:0,
        orgId:0,
        /*Detalle Tecnico*/
        propiedad:"",
        tipo_terreno:"",
        est_sanea:false,
        area:0,
        um:"",
        part_elec:"",
        nro_fic_reg:"",
        reg_sinabip:"",
        /*Ambiente*/
        amb_id:0  
    };
    
    
    /*INFORMACION ADICIONAL*/
       $scope.propiedad= [
           {nombre:"Estatal"}, {nombre:"Particular"}  
       ];
       
       $scope.tipo_terreno = [
           {nombre:"Estatal"},{nombre:"Particular"}
       ];
       $scope.unidades_metricas = [
           {nombre:"m2"} , {nombre:"km2"}  
       ];
       
       /*Direccion*/
       $scope.direccion= [
           {nombre:"Avenida"}, {nombre:"Jiron"}, {nombre:"Calle"}, 
           {nombre:"Urbanizacion"}, {nombre:"Alameda"}, {nombre:"Malecon"}, 
           {nombre:"Ovalo"}, {nombre:"Alameda"}, {nombre:"Plaza"}, 
           {nombre:"Unidad Vecinal"}, {nombre:"Cooperativa"}, {nombre:"Parque"}, 
           {nombre:"Residencial"}, {nombre:"Carretera"}, {nombre:"Asent.Humano"}
       ];$scope.dire={};
      
      var listar_ambientes = false;
    
    /*FUNCIONES AUXILIARES*/
    $scope.listar_tipo_propiedad = function(){
        
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_tipo_propiedad');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.propiedad = data.data;
            }
            },function(data){    
            console.info(data);
        });  
    }
    
    $scope.listar_tipo_terreno = function(){
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_tipo_terreno');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.tipo_terreno = data.data;
            }
            },function(data){
                
            console.info(data);
        });
   
    }
    $scope.listar_unidades_metricas = function(){
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_unidades_metricas');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.unidades_metricas = data.data;
            }
            },function(data){
                
            console.info(data);
        });
    } 
    /*----------------------*/

    /*Tabla de Ambientes*/
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_ambientes = new NgTableParams(params, setting); 
    
    
    $scope.registrar_bien_inmueble = function(user,orgid){
        
        /*OBTENEMOS LAS LISTAS*/
        $scope.bien_inmueble.tip_dire=$scope.lista.direccion.nombre;
        $scope.bien_inmueble.propiedad=$scope.lista.propiedad.nombre;
        $scope.bien_inmueble.tipo_terreno=$scope.lista.tipo_terreno.nombre;
        $scope.bien_inmueble.um=$scope.lista.um.nombre;
        
        
        
        $scope.bien_inmueble.user = user;
        $scope.bien_inmueble.orgId = orgid;
        
        var accept = true;
        for(var atrib in $scope.bien_inmueble){
            if($scope.bien_inmueble[atrib]==""){
                accept = false;
                break;
            }
        }
        if(accept==true){
              modal.mensajeConfirmacion($scope,"Seguro que desea registrar el Bien Inmueble",function(){
               
               var request = crud.crearRequest('ingresos',1,'registrar_bien_inmueble');
               request.setData($scope.bien_inmueble);        
               
               crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                    
                    $scope.borrar_valores_bien();   
                    
                   }
               },function(data){
                   console.info(data);
               });
            });
        }
        else{
             modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten correctamente llenados");
        }

    }
    
    
    $scope.borrar_valores_bien = function(){
        $scope.bien_inmueble = {
            /*Cabecera*/
            direccion:"",
            nro:0,
            mz:"",
            lt:"",
            departamento:"",
            provincia:"",
            distrito:"",
            user:0,
            orgId:0,
            /*Detalle Tecnico*/
            propiedad:"",
            tipo_terreno:"",
            est_sanea:'N',
            area:0,
            um:"",
            part_elec:"",
            nro_fic_reg:"",
            /*Ambiente*/
            amb_id:0  
        };
    }
 
    $scope.mostrarAmbientes = function(){
        var request = crud.crearRequest('ingresos',1,'listar_ambientes');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();
                
            }
        },function(data){
            console.info(data);
        });
   }
   
   $scope.registrarAmbientes = function(user,org_id){
       
    //   $scope.ambiente.estado = $scope.ambiente.estado.cond_nom;
       $scope.ambiente.estado = $scope.lista.condicion.cond_id;
       
       $scope.ambiente.fec_cons = convertirFecha($scope.ambiente.fec_cons);
       $scope.ambiente.user = user;
       $scope.ambiente.orgid = org_id;
       var accept = true;
        for(var atrib in $scope.ambiente){
            if($scope.ambiente[atrib]==""){
                accept = false;
                break;
            }
        }
        if(accept==true){
            modal.mensajeConfirmacion($scope,"Seguro que desea registrar Ambiente",function(){
               
               var request = crud.crearRequest('ingresos',1,'registrar_ambientes');
               request.setData($scope.ambiente);        
               
               crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                    
                    $scope.borrar_valores_ambiente();   
                    
                   }
               },function(data){
                   console.info(data);
               });
            });
        }else{
             modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten correctamente llenados");
        }
   }
   
   
   $scope.borrar_valores_ambiente = function(){
        $scope.ambiente = {
        descripcion:"",
        ubicacion:"",
        estado:"",
        area:0,
        inst_cons:"",
        fec_cons:new Date()
    };
   }
   
   $scope.reporte_bienes_inmuebles = function(){
       
       
   }
   
   $scope.mostar_ambientes = function(orgId){
       
        var request = crud.crearRequest('ingresos',1,'listar_ambientes');
            request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();

                if($scope.update_bien_inmueble.interfaz == "consulta_inmuebles"){
                        $scope.buscar_bien_inmueble($scope.update_bien_inmueble.id_bien,$scope.update_bien_inmueble.org_id);    
                        localStorage.removeItem('update_bien_inmueble');
                }
            }
        },function(data){
            console.info(data);
        });
 
   }
   
   $scope.ElegirAmbiente = function(amb){

       $scope.bien_inmueble.amb_id = amb.amb_id;
       var my_array = new Array();
       my_array[0] = amb;
       setting.dataset = my_array;
       iniciarPosiciones(setting.dataset);
       $scope.tabla_ambientes.settings(setting);
       $scope.tabla_ambientes.reload();
   }

    $scope.buscar_bien_inmueble = function(id_bien,orgId){
        
       
        var request = crud.crearRequest('ingresos',1,'obtener_bien_inmueble');
            request.setData({id_bie_inm:id_bien});
            crud.listar("/controlPatrimonial",request,function(data){
                if(data.data){    
                    $scope.bien_inmueble.tip_dire = data.data[0].direccion;   
                    $scope.obtenerDireccion(data.data[0].tip_dire);
                    $scope.bien_inmueble.descripcion = data.data[0].descripcion;
                    $scope.bien_inmueble.nro = data.data[0].nro;
                    $scope.bien_inmueble.mz = data.data[0].mz;    
                    $scope.bien_inmueble.lt = data.data[0].lt;    
                    $scope.bien_inmueble.departamento = data.data[0].departamento;
                    $scope.bien_inmueble.provincia = data.data[0].provincia;
                    $scope.bien_inmueble.distrito = data.data[0].distrito;
                    $scope.obtenerPropiedad(data.data[0].propiedad);
                    $scope.obtenerTipoTerreno(data.data[0].tipo_terreno);
                    $scope.obtenerUM(data.data[0].um);
                    $scope.bien_inmueble.part_elec = data.data[0].part_elec;
                    $scope.bien_inmueble.nro_fic_reg = data.data[0].nro_fic_reg;
                    $scope.bien_inmueble.reg_sinabip = data.data[0].reg_sinabip;
                    $scope.bien_inmueble.est_sanea = data.data[0].est_sanea;
                //    $scope.obtenerAmbiente(data.data[0].amb_id);
                    $scope.bien_inmueble.amb_id = data.data[0].amb_id;
                    
                   
                }
            },function(data){
                    console.info(data);
            }); 
 
    }
    
    $scope.update_bien_inmueble = function(){
        $scope.update_bien_inmueble = JSON.parse( window.atob( localStorage.getItem('update_bien_inmueble')) );
        
        if(listar_ambientes==true)
        {
            $scope.buscar_bien_inmueble($scope.update_bien_inmueble.id_bien,$scope.update_bien_inmueble.org_id);    
            localStorage.removeItem('update_bien_inmueble');
        }
        
    }
    $scope.obtenerPropiedad = function(NomProp){
       
        if($scope.propiedad[0].nombre == NomProp){
            $scope.lista.propiedad= $scope.propiedad[0];
        }else{
            $scope.lista.propiedad = $scope.propiedad[1];

        }
    }
    $scope.obtenerTipoTerreno = function(NomTipTer){
        
        if($scope.tipo_terreno[0].nombre == NomTipTer){
             $scope.lista.tipo_terreno=$scope.tipo_terreno[0];
        }else{
             $scope.lista.tipo_terreno=$scope.tipo_terreno[1];
        }

    }
    
    $scope.obtenerUM = function(NomUM){
        
        if($scope.unidades_metricas[0].nombre == NomUM){
            $scope.lista.um = $scope.unidades_metricas[0];
        }else{
            $scope.lista.um = $scope.unidades_metricas[1];
        }
    }
    
    $scope.obtenerDireccion = function(Dire){
        
        if($scope.direccion[0].nombre == Dire){
            $scope.lista.direccion = $scope.direccion[0];
        }else{
            $scope.lista.direccion = $scope.direccion[1];
        }
    }
    
    $scope.obtenerAmbiente = function(){
        
    }

}]);

