/* global modal */

app.controller("generarDocumentosCtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", function ($rootScope, $scope, NgTableParams, crud, modal) {



        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.tipoPlantilla = {};
        $scope.estados = [{id: 1, title: "Registrado"}, {id: 2, title: "Pendiente"}];
        $scope.descripcionPlantilla = {};
        $scope.alineacion = [{id: 1, title: 'Centrado'}, {id: 2, title: 'Justificado'}, {id: 3, title: 'Izquierda'}, {id: 4, title: 'Derecha'}];
        $scope.dataBase64 = "";
        $scope.nombreTipoDocumento = {};
        $scope.tipoDocumentos = [{tipoDocumentoID: 0, nombre: "", descripcion: "", fecha: "", estado: ""}];
        $scope.nuevoDocumento = {
            planId: 0,
            tipoDoc: {},
            descripcion: "",
            version: 0,
            contenidoPlantilla: [{nombreLetra: null, size: 0, contenido: "", alineacion: {}, isBold: false, isCursiva: false,isSubrayado:false}],
            personaID: $rootScope.usuMaster.usuario.ID,
            marcaAgua:true,
            verImagen:false,
            isSaved:false,
            documento:{nombreArchivo:"",archivo:{},url:""}
        };
        $scope.verImagen={};


        $scope.listarPlantillas = function () {

            //preparamos un objeto request
            var request = crud.crearRequest('configuracion', 1, 'listarPlantillas');
            request.setData({organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarTipoDocumentos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('tramite_datos', 1, 'listarTipoDocumentos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/tramiteDocumentario", request, function (data) {
                $scope.tipoDocumentos = data.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarImagenes = function (t) {
            //preparamos un objeto request
            var request = crud.crearRequest('configuracion', 1, 'listarImagenes');
            request.setData({plantillaID: t});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.imagenes = data.data;
                
//                $scope.iniciarDatos();
            }, function (data) {
                console.info(data);
            });
            
        };
        
        $scope.generarDocumento = function (t) {

            $scope.plantillaSel = JSON.parse(JSON.stringify(t));
            $scope.verImagen={};
            $scope.nuevoDocumento.documento.nombreArchivo="";
            $scope.nuevoDocumento.documento.url="";
            $scope.nuevoDocumento.documento.nombreArchivo=t.archivo;
            $scope.nuevoDocumento.documento.url=t.url;
            $scope.nuevoDocumento.descripcion = t.descripcion;
            $scope.nuevoDocumento.planId = t.plantillaId;
            $scope.nuevoDocumento.tipoDoc = t.tipoDocumento;
            $scope.planId = $scope.plantillaSel.plantillaId;
            $scope.tipoDoc = t.tipoDocumento;
            
            var request = crud.crearRequest('generarDocumento', 1, 'traerPlantilla2Editar');
            request.setData({plantillaID: $scope.plantillaSel.plantillaId});
            modal.mensajeConfirmacion($scope, "Esta seguro que desea usar la plantilla?", function () {
                crud.listar("/documentosComunicacion", request, function (response) {
                    if (response)
                    {
                        $scope.plantillaSel = response.data;
                        $scope.listarImagenes($scope.planId );
                        $scope.nuevoDocumento.tipoDoc = $scope.tipoDocumentos[t.tipoDocumentoID - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[0].alineacion = $scope.alineacion[$scope.plantillaSel[0].alineacion - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[1].alineacion = $scope.alineacion[$scope.plantillaSel[1].alineacion - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[2].alineacion = $scope.alineacion[$scope.plantillaSel[2].alineacion - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[0].nombreLetra = $scope.letras[$scope.plantillaSel[0].letraId - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[1].nombreLetra = $scope.letras[$scope.plantillaSel[1].letraId - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[2].nombreLetra = $scope.letras[$scope.plantillaSel[2].letraId - 1];
                        $scope.nuevoDocumento.contenidoPlantilla[0].size = $scope.plantillaSel[0].tamanho;
                        $scope.nuevoDocumento.contenidoPlantilla[1].size = $scope.plantillaSel[1].tamanho;
                        $scope.nuevoDocumento.contenidoPlantilla[2].size = $scope.plantillaSel[2].tamanho;
                        $scope.nuevoDocumento.contenidoPlantilla[0].isCursiva = $scope.plantillaSel[0].isCursiva;
                        $scope.nuevoDocumento.contenidoPlantilla[1].isCursiva = $scope.plantillaSel[1].isCursiva;
                        $scope.nuevoDocumento.contenidoPlantilla[2].isCursiva = $scope.plantillaSel[2].isCursiva;
                        $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = $scope.plantillaSel[0].isSubrayado;
                        $scope.nuevoDocumento.contenidoPlantilla[1].isSubrayado = $scope.plantillaSel[1].isSubrayado;
                        $scope.nuevoDocumento.contenidoPlantilla[2].isSubrayado = $scope.plantillaSel[2].isSubrayado;
                        $scope.nuevoDocumento.contenidoPlantilla[0].isBold = $scope.plantillaSel[0].isBold;
                        $scope.nuevoDocumento.contenidoPlantilla[1].isBold = $scope.plantillaSel[1].isBold;
                        $scope.nuevoDocumento.contenidoPlantilla[2].isBold = $scope.plantillaSel[2].isBold;
                        $scope.nuevoDocumento.contenidoPlantilla[0].contenido = $scope.plantillaSel[0].contenido;
                        $scope.nuevoDocumento.contenidoPlantilla[1].contenido = $scope.plantillaSel[1].contenido;
                        $scope.nuevoDocumento.contenidoPlantilla[2].contenido = $scope.plantillaSel[2].contenido;
                        $scope.nuevoDocumento.marcaAgua=true;
                        $('#modalGenerarDocumento').modal('show');
                    }

                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.generar = function ()
        {
            if (typeof $scope.verImagen.verImagen == "undefined") {
                $scope.nuevoDocumento.verImagen = false;
            }
            else
            {
                $scope.nuevoDocumento.verImagen = true;
                $scope.nuevoDocumento.isSaved = false;
                $scope.nuevoDocumento.nombreArchivo=$scope.verImagen.verImagen.nombreArchivo;
                $scope.nuevoDocumento.imgID=$scope.verImagen.verImagen.imgID;
            }
            var request = crud.crearRequest('generarDocumento', 1, 'nuevoDocumento');
            request.setData($scope.nuevoDocumento);

            crud.insertar("/documentosComunicacion", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if(data.responseSta)
                {
                    $scope.vistaPreliminar(false);
                    $('#modalGenerarDocumento').modal('hide');
                }
                
            }, function (data) {
                console.info(data);
            });

//            $('#modalGenerarDocumento').modal('hide');



        };

        $scope.listarNombresLetra = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('generarPlantilla', 1, 'listarPropLetras');

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.letras = data.data;

//                $scope.iniciarDatos();
            }, function (data) {
                console.info(data);
            });

        };

        $scope.vistaPreliminar = function (t)
        {
            $scope.nuevoDocumento.marcaAgua=t;
            var request = crud.crearRequest('generarPlantilla', 1, 'vistaPreliminar');
            request.setData($scope.nuevoDocumento);

            crud.listar("/documentosComunicacion", request, function (data) {
               
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });

        };
        
        $scope.ver = function (t)
        {
            if (typeof $scope.verImagen.verImagen == "undefined") {
                $scope.nuevoDocumento.verImagen = false;
                $scope.vistaPreliminar(t);
                return;
            }
            else
            {
                $scope.nuevoDocumento.verImagen = true;
                $scope.nuevoDocumento.isSaved = false;
                $scope.nuevoDocumento.nombreArchivo=$scope.verImagen.verImagen.nombreArchivo;
                $scope.vistaPreliminar(t);
            }

        };
        
        //Encabezado
        $scope.botonAlineacionIzqEnc = function () {
            $scope.nuevoDocumento.contenidoPlantilla[0].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenEnc = function () {
            $scope.nuevoDocumento.contenidoPlantilla[0].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerEnc = function () {
            $scope.nuevoDocumento.contenidoPlantilla[0].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusEnc = function () {
            $scope.nuevoDocumento.contenidoPlantilla[0].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldEnc = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[0].isBold)
            $scope.nuevoDocumento.contenidoPlantilla[0].isBold = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[0].isBold = false;
        };
        $scope.botonCursivaEnc = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[0].isCursiva)
            $scope.nuevoDocumento.contenidoPlantilla[0].isCursiva = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[0].isCursiva = false;
        };
        $scope.botonSubrayadoEnc = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado)
            $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = false;
        };
        //Cuerpo
        $scope.botonAlineacionIzqCue = function () {
            $scope.nuevoDocumento.contenidoPlantilla[1].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenCue = function () {
            $scope.nuevoDocumento.contenidoPlantilla[1].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerCue = function () {
            $scope.nuevoDocumento.contenidoPlantilla[1].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusCue = function () {
            $scope.nuevoDocumento.contenidoPlantilla[1].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldCue = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[1].isBold)
            $scope.nuevoDocumento.contenidoPlantilla[1].isBold = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[1].isBold = false;
        };
        $scope.botonCursivaCue = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[1].isCursiva)
            $scope.nuevoDocumento.contenidoPlantilla[1].isCursiva = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[1].isCursiva = false;
        };
        $scope.botonSubrayadoEnc = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado)
            $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = false;
        };
        //Despedida
        $scope.botonAlineacionIzqDes = function () {
            $scope.nuevoDocumento.contenidoPlantilla[2].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenDes = function () {
            $scope.nuevoDocumento.contenidoPlantilla[2].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerDes = function () {
            $scope.nuevoDocumento.contenidoPlantilla[2].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusDes = function () {
            $scope.nuevoDocumento.contenidoPlantilla[2].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldDes = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[2].isBold)
            $scope.nuevoDocumento.contenidoPlantilla[2].isBold = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[2].isBold = false;
        };
        $scope.botonCursivaDes = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[2].isCursiva)
            $scope.nuevoDocumento.contenidoPlantilla[2].isCursiva = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[2].isCursiva = false;
        };
        $scope.botonSubrayadoEnc = function () {
            if(!$scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado)
            $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = true;
            else
                $scope.nuevoDocumento.contenidoPlantilla[0].isSubrayado = false;
        };
      

        $scope.iniciarDatos = function () {

            $scope.listarPlantillas();
            $scope.listarTipoDocumentos();
            $scope.listarNombresLetra();
            $scope.nuevoDocumento.contenidoPlantilla = [{}, {}, {}];
        };
    }]);
