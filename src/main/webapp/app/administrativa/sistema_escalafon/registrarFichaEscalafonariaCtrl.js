app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.controller('registrarFichaEscalafonariaCtrl', ['$scope', '$rootScope', '$http','NgTableParams','crud', 'modal', 'ModalService', function ($scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false, false];
        
        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
               optionsSet[i] = false;
            }
           optionsSet[numOption] = true;
        };
        
        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.nuevaFichaEscalafonaria.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.nuevaFichaEscalafonaria.perDis = false;
        };
        
        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };
        
        $rootScope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else{
                o = "No";
            }
            return o;
        };
        
        $rootScope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else{
                o = "No Habilitado";
            }
            return o;
        };
        
        
        /*$scope.guardarLegajo = function () {
            if ($scope.nuevoLegajoPersonal.nom !== null){
                $scope.legajoPersonal.push()
            } else {
                console.log("No se adjunto archivo :P");
            }
        };*/
        
        $rootScope.paramsParientes = {count: 10};
        $rootScope.settingParientes = {counts: []};
        $rootScope.tablaParientes = new NgTableParams($rootScope.paramsParientes, $rootScope.settingParientes);
        
        $rootScope.paramsForEdu = {count: 10};
        $rootScope.settingForEdu  = {counts: []};
        $rootScope.tablaForEdu  = new NgTableParams($rootScope.paramsForEdu, $rootScope.settingForEdu);
        
        $rootScope.paramsEstCom = {count: 10};
        $rootScope.settingEstCom  = {counts: []};
        $rootScope.tablaEstCom  = new NgTableParams($rootScope.paramsEstCom, $rootScope.settingEstCom);
        
        $rootScope.paramsExpPon = {count: 10};
        $rootScope.settingExpPon  = {counts: []};
        $rootScope.tablaExpPon  = new NgTableParams($rootScope.paramsExpPon, $rootScope.settingExpPon);
        
        $rootScope.paramsPublicaciones = {count: 10};
        $rootScope.settingPublicaciones  = {counts: []};
        $rootScope.tablaPublicaciones = new NgTableParams($rootScope.paramsPublicaciones, $rootScope.settingPublicaciones);
        
        $rootScope.paramsDesplazamientos = {count: 10};
        $rootScope.settingDesplazamientos  = {counts: []};
        $rootScope.tablaDesplazamientos= new NgTableParams($rootScope.paramsDesplazamientos, $rootScope.settingDesplazamientos);

        $rootScope.paramsColegiaturas = {count: 10};
        $rootScope.settingColegiaturas  = {counts: []};
        $rootScope.tablaColegiaturas = new NgTableParams($rootScope.paramsColegiaturas, $rootScope.settingColegiaturas);
        
        $rootScope.paramsAscensos = {count: 10};
        $rootScope.settingAscensos  = {counts: []};
        $rootScope.tablaAscensos = new NgTableParams($rootScope.paramsAscensos, $rootScope.settingAscensos);
        
        $rootScope.paramsCapacitaciones = {count: 10};
        $rootScope.settingCapacitaciones  = {counts: []};
        $rootScope.tablaCapacitaciones = new NgTableParams($rootScope.paramsCapacitaciones, $rootScope.settingCapacitaciones);
        
        $rootScope.paramsReconocimientos= {count: 10};
        $rootScope.settingReconocimientos  = {counts: []};
        $rootScope.tablaReconocimientos = new NgTableParams($rootScope.paramsReconocimientos, $rootScope.settingReconocimientos);
        
        $rootScope.paramsEstPos = {count: 10};
        $rootScope.settingEstPos  = {counts: []};
        $rootScope.tablaEstPos = new NgTableParams($rootScope.paramsEstPos, $rootScope.settingEstPos);
        
        $rootScope.paramsDemeritos = {count: 10};
        $rootScope.settingDemeritos  = {counts: []};
        $rootScope.tablaDemeritos = new NgTableParams($rootScope.paramsDemeritos, $rootScope.settingDemeritos);
        
        //Variables para el registro de datos
        $scope.nuevaPersona = {perId:0, apePat: "", apeMat: "", nom: "", dni: "", fecNac: "", num1: "", num2: "", fij: "", email: "", sex: "", estCiv: "", depNac: "", proNac: "", disNac: "", estado: 'A'};
        $scope.nuevaDireccionDNI = {tipDir: "R", nomDir: "", depDir: "", proDir: "", disDir: "", num: "", dpto: "", int: "", mz: "", lote: "", km: "", bloque: "", etapa: "", nomZon: "", desRef: ""};
        $scope.nuevaDireccionDA = {tipDir: "A", nomDir: "", depDir: "", proDir: "", disDir: "", num: "", dpto: "", int: "", mz: "", lote: "", km: "", bloque: "", etapa: "", nomZon: "", desRef: ""};
        $scope.nuevoTrabajador = {traId:0, traCon:""};
        $scope.nuevaFichaEscalafonaria = {ficEscId:0, autEss:"", sisPen:"", nomAfp:"", codCuspp:"", fecIngAfp:"", perDis:false, regCon:"", gruOcu:""};
        $scope.legajoDatosPersonales = [
            {ficEscId:"", nom:"", archivo:"", url:"", des:"DNI", catLeg:"1", subCat:"1", codAspOri:0}, 
            {ficEscId:"", nom:"", archivo:"", url:"", des:"Partida de Nacimiento", catLeg:"1", subCat:"2", codAspOri:0},
            {ficEscId:"", nom:"", archivo:"", url:"", des:"Registro CONADIS", catLeg:"1", subCat:"4", codAspOri:0}
        ];
        $scope.direcciones = [];
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];
        
        $scope.showNuevoPariente = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPariente.html",
                controller: "agregarNuevoParienteCtrl",
                inputs: {
                    title: "Nuevo Pariente",
                    personaId: $scope.nuevaPersona.perId,
                    sexo: $scope.sexo,
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $rootScope.legajoPersonal = [];
        $scope.agregarDatosGenerales = function () {
            
            if (!$scope.statusDA) {
                $scope.direcciones = [$scope.nuevaDireccionDNI];
            } else {
                $scope.direcciones = [$scope.nuevaDireccionDNI, $scope.nuevaDireccionDA];
            }
            var request = crud.crearRequest('datos_personales', 1, 'agregarDatosPersonales');
            request.setData({persona: $scope.nuevaPersona, trabajador: $scope.nuevoTrabajador, fichaEscalafonaria:$scope.nuevaFichaEscalafonaria, direcciones: $scope.direcciones, orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                
                $scope.nuevaPersona.perId = response.data.perId;
                $scope.nuevoTrabajador.traId = response.data.traId;
                $scope.nuevaFichaEscalafonaria.ficEscId = response.data.ficEscId;
                
                var legajos = [];
                for (var i=0; i< $scope.legajoDatosPersonales.length; i++){
                    if ($scope.legajoDatosPersonales[i].archivo){
                        $scope.legajoDatosPersonales[i].ficEscId = $scope.nuevaFichaEscalafonaria.ficEscId;
                        legajos.push($scope.legajoDatosPersonales[i]);
                    }
                }
                
                console.log(legajos);
                if(legajos.length > 0){
                    var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajos');
                    request.setData({legajos:legajos});
                    crud.insertar("/sistema_escalafon", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    }, function (data) {
                        console.info(data);
                    });
                }
            }, function (data) {
                console.info(data);
            });
        };


        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaPersona.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };

        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaPersona.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDNI.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };

        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDNI.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDA.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDA.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };
        
        $scope.listarParientes = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.nuevoTrabajador);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingParientes.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingParientes.dataset);
                $rootScope.tablaParientes.settings($rootScope.settingParientes);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevaFormacionEducativa = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarFormacionEducativa.html",
                controller: "agregarNuevaFormacionEducativaCtrl",
                inputs: {
                    title: "Nueva Formacion Educativa",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.listarFormacionesEducativas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingForEdu.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingForEdu.dataset);
                $rootScope.tablaForEdu.settings($rootScope.settingForEdu);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.showNuevoEstudioComplementario = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioComplementario.html",
                controller: "agregarNuevoEstudioComplementarioCtrl",
                inputs: {
                    title: "Nuevo Estudio Complementario",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.listarEstudiosComplementarios = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingEstCom.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingEstCom.dataset);
                $rootScope.tablaEstCom.settings($rootScope.settingEstCom);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevaExposicion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarExposicion.html",
                controller: "agregarNuevaExposicionCtrl",
                inputs: {
                    title: "Nueva Exposicion y/o Ponencia",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.listarExposiciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingExpPon.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingExpPon.dataset);
                $rootScope.tablaExpPon.settings($rootScope.settingExpPon);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevaPublicacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPublicacion.html",
                controller: "agregarNuevaPublicacionCtrl",
                inputs: {
                    title: "Nueva Publicacion",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.listarPublicaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingPublicaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingPublicaciones.dataset);
                $rootScope.tablaPublicaciones.settings($rootScope.settingPublicaciones);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevoDesplazamiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDesplazamiento.html",
                controller: "agregarNuevoDesplazamientoCtrl",
                inputs: {
                    title: "Nueva Desplazamiento",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.listarDesplazamientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.nuevaFichaEscalafonaria);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingDesplazamientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingDesplazamientos.dataset);
                $rootScope.tablaDesplazamientos.settings($rootScope.settingDesplazamientos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevaColegiatura = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarColegiatura.html",
                controller: "agregarNuevaColegiaturaCtrl",
                inputs: {
                    title: "Nueva Colegiatura",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.listarColegiaturas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.conReg = $rootScope.parsearBooleano2(data.data.conReg)
                $rootScope.settingColegiaturas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingColegiaturas.dataset);
                $rootScope.tablaColegiaturas.settings($rootScope.settingColegiaturas);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevoAscenso = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarAscenso.html",
                controller: "agregarNuevoAscensoCtrl",
                inputs: {
                    title: "Nueva Ascenso",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.listarAscensos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingAscensos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingAscensos.dataset);
                $rootScope.tablaAscensos.settings($rootScope.settingAscensos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevoReconocimiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarReconocimiento.html",
                controller: "agregarNuevoReconocimientoCtrl",
                inputs: {
                    title: "Nueva Reconocimiento",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.listarReconocimientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingReconocimientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingReconocimientos.dataset);
                $rootScope.tablaReconocimientos.settings($rootScope.settingReconocimientos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevoEstudioPostgrado = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioPostgrado.html",
                controller: "agregarNuevoEstudioPostgradoCtrl",
                inputs: {
                    title: "Nueva Estudio Postgrado",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.listarEstudiosPostgrado = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingEstPos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingEstPos.dataset);
                $rootScope.tablaEstPos.settings($rootScope.settingEstPos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevoDemerito = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDemerito.html",
                controller: "agregarNuevoDemeritoCtrl",
                inputs: {
                    title: "Nueva Demerito",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.listarDemeritos= function () {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingDemeritos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingDemeritos.dataset);
                $rootScope.tablaDemeritos.settings($rootScope.settingDemeritos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.showNuevaCapacitacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarCapacitacion.html",
                controller: "agregarNuevaCapacitacionCtrl",
                inputs: {
                    title: "Nueva capacitación",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.listarCapacitaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.nuevaFichaEscalafonaria);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingCapacitaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingCapacitaciones.dataset);
                $rootScope.tablaCapacitaciones.settings($rootScope.settingCapacitaciones);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $scope.nuevaPersona.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    estudioComplementario: estComSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioPostgrado = function (e) {
            var estPosSel = JSON.parse(JSON.stringify(e));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioPostgrado.html",
                controller: "editarEstudioPostgradoCtrl",
                inputs: {
                    title: "Editar datos de Estudio de Postgrado",
                    estudioPostgrado: estPosSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDesplazamiento = function (d) {
            var desSel = JSON.parse(JSON.stringify(d));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    title: "Editar datos de Desplazamiento",
                    desplazamiento: desSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarAscenso = function (a) {
            var ascSel = JSON.parse(JSON.stringify(a));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    title: "Editar datos de Ascenso",
                    ascenso: ascSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        
        $scope.eliminarPariente = function(p){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $scope.personaSel.perId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function(item){
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };

    }]);

app.controller('agregarNuevoParienteCtrl', ['$scope', '$rootScope', '$element','title', 'personaId', 'sexo', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, sexo, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoPariente = {perId:personaId, tipoPariente: "", parApePat: "", parApeMat: "", parNom: "", parDni: "", parFecNac:"", parSex: "", parNum1: "", parNum2: "", parFij: "", parEmail: ""};
        $scope.nuevoLegajoPar = {ficEscId:fichaEscalafonariaId ,nom:"", archivo:"", des:"DNI-Pariente", url:"", catLeg:"2", subCat:"1", codAspOri:""};
        $scope.title = title;
        $scope.sexoPariente = sexo;
        $scope.tipoParentesco = [];

        listarTipoParentesco();
        function listarTipoParentesco() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoParentesco = data.data;
            }, function (data) {
                console.info(data);
            });
        };


        $scope.agregarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'agregarPariente');
            request.setData($scope.nuevoPariente);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {   
                    console.log(response);
                    response.data.parentesco = buscarContenido($scope.tipoParentesco, 'tpaId', 'tpaDes', response.data.tipoParienteId);
                    insertarElemento($rootScope.settingParientes.dataset, response.data);
                    $rootScope.tablaParientes.reload();
                    
                    if ($scope.nuevoLegajoPar.archivo){
                        $scope.nuevoLegajoPar.codAspOri = response.data.parId;
                        console.log($scope.nuevoLegajoPar);
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoPar);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('editarParienteCtrl', ['$rootScope','$scope', '$element', 'title', 'pariente', 'personaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, pariente, personaId, close, crud, modal) {
        $scope.title = title;
        
        /*mostrarValores();
        function mostrarValores(){
            console.log(pariente);
            console.log(datos);
            
        };*/
        $scope.parienteSel = {
            parId: pariente.parId,
            perId: personaId, 
            tipoParienteId: pariente.tipoParienteId, 
            parentesco: "",
            parApePat: pariente.parApePat, 
            parApeMat: pariente.parApeMat, 
            parNom: pariente.parNom, 
            parDni: pariente.parDni, 
            parFecNac: new Date(pariente.parFecNac), 
            parSex: pariente.parSex, 
            parNum1: pariente.parNum1, 
            parNum2: pariente.parNum2, 
            parFij: pariente.parFij, 
            parEmail: pariente.parEmail
        };
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.tipoPariente = [];
        
        listarTipoParentesco();
        
        function listarTipoParentesco() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoPariente = data.data;
            }, function (data) {
                console.info(data);
            });
           
        };
        
        $scope.pruebas = function () {
            console.log($scope.parienteSel.tipoParienteId);
        };

        $scope.actualizarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'actualizarFamiliar');
            request.setData($scope.parienteSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.parentesco = buscarContenido($scope.tipoPariente,'tpaId','tpaDes', response.data.tipoParienteId);
                    response.data.i = pariente.i;
                    $rootScope.settingModParientes.dataset[pariente.i] = response.data;
                    $rootScope.tablaModParientes.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);

app.controller('agregarNuevaFormacionEducativaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaForEdu = {ficEscId:fichaEscalafonariaId, tip:"", niv:"", numTit:"", esp:"", estCon:false, fecExp:"", cenEst:""};
        $scope.nuevoLegajoForEdu = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:" Titulo y/o Certificado", url:"", catLeg:"3", subCat:"1", codAspOri:""};
        $scope.tiposFormacion = [{id: "1", title: "Estudios Basicos Regulares"}, {id: "2", title: "Estudios Tecnicos"}, {id: "3", title: "Bachiller"}, {id: "4", title: "Universitario"}, {id: "5", title: "Licenciatura"}, {id: "6", title: "Maestria"}, {id: "7", title: "Doctorado"}, {id: "8", title: "Otros"}];
           
        $scope.agregarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'agregarFormacionEducativa');
            request.setData($scope.nuevaForEdu);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estCon = $rootScope.parsearBooleano(response.data.estCon);
                    insertarElemento($rootScope.settingForEdu.dataset, response.data);
                    $rootScope.tablaForEdu.reload();
                    
                    if($scope.nuevoLegajoForEdu.archivo){
                        $scope.nuevoLegajoForEdu.codAspOri = response.data.forEduId;
                        var request = crud.crearRequest('sistema_escalafon', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoForEdu);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        

    }]);

app.controller('agregarNuevaColegiaturaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaColegiatura = {ficEscId:fichaEscalafonariaId, nomColPro:"", numRegCol:"", conReg:false};
        $scope.nuevoLegajoCol = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Constancia", catLeg:"3", subCat:"2", codAspOri:""};
        
        $scope.agregarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'agregarColegiatura');
            request.setData($scope.nuevaColegiatura);
            console.log(JSON.stringify($scope.nuevaColegiatura));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conReg = $rootScope.parsearBooleano2(response.data.conReg)
                    insertarElemento($rootScope.settingColegiaturas.dataset, response.data);
                    $rootScope.tablaColegiaturas.reload();
                    
                    if($scope.nuevoLegajoCol.archivo){
                        $scope.nuevoLegajoCol.codAspOri = response.data.colId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoCol);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevoEstudioComplementarioCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstCom = {ficEscId:fichaEscalafonariaId, tip:"", des:"", niv:"", insCer:"", tipPar:"", fecIni:"", fecTer:"", horLec:""};
        $scope.nuevoLegajoEC = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado y/o Titulo", url:"", catLeg:"3", subCat:"3", codAspOri:""};
        $scope.tiposEstCom = [{id: "1", title: "Informatica"}, {id: "2", title: "Idiomas"}, {id: "3", title: "Certificación"}, {id: "4", title: "Diplomado"}, {id: "5", title: "Especialización"}, {id: "6", title: "Otros"}];
        $scope.niveles = [{id: "N", title: "Ninguno"}, {id: "B", title: "Básico"}, {id: "I", title: "Intermedio"}, {id: "A", title: "Avanzado"}];
   
        $scope.agregarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'agregarEstudioComplementario');
            request.setData($scope.nuevoEstCom);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingEstCom.dataset, response.data);
                    $rootScope.tablaEstCom.reload();
                    
                    if ($scope.nuevoLegajoEC.archivo){
                        $scope.nuevoLegajoEC.codAspOri = response.data.estComId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoEC);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevoEstudioPostgradoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstPos = {ficEscId:fichaEscalafonariaId, tip:"", des:"", niv:"", insCer:"", tipPar:"", fecIniEst:"", fecTerEst:"", horLec:""};
        $scope.nuevoLegajoEP = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Titulo", url:"", catLeg:"3", subCat:"4", codAspOri: ""};
        $scope.tiposEstPos = [{id: "1", title: "Maestria"}, {id: "2", title: "Doctorado"}];
   
        $scope.agregarEstudioPostgrado= function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'agregarEstudioPostgrado');
            request.setData($scope.nuevoEstPos);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    var auxTip = buscarObjeto($scope.tiposEstPos,"id",response.data.tip);
                    response.data.tip = auxTip.title;
                    insertarElemento($rootScope.settingEstPos.dataset, response.data);
                    $rootScope.tablaEstPos.reload();
                    
                    if ($scope.nuevoLegajoEP.archivo){
                        $scope.nuevoLegajoEP.codAspOri = response.data.estPosId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoEP);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevaExposicionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaExpPon = {ficEscId:fichaEscalafonariaId, des:"", insOrg:"", tipPar:"", fecIni:"", fecTer:"", horLec:""};
        $scope.nuevoLegajoExp = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado de participación", url:"", catLeg:"3", subCat:"5", codAspOri:""};
        
        $scope.agregarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'agregarExposicion');
            request.setData($scope.nuevaExpPon);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingExpPon.dataset, response.data);
                    $rootScope.tablaExpPon.reload();
                    
                    if ($scope.nuevoLegajoExp.archivo){
                        $scope.nuevoLegajoExp.codAspOri = response.data.expId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoExp);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevaPublicacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaPublicacion = {ficEscId:fichaEscalafonariaId, nomEdi:"", tipPub:"", titPub:"", graPar:"", lug:"", fecPub:"", numReg:""};
        $scope.nuevoLegajoPub = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Constancia", catLeg:"3", subCat:"6", codAspOri:""};
        
        $scope.agregarPublicacion = function () {
            var request = crud.crearRequest('publicacion', 1, 'agregarPublicacion');
            request.setData($scope.nuevaPublicacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingPublicaciones.dataset, response.data);
                    $rootScope.tablaPublicaciones.reload();
                    
                    if ($scope.nuevoLegajoPub.archivo){
                        $scope.nuevoLegajoPub.codAspOri = response.data.pubId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoPub);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevoDesplazamientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoDesplazamiento = {ficEscId:fichaEscalafonariaId, tip:"", numRes:"", fecRes:"", insEdu:"", car:"", jorLab:"", fecIni:"", fecTer:""};
        $scope.nuevoLegajoDes = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolución", url:"", catLeg:"4", subCat:"1", cosAspOri:""};
        
        $scope.tiposDesplazamientos = [{id: "1", title: "Designación"}, {id: "2", title: "Rotación"}, {id: "3", title: "Reasignación"}, {id: "4", title: "Destaque"}, {id: "5", title: "Permuta"}, {id: "6", title: "Encargo"},{id: "7", title: "Comisión de servicio"}, {id: "8", title: "Transferencia"}, {id: "9", title: "Otros"}];

        $scope.agregarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
            request.setData($scope.nuevoDesplazamiento);
            console.log(JSON.stringify($scope.nuevoDesplazamiento));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingDesplazamientos.dataset, response.data);
                    $rootScope.tablaDesplazamientos.reload();
                    
                    if ($scope.nuevoLegajoDes.archivo){
                        $scope.nuevoLegajoDes.cosAspOri = response.data.desId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoDes);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevoAscensoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoAscenso = {ficEscId:fichaEscalafonariaId, numRes:"", fecRes:"", fecEfe:"", esc:""};
        $scope.nuevoLegajoAsc = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolución", url:"", catLeg:"4", subCat:"2", codAspOri:""};
        
        $scope.escalas = [{title: "I"}, {title: "II"}, {title: "III"}, {title: "IV"}, {title: "V"}, {title: "VI"},{title: "VII"}, {title: "VIII"}];

        $scope.agregarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'agregarAscenso');
            request.setData($scope.nuevoAscenso);
            console.log(JSON.stringify($scope.nuevoAscenso));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingAscensos.dataset, response.data);
                    $rootScope.tablaAscensos.reload();
                    
                    if ($scope.nuevoLegajoAsc.archivo){
                        $scope.nuevoLegajoAsc.codAspOri = response.data.ascId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoAsc);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevaCapacitacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaCapacitacion = {ficEscId:fichaEscalafonariaId, nom:"", tip:"", fec:"", cal:0, lug:""};
        $scope.nuevoLegajoCap = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado", url:"", catLeg:"4", subCat:"3", codAspOri:""};
        

        $scope.agregarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'agregarCapacitacion');
            request.setData($scope.nuevaCapacitacion);
            console.log(JSON.stringify($scope.nuevaCapacitacion));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingCapacitaciones.dataset, response.data);
                    $rootScope.tablaCapacitaciones.reload();
                    
                    if ($scope.nuevoLegajoCap.archivo) {
                        $scope.nuevoLegajoCap.codAspOri = response.data.capId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoCap);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevoReconocimientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoReconocimiento = {ficEscId:fichaEscalafonariaId, mot:"", numRes:"", fecRes:"", entEmi:""};
        $scope.nuevoLegajoRec = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolucion", url:"", catLeg:"5", subCat:"1", codAspOri:""};
        
        $scope.motivos = [{id: "1", title: "Mérito"}, {id: "2", title: "Felicitación"}, {id: "3", title: "25 años de servicio"}, {id: "4", title: "30 años de servicio"}, {id: "5", title: "Luto y sepelio"}, {id: "6", title: "Otros"}];

        $scope.agregarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'agregarReconocimiento');
            request.setData($scope.nuevoReconocimiento);
            console.log(JSON.stringify($scope.nuevoReconocimiento));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    var auxMot = buscarObjeto($scope.motivos,"id",response.data.mot);
                    response.data.mot = auxMot.title;
                    insertarElemento($rootScope.settingReconocimientos.dataset, response.data);
                    $rootScope.tablaReconocimientos.reload();
                    
                    if ($scope.nuevoLegajoRec.archivo){
                        $scope.nuevoLegajoRec.codAspOri = response.data.recId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoRec);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                    request.setData($scope.nuevoLegajoRec);
                    crud.insertar("/sistema_escalafon", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    }, function (data) {
                        console.info(data);
                    });
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);

app.controller('agregarNuevoDemeritoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoDemerito = {ficEscId:fichaEscalafonariaId, entEmi:"", numRes:"", fecRes:"", sep:false, fecIni:"", fecFin:"", mot:""};
        $scope.nuevoLegajoDem = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Resolución", catLeg:"6", subCat:"1", codAspOri:""};
        
        $scope.agregarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'agregarDemerito');
            request.setData($scope.nuevoDemerito);
            console.log(JSON.stringify($scope.nuevoDemerito));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.sep = $rootScope.parsearBooleano(response.data.sep)
                    insertarElemento($rootScope.settingDemeritos.dataset, response.data);
                    $rootScope.tablaDemeritos.reload();
                    
                    if ($scope.nuevoLegajoDem.archivo){
                        $scope.nuevoLegajoDem.codAspOri = response.data.demId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoDem);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);