app.controller("trasladoIngresoCtrl", ["$scope", "$rootScope", "crud", "NgTableParams", "modal", function ($scope, $rootScope, crud, NgTableParams, modal) {

        $scope.myusuario = $rootScope.usuMaster.usuario.usuarioID;
        $scope.orgId = $rootScope.usuMaster.organizacion.organizacionID;
        $scope.orgNom = $rootScope.usuMaster.organizacion.nombre;

        var paramsTrasladoIngreso = {count: 10};
        var settingTrasladoIngreso = {counts: []};
        $scope.tablaPrincipal = new NgTableParams(paramsTrasladoIngreso, settingTrasladoIngreso);
        $scope.estadoTraslado = [{id: 'P', title: "En Proceso"}, {id: 'A', title: "Aseptada"}, {id: 'R', title: "Rechazada"}];
        $scope.tipoTraslado = [{id: 1, title: "Por Cambio de Nivel"}, {id: 2, title: "Por Cambio de Año"}, {id: 3, title: "En el Mismo año"}];

        $scope.trasladoCreate = {
            traIngEst: 'P',
            traIngEstNom: "EN PROCESO",
            traIngAnyoOri: new Date().getFullYear(),
            traIngAnyoDes: new Date().getFullYear(),
            traIgnOrgOri: -1,
            traIgnOrgOriNom: "IE. Desconocida",
            traIgnOrgDes: $scope.orgId,
            traIgnOrgDesNom: $scope.orgNom,
            traIngEstDni: "",
            traIngEstId: -1,
            traIgnEstNom: "Desconocido",
            traIngNivId: -1,
            traIngJorEscId: -1,
            traIngTurId: -1,
            traIngGraId: -1,
            traIngTipTra: "1", //
            traIgnResTra: "", //
            traIgnObs: "", //
            usuMod: $scope.myusuario    //
        };

        $scope.datosVacantes = {
            vacantesxGrado: 0,
            ultGraCul: 0,
            sigGra: 1
        };

        $scope.arrays = {
            gradosySecciones: [],
            niveles: [],
            jornadas: [],
            turnos: [],
            grados: []
        };

        $scope.errorMsj = {
            tipError: "",
            traIngId: "",
            traIngTipId: "",
            traIngTipNom: "",
            traIngEst: "",
            traIngFec: "",
            traIngEstNom: "",
            traIngOrgOri: "",
            traIngOrgDes: ""
        };

        $scope.buscarTraslado = function () {
            var busqueda = {
                orgDesId: $scope.orgId
            };
            var request = crud.crearRequest('trasladoIngreso', 1, 'listarTrasladosIngresosEmitidos');
            request.setData(busqueda);
            crud.listar("/matriculaInstitucional", request, function (data) {
                settingTrasladoIngreso.dataset = data.data;
                iniciarPosiciones(settingTrasladoIngreso.dataset);
                $scope.tablaPrincipal.settings(settingTrasladoIngreso);
                if (data.responseSta) {
                    if (settingTrasladoIngreso.dataset.length === 0) {
                        modal.mensaje("Busqueda vacia", "No se encontraron Traslados");
                    }
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.nuevoTrasladoIngreso = function () {
            $scope.cargarGradosySecciones();
            $scope.trasladoCreate.traIngNivId = -1;
            $scope.trasladoCreate.traIngJorEscId = -1;
            $scope.trasladoCreate.traIngTurId = -1;
            $scope.trasladoCreate.traIngGraId = -1;
            $scope.cargarNiveles();
            $('#modalTrasladoIngreso').modal('toggle');

        };

        $scope.buscarEstudianteByDNI = function () {
            var rq = {
                estDNI: $scope.trasladoCreate.traIngDniEst
            };
            var request = crud.crearRequest('trasladoIngreso', 1, 'buscarEstudianteTraslado');
            request.setData(rq);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.trasladoCreate.traIngEstId = data.data.estPerId;
                    $scope.trasladoCreate.traIgnEstNom = data.data.estPerNom;
                    $scope.trasladoCreate.traIgnOrgOri = data.data.estOrgOriId;
                    $scope.trasladoCreate.traIgnOrgOriNom = data.data.estOrgOriNom;
                    $scope.datosVacantes.ultGraCul = parseInt(data.data.ultGraCul);
                    $scope.datosVacantes.sigGra = $scope.datosVacantes.ultGraCul + 1;
                    $scope.cargarGradosySecciones();
                    $scope.trasladoCreate.traIngNivId = -1;
                    $scope.trasladoCreate.traIngJorEscId = -1;
                    $scope.trasladoCreate.traIngTurId = -1;
                    $scope.trasladoCreate.traIngGraId = -1;
                    $scope.cargarNiveles();
                } else {
                    modal.mensaje('Error', 'No se encontro ningun Estudiante');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarGradosySecciones = function () {
            var itmRequest = {
                orgIdUser: $scope.orgId
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'listarGradosySecciones');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.gradosySecciones = data.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarNiveles = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var tempID, tempNom, obj, itmRepeat, graDis;
            var flagRepeat = true;
            $scope.arrays.niveles = [];
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                tempID = $scope.arrays.gradosySecciones[i].nivelId;
                tempNom = $scope.arrays.gradosySecciones[i].nivelNom;
                graDis = $scope.arrays.gradosySecciones[i].gradoId;

                if (parseInt(graDis) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                obj = {
                    nivelId: tempID,
                    nivelNom: tempNom
                };
                for (var j = 0; j < $scope.arrays.niveles.length; j++) {
                    itmRepeat = $scope.arrays.niveles[j];
                    if (obj.nivelId === itmRepeat.nivelId) {
                        flagRepeat = false;
                    }
                }
                if (flagRepeat) {
                    $scope.arrays.niveles.push(obj);
                }
                flagRepeat = true;
            }
            $scope.cargarJornada();
        };

        $scope.cargarJornada = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.nivelId === $scope.trasladoCreate.traIngNivId) {
                    obj = {
                        jornadaId: itm.jornadaId,
                        jornadaNom: itm.jornadaNom
                    };
                    for (var j = 0; j < $scope.arrays.jornadas.length; j++) {
                        itmRepeat = $scope.arrays.jornadas[j];
                        if (obj.jornadaId === itmRepeat.jornadaId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.jornadas.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarTurno();
        };

        $scope.cargarTurno = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.jornadaId === $scope.trasladoCreate.traIngJorEscId) {
                    obj = {
                        turnoId: itm.turnoId,
                        turnoNom: itm.turnoNom
                    };
                    for (var j = 0; j < $scope.arrays.turnos.length; j++) {
                        itmRepeat = $scope.arrays.turnos[j];
                        if (obj.turnoId === itmRepeat.turnoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.turnos.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarGrados();
        };

        $scope.cargarGrados = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.grados = [];
            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.turnoId === $scope.trasladoCreate.traIngTurId) {
                    obj = {
                        gradoSecId: itm.gradoSecId,
                        planNivelId: itm.planNivelId,
                        gradoId: itm.gradoId,
                        gradoNom: itm.gradoNom
                    };
                    for (var j = 0; j < $scope.arrays.grados.length; j++) {
                        itmRepeat = $scope.arrays.grados[j];
                        if (obj.gradoId === itmRepeat.gradoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.grados.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarVacantes();
        };

        $scope.cargarVacantes = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var itm;
            var vacantesXGrado = 0;

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.gradoId === $scope.trasladoCreate.traIngGraId &&
                        itm.turnoId === $scope.trasladoCreate.traIngTurId &&
                        itm.jornadaId === $scope.trasladoCreate.traIngJorEscId &&
                        itm.nivelId === $scope.trasladoCreate.traIngNivId) {
                    vacantesXGrado = vacantesXGrado + (itm.gradoSecNumMax - itm.gradoSecMat);
                }
            }
            $scope.datosVacantes.vacantesxGrado = vacantesXGrado;
        };

        $scope.cargaInicial = function () {
            $scope.cargarGradosySecciones();
            $scope.cargarNiveles();
        };

        $scope.generarTrasladoIngreso = function () {
            if ($scope.trasladoCreate.traIngEstId === -1) {
                modal.mensaje("Error", "Seleccione un Estudiante!");
                return;
            }

            if ($scope.trasladoCreate.traIgnOrgOri.toString() === $scope.trasladoCreate.traIgnOrgDes.toString() ||
                    $scope.trasladoCreate.traIgnOrgDes === -1 ||
                    $scope.trasladoCreate.traIgnOrgOri === -1) {
                modal.mensaje("Error", "No se puede Realizar Traslados de la mimsa Organizacion Origen!");
                return;
            }

            if ($scope.trasladoCreate.traIngNivId === -1 ||
                    $scope.trasladoCreate.traIngJorEscId === -1 ||
                    $scope.trasladoCreate.traIngTurId === -1 ||
                    $scope.trasladoCreate.traIngGraId === -1) {
                modal.mensaje("Error", "Seleccione un Grado correctamente!");
                return;
            }

            var request = crud.crearRequest('trasladoIngreso', 1, 'generarTrasladoIngreso');

            request.setData($scope.trasladoCreate);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.buscarTraslado();
                    $('#modalTrasladoIngreso').modal('hide');
                    modal.mensaje("Operacion Exitosa", "Traslado Registrado");

                } else {
                    $scope.errorMsj = data.data;
                    if ($scope.errorMsj.tipError === 1) {
                        $('#modalTrasladoIngreso').modal('hide');
                        $('#modalError').modal('toggle');
                    }
                    if ($scope.errorMsj.tipError === 2) {
                        $('#modalTrasladoIngreso').modal('hide');
                        modal.mensaje("Error", "Error al Procesar el Traslado");
                    }
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.descargarTrasladoIngreso = function (estDNI) {
            var path = "../archivos/matricula_institucional/constanciaVacante/constanciaVacante_" + estDNI + ".pdf";
            window.open(path);
        };

    }]);


