/**
 * Created by Administrador on 10/10/2016.
 */
function CarpetaDigitalService($log,$rootScope,crud){
    this.listarElementoCarpeta = function(meth,data,succ,err){
        var request = crud.crearRequest('carpeta_digital',1,meth);
        request.setData(data);
        crud.listar('/maestro',request,succ,err);
    };
    this.registrarElementoCarpeta = function(meth,data,succ,err){
        var request = crud.crearRequest('carpeta_digital',1,meth);
        request.setData(data);
        crud.insertar('/maestro',request,succ,err);
    };
}
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/confCarpPeda/:car_dig',{
            templateUrl:'pedagogico/maestro/carpeta/detalle_configuracion_carpeta.html',
            controller:'detalleConfCarpCtrl',
            controllerAs:'ctrl'
        });
    }])
    .service('CarpetaDigitalService',['$log','$rootScope','crud',CarpetaDigitalService])
    .controller('confCarpetaDigital',['$log','$location','NgTableParams','modal','UtilAppServices','crud',function($log,$location,NgTableParams,modal,util,crud){
        var self = this;
        self.tablaCarpetas = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarCarpetas();
        function listarCarpetas(){
            var request = crud.crearRequest('carpeta_digital',1,'listarCarpetas');
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.tablaCarpetas.settings().dataset = response.data;
                    self.tablaCarpetas.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        }

        self.nuevaCarpeta = function(){
            var resolve = {
                data: function(){
                    return {
                        isEdit : false
                    }
                }
            }
            var modalInstance = util.openModal('nuevaCarpeta.html','registrarCarpetaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                self.tablaCarpetas.settings().dataset.push(data);
                self.tablaCarpetas.reload();
            },function(errData){});
        }
        self.eliminarCarpeta = function($event,row){
            util.openDialog('Eliminar Carpeta ','¿Seguro que desea eliminar la carpeta?',$event,
                function (response) {
                    var request = crud.crearRequest('carpeta_digital',1,'eliminarCarpeta');
                    request.setData({cod:row.cod});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(sec.contenidos.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            sec.contenidos.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        self.verDetalles = function(row){
            $location.url('/confCarpPeda/'+btoa(JSON.stringify(row)));
        }
    }])
    .controller('registrarCarpetaCtrl',['$log','$uibModalInstance','data','modal','CarpetaDigitalService',function($log,$uibModalInstance,data,modal,cService){
        var self = this;
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(),
            startingDay: 1,
            minMode: 'year',
            datepickerMode:'year'
        };
        self.titulo = data.isEdit ? 'Editar Carpeta Digital' : 'Registrar Nueva Carpeta Digital';
        //self.etapas = _.range(2015, 2051);
        self.guardar = function(){
            self.car.eta  = self.car.eta.getFullYear();
            cService.registrarElementoCarpeta('registrarCarpeta',self.car,function(response){
                if(response.response === 'OK'){
                    angular.extend(self.car,response.data);
                    $uibModalInstance.close(self.car);
                }else if(response.response === 'BAD'){
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errRsponse){
                modal.mensaje('ERROR','El servidor no responde');
            })
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])
    .controller('detalleConfCarpCtrl',['$log','$routeParams','NgTableParams','modal','UtilAppServices','crud',function($log,$routeParams,NgTableParams,modal,util,crud){
        var self = this;
        self.car = JSON.parse(atob($routeParams.car_dig));
        self.tipos = [{id:0,nom:"DREMO"},{id:1,nom:"UGEL"},{id:2,nom:"DIRECTOR IE"},{id:3,nom:"DOCENTE"}]
        cargarSecciones();
        function cargarSecciones(){
            var request = crud.crearRequest('carpeta_digital',1,'listarSeccionesCarpeta');
            request.setData({id:self.car.cod});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    $log.log("secciones",response.data);
                    self.secciones = response.data;
                    angular.forEach(self.secciones,function(obj,key){
                        obj.contenidos =  new NgTableParams({count:15},{
                            counts: [],
                            paginationMaxBlocks: 13,
                            paginationMinBlocks: 2,
                            dataset:obj.conts
                        });
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarTipo = function(tip){
            var index = _.findIndex(self.tipos,function(obj){
                return obj.id == tip;
            });
            if(index != -1) return self.tipos[index].nom;
            return "";
        }
        self.nuevoContenido = function(sec){
            var resolve = {
                data : function () {
                    return {
                        edit: false,
                        secId: sec.id,
                        tipos: self.tipos
                    }
                }
            }
            var modalInstance = util.openModal('registrar_contenido.html','registrarContenidoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(contenido){
                sec.contenidos.settings().dataset.push(contenido);
                sec.contenidos.reload();
            },function(cdata){});
        }
        self.editarContenido = function(row){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        tipos: self.tipos,
                        cont : angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('registrar_contenido.html','registrarContenidoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(contenido){
               angular.extend(row,contenido);
            },function(cdata){});
        }
        self.eliminarContenido = function($event,sec,row){
            util.openDialog('Eliminar Contenido ','¿Seguro que desea eliminar el contendio?',$event,
                function (response) {
                    var request = crud.crearRequest('carpeta_digital',1,'eliminarContenido');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(sec.contenidos.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            sec.contenidos.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.registrarSeccion = function(){
            var resolve = {
                data : function () {
                    return {
                        edit: false,
                        car: self.car,
                        ord:self.secciones.length + 1
                    }
                }
            }
            var modalInstance = util.openModal('registrar_seccion.html','registrarSeccionCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(seccion){
                self.secciones.push(seccion);
            },function(cdata){});
        }
        self.eliminarSeccion = function($event,sec){
            util.openDialog('Eliminar Seccion de Carpeta','¿Seguro que desea eliminar la seccion?',$event,
                function (response) {
                    var request = crud.crearRequest('carpeta_digital',1,'eliminarSeccion');
                    request.setData({id:sec.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.secciones,function(item){
                                return sec.id === item.id;
                            });
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.editarSeccion = function(sec){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        sec: angular.copy(sec)
                    }
                }
            }
            var modalInstance = util.openModal('registrar_seccion.html','registrarSeccionCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(seccion){
                angular.extend(sec,seccion);
            },function(cdata){});
        }
    }])
    .controller('registrarContenidoCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;

        self.titulo = !data.edit ? 'Nuevo Contenido' : 'Editar Contenido';
        self.tipos = data.tipos;
        self.cont = !data.edit ? new Object() : data.cont;
        if(data.edit){
            self.cont.tip = _.find(self.tipos,function(obj){
                return data.cont.tip == obj.id;
            });
        }
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.save = !data.edit ? guardar : editar;
        function guardar(){
            self.cont.id = data.secId;
            self.cont.tip = self.cont.tip.id;
            var request = crud.crearRequest('carpeta_digital',1,'registrarContenido');
            request.setData(self.cont);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.cont,response.data);
                    $uibModalInstance.close(self.cont);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        function editar(){
            self.cont.tip = self.cont.tip.id;
            var request = crud.crearRequest('carpeta_digital',1,'editarContenido');
            request.setData(self.cont);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    $uibModalInstance.close(self.cont);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }

    }])
    .controller('registrarSeccionCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;

        self.titulo = !data.edit ? 'Nueva Seccion de Carpeta' : 'Editar Seccion de Carpeta';
        self.sec = !data.edit ? new Object() : data.sec;
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.save = !data.edit ? guardar : editar;
        function guardar(){
            self.sec.car = data.car.cod;
            self.sec.ord = data.ord;
            var request = crud.crearRequest('carpeta_digital',1,'registrarSeccion');
            request.setData(self.sec);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.sec,response.data);
                    $uibModalInstance.close(self.sec);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        function editar(){
            var request = crud.crearRequest('carpeta_digital',1,'editarSeccion');
            request.setData(self.sec);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    $uibModalInstance.close(self.sec);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }]);;