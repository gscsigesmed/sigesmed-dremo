/**
 * Created by Administrador on 19/10/2016.
 */
angular.module('app')
    .controller('areaCurrMaestro',['$log','$location','NgTableParams','modal','UtilAppServices','MaestroService',function($log,$location,NgTableParams,modal,util,mService){
        var self = this;
        self.tabla = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });

        cargarCursos();
        function cargarCursos(){
            self.showProgress = true;
            mService.listarElemento('listarAreasCurriculares',{all:true},function(response){
                self.showProgress = false;
                if(response.responseSta){
                    $log.log('areas', response.data);
                    self.tabla.settings().dataset = response.data;
                    self.tabla.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje('ERROR','El Servidor no responde');
            });
        }
        self.nuevo = function(){

            var resolve = {
                data : function () {
                    return {
                        edit: false
                    }
                }
            }
            var modalInstance = util.openModal('nueva_area.html','nuevoCursoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                self.tabla.settings().dataset.push(data);
                self.tabla.reload();
            },function(cdata){});
        }
        self.editar = function(row){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        com: angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('nueva_competencia.html','competenciaBancoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                angular.extend(row,data);
            },function(cdata){});
        }
        self.eliminar= function($event,row){
            util.openDialog('Eliminar Competencia','¿Seguro que desea eliminar el curso?',$event,
                function (response) {
                    mService.eliminarElemento('eliminarAreaCurricular',row,function(response){
                        if(response.responseSta){
                            _.remove(self.tabla.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tabla.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
    }])
    .controller('nuevoCursoCtrl',['$log','$uibModalInstance','MaestroService','modal','data',function($log,$uibModalInstance,mService,modal,data){
        var self = this;
        self.titulo = data.edit ? 'Editar Area Curricular' : 'Nuevo Area Curricular';
        self.are = data.edit? data.are : new Object();
        loadCurriculas();
        function loadCurriculas(){
            mService.listarElemento('listarDisenosCurriculares',null,function(response){
                if(response.responseSta){
                    self.currs = response.data;
                    if(data.edit){

                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        }

        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
        self.selectCurr = function(row){
            self.are.curr = row.cod;
        }
        self.save = data.edit ? editar : guardar;
        function guardar(){
            //self.are.curr = self.curr.cod;
            mService.guardarElemento('registrarAreaCurricular',self.are,function(response){
                if(response.responseSta){
                    angular.extend(self.are,response.data);
                    $uibModalInstance.close(self.are);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }

            },function(errResponse){
                modal.mensaje('ERROR','el servidor no responde');
            });
        }
        function editar(){

        }
    }]);
