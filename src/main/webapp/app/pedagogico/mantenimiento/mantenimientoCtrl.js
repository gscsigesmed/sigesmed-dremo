app.config(["ngTableFilterConfigProvider",function(ngTableFilterConfigProvider){
    ngTableFilterConfigProvider.setConfig({
      aliasUrls: {"checkbox": "app/ng-table/filters/checkbox.html"}
    });
}]);

app.controller("mantenimientoCtrl",["$rootScope","$scope","NgTableParams","crud","modal", function ($rootScope,$scope,NgTableParams,crud,modal) {
       $scope.mod_selec={};
    $scope.sub_mod_selec={};
    $scope.func_selec={};
    $scope.mensajeEnviar={
      destinatario:"",
      destinatarioID:0,//mientras tanto es la session automatica, falta averiguar como obtener el administrador de una series de usuarios
      remitente:"",
      remitenteID:0,
      asunto:"",
      tipoMensaje:"",//Peticion o Soporte
      subTipoMensaje:"",//Guia de pasos o reporte de error
      fechaEnvioMostrar:"",
      fechaEnvio:"",
      img:"",
      descripcion:"",
      funcionID:0
    };
    $scope.mensajesEnviados=[];
    $scope.mensajesRecibidos=[];
    
    var paramsFuncion = {count: 10};
    var settingEnviados = { counts: []};
    $scope.tablaEnviados = new NgTableParams(paramsFuncion, settingEnviados);
    
   var settingRecibidos = { counts: []};
    $scope.tablaRecibidos = new NgTableParams(paramsFuncion, settingRecibidos);
 
    $scope.mostrarBotonAgregarAyuda=true;
    $scope.mostrarBotonesPasos=true;

    $scope.descripcionFuncionalidad="";

 
    $scope.tipoAyuda="General";//esto cambia con el valor de los radiobutton
    $scope.subTipoMensaje="GuiaPasos";//esto cambia con el valor de los radiobutton
    $scope.ayuda={funcionId:0,ayudaTipo:"",ayudaEstReg:""};
    $scope.paso={pasoDes:"",edi:false,pasoEstReg:""};
    $scope.pasos=[];

     
        
    $scope.funcionalidadRuta={
        id_mod: 0,
        id_submod: 0,
        id_func: 0
    }
    
    $scope.hayPeticionesEnviadas=false;
    $scope.hayPeticionesRecibidas=false;
    $scope.haySoporteRecibido=false;
    $scope.haySoporteEnviado=false;

    
  $scope.tabs = [
    { title:'Dynamic Title 1', content:'Dynamic content 1' },
    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
  ];
    //Temporal, carga lo necesario para escribir un mensaje.
        iniciarRedactarMensaje();
        
  $scope.cambiarVistaUsuario=function(){
      //TEMPORAL, SOLO PARA PRUEBAS
      if($rootScope.usuMaster.rol.rolID != 1){//Usuario
        $scope.mostrarUsuario=true;
        $scope.mostrarAdministrador=false;
      }else if($rootScope.usuMaster.rol.rolID ==1){//Administrador
        $scope.mostrarAdministrador=true;
        $scope.mostrarUsuario=false; 
      }
  
  };
$scope.cambiarPestanas=function(contenidoPestanaActual,pestanasTotales){
    pestanasTotales=document.getElementById(pestanasTotales);
    
    var pestanas=pestanasTotales.children;
    for(var i=0; i<pestanas.length;i++){
      pestanas[i].style.display='none';
    }
    document.getElementById(contenidoPestanaActual).style.display='block';
  };

    $scope.cambioModulo=function(sub_mod_selec, func_selec){
      sub_mod_selec={};
      func_selec={};
      pasos={};
    };
    $scope.cambioSubMod=function(func_selec){
        func_selec={};
    };
    $scope.cargarPasos=function (func_selec){        
      var request=crud.crearRequest('mantenimiento',1,'listarPasosDeFuncionalidad');
        if($scope.tipoAyuda=="General")
          $scope.ayuda.ayudaTipo="G";
        else if ($scope.tipoAyuda="Detallada")
          $scope.ayuda.ayudaTipo="D";
      
        if(func_selec){
            request.setData({funcionId:func_selec.funcionID, ayudaTipo:$scope.ayuda.ayudaTipo});

            crud.listar('/mantenimiento',request,function(data){
                if(data.data.length > 0){
                    $scope.pasos=data.data;
                    $scope.mostrarBotonEliminarAyuda=true;
                    $scope.mostrarBotonesPasos=false;
                    $scope.mostrarBotonAgregarAyuda=false;
                }else{
                    $scope.mostrarBotonEliminarAyuda=false;
                    $scope.mostrarBotonesPasos=true;
                    $scope.mostrarBotonAgregarAyuda=true;
                    $scope.pasos=[];
                }

          },function(data){
              console.info(data);
          });
          
        }else{
            $scope.pasos=[];
            $scope.mostrarBotonAgregarAyuda=true;
            $scope.mostrarBotonesPasos=true;
            $scope.mostrarBotonEliminarAyuda=false;
        }
      
    };
    
    $scope.cambiarTipoAyuda=function(func_selec){
      if($scope.tipoAyuda=="General") {
          $scope.tipoAyuda="Detallada";          
      }          
      else {
          $scope.tipoAyuda="General";
      }
      $scope.cargarPasos(func_selec);          
    };
    $scope.agregarAyuda=function(func_selec){
        if($scope.pasos.length>0 && func_selec.funcionID){
            var request=crud.crearRequest('mantenimiento',1,'insertarAyuda');
            //$scope.ayuda.funcionId=$scope.funcionalidadRuta.id_func            
            
            $scope.ayuda.funcionId=(func_selec.funcionID);
            console.log($scope.ayuda.funcionId);
            
            $scope.ayuda.ayudaEstReg='1';      
              if($scope.tipoAyuda=="General")
                $scope.ayuda.ayudaTipo="G";
            else if ($scope.tipoAyuda="Detallada")
                $scope.ayuda.ayudaTipo="D";

            $scope.ayuda.pasos=$scope.pasos;
            request.setData($scope.ayuda);      
            crud.insertar("/mantenimiento",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                $scope.mostrarBotonAgregarAyuda=false;//Ocultamos boton agregar
                $scope.mostrarBotonEliminarAyuda=true;//Mostramos boton eliminar
                $scope.mostrarBotonesPasos=false;//Ocultamos botones editar y eliminar de cada paso
            },function(data){
                console.info(data);
            });            
        }else {
            modal.mensaje("CONFIRMACION","No se llenÃ³ la informaciÃ³n correcta");
        }
      
    };

    $scope.agregarPaso=function(tipo){
        if(tipo){//Si se hace la ediciÃ³n, REVISAR tipoTramiteCtrl.js

        }else{//Si se seta insertado
            if($scope.paso.pasoDes==""){
                modal.mensaje=("CONFIRMACION","Ingrese descripciÃ³n del paso");
                return;
            }      
            $scope.paso.pasoEstReg="1";
            $scope.pasos.push($scope.paso);
            $scope.paso={pasoDes:"",pasoEstReg:""};        
        }
    };
    $scope.editarPaso=function(i,r){
        //Si se esta editando, presiona el boton confirmacion de cambio
        if(r.edi){
            $scope.pasos[i] = r.copia;
        }else{
            
            r.copia=JSON.parse(JSON.stringify(r));
            r.edi=true;
        }
    };
    $scope.eliminarPaso=function(i,r){
        //Si se esta cancelando la ediciÃ³n
        if(r.edi){
            r.edi=false;
            delete r.copia;
        }else{//Si eliminamos el elemento
            $scope.pasos.splice(i,1);
        }
    };
    $scope.eliminarAyuda=function(func_selec){
        //Se cambiara de estado de registro en ayuda y los pasos asociados
        var request=crud.crearRequest('mantenimiento',1,'eliminarAyuda');
      $scope.ayuda.funcionId=func_selec.funcionID;
      $scope.ayuda.ayudaEstReg='1';      
        if($scope.tipoAyuda=="General")
          $scope.ayuda.ayudaTipo="G";
      else if ($scope.tipoAyuda="Detallada")
          $scope.ayuda.ayudaTipo="D";
      
      $scope.ayuda.pasos=$scope.pasos;
      request.setData($scope.ayuda);      
      crud.eliminar("/mantenimiento",request,function(response){
                $scope.pasos=[];
                $scope.mostrarBotonEliminarAyuda=false;
                $scope.mostrarBotonAgregarAyuda=true;
                $scope.mostrarBotonesPasos=true;
                modal.mensaje("CONFIRMACION",response.responseMsg);
            },function(data){
                console.info(data);
            });
  
    };
    function iniciarRedactarMensaje(){
        var f=new Date();
        $scope.mensajeEnviar.fechaEnvio=f;
        $scope.mensajeEnviar.fechaEnvioMostrar=f.getDate()+"/"+f.getMonth()+"/"+f.getFullYear(); 
       
        //Obteniendo remitente
        var request=crud.crearRequest('mantenimiento',1,'buscarPersonaPorId');
        request.setData({perID: $rootScope.usuMaster.usuario.usuarioID});
        crud.listar("/mantenimiento",request,function(data){
            $scope.mensajeEnviar.remitente=data.data.parMat + " " + data.data.parPat+" " + data.data.parNom;
            $scope.mensajeEnviar.remitenteID=data.data.parId;
        },function(response){
            console.info(response);
        });
        
        //Obteniendo destinatario
        //Es el codigo de usuari que busque el de session.
        request=crud.crearRequest('mantenimiento',1,'buscarPersonaPorId');
        request.setData({perID:53});
        crud.listar("/mantenimiento",request,function(data){
            $scope.mensajeEnviar.destinatario=data.data.parMat + " " + data.data.parPat+" " + data.data.parNom;
            console.log(data.data);
            //$scope.mensajeEnviar.destinatarioID=data.data.parId;
            $scope.mensajeEnviar.destinatarioID=0;
        },function(response){
            console.info(response)
        }); 
    };
    $scope.cambiarSubTipoMensaje=function(){
      if($scope.subTipoMensaje=="GuiaPasos") {
          $scope.subTipoMensaje="ReporteError";         
      }          
      else if($scope.subTipoMensaje=="ReporteError"){
          $scope.subTipoMensaje="GuiaPasos";        
      }
    };    
    $scope.listarPeticionesEnviadas=function(){
        var request=crud.crearRequest('mantenimiento',1,'listarMensajesEnviadosPeticion');
        request.setData({usuarioID:$rootScope.usuMaster.usuario.usuarioID})
        crud.listar("/mantenimiento",request,function(data){
            if(data.data.length!=0){
                settingEnviados.dataset=data.data;            
                iniciarPosiciones(settingEnviados.dataset);
                $scope.tablaEnviados.settings(settingEnviados);
                $scope.hayPeticionesEnviadas=true;
            }else{
                $scope.hayPeticionesEnviadas=false;                
            }            
        },function(data){
            console.info(data);
        });
    };
    $scope.archivarMensaje=function(mensaje){
        if(mensaje.estado!='Archivado'){
            var request=crud.crearRequest('mantenimiento',1,'archivarMensaje');
            request.setData({menID: mensaje.mensajeID});
            crud.actualizar("/mantenimiento", request, function(data){
                mensaje.estado='Archivado';
                modal.mensaje("MENSAJE",data.responseMsg);
            },function(data){            
                modal.mensaje("MENSAJE","No se pudo archivar el mensaje");
            });
        }else{
            modal.mensaje("MENSAJE","El mensaje ya esta archivado");
        }        
    };
    $scope.eliminarMensaje=function(mensaje, tipo){
        modal.mensajeConfirmacion($scope, "Esta seguro de eliminar el mensaje", function () {
            var request=crud.crearRequest('mantenimiento',1,'eliminarMensaje');
            request.setData({menID: mensaje.mensajeID});
            crud.eliminar("/mantenimiento", request, function(data){
                if(tipo=='Enviados')//tabla enviados
                {
                    eliminarElemento(settingEnviados.dataset,mensaje.i);
                    $scope.tablaEnviados.reload();                        
                }else if(tipo=='Recibidos')//tabla recibidos
                {
                    eliminarElemento(settingRecibidos.dataset,mensaje.i);
                    $scope.tablaRecibidos.reload();
                }
                modal.mensaje("MENSAJE", data.responseMsg);
            }, function(data){
                modal.mensaje("MENSAJE", "No se pudo eliminar el mensaje");
            });
            
        });        
    };
    $scope.reenviarMensaje=function(mensaje, tipo){
        $rootScope.menuPrincipal;
        if(tipo=='Peticion'){
            $scope.cambiarPestanas("csubTabEscribirPeticion","contenidoSubTabEntornoPeticiones");
            $scope.mensajeEnviar.asunto=mensaje.asunto;
            var temp_sub_mod={};

            for(var i=0; i<$rootScope.menuPrincipal.length;i++){
                if( $rootScope.menuPrincipal[i].moduloID=mensaje.mod){
                    $scope.sub_mod_selec_men=$rootScope.menuPrincipal[i].subModulos;
                    $scope.mod_selec_men=$rootScope.menuPrincipal[i];                    
                    break;
                }
            }
            for(var i=0; i<$scope.sub_mod_selec_men.length;i++){
                if( $scope.sub_mod_selec_men[i].subModuloID=mensaje.submod){
                    $scope.func_selec_men=$scope.sub_mod_selec_men[i].funciones;
                    $scope.sub_mod_selec_men=$scope.sub_mod_selec_men[i];                    
                    break;
                }                
            }
            for(var i=0; i<$scope.func_selec_men.length;i++){
                if( $scope.func_selec_men[i].funcionID=mensaje.func){
                    $scope.func_selec_men=$scope.func_selec_men[i];                
                    break;
                }                
            }
            if(mensaje.tipoSoporte=='Guia Pasos'){
                $scope.subTipoMensaje="GuiaPasos";
            }else if(mensaje.tipoSoporte=='Reporte Error'){
                $scope.subTipoMensaje="ReporteError";
            }            
            $scope.mensajeEnviar.descripcion=mensaje.mensajeSoporte;
            
            
        }else if(tipo=='Soporte'){
            
        }
    };
    
    
    $scope.listarMensajesRecibidos=function(){
        var request=crud.crearRequest('mantenimiento',1,'listarMensajesRecibidos');
        request.setData({usuarioID:$rootScope.usuMaster.usuario.usuarioID})
        crud.listar("/mantenimiento",request,function(data){
            if(data.data.length!=0){
                settingRecibidos.dataset=data.data;
               iniciarPosiciones(settingRecibidos.dataset);
                $scope.tablaRecibidos.settings(settingRecibidos);
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.confirmarMensaje=function(){
        $('#modalConfirmacionMensaje').modal('show');
    }
    $scope.vistaPrevia=function(){
        $('#modalVistaPrevia').modal('show');
    };
    $scope.enviarMensaje=function(mensajeEnviar,func_selec,tipo){
        //console.log(mensajeEnviar);
        //mensajeEnviar.funcionID=func_selec.funcionID;
       // console.log(func_selec);
        if(mensajeEnviar.asunto != ""){
            if(mensajeEnviar.descripcion != ""){
                if(mensajeEnviar.funcionID){
                    var request=crud.crearRequest('mantenimiento',1,'insertarMensaje');
                    if(tipo){//Soporte
                        mensajeEnviar.tipoMensaje="S";
                    }else{//Peticion
                        mensajeEnviar.tipoMensaje="P";
                    }
                    mensajeEnviar.subTipoMensaje=$scope.subTipoMensaje.charAt(0);
//                    mensajeEnviar.funcionID=func_selec.funcionID;
                    
                   
                   //Se recibe del remitente su id de usuario, del destinatario el de session.
                   mensajeEnviar.destinatarioID=20;
                   console.log(mensajeEnviar);
                    request.setData(mensajeEnviar);
                    crud.insertar("/mantenimiento",request,function(data){    
                          $scope.mensajeEnviar.asunto="";
                          $scope.mensajeEnviar.descripcion="";
                          $scope.mensajeEnviar.img="";
                        modal.mensaje("CONFIRMACION","Mensaje enviado con Ã©xito");
                        //Actualizamos bandeja de mensajes enviados
                        $scope.listarPeticionesEnviadas();
                    },function(response){
                        console.log(response)
                    });  
                }else{
                    modal.mensaje("MENSAJE","No se selecciono la funcionalidad");
                }                
            }else{
                modal.mensaje("MENSAJE","No se ingreso una descripciÃ³n del mensaje")
            }
        }else{
            modal.mensaje("MENSAJE","No se ingreso el asunto del mensaje")
        }             
    };
    $scope.seleccionarTodos = function(s,nombre){
        if(s)
            $scope[nombre].data.forEach(function(item){
                item.selec= true;               
            });
        else
            $scope[nombre].data.forEach(function(item){
                item.selec= false;
            });
    };
    
    $scope.prepararFuncionParaMensaje=function(func_selec,mensajeEnviar){
        mensajeEnviar.funcionID=func_selec.funcionID;
    };
    $scope.alertMe = function() {
    setTimeout(function() {
      $window.alert('You\'ve selected the alert tab!');
    });
  };
  
//  $scope.preparaFuncionEnMensaje=function(func_selec){
//      $scope.mensajeEnviar.funcionID=func_selec.funcionID;
//  }

   $scope.dt=new Date();
  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  

    // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 );
  }

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };
  $scope.popup2 = {
    opened: false
  };

 $scope.model = {
    name: 'Tabs'
  };
}]);
