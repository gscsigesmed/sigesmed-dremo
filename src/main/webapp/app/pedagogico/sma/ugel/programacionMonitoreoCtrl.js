app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.controller("programacionMonitoreoCtrl",["$rootScope","$scope","NgTableParams","crud","modal",'ModalService', function ($rootScope,$scope,NgTableParams,crud,modal,ModalService){

    
    //Listar Monitoreos
    
    $scope.nuevoMonitoreo = {
        monId:"", 
        codMon:"",
        anioMon:"",
        nomMon:"",
        nombreURL:"",
        fecReg:"", 
        nomIE:"",
        numEsp:"",
        numDoc:"", 
        estMon:"", 
        tipUgel:"" 
    }; 
    
    

    $rootScope.paramsMonitoreos = {count: 10};
    $rootScope.settingMonitoreos = {counts: []};
    $rootScope.tablaMonitoreos = new NgTableParams($rootScope.paramsMonitoreos, $rootScope.settingMonitoreos);
    
    $scope.listarMonitoreos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('programacion_monitoreo', 1, 'listarMonitoreosPorUgel');
            //ugelID es el dato que necesito
            //usuMaster es el usuario actual
            //organizacion es la organizacion del usuario
            //organizacionID es la ID de la organizacion del usuario
            request.setData({ugelId:$rootScope.usuMaster.organizacion.organizacionID});
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (data) {
                $rootScope.settingMonitoreos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMonitoreos.dataset);
                 $scope.tablaMonitoreos.settings($rootScope.settingMonitoreos);
            }, function (data) {
                console.info(data);
            });
    };
    
    //Listar Monitoreos Detalle
    
    $scope.nuevoMonitoreoDetalle = {especialista:"",docente:"",area:"",grado:"" ,seccion:"", fechaDeVisita:"" ,etapa:"" }; 
    $rootScope. paramsMonitoreoDetalle = {count: 10};
    $rootScope.settingMonitoreoDetalle = {counts: []};
    $rootScope.tablaMonitoreoDetalle = new NgTableParams($rootScope.paramsMonitoreoDetalle, $rootScope.settingMonitoreoDetalle);
       
    function mostrarEtaMonDetDes(etaMon){
       switch(etaMon){
           case '1': return "Entrada";
           case '2': return "Proceso";
           case '3': return "Salida";    
       }
    }
   
    $scope.listarMonitoreoDetalle = function (monId) {
            
            //preparamos un objeto request
            var request = crud.crearRequest('programacion_monitoreo', 1, 'listarMonitoreoDetalle');
            //
            request.setData({monId:monId});
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (data) {
                data.data.forEach(function(item){
                    item.etapaDes = mostrarEtaMonDetDes(item.etapa);
                });
                $rootScope.settingMonitoreoDetalle.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMonitoreoDetalle.dataset);
                $rootScope.tablaMonitoreoDetalle.settings($rootScope.settingMonitoreoDetalle);
                $('#modalDetalle').modal('show'); 
                 
            }, function (data) {
                console.info(data);
            });
    };
    
    $scope.prepararProAnual = function () {
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ugel/modalProgramacionAnual.html",
            controller: "programacionAnualCtrl",
            inputs: {
                title: "Programacion Anual"
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                }
            });
        });
    };
    
    
    $scope.mostrarEstadoMonitoreo = function(m){
        if(m.estMon === undefined || m.estMon === null) {
            m.estMon = "NR";
        };
        switch(m.estMon) {
            case "NR": $("#btn-estado" + m.monId).removeClass("btn-success").addClass("btn-warning"); return 'No realizado';
            case "R": $("#btn-estado" + m.monId).removeClass("btn-warning").addClass("btn-success"); return 'Realizado';
        }
    };
    
    $scope.cambiarEstadoMonitoreo = function(m){
        if(m.estMon === "NR"){
            m.estMon = "R";
        }else{
            m.estMon = "NR";
        }
        switch(m.estMon){
            case "NR": $("#btn-estado" + m.monId).removeClass("btn-success").addClass("btn-warning"); break;
            case "R": $("#btn-estado" + m.monId).removeClass("btn-warning").addClass("btn-success"); break;
        } 
        
        //preparamos un objeto request
        var request = crud.crearRequest('programacion_monitoreo', 1, 'actualizarMonitoreo');
        var monitoreoSel = JSON.stringify(m);
        request.setData(monitoreoSel);
        crud.insertar("/sma", request, function (response) {
            modal.mensaje("CONFIRMACION", response.responseMsg);
            if (response.responseSta) {
                
            }

        }, function (data) {
            console.info(data);
        });
    };
    
    $scope.prepararEditarMonitoreo = function (mon) {
        var monitoreoSel = mon;
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ugel/modalEditarProgramacionMonitoreo.html",
            controller: "editarProgramacionMonitoreoCtrl",
            inputs: {
                title: "Actualizar Monitoreo",
                monitoreo: monitoreoSel
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    };
    
    $scope.prepararEditarMonitoreoDetalle = function (monDet) {
        var monitoreoDetalleSel = monDet;
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ugel/modalEditarPMDetalle.html",
            controller: "editarPMDetalleCtrl",
            inputs: {
                title: "Detalle de Monitoreo",
                monitoreoDetalleSel: monitoreoDetalleSel
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    };
    
    //graficas 

    $scope.labels = ['Inicial', 'Primaria', 'Secundaria'];
    $scope.series = ['Monitoreos Totales', 'Monitoreos Realizados'];

    $scope.data = [
     [50, 100, 50, 0 ],
        [25, 25, 30, 0]
    ];
    
    //graficas 1
    
    $scope.labels1 = ['Inicial', 'Primaria', 'Secundaria'];
    $scope.series1 = ['Primera Visita', 'Segunda Visita', 'Tercera Visita'];

    $scope.data1 = [
     [9, 3, 9, 0 ],
     [7, 6, 17, 0 ],
     [8, 4, 3, 0]
    ];
    
            $scope.imprimirReporte1 = function(){
            var grafico = new MyFile("Avance de Monitoreo");
            grafico.parseDataURL( document.getElementById("avanceMonitoreo").toDataURL("image/png") );

            var request = crud.crearRequest('programacion_monitoreo',1,'reporteSMA');
            request.setData({grafico: grafico, orgNom: $rootScope.usuMaster.organizacion.nombre});
            crud.insertar("/sma",request,function(response){
                if(response.responseSta){                
                    verDocumento(response.data.reporte);
                }            
            },function(data){
                console.info(data);
            });       
        };
        
        $scope.imprimirReporte2 = function(){
            var grafico = new MyFile("Monitoreos realizados por visita");
            grafico.parseDataURL( document.getElementById("monitoreosRealizadosPorVisita").toDataURL("image/png") );

            var request = crud.crearRequest('programacion_monitoreo',1,'reporteSMA');
            request.setData({grafico: grafico, orgNom: $rootScope.usuMaster.organizacion.nombre});
            crud.insertar("/sma",request,function(response){
                if(response.responseSta){                
                    verDocumento(response.data.reporte);
                }            
            },function(data){
                console.info(data);
            });       
        };
   
}]);

app.controller('programacionAnualCtrl', ['$scope', '$rootScope', '$element', 'title', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, close, crud, modal, ModalService) {
        $scope.title = title;
        
        $scope.nuevoMonitoreo = {
            monId:"", 
            codMon:"",
            anioMon:"",
            nomMon:"",
            nombreURL:"",
            fecReg:"", 
            nomIE:"",
            numEsp:"",
            numDoc:"", 
            estMon:"", 
            tipUgel:"" 
        }; 
    
        $scope.tiposUgel = [
            {id: 'A', title: "Tipo A"},
            {id: 'BC', title: "Tipo BC"},
            {id: 'D', title: "Tipo D"},
            {id: 'E', title: "Tipo E"},
            {id: 'F', title: "Tipo F"},
            {id: 'GH', title: "Tipo GH"},
            {id: 'I', title: "Tipo I"}
        ];
        
        $scope.programarMonitoreos = function () {
            var request = crud.crearRequest('programacion_monitoreo', 1, 'programarMonitoreoMasivo');

            request.setData({ugelId:$rootScope.usuMaster.organizacion.organizacionID, tipUgel:$scope.nuevoMonitoreo.tipUgel});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.insertar("/sma", request, function (data) {
                console.log(data);
                $rootScope.settingMonitoreos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMonitoreos.dataset);
                $rootScope.tablaMonitoreos.settings($rootScope.settingMonitoreos);
            }, function (data) {
                console.info(data);
            });
        };
        

        
    }]);

app.controller('editarPMDetalleCtrl', ['$scope', '$rootScope', '$element', 'title', 'monitoreoDetalleSel', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, monitoreoDetalleSel,close, crud, modal, ModalService) {
        $scope.title = title;
        
        $scope.nuevoMonitoreoDetalle = {
            monDetId: monitoreoDetalleSel.monDetId,
            espId: monitoreoDetalleSel.espId,
            espDetalle: monitoreoDetalleSel.espDetalle,
            docId: monitoreoDetalleSel.docId,
            docDetalle: monitoreoDetalleSel.docDetalle,
            fecVis:"",
            etapa: monitoreoDetalleSel.etapa,
            etapaDes: monitoreoDetalleSel.etapaDes
        };
        
        listarEspecialistasUgel();
        function listarEspecialistasUgel(){
            var request = crud.crearRequest('programacion_monitoreo', 1, 'listarEspecialistasDeUgel');
            request.setData({ugelId:$rootScope.usuMaster.organizacion.organizacionID});
            crud.listar("/sma", request, function (response) {
                $scope.especialistasUGEL = response.data;
                console.log($scope.especialistasUGEL);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.actualizarDetalleMonitoreo = function () {
            var request = crud.crearRequest('programacion_monitoreo', 1, 'actualizarDetalleMonitoreo');
            request.setData($scope.nuevoMonitoreoDetalle);
            crud.actualizar("/sma", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = monitoreoDetalleSel.i;
                    $rootScope.settingMonitoreoDetalle.dataset[monitoreoDetalleSel.i] = response.data;
                    $rootScope.tablaMonitoreoDetalle.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        
    }]);

app.controller('editarProgramacionMonitoreoCtrl', ['$rootScope', '$scope', 'crud', 'modal', 'title', 'monitoreo', function ($rootScope, $scope, crud, modal,  title, monitoreo) {
        $scope.title = title;
        
        
        $scope.monitoreoSelect = {
            numEsp: monitoreo.numEsp,
            monId: monitoreo.monId,
            idIE: monitoreo.idIE,
            nomIE: monitoreo.nomIE,
            nivIE: monitoreo.nivIE,
            url: "",
            nomArchivo: "",
            archivo: ""
        };
        
        $scope.actualizarMonitoreo = function () {
            console.log(monitoreo);
            console.log($scope.monitoreoSelect);
            var request = crud.crearRequest('programacion_monitoreo', 1, 'actualizarMonitoreo');
            request.setData($scope.monitoreoSelect);
            crud.actualizar("/sma", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = monitoreo.i;
                    $rootScope.settingMonitoreos.dataset[monitoreo.i] = response.data;
                    $rootScope.tablaMonitoreos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);


