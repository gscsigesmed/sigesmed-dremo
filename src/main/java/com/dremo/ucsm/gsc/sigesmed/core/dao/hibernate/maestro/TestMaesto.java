package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.carpeta.ContenidoSeccionCarpetaDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan.CompetenciaAprendizajeDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ArchivosCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx.ListarGradosMaestroTx;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx.ListarDetalleCarpetaDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx.ListarBancoCompetenciasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx.ListarSecuenciasSesionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx.ListarCompetenciasAreaTx;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 23/10/16.
 */
public class TestMaesto  {
    private static final Logger logger = Logger.getLogger(TestMaesto.class.getName());
    public static void main(String[] args){
        //buscarCompetencia();
        /*ContenidoSeccionCarpetaDaoHibernate aaa = new ContenidoSeccionCarpetaDaoHibernate();
        List<ArchivosCarpeta> archivos = aaa.listarDocumentosCarpeta(1,6,4,38);
        logger.log(Level.INFO,"cantidad de archivos {0}",archivos.size());*/
        //listarGradosMaestro(38,6);
        ListarDetalleCarpetaDocenteTx ll = new ListarDetalleCarpetaDocenteTx();
        WebResponse response = ll.listarDetalleCarpeta(1, 6, 38);
        logger.log(Level.INFO,"cantidad de archivos {0}",response.toJSON().toString());
    }
    private static void listarGradosMaestro(int idDocente,int idOrg){
        ListarGradosMaestroTx lst = new ListarGradosMaestroTx();

    }
    private static void mostrarSecuenciasSesion(int idSesion){
        ListarSecuenciasSesionTx lss = new ListarSecuenciasSesionTx();
        logger.log(Level.INFO,"salida :{0}",lss.listarSecuenciasDidacticas(idSesion).toJSON().toString());
    }
    private static  Trabajador buscarTrabajadorPorUsuario(int user){
        DocenteDaoHibernate dao = new DocenteDaoHibernate();
        try {
            return dao.buscarTrabajadorPorUsuario(user);
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarTrabajador",e);
        }
        return null;
    }
    private static Docente buscarDocente(int id){
        DocenteDaoHibernate dao = new DocenteDaoHibernate();
        try {
            return dao.buscarDocentePorId(id);
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarTrabajador",e);
        }
        return null;
    }
    private static void listarCompetencias(int idArea){
        WebRequest wr = WebRequest.create();
        wr.setData(new JSONObject().put("are",idArea));

        ListarCompetenciasAreaTx tx = new ListarCompetenciasAreaTx();
        WebResponse response = tx.execute(wr);
        logger.log(Level.INFO,"response {0}",response.toJSON().toString());
    }
    private static void listarCompetencias(){
        ListarBancoCompetenciasTx tx = new ListarBancoCompetenciasTx();
        WebResponse response = tx.execute(WebRequest.create());
        logger.log(Level.INFO,"response {0}",response.toJSON().toString());
    }
    private static void buscarCompetencia(){
        CompetenciaAprendizajeDaoHibernate cdh = new CompetenciaAprendizajeDaoHibernate();
        CompetenciaAprendizaje ca = cdh.buscarCompetenciPorCodigo(2);
        logger.log(Level.INFO,"response {0}",ca.getCapacidades().size());
    }

}
