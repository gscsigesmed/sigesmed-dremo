package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarEvaluacionParticipanteTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarEvaluacionParticipanteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        WebResponse response = null;

        switch (data.getInt("opt")) {
            case 0:
                response = registrarIndividual(data.getInt("codEva"), data.getInt("codDoc"));
                break;

            case 1:
                response = registrarGrupal(data.getInt("codEva"), data.getInt("usuId"));
                break;
        }

        return response;
    }

    private WebResponse registrarIndividual(int codEva, int perId) {
        try {
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            EvaluacionCursoCapacitacion evaluacion = evaluacionCursoCapacitacionDao.buscarPorId(codEva);
            EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");
            
            EvaluacionParticipanteCapacitacion evaPar = new EvaluacionParticipanteCapacitacion(new Date(), 0, perId, 'A', evaluacion);
            evaluacionParticipanteCapacitacionDao.insert(evaPar);
            
            return WebResponse.crearWebResponseExito("La evaluación fue generada correctamente para el participitante", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarEvaluacion", e);
            return WebResponse.crearWebResponseError("No se pudo registrar la evaluacion del curso de capacitación", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse registrarGrupal(int codEva, int usuId) {
        try {
            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");
            
            EvaluacionCursoCapacitacion evaluacion = evaluacionCursoCapacitacionDao.buscarPorId(codEva);
            evaluacion.setEstReg('I');
            evaluacionCursoCapacitacionDao.update(evaluacion);
                
            for(BigInteger perId: participanteCapacitacionDao.listarParticipantes(evaluacion.getSede().getSedCapId())) {
                EvaluacionParticipanteCapacitacion evaPar = new EvaluacionParticipanteCapacitacion(new Date(), 0, perId.intValue(), 'A', evaluacion);
                evaluacionParticipanteCapacitacionDao.insert(evaPar);
            }
            
            MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
            List<Integer> participantes = participanteCapacitacionDao.listarMensaje(evaluacion.getSede().getSedCapId());
            MensajeElectronico mensaje = new MensajeElectronico(0, 
                (evaluacion.getTip().equals('E'))?"NUEVA ENCUESTA":"NUEVA EVALUACIÓN", "Se ha publicado una nueva " + ((evaluacion.getTip().equals('E'))?"encuesta":"evaluación") + " , por favor revísela", "", "", new Date(), usuId);
            mensajeDao.enviarMensaje(mensaje, participantes);
            
            return WebResponse.crearWebResponseExito("La evaluación fue generada correctamente para cada participitante", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarEvaluacion", e);
            return WebResponse.crearWebResponseError("No se pudo generar las evaluaciones para cada participante", WebResponse.BAD_RESPONSE);
        }
    }
}
