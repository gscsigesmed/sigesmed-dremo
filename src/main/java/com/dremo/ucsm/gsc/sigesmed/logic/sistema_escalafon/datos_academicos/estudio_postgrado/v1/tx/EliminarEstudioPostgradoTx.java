/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarEstudioPostgradoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarEstudioPostgradoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer estPosId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            estPosId = requestData.getInt("estPosId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarEstudioPostgrado",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        EstudioPostgradoDao estPosDao = (EstudioPostgradoDao)FactoryDao.buildDao("se.EstudioPostgradoDao");
        try{
            estPosDao.deleteAbsolute(new EstudioPostgrado(estPosId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el estudio de postgrado\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el estudio de postgrado", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El estudio de postgrado se elimino correctamente");
    }
    
}
