package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PersonaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarPersonaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarPersonaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            JSONObject personaObj = data.getJSONObject("persona");

            PersonaCapacitacionDao personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
            Persona persona = persona = new Persona(personaObj.getString("apePat"),
                    personaObj.getString("apeMat"),
                    personaObj.getString("nom"),
                    new Date(),
                    personaObj.getString("dni"),
                    personaObj.getString("email"));

            personaCapacitacionDao.insert(persona);

            return WebResponse.crearWebResponseExito("La persona fue creada correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONObject(EntityUtil.objectToJSONString(
                                            new String[]{"perId"},
                                            new String[]{"cod"},
                                            persona
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarPersona", e);
            return WebResponse.crearWebResponseError("No se pudo registrar a la persona de la capacitación", WebResponse.BAD_RESPONSE);
        }
    }
}
