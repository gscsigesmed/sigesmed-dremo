package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/01/2017.
 */

public class ListarPlanEstudiosActualTx implements ITransaction {
    private static Logger logger = Logger.getLogger(ListarPlanEstudiosActualTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int orgId = data.getInt("org");
        return listarPlanActual(orgId);
    }

    private WebResponse listarPlanActual(int orgId) {
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            PlanEstudios plan = planDao.buscarVigentePorOrganizacion(orgId);
            return WebResponse.crearWebResponseExito("Se listo correctamente",new JSONObject().put("idplan",plan.getPlaEstId()));
        }catch (Exception e){
            logger.log(Level.SEVERE,"ListarPlanes",e);
            return WebResponse.crearWebResponseError("No se puede listar el plan");
        }
    }
}
