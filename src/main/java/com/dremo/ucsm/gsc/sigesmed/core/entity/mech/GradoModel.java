/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

/**
 *
 * @author abel
 */
public class GradoModel {
    public int gradoID;
    public int nivelID;
    
    public String grado;
    public String nivel;
    public int secciones;
    
    public GradoModel(int gradoID,String grado,int nivelID,int secciones){
        this.gradoID = gradoID;
        this.grado = grado;
        this.nivelID = nivelID;
        //this.nivel = nivel;
        this.secciones = secciones;
    }
}
