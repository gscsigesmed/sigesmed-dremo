/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.PlazaMagisterial;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public interface AsistenciaAulaDao extends GenericDao<AsistenciaAula>{
   public Boolean registrarHoraSalida(Long idRegistroAsistencia, Date hora,Integer hrs,String relacionHoras);
   public List<PlanNivel> listarNivelesByOrganizacion(Integer organizacion);
   public List<AsistenciaAula> listarAsistenciaAula(Integer docenteId, Date inicio, Date fin);
   public List<Docente> listarDocenteAndPlazas(Integer organizacion, Integer planNivelId);
   public PlazaMagisterial getPlazaMagisterialAndDistribucion(Integer plazaId);
   
}