package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TemarioCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TemarioCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarTemasTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarTemasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        WebResponse response = null;

        switch (data.getInt("opt")) {
            case 0:
                response = buscarTemas(data.getInt("sedCod"));
                break;

            case 1:
                response = listarTemasAvance(data.getInt("sedCod"));
                break;

            case 2: response = verificarComentarios(data.getInt("temCod"));
                break;
        }

        return response;
    }

    private WebResponse buscarTemas(int sedCod) {
        try {
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            List<TemarioCursoCapacitacion> temas = temarioCursoCapacitacionDao.buscarPorSede(sedCod);

            JSONArray array = new JSONArray();
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

            for (TemarioCursoCapacitacion tema : temas) {
                JSONObject object = new JSONObject();
                object.put("cod", tema.getTemCurCapId());
                object.put("nom", tema.getTem());
                object.put("ini", format.format(tema.getFecIni()));
                object.put("fin", format.format(tema.getFecFin()));
                object.put("est", tema.getEstReg());

                array.put(object);
            }

            return WebResponse.crearWebResponseError("Exito al listar los temas", WebResponse.OK_RESPONSE)
                    .setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarTemas", e);
            return WebResponse.crearWebResponseError("Error al listar los temas", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarTemasAvance(int sedCod) {
        try {
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            List<TemarioCursoCapacitacion> temas = temarioCursoCapacitacionDao.buscarPorSede(sedCod);

            JSONArray temasArray = new JSONArray();
            double total = temas.size();
            int fin = 0;

            for (TemarioCursoCapacitacion tema : temas) {
                JSONObject temaObj = new JSONObject();
                temaObj.put("cod", tema.getTemCurCapId());
                temaObj.put("nom", tema.getTem());
                temaObj.put("est", tema.getEstReg());
                temaObj.put("sel", false);
                fin += (tema.getEstReg().equals('F')) ? 1 : 0;

                temasArray.put(temaObj);
            }

            JSONObject obj = new JSONObject();
            obj.put("temas", temasArray);
            obj.put("res", total - fin);

            if (total > 0) {
                obj.put("avn", (fin * 100 / total));
            }

            return WebResponse.crearWebResponseError("Exito al listar los temas", WebResponse.OK_RESPONSE).setData(obj);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarTemas", e);
            return WebResponse.crearWebResponseError("Error al listar los temas", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse verificarComentarios(int temCod) {
         try {
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            JSONObject object = new JSONObject();
            object.put("state", temarioCursoCapacitacionDao.verificarComentarios(temCod));
            
            return WebResponse.crearWebResponseError("Exito al listar los temas", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarTemas", e);
            return WebResponse.crearWebResponseError("Error al listar los temas", WebResponse.BAD_RESPONSE);
        }
    }
}
