package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class ModuloSistemaDaoHibernate extends GenericDaoHibernate<ModuloSistema> implements ModuloSistemaDao {

    private static final Logger logger = Logger.getLogger(ModuloSistemaDaoHibernate.class.getName());

    @Override
    public List<ModuloSistema> listarConSubModulos() {
        List<ModuloSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT m FROM ModuloSistema m LEFT JOIN FETCH m.subModuloSistemas sm WHERE m.estReg='A' and ( sm.estReg!='E' or sm IS NULL )";

            Query query = session.createQuery(hql);
            objetos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar los modulos \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar llos modulos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objetos;
    }

    @Override
    public List<ModuloSistema> listarModulos() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(ModuloSistema.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .addOrder(Order.asc("nom"))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarModulos", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
