/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ClaseGenerica;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ClaseGenericaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.GrupoGenericoDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Administrador
 */
public class RegistrarClaseTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        ClaseGenerica clase_generica = null;
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray clases = requestData.getJSONArray("clases");
        for(int i = 0; i < clases.length();i++){
             JSONObject grupo =clases.getJSONObject(i);
             int gru_gen_id = grupo.getInt("gru_gen_id");
             String cod = grupo.getString("cod");
             String nom = grupo.getString("nom");
             Date fec_mod = new Date();
             int usu_mod = grupo.getInt("usu_mod");
             char est_reg = 'A';
             
             clase_generica = new ClaseGenerica(0,gru_gen_id,cod,nom,fec_mod,usu_mod,est_reg);
            
             ClaseGenericaDAO cla_gen_dao = (ClaseGenericaDAO)FactoryDao.buildDao("scp.ClaseGenericaDAO");
             cla_gen_dao.insert(clase_generica);
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
