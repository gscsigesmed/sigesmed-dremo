/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author abel
 */
public class EntregarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int expedienteID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            expedienteID = requestData.getInt("expedienteID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo entregar expediente, datos incorrectos", e.getMessage() );
        }
        
        ExpedienteDao expediente = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        try{
            expediente.entregarExpediente(expedienteID,wr.getIdUsuario());
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo entregar expediente", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Expediente se entrego correctamente");
        //Fin
    }
    
}
