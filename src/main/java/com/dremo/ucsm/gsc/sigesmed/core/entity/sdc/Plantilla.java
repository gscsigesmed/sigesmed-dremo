package com.dremo.ucsm.gsc.sigesmed.core.entity.sdc;

import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.*;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="plantilla" ,schema="administrativo")
public class Plantilla  implements java.io.Serializable {


    @Id
    @Column(name="pla_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_plantilla", sequenceName="administrativo.plantilla_pla_id_seq" )
    @GeneratedValue(generator="secuencia_plantilla")
    private Integer plaIde;
    
    @Column(name="pla_des")
    private String plaDes;
    
    @Column(name="pla_ver")
    private Integer plaVer;

    @Column(name="pla_est")
    private String plaEst;  
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="pla_fec_reg")
    private Date fecRegistro;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_usu_id", nullable=false )
    private UsuarioSession plaUsu;  
    
    
    @Column(name="pla_id_ant")
    private Integer plaIdAnt;
    
    @Column(name="pla_ubi_arc")
    private String archivo;
    
    private static String url="archivos/documentos_comunicacion/";
    private static String urlImagenes="archivos/documentos_comunicacion_imagenes/";
    
    @OneToMany(mappedBy="plaId",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<ImagenPlantilla> rutasImagenes;
    
    
    @OneToMany(mappedBy="plaId",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<ContenidoPlantilla> contenidoPlantillas;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_tip_doc_id", nullable=false )
    private TipoDocumento tipoDocumento;  

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Plantilla() {
    }
    
    public Plantilla(String plaDes, Integer plaVer, String plaEst, Date fecRegistro, UsuarioSession plaUsu, TipoDocumento tipoDocumento,Integer plaIdAnt,String archivo) {
        this.plaDes = plaDes;
        this.plaVer = plaVer;
        this.plaEst = plaEst;
        this.fecRegistro = fecRegistro;
        this.plaUsu = plaUsu;
        this.tipoDocumento = tipoDocumento;
        this.plaIdAnt=plaIdAnt;
        this.archivo=archivo;
    }
    
    public Plantilla(String plaDes, Integer plaVer, String plaEst, Date fecRegistro, UsuarioSession plaUsu, TipoDocumento tipoDocumento) {
        this.plaDes = plaDes;
        this.plaVer = plaVer;
        this.plaEst = plaEst;
        this.fecRegistro = fecRegistro;
        this.plaUsu = plaUsu;
        this.tipoDocumento = tipoDocumento;
      
    }

    public Plantilla(String plaDes, Integer plaVer, String plaEst, Date fecRegistro, UsuarioSession plaUsu, Integer plaIdAnt, String archivo, List<ImagenPlantilla> rutasImagenes, List<ContenidoPlantilla> contenidoPlantillas, TipoDocumento tipoDocumento) {
        this.plaDes = plaDes;
        this.plaVer = plaVer;
        this.plaEst = plaEst;
        this.fecRegistro = fecRegistro;
        this.plaUsu = plaUsu;
        this.plaIdAnt = plaIdAnt;
        this.archivo = archivo;
        this.rutasImagenes = rutasImagenes;
        this.contenidoPlantillas = contenidoPlantillas;
        this.tipoDocumento = tipoDocumento;
    }
    
    
    
    public Plantilla(Integer plaIde) {
        this.plaIde = plaIde;
    }


    public Integer getPlaIde() {
        return plaIde;
    }

    public void setPlaIde(Integer plaIde) {
        this.plaIde = plaIde;
    }

    public String getPlaDes() {
        return plaDes;
    }

    public void setPlaDes(String plaDes) {
        this.plaDes = plaDes;
    }

    public Integer getPlaVer() {
        return plaVer;
    }

    public void setPlaVer(Integer plaVer) {
        this.plaVer = plaVer;
    }

    public String getPlaEst() {
        return plaEst;
    }

    public void setPlaEst(String plaEst) {
        this.plaEst = plaEst;
    }

    public Date getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(Date fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public UsuarioSession getPlaUsu() {
        return plaUsu;
    }

    public void setPlaUsu(UsuarioSession plaUsu) {
        this.plaUsu = plaUsu;
    }

    public List<ContenidoPlantilla> getContenidoPlantillas() {
        return contenidoPlantillas;
    }

    public void setContenidoPlantillas(List<ContenidoPlantilla> contenidoPlantillas) {
        this.contenidoPlantillas = contenidoPlantillas;
    }
    
     public Integer getPlaIdAnt() {
        return plaIdAnt;
    }

    public void setPlaIdAnt(Integer plaIdAnt) {
        this.plaIdAnt = plaIdAnt;
    }
    
    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ImagenPlantilla> getRutasImagenes() {
        return rutasImagenes;
    }

    public void setRutasImagenes(List<ImagenPlantilla> rutasImagenes) {
        this.rutasImagenes = rutasImagenes;
    }

    public static String getUrlImagenes() {
        return urlImagenes;
    }

    public static void setUrlImagenes(String urlImagenes) {
        Plantilla.urlImagenes = urlImagenes;
    }
    
    
}


