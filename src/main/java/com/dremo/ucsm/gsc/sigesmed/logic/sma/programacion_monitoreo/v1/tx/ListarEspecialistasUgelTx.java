/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx.SMA_Constantes.ESPECIALISTA_UGEL_CARGO_ID;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ListarEspecialistasUgelTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarEspecialistasUgelTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ugelId = requestData.getInt("ugelId");
                
        List<Trabajador> trabajadores = null;
        TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("sma.TrabajadorDao");
        
        try{
            trabajadores = trabajadorDao.listarPorOrganizacion(ugelId, ESPECIALISTA_UGEL_CARGO_ID);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar instituciones",e);
            System.out.println("No se pudo listar las instituciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las instituciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Trabajador tra:trabajadores ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("traId", tra.getTraId());
            oResponse.put("traDetalle", tra.getPersona().getNom() + " " + tra.getPersona().getApePat() + " " + tra.getPersona().getApeMat());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los especialistas de UGEL fueron listados exitosamente", miArray);
    }
    
}
