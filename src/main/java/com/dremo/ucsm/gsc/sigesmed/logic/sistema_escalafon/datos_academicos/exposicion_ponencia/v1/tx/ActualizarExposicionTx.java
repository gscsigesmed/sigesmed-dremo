/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarExposicionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarExposicionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer expId = requestData.getInt("expId");
            String des = requestData.getString("des");
            String insOrg = requestData.getString("insOrg");
            String tipPar = requestData.getString("tipPar");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            Integer horLec = requestData.getInt("horLec");     
            
            return actualizarExposicion(expId, des, insOrg, tipPar, fecIni, fecTer, horLec);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar exposicion",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarExposicion(Integer expId, String des, String insOrg, String tipPar, Date fecIni,
            Date fecTer, Integer horLec) {
        try{
            ExposicionDao exposicionDao = (ExposicionDao)FactoryDao.buildDao("se.ExposicionDao");        
            Exposicion exposicion = exposicionDao.buscarPorId(expId);

            exposicion.setDes(des);
            exposicion.setInsOrg(insOrg);
            exposicion.setTipPar(tipPar);
            exposicion.setFecIni(fecIni);
            exposicion.setFecTer(fecTer);
            exposicion.setHorLec(horLec);
            
            exposicionDao.update(exposicion);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("expId", exposicion.getExpId());
            oResponse.put("des", exposicion.getDes());
            oResponse.put("insOrg", exposicion.getInsOrg());
            oResponse.put("tipPar", exposicion.getTipPar());
            oResponse.put("fecIni", sdo.format(exposicion.getFecIni()));
            oResponse.put("fecTer", sdo.format(exposicion.getFecTer()));
            oResponse.put("horLec", exposicion.getHorLec());
            return WebResponse.crearWebResponseExito("Exposicion actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarExposcion",e);
            return WebResponse.crearWebResponseError("Error, la exposicion no fue actualizada");
        }
    } 
    
}
