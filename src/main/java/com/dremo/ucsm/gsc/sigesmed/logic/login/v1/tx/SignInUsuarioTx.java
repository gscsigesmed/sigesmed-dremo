package com.dremo.ucsm.gsc.sigesmed.logic.login.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AjustePaginaWebDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.security.TokenHandler;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.AjustePaginaWeb;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.BuiltJSON;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class SignInUsuarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(SignInUsuarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        /* Parte para la lectura, verificacion y validacion de datos */
        String username = "";
        String password = "";
        int organizacionID = 0;
        int rolID = 0;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            username = requestData.getString("nombre");
            password = requestData.getString("password");
            organizacionID = requestData.getInt("organizacionID");
            rolID = requestData.getInt("rolID");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "signInUsuario", e);
            return WebResponse.crearWebResponseError("Los datos enviados son incorrectos", e.getMessage());
        }

        /* Parte para la operacion en la Base de Datos */
        Usuario usuario = null;
        try {
            usuario = ((UsuarioDao) FactoryDao.buildDao("UsuarioDao")).buscarPorUsuarioYPassword(username, password);
        } catch (Exception e) {
            System.out.println("No se pudo buscar por Nombre y Password\n" + e);
            return WebResponse.crearWebResponseError("No se pudo buscar por Nombre y Password", e.getMessage());
        }

        /* Validación de usuario */
        if (usuario != null) {
            UsuarioSession session = null;

            for (UsuarioSession us : usuario.getSessiones()) {
                if (us.getOrganizacion().getOrgId() == organizacionID && us.getRol().getRolId() == rolID) {
                    session = us;
                    break;
                }
            }

            if (session == null) {
                return WebResponse.crearWebResponseExito("error el rol y oganizacion no pertenecen al usuario");
            }

            List<ModuloSistema> modulos = new ArrayList<ModuloSistema>();
            List<RolFuncionModel> funciones = ((FuncionSistemaDao) FactoryDao.buildDao("FuncionSistemaDao")).buscarFuncionesPersonalizadasPorRol(session.getRol().getRolId());
            
            ActividadCalendarioDao actividadCalendarioDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
            MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
            List<ActividadCalendario> activities = actividadCalendarioDao.buscarInstitucionalesInicio(session.getUsuSesId());
                        
            for(ActividadCalendario activity: activities) {
                String message = "La funcionalidad " + activity.getFuncion().getNom() + " se ha habilitado en el sistema. Esta actividad se encuentra programada en el calendario institucional.";
                MensajeElectronico mensaje = new MensajeElectronico(0, "CCI: HABILITACIÓN", message, "", "", activity.getFecIni(), session.getUsuSesId());
                mensajeDao.enviarMensaje(mensaje, session.getUsuSesId());
               
                activity.setEstReg('I');
                activity.setNotIni(true);
                actividadCalendarioDao.update(activity);
            }
            
            activities = actividadCalendarioDao.buscarInstitucionalesFin(session.getUsuSesId());
            
            for(ActividadCalendario activity: activities) {
                String message = "La funcionalidad " + activity.getFuncion().getNom() + " se ha deshabilitado en el sistema. Esta actividad se encuentra programada en el calendario institucional.";
                MensajeElectronico mensaje = new MensajeElectronico(0, "CCI: : DESHABILITACIÓN", message, "", "", activity.getFecIni(), session.getUsuSesId());
                mensajeDao.enviarMensaje(mensaje, session.getUsuSesId());
               
                activity.setEstReg('F');
                activity.setNotFin(true);
                actividadCalendarioDao.update(activity);
            }
            
            List<Integer> lockedFunctions = actividadCalendarioDao.buscarFuncionalidades(session.getUsuSesId(), rolID);
            for (RolFuncionModel f : funciones) {
                buscarModulo(modulos, f);
            }

            JSONArray aModulos = new JSONArray();

            for (ModuloSistema modulo : modulos) {
                JSONObject oModulo = new JSONObject();
                oModulo.put("moduloID", modulo.getModSisId());
                oModulo.put("nombre", modulo.getNom());
                oModulo.put("codigo", modulo.getCod());
                oModulo.put("icono", modulo.getIco());

                if (modulo.getSubModuloSistemas().size() > 0) {
                    JSONArray aSubModulos = new JSONArray();

                    for (SubModuloSistema subModulo : modulo.getSubModuloSistemas()) {
                        JSONObject oSubModulo = new JSONObject();
                        oSubModulo.put("subModuloID", subModulo.getSubModSisId());
                        oSubModulo.put("nombre", subModulo.getNom());
                        oSubModulo.put("codigo", subModulo.getCod());
                        oSubModulo.put("icono", subModulo.getIco());

                        if (subModulo.getFunciones().size() > 0) {
                            JSONArray aFunciones = new JSONArray();

                            for (RolFuncionModel funcion : subModulo.getFunciones()) {
                                if(!lockedFunctions.contains(funcion.funcionID)) {
                                    JSONObject oFuncion = new JSONObject();
                                    oFuncion.put("funcionID", funcion.funcionID);
                                    oFuncion.put("nombre", funcion.nombre);
                                    oFuncion.put("url", funcion.url);
                                    oFuncion.put("clave", funcion.clave);
                                    oFuncion.put("controlador", funcion.controlador);
                                    oFuncion.put("interfaz", funcion.interfaz);
                                    oFuncion.put("icono", funcion.icono);
                                    oFuncion.put("tipo", funcion.tipo);
                                    oFuncion.put("dependencias", funcion.dependencias);
                                    aFunciones.put(oFuncion);
                                }                                
                            }
                            oSubModulo.put("funciones", aFunciones);
                        }
                        aSubModulos.put(oSubModulo);
                    }
                    oModulo.put("subModulos", aSubModulos);
                }
                aModulos.put(oModulo);
            }

            JSONObject oResponse = new JSONObject();
            oResponse.put("jwt", TokenHandler.getInstance().createTokenForUser(usuario));
            oResponse.put("url", "/app/");

            JSONObject usuarioReponse = new JSONObject();
            usuarioReponse.put("ID", session.getUsuSesId());
            usuarioReponse.put("usuarioID", session.getUsuario().getUsuId());
            usuarioReponse.put("nombre", usuario.getNom());
            oResponse.put("usuario", usuarioReponse);

            String[] atributosO = {"OrgId", "Nom", "Ali"};
            String[] etiquetasO = {"organizacionID", "nombre", "alias"};
            oResponse.put("organizacion", BuiltJSON.builtJSONObjectFromPojo(session.getOrganizacion(), atributosO, etiquetasO));

            if (session.getArea() != null) {
                String[] atributosA = {"AreId", "Nom", "TipAreID"};
                String[] etiquetasA = {"areaID", "nombre", "tipoID"};
                oResponse.put("area", BuiltJSON.builtJSONObjectFromPojo(session.getArea(), atributosA, etiquetasA));
            }

            String[] atributosR = {"RolId", "Nom"};
            String[] etiquetasR = {"rolID", "nombre"};
            oResponse.put("rol", BuiltJSON.builtJSONObjectFromPojo(session.getRol(), atributosR, etiquetasR));

            oResponse.put("modulos", aModulos);

            /* Carga de Personalizacion */
            AjustePaginaWeb ajuste = ((AjustePaginaWebDao) FactoryDao.buildDao("mnt.AjustePaginaWebDao")).obtenerAjustePorIdUsuario(usuario);
            if (ajuste != null) {//Tiene personalizacion
                JSONObject prop = new JSONObject();
                prop.put("ladoIzq", ajuste.getAjusteIzq());
                prop.put("ladoDer", ajuste.getAjusteDer());
                prop.put("color", ajuste.getAjusteColor());
                oResponse.put("personalizacion", prop);
            }

            /* Carga de Calendario */
            JSONArray dates = actividadCalendarioDao.buscarActividadesFecha(session.getUsuSesId());
            oResponse.put("events", dates);
            
            return WebResponse.crearWebResponseExito("el usuario se encuentra en la BD", oResponse);
        } else {
            return WebResponse.crearWebResponseError("contraseña incorrecta");
        }
    }

    public void buscarModulo(List<ModuloSistema> modulos, RolFuncionModel funcion) {
        SubModuloSistema subModulo = new SubModuloSistema(funcion.subModuloID);
        subModulo.setNom(funcion.subNombre);
        subModulo.setCod(funcion.subCodigo);
        subModulo.setIco(funcion.subIcono);
        ModuloSistema modulo = new ModuloSistema(funcion.moduloID);
        modulo.setNom(funcion.modNombre);
        modulo.setCod(funcion.modCodigo);
        modulo.setIco(funcion.modIcono);

        for (ModuloSistema m : modulos) {
            if (m.getModSisId() == modulo.getModSisId()) {
                buscarSubModulo(m.getSubModuloSistemas(), subModulo, funcion);
                return;
            }
        }

        /* Añadimos un nuevo modulo */
        modulo.setSubModuloSistemas(new ArrayList<SubModuloSistema>());
        subModulo.setFunciones(new ArrayList<RolFuncionModel>());
        subModulo.getFunciones().add(funcion);
        modulo.getSubModuloSistemas().add(subModulo);
        modulos.add(modulo);
    }

    public void buscarSubModulo(List<SubModuloSistema> subModulos, SubModuloSistema subModulo, RolFuncionModel funcion) {
        for (SubModuloSistema sm : subModulos) {
            if (sm.getSubModSisId() == subModulo.getSubModSisId()) {
                sm.getFunciones().add(funcion);
                return;
            }
        }
        /* Añadimos un nuevo subModulo */
        subModulo.setFunciones(new ArrayList<RolFuncionModel>());
        subModulo.getFunciones().add(funcion);
        subModulos.add(subModulo);
    }
    
    
}
