package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx.ListarActasReunionTx;

/**
 * Created by Administrador on 10/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("curricula_banco");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //Competencias
        component.addTransactionGET("listarBancoCompetencias", ListarBancoCompetenciasTx.class);
        component.addTransactionPOST("registrarCompetenciaBanco", RegistrarCompetenciaBancoTx.class);
        component.addTransactionDELETE("eliminarCompetenciaBanco",EliminarCompetenciaBancoTx.class);
        component.addTransactionPUT("editarCompetenciaBanco", EditarCompetenciaBancoTx.class);

        //Capacidades
        component.addTransactionPOST("registrarCapacidadBanco", RegistrarCapacidadBancoTx.class);
        component.addTransactionGET("listarCapacidadesCompetencia",ListarCapacidadesCompetenciaBancoTx.class);
        component.addTransactionPUT("editarCapacidadBanco", EditarCapacidadBancoTx.class);
        component.addTransactionDELETE("eliminarCapacidadBanco",EliminarCapacidadBancoTx.class);

        ///Indicadores
        component.addTransactionPOST("registrarIndicadorBanco", RegistrarIndicadorBancoTx.class);
        component.addTransactionPUT("editarIndicadorBanco",EditarIndicadorBancoTx.class);
        component.addTransactionDELETE("eliminarIndicadorBanco",EliminarIndicadorBancoTx.class);

        ///Areas Curriculares
        component.addTransactionGET("listarDisenosCurriculares", ListarDisenosCurricularesTx.class);
        component.addTransactionGET("listarAreasCurriculares", ListarAreasCurricularesTx.class);
        component.addTransactionPOST("registrarAreaCurricular", RegistrarAreaCurricularTx.class);
        component.addTransactionPUT("editarAreaCurricular",EditarAreaCurricularTx.class);
        component.addTransactionDELETE("eliminarAreaCurricular",EliminarAreaCurricularTx.class);
        return component;
    }
}
