package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "TrabajadorCapacitacion")
@Table(name = "trabajador", schema = "public")
public class Trabajador implements Serializable {

    @Id
    @Column(name = "tra_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_trabajador", sequenceName = "trabajador_tra_id_seq")
    @GeneratedValue(generator = "secuencia_trabajador")
    private int traId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_id", nullable = false)
    private Persona persona;
    
    public Trabajador() {}

    public Trabajador(Organizacion organizacion, Persona persona) {
        this.organizacion = organizacion;
        this.persona = persona;
    }

    public int getTraId() {
        return traId;
    }

    public void setTraId(int traId) {
        this.traId = traId;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
