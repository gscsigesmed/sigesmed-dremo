/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumns;
/**
 *
 * @author Jeferson
 */

@Entity
@Table(name="serie_documental", schema="administrativo")
public class SerieDocumental implements java.io.Serializable {
    
    @Id
    @Column(name="ser_doc_id", unique = true , nullable=false)
    @SequenceGenerator(name = "secuencia_ser_doc", sequenceName="administrativo.serie_documental_ser_doc_id_seq" )
    @GeneratedValue(generator="secuencia_ser_doc")
    private int ser_doc_id;
    
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_id" , insertable=false , updatable= false)
     private Area area; 
    
    
   // @ManyToOne(fetch=FetchType.LAZY)
   // @JoinColumn(name="uni_org_id" , insertable=false, updatable = false)
    
   
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="uni_org_id",referencedColumnName="uni_org_id",insertable=false,updatable=false),
        @JoinColumn(name="are_id",referencedColumnName="are_id",insertable=false,updatable=false)
    })
    private UnidadOrganica uni_org;
    
    
    @Column(name="uni_org_id")
    private int uni_org_id;
    
    
    @Column(name="are_id")
    private int are_id;
   
    @Column(name="cod")
    private String codigo;
    
    @Column(name="nom")
    private String nombre;
    
    @Column(name="val")
    private int valor;
    
    @OneToMany(mappedBy="serie" , cascade= CascadeType.PERSIST)
    private List<InventarioTransferencia> inventarios_transferencia;
    
    @OneToMany(mappedBy="serie" , cascade = CascadeType.PERSIST)
    private List<InventarioEliminacion> inventarios_eliminacion;
    
    public SerieDocumental(){
        
    }
    public SerieDocumental(int serie_doc_id){
        this.ser_doc_id = serie_doc_id;
    }
    public SerieDocumental(int serie_doc_id,int are_id,int uni_org_id){
        this.ser_doc_id = serie_doc_id;
        this.are_id = are_id;
        this.uni_org_id = uni_org_id;
        
        this.area = new Area(are_id);
        this.uni_org = new UnidadOrganica(uni_org_id);
    }
    public SerieDocumental(int serie_doc_id,int are_id,int uni_org_id,String codigo,String nombre , int valor){
        this.ser_doc_id = serie_doc_id;
        this.are_id = are_id;
        this.uni_org_id = uni_org_id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.valor = valor;
        
        this.uni_org = new UnidadOrganica(uni_org_id);
    }
    
    public int getSerDocId(){
        return this.ser_doc_id;
    }
    
    public void setSerDocId(int ser_doc_id){
        this.ser_doc_id = ser_doc_id;
    }
    
    public int getIdArea(){
        return this.are_id;
    }
    
    public void setArea(int id_area){
        this.are_id=  id_area;
    }
           
    public UnidadOrganica getUniOrg(){
        return this.uni_org;
    }
    public void setUniOrg(UnidadOrganica uni_org){
        this.uni_org = uni_org;
    }
    
    public String getCodigo(){
        return this.codigo;
    }
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    
    public int getValor(){
        return this.valor;
    }
    
    public void setValor(int valor){
        this.valor = valor;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public  List<InventarioTransferencia> getInventarios(){
        return this.inventarios_transferencia;
    }
    public void setInventarios(List<InventarioTransferencia> inventarios){
        this.inventarios_transferencia = inventarios;
    }
    
    
    public List<InventarioEliminacion> getInventariosElimi(){
        return this.inventarios_eliminacion;
    }
    public void setInventarioEli(List<InventarioEliminacion> inv_eli){
        this.inventarios_eliminacion = inv_eli;
    }
    public int getUniOrgId(){
        return this.uni_org_id;
    }
}
