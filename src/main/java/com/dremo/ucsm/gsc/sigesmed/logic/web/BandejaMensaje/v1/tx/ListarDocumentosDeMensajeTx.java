/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarDocumentosDeMensajeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int mensajeID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            mensajeID = requestData.getInt("mensajeID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los mensajes de la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<MensajeDocumento> mensajes = null;
        MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
        try{
            mensajes = mensajeDao.verDocumentosMensaje(mensajeID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documentos del mensaje\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los documentos del mensaje", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(MensajeDocumento mensaje:mensajes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("mensajeDocumentoID",mensaje.getMenDocId() );
            oResponse.put("documento",mensaje.getNomDoc());
            oResponse.put("url",Sigesmed.UBI_ARCHIVOS+Mensaje.BANDEJA_MENSAJE_PATH);
            
            oResponse.put("i",i++);
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listaron los documentos del mensaje correctamente",miArray);        
        //Fin
    }
    
}

