/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.me.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
public class EnviarTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        TareaEscolar tarea = new TareaEscolar();
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();            
            
            organizacionID =  requestData.getInt("organizacionID" );
            
            tarea.setTarEscId( requestData.getInt("tareaID") );
            tarea.setGraId( requestData.getInt("gradoID") );
            tarea.setSecId( requestData.getString("seccionID").charAt(0) );
            tarea.setFecEnt( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(requestData.getString("fechaEntrega")) );
            tarea.setEstado(Tarea.ESTADO_ENVIADO);
            tarea.setFecEnv(new Date());
            
            //verificando que la fecha de envio sea menor que la fecha de entrega de la tarea
            if( tarea.getFecEnv().compareTo( tarea.getFecEnt() ) >=0 ){
                System.out.println("la fecha de entrega es menor que la fecha de envio");
                return WebResponse.crearWebResponseError("error con la fecha de entrega", "la fecha de entrega es menor que la fecha de envio" );
            }
        
        }catch(Exception e){
            System.out.println("No se pudo enviar lar tarea, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar lar tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        EstudianteDao estDao = (EstudianteDao)FactoryDao.buildDao("me.EstudianteDao");
        try{
            
            List<Object[]> alumnos = estDao.buscarEstudiantesIdsPorGradoYSeccion(organizacionID, tarea.getGraId(), tarea.getSecId());
            
            //si hay alumnos se envia la tarea
            if(alumnos==null || alumnos.size()==0){
                System.out.println("No hay alumnos para enviar tarea");
                return WebResponse.crearWebResponseError("No hay alumnos para enviar tarea" );
            }
            List<BandejaTarea> bandejas = new ArrayList<BandejaTarea>();
            List<Integer> ids = new ArrayList<Integer>();
            for(Object[] alumno : alumnos){
                bandejas.add( new BandejaTarea(0,null,null,0,Tarea.ESTADO_NUEVO,tarea.getTarEscId(),((BigInteger)alumno[0]).intValue() ) );  
                ids.add((Integer)alumno[1]);                              
            }            
            tareaDao.enviarTarea(tarea,bandejas);
            AlertManager.sendAll(Alertas.WEB_NUEVA_TAREA, ids);
        }catch(Exception e){
            System.out.println("No se pudo enviar la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar la tarea", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+tarea.getEstado());
        return WebResponse.crearWebResponseExito("Se envio la tarea correctamente",oResponse);
        //Fin
    }    
    
    
}
