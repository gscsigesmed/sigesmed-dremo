package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.util.Date;

public class BandejaEvaluacionModel{

    public int banTarId;    
    public int nota;
    
    public Date fecEnt;
    public Date fecVis;    
    
    public String nombres;
    public String apellido1;
    public String apellido2;
    
    public char estado;

    public BandejaEvaluacionModel() {
    }
    public BandejaEvaluacionModel(int banTarId) {
        this.banTarId = banTarId;
    }
    public BandejaEvaluacionModel(int banTarId, Date fecEnt,Date fecVis, int nota,char estado,String nombres,String apellido1,String apellido2) {
       this.banTarId = banTarId;
       this.fecVis = fecVis;
       this.fecEnt = fecEnt;
       this.nota = nota;
       this.estado = estado;
       this.nombres = nombres;
       this.apellido1 = apellido1;
       this.apellido2 = apellido2;
    }
}


