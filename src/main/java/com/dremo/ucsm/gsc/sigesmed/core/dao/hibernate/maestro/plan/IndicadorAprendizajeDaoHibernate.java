package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public class IndicadorAprendizajeDaoHibernate extends GenericDaoHibernate<IndicadorAprendizaje> implements IndicadorAprendizajeDao{
    @Override
    public IndicadorAprendizaje buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            IndicadorAprendizaje indicador = (IndicadorAprendizaje) session.get(IndicadorAprendizaje.class,id);
            return indicador;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> buscarIndicadoresPorCapacidad(int capId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(IndicadorAprendizaje.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("cap").add(Restrictions.eq("capId",capId));
            return criteria.list();

        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
