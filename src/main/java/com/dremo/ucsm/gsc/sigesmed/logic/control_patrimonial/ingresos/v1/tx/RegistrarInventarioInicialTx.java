/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicialDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;


import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleInventarioInicialDAO;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.text.SimpleDateFormat;

/**
 *
 * @author Administrador
 */
public class RegistrarInventarioInicialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        InventarioInicial ii = null;
        InventarioInicialDetalle iid = null;
        MovimientoIngresos mv = null;
        JSONObject requestData = (JSONObject)wr.getData(); 
        MovimientoIngresos mov_ing = null;
        
           
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
            /*Registramos  los Bienes Muebles Insertados*/
                /*CABECERA*/
               
            JSONObject cab = requestData.getJSONObject("inventario_inicial_cab");

            
            /*Datos del Movimiento*/
            JSONObject mov = requestData.getJSONObject("movimiento"); 
            int cont_pat_id = mov.getInt("cont_pat_id");
            int tipo_mov_ing = mov.getInt("tip_mov_ing");
            String num_res = mov.getString("num_res");
            Date fec_res = formato2.parse(mov.getString("fec_res"));
            Date fec_mov = new Date();
            String obs = mov.getString("obs");
           
 
            /*Datos del Inventario Inicial*/
      //      int cont_pat_id = cab.getInt("cont_pat_id");
            char fla_cie    = cab.getString("fla_cie").charAt(0);
            char est_reg    = 'A';
            Date fec_mod = new Date();
            int usu_mod     = cab.getInt("usu_mod");
            int org_id = cab.getInt("org_id");
            
            boolean tipo_registro = cab.optBoolean("actualizar_inventario");
            /*true =actualizar , false= insertar*/
            if(tipo_registro==true){
                /*ACTUALIZAR*/
                /*Actualizamos el Movimiento de Ingreso*/
                int inv_ini_id = cab.getInt("inv_ini_id");
                int mov_ing_id = mov.getInt("mov_ing_id");
                mov_ing = new MovimientoIngresos(mov_ing_id,tipo_mov_ing,fec_mov,num_res,fec_res,obs,fec_mov,usu_mod,est_reg,cont_pat_id);


                MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO"); 
                mov_ing_dao.update(mov_ing);

                
                /*Insertamos el Inventario Inicial*/ 
                ii = new InventarioInicial(inv_ini_id,mov_ing.getMov_ing_id(),cont_pat_id,fla_cie,fec_mod,usu_mod,est_reg);
                ii.setOrg_id(org_id);
                InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
                
                ii.setInv_ini_det(new ArrayList<InventarioInicialDetalle>());
                inv_ini_dao.eliminar_detalle(inv_ini_id);
                
                JSONArray det = requestData.getJSONArray("bienes_inv_ini");
                DetalleInventarioInicialDAO det_inv_ini = (DetalleInventarioInicialDAO)FactoryDao.buildDao("scp.DetalleInventarioInicialDAO");
                
                    int size = det.length();
                    for( int i = 0 ; i < size; i++){
                        JSONObject bo = det.getJSONObject(i);
                        int cod_bie = bo.getInt("cod_bie");
                        //iid = new InventarioInicialDetalle(0,ii.getInv_ini_id(),mov_ing.getMov_ing_id(),cod_bie);
                        iid = new InventarioInicialDetalle(0,ii,cod_bie);
                        ii.getInv_ini_det().add(iid);     
                    }   
                      inv_ini_dao.update(ii); 
            }else{
                
            /*INSERT*/
            /*Insertamos el Movimiento de Ingreso*/
            mov_ing = new MovimientoIngresos(0,tipo_mov_ing,fec_mov,num_res,fec_res,obs,fec_mov,usu_mod,est_reg,cont_pat_id);
         
            
            MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO"); 
            mov_ing_dao.insert(mov_ing);
            
            
            /*Insertamos el Inventario Inicial*/ 
            ii = new InventarioInicial(0,mov_ing.getMov_ing_id(),cont_pat_id,fla_cie,fec_mod,usu_mod,est_reg);
            ii.setOrg_id(org_id);
            InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
            ii.setInv_ini_det(new ArrayList<InventarioInicialDetalle>());
            JSONArray det = requestData.getJSONArray("bienes_inv_ini");
                int size = det.length();
                for( int i = 0 ; i < size; i++){
                    JSONObject bo = det.getJSONObject(i);
                    int cod_bie = bo.getInt("cod_bie");

                    iid = new InventarioInicialDetalle(0,ii,cod_bie);
                    ii.getInv_ini_det().add(iid);
                }   
              
                  inv_ini_dao.insert(ii); 

            }
     
        }catch(Exception e){
             System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Inventario Inicial, datos incorrectos", e.getMessage() );

        }
        return WebResponse.crearWebResponseExito("El registro del Inventario Inicial se realizo , correctamente");
 
        
    }
    
    
    
}
