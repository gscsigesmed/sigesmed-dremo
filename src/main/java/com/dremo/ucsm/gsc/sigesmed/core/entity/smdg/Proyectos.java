/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "proyectos", schema = "institucional")

public class Proyectos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pro_id")
    private Integer proId;
    @Column(name = "pro_nom")
    private String proNom;
    @Column(name = "pro_tip")
    private String proTip;
    @Column(name = "pro_ini")
    @Temporal(TemporalType.DATE)
    private Date proIni;
    @Column(name = "pro_fin")
    @Temporal(TemporalType.DATE)
    private Date proFin;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";
    @Column(name = "pro_res")
    private Integer proRes;
//    @OneToMany(mappedBy = "proId")
//    private Collection<ProyectoActividades> proyectoActividadesCollection;
    @OneToMany(cascade=CascadeType.ALL )
    @JoinColumn(name="pro_id")    
    @LazyCollection(LazyCollectionOption.FALSE)
//    @Filter(name="filtroplantilla", condition=" est_reg = 'A'")
    private Set<ProyectoActividades> actividades;
    
    @OneToOne(cascade=CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ProyectoDetalle detalle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_cre_id")
    private Usuario usuarioCreador;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;
    public Proyectos() {
    }
    
    public Proyectos(Integer proId) {
        this.proId = proId;
    }
    
    public Proyectos(String proNom, String proTip, Date proIni, Date proFin, Integer proRes) {
        this.proNom = proNom;
        this.proTip = proTip;
        this.proIni = proIni;
        this.proFin = proFin;        
        this.proRes = proRes;    
    }
    
    public Proyectos(String proNom, String proTip, Date proIni, Date proFin, Integer proRes, ProyectoDetalle detalle) {        
        this.proNom = proNom;
        this.proTip = proTip;
        this.proIni = proIni;
        this.proFin = proFin;        
        this.proRes = proRes;
        this.detalle = detalle;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getProNom() {
        return proNom;
    }

    public void setProNom(String proNom) {
        this.proNom = proNom;
    }

    public String getProTip() {
        return proTip;
    }

    public void setProTip(String proTip) {
        this.proTip = proTip;
    }

    public Date getProIni() {
        return proIni;
    }

    public void setProIni(Date proIni) {
        this.proIni = proIni;
    }

    public Date getProFin() {
        return proFin;
    }

    public void setProFin(Date proFin) {
        this.proFin = proFin;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Integer getProRes() {
        return proRes;
    }

    public void setProRes(Integer proRes) {
        this.proRes = proRes;
    }

    @XmlTransient
    public Set<ProyectoActividades> getActividades() {
        return actividades;
    }

    public void setActividades(Set<ProyectoActividades> actividades) {
        this.actividades = actividades;
    }

    @XmlTransient
    public ProyectoDetalle getDetalle() {
        return detalle;
    }

    public void setDetalle(ProyectoDetalle detalle) {
        this.detalle = detalle;
    }

    public Usuario getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(Usuario usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    //    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (proId != null ? proId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Proyectos)) {
//            return false;
//        }
//        Proyectos other = (Proyectos) object;
//        if ((this.proId == null && other.proId != null) || (this.proId != null && !this.proId.equals(other.proId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.Proyectos[ proId=" + proId + " ]";
//    }
    
}
