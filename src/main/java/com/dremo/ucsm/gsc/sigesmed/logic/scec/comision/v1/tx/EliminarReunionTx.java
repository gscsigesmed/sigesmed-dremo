package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class EliminarReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EliminarReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return eliminarReunion(data.getInt("cod"));
    }

    private WebResponse eliminarReunion(int cod) {
        try {
            ReunionComisionDao reunionComisionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
            ReunionComision reunionComision = reunionComisionDao.buscarReunionConActas(cod);
            if(!reunionComision.getActasReunion().isEmpty()){
                return WebResponse.crearWebResponseError("No se puede eliminar la reunion, por que tiene actas",WebResponse.BAD_RESPONSE);
            }
            reunionComisionDao.deleteAbsolute(reunionComision);
            return  WebResponse.crearWebResponseExito("Exito al eliminar la reunion",WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"eliminarReunion",e);
            return WebResponse.crearWebResponseError("Error al eliminar la reunion",WebResponse.BAD_RESPONSE);
        }
    }
}
