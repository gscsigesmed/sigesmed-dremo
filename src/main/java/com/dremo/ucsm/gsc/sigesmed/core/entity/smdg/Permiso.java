/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Permiso")
@Table(name = "permiso", schema="institucional")

public class Permiso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "prm_ide")
    private Short prmIde;
    @Column(name = "prm_nom")
    private String prmNom;
    @Column(name = "prm_des")
    private String prmDes;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
    @OneToMany(mappedBy = "ifdPrmIde")
    private Collection<ItemFileDetalle> itemFileDetalleCollection;

    public Permiso() {
    }

    public Permiso(Short prmIde) {
        this.prmIde = prmIde;
    }

    public Short getPrmIde() {
        return prmIde;
    }

    public void setPrmIde(Short prmIde) {
        this.prmIde = prmIde;
    }

    public String getPrmNom() {
        return prmNom;
    }

    public void setPrmNom(String prmNom) {
        this.prmNom = prmNom;
    }

    public String getPrmDes() {
        return prmDes;
    }

    public void setPrmDes(String prmDes) {
        this.prmDes = prmDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @XmlTransient
    public Collection<ItemFileDetalle> getItemFileDetalleCollection() {
        return itemFileDetalleCollection;
    }

    public void setItemFileDetalleCollection(Collection<ItemFileDetalle> itemFileDetalleCollection) {
        this.itemFileDetalleCollection = itemFileDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prmIde != null ? prmIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permiso)) {
            return false;
        }
        Permiso other = (Permiso) object;
        if ((this.prmIde == null && other.prmIde != null) || (this.prmIde != null && !this.prmIde.equals(other.prmIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.Permiso[ prmIde=" + prmIde + " ]";
    }
    
}
