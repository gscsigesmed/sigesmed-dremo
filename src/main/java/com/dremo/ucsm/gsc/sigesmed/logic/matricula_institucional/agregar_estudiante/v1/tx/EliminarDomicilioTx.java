package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.DomicilioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Domicilio;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DomicilioId;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EliminarDomicilioTx implements ITransaction {

    private static Logger logger = Logger.getLogger(EliminarDomicilioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        long perId, domCod;

        try {
            perId = data.getInt("perId");
            domCod = data.getInt("domCod");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al cargar datos al eliminar domicilio", e);
            return WebResponse.crearWebResponseError("Nose pudo eliminar domicilio");
        }

        DomicilioDaoHibernate dhDomicilio = new DomicilioDaoHibernate();

        try {
            Domicilio miDomicilio = new Domicilio();
            miDomicilio.setId(new DomicilioId(domCod, perId));
            dhDomicilio.delete(miDomicilio);

            return WebResponse.crearWebResponseExito("Se elimino el Domicilio exitosamente", miDomicilio);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al Eliminar Domicilio", e);
            return WebResponse.crearWebResponseError("Nose pudo eliminar Domicilio");
        }
    }
}
