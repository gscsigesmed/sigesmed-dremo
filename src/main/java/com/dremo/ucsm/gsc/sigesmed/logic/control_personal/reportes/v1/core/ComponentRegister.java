/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.core;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.core.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.BuscarPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.ListarCargoTrabajadorByTipoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.ListarTrabajadorDetalladoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.nuevoTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.BuscarAsistenciaByFechaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.BuscarPersonaxDniTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ListarDRTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ListarIETx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ListarNivelesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ListarUgelesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ReporteConsolidadoMensualTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ReporteCuadroAsistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ReporteHEDiaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ReporteHEMesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ReporteImagenTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.ReporteListadoAsistenciasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx.VerificarOrganizacionesByTrabajadorTx;




/**
 *
 * @author carlos
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONTROL_PERSONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("reportes");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente

        component.addTransactionGET("reporteConsolidadoMensual", ReporteConsolidadoMensualTx.class);
        component.addTransactionGET("reporteCuadroAsistencia", ReporteCuadroAsistenciaTx.class);
        component.addTransactionGET("verificarOrganizacionesByTrabajador", VerificarOrganizacionesByTrabajadorTx.class);
        component.addTransactionGET("buscarPersonaxDni", BuscarPersonaxDniTx.class);
        component.addTransactionGET("buscarAsistenciaByFecha", BuscarAsistenciaByFechaTx.class);
        component.addTransactionGET("reporteListadoAsistencias", ReporteListadoAsistenciasTx.class);
        component.addTransactionGET("listarDR", ListarDRTx.class);
        component.addTransactionGET("listarUgeles", ListarUgelesTx.class);
        component.addTransactionGET("listarIE", ListarIETx.class);
        component.addTransactionGET("listarNiveles", ListarNivelesTx.class);
        component.addTransactionPOST("reporteListadoAsistenciasImagen", ReporteImagenTx.class);
        component.addTransactionPOST("reporteHorasEfectivasDia", ReporteHEDiaTx.class);
        component.addTransactionPOST("reporteHorasEfectivasMes", ReporteHEMesTx.class);
//reporteListadoAsistencias
      
        
        return component;
    }
}
