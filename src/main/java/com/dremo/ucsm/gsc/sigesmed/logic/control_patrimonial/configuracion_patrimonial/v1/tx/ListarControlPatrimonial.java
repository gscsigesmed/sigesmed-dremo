/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx;

/**
 *
 * @author Administrador
 */
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ControlPatrimonialDAO;
import java.text.SimpleDateFormat;
import org.json.JSONArray;
import java.util.List;



public class ListarControlPatrimonial implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        

            JSONArray miArray = new JSONArray(); 
            List<ControlPatrimonial> cp_list = null;
     //       SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int org_id = requestData.getInt("org_id");
            
            ControlPatrimonialDAO cp_dao = (ControlPatrimonialDAO)FactoryDao.buildDao("scp.ControlPatrimonialDAO");
            cp_list = cp_dao.listarPatrimonio(org_id);
            
            for(ControlPatrimonial cp : cp_list){
                JSONObject oResponse = new JSONObject();
                oResponse.put("cp_id",cp.getCon_pat_id());
                oResponse.put("obs",cp.getObs());
              
                
                String fec_ini = formatter.format(cp.getFec_ini());
                String fec_cie = formatter.format(cp.getFec_cie());
                
                oResponse.put("fec_ini",fec_ini);
                oResponse.put("fec_cie",fec_cie);
                oResponse.put("per_res",cp.getPer_res()); 
                oResponse.put("org_id",cp.getOrg_id());
                oResponse.put("usu_mod",cp.getUsu_mod());
                
                miArray.put(oResponse);
            }
        }catch(Exception e){
            System.out.println("No se pudo Obtener los Controles Patrimoniales para la I.E.\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Obtener los Controles Patrimoniales para la I.E.", e.getMessage() );        

        }
           return WebResponse.crearWebResponseExito("Se obtuvo correctamente los Controles Patrimoniales para la I.E.",miArray); 
 
    }
    
    
}
