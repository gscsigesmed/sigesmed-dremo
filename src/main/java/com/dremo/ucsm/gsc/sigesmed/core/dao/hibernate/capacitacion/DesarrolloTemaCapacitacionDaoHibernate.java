package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.DesarrolloTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.DesarrolloTemaCapacitacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class DesarrolloTemaCapacitacionDaoHibernate extends GenericDaoHibernate<DesarrolloTemaCapacitacion> implements DesarrolloTemaCapacitacionDao {

    private static final Logger logger = Logger.getLogger(DesarrolloTemaCapacitacionDaoHibernate.class.getName());

    @Override
    public List<DesarrolloTemaCapacitacion> buscarPorTema(int temCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(DesarrolloTemaCapacitacion.class)
                    .add(Restrictions.eq("temCurCapId", temCod))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorTema", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public DesarrolloTemaCapacitacion buscarPorId(int desCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (DesarrolloTemaCapacitacion) session.get(DesarrolloTemaCapacitacion.class, desCod);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }
    @Override
    public int totalEvaluaciones(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(DesarrolloTemaCapacitacion.class)
                    .add(Restrictions.eq("sedCapId", codSed))
                    .add(Restrictions.eq("tip", 'E'))
                    .setProjection(Projections.rowCount())
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return ((Long) query.uniqueResult()).intValue();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "totalEvaluaciones", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public int totalTareas(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(DesarrolloTemaCapacitacion.class)
                    .add(Restrictions.eq("sedCapId", codSed))
                    .add(Restrictions.eq("tip", 'T'))
                    .setProjection(Projections.rowCount())
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return ((Long) query.uniqueResult()).intValue();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "totalTareas", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
