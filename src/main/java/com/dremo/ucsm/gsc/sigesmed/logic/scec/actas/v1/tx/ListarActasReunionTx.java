package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class ListarActasReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarActasReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        return listarActas(data.getInt("reu"));
    }
    public WebResponse listarActas (int idReu){
        try{
            ActasReunionComisionDao actasReunionComisionDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
            List<ActasReunionComision> actas = actasReunionComisionDao.buscarPorReunion(idReu);

            return WebResponse.crearWebResponseExito("Exito al listar las actas", WebResponse.OK_RESPONSE).setData(
                    new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"actComId","des","fecRegAct"},
                            new String[]{"cod","des","reg"},
                            actas))
            );
        }catch (Exception  e){
            logger.log(Level.SEVERE,"listarActas",e);
            return WebResponse.crearWebResponseError("Error al listar las actas",WebResponse.BAD_RESPONSE);
        }
    }
}
