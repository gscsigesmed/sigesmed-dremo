package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/11/2016.
 */
public class ListarSesionesAprendizajeTx implements ITransaction {
    private static Logger logger = Logger.getLogger(ListarSesionesAprendizajeTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int unidadId  = data.getInt("unid");
        return listarSesiones(unidadId);
    }

    private WebResponse listarSesiones(int unidadId) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            List<SesionAprendizaje> sesiones = sesionDao.listarSesionesUnidad(unidadId);
            JSONArray jsonSesiones = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"sesAprId","numSes","tit","nivAva","fecIni","fecFin"},
                    new String[]{"id","numSes","tit","est","ini","fin"},
                    sesiones
            ));
            return WebResponse.crearWebResponseExito("Se listo correctamente los datos",jsonSesiones);
        }catch (Exception e){
            logger.log(Level.SEVERE,"ListarSesiones",e);
            return WebResponse.crearWebResponseError("No se pudieron listar las sesiones");
        }

    }
}
