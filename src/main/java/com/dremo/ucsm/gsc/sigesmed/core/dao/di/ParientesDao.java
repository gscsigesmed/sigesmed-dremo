/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface ParientesDao extends GenericDao<Parientes>{
    public String buscarUltimoCodigo();
    public List<Parientes> listarParientesxTrabajador(int idTra);
    //public List<Area> buscarPorOrganizacion(int organizacionID);
}
