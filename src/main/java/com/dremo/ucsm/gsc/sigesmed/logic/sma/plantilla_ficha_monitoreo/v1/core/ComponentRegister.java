/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_ficha_monitoreo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_ficha_monitoreo.v1.tx.*;

/**
 *
 * @author Administrador
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.SISTEMA_MONITOREO_ACOMPANIAMIENTO);        
        
        //Registrando el Nombre del componente
        component.setName("plantilla_ficha_monitoreo");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        
        component.addTransactionGET("listarPlantillas", ListarPlantillasTx.class);
        component.addTransactionGET("listarPlantilla", ListarPlantillaTx.class);
        component.addTransactionPUT("actualizarPlantilla", ActualizarPlantillaTx.class);
        component.addTransactionPOST("registrarPlantillaMonitoreo", RegistrarPlantillaMonitoreoTx.class);
        component.addTransactionPUT("eliminarPlantilla", EliminarPlantillaTx.class);        
//        component.addTransactionPUT("actualizarIndicador", ActualizarIndicadorTx.class);        
//        component.addTransactionPUT("eliminarIndicador", EliminarIndicadorTx.class);
        
        component.addTransactionGET("imprimirPlantilla", ImprimirPlantillaTx.class);
        
        return component;
    }
}


