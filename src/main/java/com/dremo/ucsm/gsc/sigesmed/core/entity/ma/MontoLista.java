/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name="monto_lista",schema="public")
@DynamicUpdate(value = true)
public class MontoLista implements java.io.Serializable{
    @Id
    @Column(name="mon_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_monto", sequenceName="public.monto_lista_mon_id_seq" )
    @GeneratedValue(generator="secuencia_monto")
    private int monId;
    
    @Column(name="mon_an", length = 4)
    private String anioMonto;
    
    @Column (name="mon_val")
    private Double valMonto;
    
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "niv_id")
    private Nivel nivel;

    public MontoLista() {
    }

    public MontoLista(Nivel nivel,String anioMonto, Double valMonto) {
        this.anioMonto = anioMonto;
        this.valMonto = valMonto;
        this.nivel = nivel;
    }

    public MontoLista(String anioMonto, Double valMonto) {
        this.anioMonto = anioMonto;
        this.valMonto = valMonto;
    }
    
    public MontoLista(String anioMonto, Double valMonto, Integer usuMod, Date fecMod, Character estReg, Nivel nivel) {
        this.anioMonto = anioMonto;
        this.valMonto = valMonto;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.nivel = nivel;
    }

    public int getMonId() {
        return monId;
    }

    public void setMonId(int monId) {
        this.monId = monId;
    }

    public String getAnioMonto() {
        return anioMonto;
    }

    public void setAnioMonto(String anioMonto) {
        this.anioMonto = anioMonto;
    }

    public Double getValMonto() {
        return valMonto;
    }

    public void setValMonto(Double valMonto) {
        this.valMonto = valMonto;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }
}
