/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador;
import java.util.List;

/**
 *
 * @author Yemi
 */
public interface TrabajadorDao extends GenericDao<Trabajador>{
    public List<Trabajador> listarPorOrganizacion(int orgId, int carId);
    public Trabajador buscarPorId(Integer traId);
    public Trabajador buscarDatosPersonales(int traId);
    
}
