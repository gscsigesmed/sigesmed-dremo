package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 23/10/16.
 */
public class RegistrarPlanAnualTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarPlanAnualTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();
            int idUsuario = data.getInt("usr");
            int idOrganizacion = data.getInt("org");
            int idArea = data.getJSONObject("are").getInt("id");
            int idGrado = data.getJSONObject("grad").getInt("id");
            String nom = data.getString("nom");

            return registrarPlanAnual(idUsuario,idOrganizacion,idArea,idGrado,nom);
        }catch (JSONException e){
            logger.log(Level.SEVERE,"execute",e);
            throw e;
        }
    }

    private WebResponse registrarPlanAnual(int idUsuario, int idOrganizacion,int idArea, int idGrado, String nom) {
        try{

            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");

            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            //verificamos que el usuario sea un docente
            Docente docente = docDao.buscarDocentePorUsuario(idUsuario);
            Organizacion org = orgDao.buscarConTipoOrganizacionYPadre(idOrganizacion);

            if(docente != null){
                //verificamos que estemos en una ie
                if(org.getTipoOrganizacion().getCod().trim().toLowerCase().equals("ie")){
                    PlanEstudios plan = planDao.buscarVigentePorOrganizacion(idOrganizacion);
                    if(plan == null){
                        return WebResponse.crearWebResponseError("Se debe registrar el plan anual de la IE primero");
                    }
                    ProgramacionAnual prog = progDao.buscarProgramacionAnualPorPlanEstudios(plan.getPlaEstId(),idOrganizacion,docente.getDoc_id(),idArea,idGrado);
                    if(prog != null){
                        return WebResponse.crearWebResponseError("Ya existe una programacion para el curso en el grado seleccionado para el vigente año");
                    }else{
                        prog =  new ProgramacionAnual(nom,
                                org,docente,progDao.buscarArea(idArea),progDao.buscarGrado(idGrado));
                        prog.setPlanEstudios(plan);
                        progDao.insert(prog);
                    }


                    return WebResponse.crearWebResponseExito("Se registro con exito",
                            new JSONObject().put("cod",prog.getProAnuId()).put("org",new JSONObject(EntityUtil.objectToJSONString(
                                    new String[]{"orgId","nom"},
                                    new String[]{"cod","nom"},
                                    org
                            ))));
                }else {
                    return WebResponse.crearWebResponseError("Se debe estar logueado en una IE");
                }


            }else {
                return WebResponse.crearWebResponseError("Los datos del docente no estan registrados");
            }

        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarPlan",e);
            return WebResponse.crearWebResponseError("Error al registrar el plan");
        }
    }
}
