package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="planilla_remuneracion" ,schema="administrativo")
public class PlanillaRemuneracion  implements java.io.Serializable {


    @Id
    @Column(name="pla_rem_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_planilla_remuneracion", sequenceName="administrativo.planilla_remuneracion_pla_rem_id_seq" )
    @GeneratedValue(generator="secuencia_planilla_remuneracion")
    private Integer plaRemId;
    
    @Column(name="pla_rem_num")
    private String plaRemNum;
    
    @Column(name="est_reg")
    private String estReg;

    @Column(name="pla_est")
    private String plaEst;  
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="tra_id")
    private Trabajador trabajadorId;

    public PlanillaRemuneracion(String plaRemNum, String estReg, String plaEst, Trabajador trabajadorId) {
        this.plaRemNum = plaRemNum;
        this.estReg = estReg;
        this.plaEst = plaEst;
        this.trabajadorId = trabajadorId;
    }

    public Integer getPlaRemId() {
        return plaRemId;
    }

    public void setPlaRemId(Integer plaRemId) {
        this.plaRemId = plaRemId;
    }

    public String getPlaRemNum() {
        return plaRemNum;
    }

    public void setPlaRemNum(String plaRemNum) {
        this.plaRemNum = plaRemNum;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getPlaEst() {
        return plaEst;
    }

    public void setPlaEst(String plaEst) {
        this.plaEst = plaEst;
    }

    public Trabajador getTrabajadorId() {
        return trabajadorId;
    }

    public void setTrabajadorId(Trabajador trabajadorId) {
        this.trabajadorId = trabajadorId;
    }
    
    
   
}


