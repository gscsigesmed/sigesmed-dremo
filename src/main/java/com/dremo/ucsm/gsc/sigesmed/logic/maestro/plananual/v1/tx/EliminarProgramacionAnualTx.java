package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 16/12/2016.
 */
public class EliminarProgramacionAnualTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarProgramacionAnualTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idPlan= data.getInt("cod");
        return eliminarPlan(idPlan);
    }

    private WebResponse eliminarPlan(int idPlan) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ProgramacionAnual prog = progDao.buscarProgramacionConObjetivos(idPlan);
            progDao.delete(prog);
            return WebResponse.crearWebResponseExito("Exito al eliminar la programacion anual");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarPlan",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la programacion anual");
        }
    }
}
