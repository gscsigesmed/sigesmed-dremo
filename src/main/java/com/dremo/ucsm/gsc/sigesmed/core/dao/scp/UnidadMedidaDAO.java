/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.UnidadMedida;

/**
 *
 * @author Administrador
 */
public interface UnidadMedidaDAO extends GenericDao<UnidadMedida> {
    
    public List<UnidadMedida> listarUnidadesMedida();
    public UnidadMedida mostrarDetalleUnidadMedida(int un_med_id);

}
