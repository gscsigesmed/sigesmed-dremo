package com.dremo.ucsm.gsc.sigesmed.logic.directorio.tipo_pariente.v1.core;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrador
 */
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx.ActualizarParienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx.EliminarParienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx.InsertarParienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx.InsertarParientesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx.ListarParientesPorTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.tipo_pariente.v1.tx.ListarTiposTx;


/**
 *
 * @author ucsm
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DIRECTORIO);        
        
        //Registrando el Nombre del componente
        component.setName("tipoPariente");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
//        component.addTransactionPOST("insertarPariente", InsertarParienteTx.class);      
//        component.addTransactionPOST("insertarParientes", InsertarParientesTx.class);
//        component.addTransactionDELETE("eliminarPariente", EliminarParienteTx.class);
//        component.addTransactionPUT("eliminarPariente", EliminarParienteTx.class);
//        component.addTransactionPUT("actualizarPariente", ActualizarParienteTx.class);
        component.addTransactionGET("listarTipos", ListarTiposTx.class);
               
        
        return component;
    }
}
