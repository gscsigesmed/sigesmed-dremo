/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class InsertarTipoDocTx implements ITransaction{
    String nombreTipDoc="";
    String abreviaturaTipDoc="";
    String descripcionTipDoc="";
//    String strfecha="";
//    Date fecha = null;
    int usuarioID = 1;
    @Override
    public WebResponse execute(WebRequest wr) {
        /* LECTURA DE DATOS */
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            abreviaturaTipDoc = requestData.getString("abr");
            nombreTipDoc = requestData.getString("nom");
            descripcionTipDoc = requestData.getString("des");
//            strfecha = requestData.getString("fec");
//            SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
//            fecha = formatFecha.parse(strfecha);
            usuarioID = requestData.getInt("usuarioID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer la data enviada");        
        }
        
        /* INTERACCION CON BASE DE DATOS */
        Date fecha=new Date();
        TipoEspecializadoDao tipEspDao = (TipoEspecializadoDao) FactoryDao.buildDao("rdg.TipoEspecializadoDao");
        
        TipoEspecializado tipEsp = new TipoEspecializado(abreviaturaTipDoc,nombreTipDoc,descripcionTipDoc,fecha,usuarioID,"A");        
        
        /*Persistencia*/
        tipEspDao.insert(tipEsp);
        
        /*Respuesta de Retorno de Exito al Usuario*/
        JSONObject oResponse = new JSONObject();
        oResponse.put("codtipdoc", tipEsp.getTesIde());
        
        return WebResponse.crearWebResponseExito("El registro del Tipo de Doc fue exitoso", oResponse);
        
        
    }
    
}
