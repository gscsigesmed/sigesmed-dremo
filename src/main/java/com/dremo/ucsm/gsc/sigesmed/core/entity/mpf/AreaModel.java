/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;


/**
 *
 * @author Carlos
 */
public class AreaModel {
    private Integer areaID;
    private String area;
    private Integer docenteId;
    private String docenteNom;
    private String docentePat;
    private String docenteMat;
    private Integer listaUtilesId;

    public AreaModel(Integer areaID, String area, Integer docenteId, String docenteNom, String docentePat, String docenteMat) {
        this.areaID = areaID;
        this.area = area;
        this.docenteId = docenteId;
        this.docenteNom = docenteNom;
        this.docentePat = docentePat;
        this.docenteMat = docenteMat;
    }
    
    public AreaModel(Integer areaID, String area, Integer docenteId, String docenteNom, String docentePat, String docenteMat,Integer listaUtilesId) {
        this.areaID = areaID;
        this.area = area;
        this.docenteId = docenteId;
        this.docenteNom = docenteNom;
        this.docentePat = docentePat;
        this.docenteMat = docenteMat;
        this.listaUtilesId=listaUtilesId;
    }

    public Integer getAreaID() {
        return areaID;
    }

    public void setAreaID(Integer areaID) {
        this.areaID = areaID;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getDocenteId() {
        return docenteId;
    }

    public void setDocenteId(Integer docenteId) {
        this.docenteId = docenteId;
    }

    public String getDocenteNom() {
        return docenteNom;
    }

    public void setDocenteNom(String docenteNom) {
        this.docenteNom = docenteNom;
    }

    public String getDocentePat() {
        return docentePat;
    }

    public void setDocentePat(String docentePat) {
        this.docentePat = docentePat;
    }

    public String getDocenteMat() {
        return docenteMat;
    }

    public void setDocenteMat(String docenteMat) {
        this.docenteMat = docenteMat;
    }
     
    public String getNombresPM()
    {
        return this.docentePat+" "+this.docenteMat+" "+this.docenteNom;
    }

    public Integer getListaUtilesId() {
        return listaUtilesId;
    }

    public void setListaUtilesId(Integer listaUtilesId) {
        this.listaUtilesId = listaUtilesId;
    }
  
    
}
