/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class BuscarDocenteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String tipo = "";
        int docenteID = 0;
        String docenteDNI = "";
        String docenteCodigo = "";
        
        try{            
            JSONObject requestData = (JSONObject)wr.getData();
            tipo = requestData.optString("tipo");
            
            if(tipo.contentEquals("DNI"))
                docenteDNI = requestData.optString("valor");
            else if(tipo.contentEquals("codigo"))
                docenteCodigo = requestData.optString("valor");
            else
                docenteID = requestData.optInt("docenteID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar el docente, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Docente docente = null;
        
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            
            if(tipo.contentEquals("DNI"))
                docente = disDao.buscarDocentePorDNI(docenteDNI);
            else if(tipo.contentEquals("codigo"))
                docente = disDao.buscarDocentePorCodigoModular(docenteCodigo);
            else
                docente = disDao.buscarDocentePorID(docenteID);
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el docente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar el docente", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        
        if(docente != null){        
            JSONObject oResponse = new JSONObject();
            oResponse.put("docenteID",docente.getDocId());

            oResponse.put("especialidad",docente.getEsp());
            oResponse.put("codigoModular",docente.getCodMod());
            oResponse.put("nivelMagisterial",docente.getNivMag());

            oResponse.put("DNI",docente.getPersona().getDni());
            oResponse.put("nombre",docente.getPersona().getNombrePersona());            
            return WebResponse.crearWebResponseExito("Las docentes se listo correctamente",oResponse);
        }
        
        return WebResponse.crearWebResponseError("No se encontro el docente buscado");
        //Fin
    }
    
}

