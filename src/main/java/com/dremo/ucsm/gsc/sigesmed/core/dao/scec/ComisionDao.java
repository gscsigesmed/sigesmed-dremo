/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import java.util.List;
/**
 *
 * @author ucsm
 */

public interface ComisionDao extends GenericDao<Comision>{
    Comision buscarPorId(int idComision);
    Comision buscarPorIdConIntegrantes(int idComision);
    List<Comision> listarComisionesPorOrganizacion(int idOrganizacion);
    List<Comision> listarComisionPorOrganizacionDirector(int idOrganizacion);
    List<CargoDeComision> buscarCargosDeComision(int idCom);
}