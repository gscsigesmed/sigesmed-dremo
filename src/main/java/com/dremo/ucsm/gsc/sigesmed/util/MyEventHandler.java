/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.color.DeviceCmyk;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.Property;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.layout.element.Image;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.io.image.ImageType;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import javax.imageio.ImageTypeSpecifier;

/**
 *
 * @author Administrador
 */
public class MyEventHandler implements IEventHandler{
    private PdfFont helvetica = null;
    private String titulo_de_Encabezado = "SIGESMED MOQUEGUA";
    private Image imagenCabecera = null;
    protected PdfExtGState gState;
    
    public  MyEventHandler() throws Exception {        
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);            
        imagenCabecera = new Image(ImageDataFactory.create(ServicioREST.PATH_SIGESMED + "/recursos/img/minedu.png"));//
//        imagenCabecera = new Image(ImageDataFactory.create("src/main/webapp/recursos/img/minedu.png"));
        gState = new PdfExtGState();//.setFillOpacity(0.2f);
    }
    
    public  MyEventHandler(String titulo_de_Encabezado) throws Exception {
        this.titulo_de_Encabezado = titulo_de_Encabezado;        
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);  
        imagenCabecera = new Image(ImageDataFactory.create(ServicioREST.PATH_SIGESMED + "/recursos/img/minedu.png"));
//        imagenCabecera = new Image(ImageDataFactory.create("src/main/webapp/recursos/img/minedu.png"));
        gState = new PdfExtGState();//.setFillOpacity(0.2f);
    }       
    
    public void handleEvent(Event event) {
        
        PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();
        int pageNumber = pdfDoc.getPageNumber(page);
        
        Rectangle pageSize = page.getPageSize();
        PdfCanvas pdfCanvas = new PdfCanvas(
            page.newContentStreamBefore(), page.getResources(), pdfDoc);
        
        //Add header and footer
        pdfCanvas.beginText()                
                .setFontAndSize(helvetica, 9)
                .moveText(pageSize.getWidth() / 2 - 60, pageSize.getTop() - 20)                              
                .showText("SIGESMED MOQUEGUA")
                .moveText(60, -pageSize.getTop() + 30)             
                .showText(String.valueOf(pageNumber))                
                .endText();
        
        imagenCabecera.setFixedPosition(20, pageSize.getTop() - 40);
        
        pdfCanvas.saveState().setExtGState(gState);        
        Canvas canvas = new Canvas(pdfCanvas, pdfDoc, page.getPageSize());
            canvas.add(imagenCabecera.scaleAbsolute(120, 30));
            
            
        pdfCanvas.restoreState();
        pdfCanvas.release();
        
                
        //Add watermark
//        Canvas canvas = new Canvas(pdfCanvas, pdfDoc, page.getPageSize());
//        canvas.setProperty(Property.FONT_COLOR, Color.WHITE);
//        canvas.setProperty(Property.FONT_SIZE, 60);
//        canvas.setProperty(Property.FONT, helveticaBold);
//        canvas.showTextAligned(new Paragraph("CONFIDENTIAL"),
//            298, 421, pdfDoc.getPageNumber(page),
//            TextAlignment.CENTER, VerticalAlignment.MIDDLE, 45);
 
        
    }
}
