/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;


import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarControlPatrimonial;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.RegistrarControlPatrimonialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarControlPatrimonialUGELTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarAnexoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarTipoPropiedadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarTipoTerrenoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarCatalogoDireccionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ListarUnidadesMetricasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.ReporteIETx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx.EditarControlPatrimonialTx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister  implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        
         // Asignando al modulo al cual Pertenece
         WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTROL_PATRIMONIAL);
         
        //Registrando el Nombre del Componente
        component.setName("configuracion_patrimonial");
        //version del componente
        component.setVersion(1);
        
        component.addTransactionGET("listar_anexos",ListarAnexoTx.class);
        component.addTransactionGET("listar_control_patrimonial",ListarControlPatrimonial.class);
        component.addTransactionPOST("registrar_control_patrimonial",RegistrarControlPatrimonialTx.class);
        component.addTransactionGET("listar_control_ugel",ListarControlPatrimonialUGELTx.class);
        component.addTransactionGET("reporte_bienes_ie",ReporteIETx.class);
        component.addTransactionGET("listar_tipo_propiedad",ListarTipoPropiedadTx.class);
        component.addTransactionGET("listar_tipo_terreno",ListarTipoTerrenoTx.class);
        component.addTransactionGET("listar_catalogo_direccion",ListarCatalogoDireccionTx.class);
        component.addTransactionGET("listar_unidades_metricas",ListarUnidadesMetricasTx.class);
        component.addTransactionPUT("editar_control_patrimonial",EditarControlPatrimonialTx.class);
        
        return component;
        
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
