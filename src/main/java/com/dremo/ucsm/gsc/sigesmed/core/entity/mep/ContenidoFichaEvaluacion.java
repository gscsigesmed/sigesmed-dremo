package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;
// Generated 15/07/2016 01:43:01 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * @author geank
 */
@Entity
@Table(name="contenido_ficha_evaluacion",schema="pedagogico")
@DynamicUpdate(value=true)
public class ContenidoFichaEvaluacion  implements java.io.Serializable, ElementoFicha{

    @Id 
    @Column(name="con_fic_eva_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_contenido_ficha_evaluacion", sequenceName="pedagogico.contenido_ficha_evaluacion_con_fic_eva_id_seq" )
    @GeneratedValue(generator="secuencia_contenido_ficha_evaluacion")
    private int conFicEvaId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fic_eva_per_id")
    @Fetch(FetchMode.JOIN)
    private FichaEvaluacionPersonal fichaEvaluacionPersonal;

    @ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name="par_cont_id")
    private ContenidoFichaEvaluacion parCont;

    @Column(name="tip", nullable=false, length=1)
    private char tip;
    
    @Column(name="nom", nullable=false)
    private String nom;
    
    @Column(name="usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    
    @Column(name="est_reg", length=1)
    private Character estReg;
    
    @OneToMany(fetch = FetchType.LAZY,mappedBy="contenidoFichaEvaluacion",cascade=CascadeType.ALL)
    //@Fetch(FetchMode.JOIN)
    private Set<IndicadorFichaEvaluacion> indicadorFichaEvaluacions = new HashSet(0);

    @OneToMany(fetch = FetchType.LAZY,mappedBy="parCont",cascade=CascadeType.ALL)
    //@Fetch(FetchMode.JOIN)
    private Set<ContenidoFichaEvaluacion> hijosContenido = new HashSet(0);

    public ContenidoFichaEvaluacion() {
    }
    public ContenidoFichaEvaluacion(int conFicEvaId, char tip, String nom) {
        this.conFicEvaId = conFicEvaId;
        this.tip = tip;
        this.nom = nom;
    }
	
    public ContenidoFichaEvaluacion(char tip, String nom) {
        this.tip = tip;
        this.nom = nom;
    }
    public ContenidoFichaEvaluacion(char tip, String nom,Date fecMod,char estReg,FichaEvaluacionPersonal fev) {
        this.tip = tip;
        this.nom = nom;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.fichaEvaluacionPersonal = fev;
    }
    public ContenidoFichaEvaluacion(int conFicEvaId, FichaEvaluacionPersonal fichaEvaluacionPersonal, char tip, String nom, Integer usuMod, Date fecMod, Character estReg, Set indicadorFichaEvaluacions) {
       this.conFicEvaId = conFicEvaId;
       this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
       this.tip = tip;
       this.nom = nom;
       this.usuMod = usuMod;
       this.fecMod = fecMod;
       this.estReg = estReg;
       this.indicadorFichaEvaluacions = indicadorFichaEvaluacions;
    }
   
    
    public int getConFicEvaId() {
        return this.conFicEvaId;
    }
    
    public void setConFicEvaId(int conFicEvaId) {
        this.conFicEvaId = conFicEvaId;
    }
    
    @Override
    public FichaEvaluacionPersonal getFichaEvaluacionPersonal() {
        return this.fichaEvaluacionPersonal;
    }
    
    @Override
    public void setFichaEvaluacionPersonal(FichaEvaluacionPersonal fichaEvaluacionPersonal) {
        this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
    }

    public char getTip() {
        return this.tip;
    }
    
    public void setTip(char tip) {
        this.tip = tip;
    }

    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return this.fecMod;
    }

    @Override
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return this.estReg;
    }
    
    @Override
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public ElementoFicha copyFromOther(ElementoFicha elemento) {
        if(elemento != null && elemento instanceof ContenidoFichaEvaluacion){
            ContenidoFichaEvaluacion cont= (ContenidoFichaEvaluacion)elemento;
            this.setNom(cont.getNom());
        }
        return this;
    }

    public Set<IndicadorFichaEvaluacion> getIndicadorFichaEvaluacions() {
        return this.indicadorFichaEvaluacions;
    }
    
    public void setIndicadorFichaEvaluacions(Set<IndicadorFichaEvaluacion> indicadorFichaEvaluacions) {
        this.indicadorFichaEvaluacions = indicadorFichaEvaluacions;
    }

    public ContenidoFichaEvaluacion getParCont() {
        return parCont;
    }

    public void setParCont(ContenidoFichaEvaluacion parCont) {
        this.parCont = parCont;
    }

    public Set<ContenidoFichaEvaluacion> getHijosContenido() {
        return hijosContenido;
    }

    public void setHijosContenido(Set<ContenidoFichaEvaluacion> hijosContenido) {
        this.hijosContenido = hijosContenido;
    }
    public StringBuilder indicadoresToString(){
        StringBuilder buffer = new StringBuilder();
        buffer.append("[");
        Iterator<IndicadorFichaEvaluacion> iterator= getIndicadorFichaEvaluacions().iterator();
        while(iterator.hasNext()){
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()){
                buffer.append(",");
            }
        }
        buffer.append("]");
        return buffer;
    }
    public StringBuilder hijosToString(){
        StringBuilder buffer = new StringBuilder();
        buffer.append("[");
        Iterator<ContenidoFichaEvaluacion> ihijos = getHijosContenido().iterator();
        while(ihijos.hasNext()){
            buffer.append(ihijos.next().toString());
            if(ihijos.hasNext()){
                buffer.append(",");
            }
        }
        buffer.append("]");
        return buffer;
    }
    @Override
    public String toString(){
        if(getHijosContenido() != null && getHijosContenido().size() > 0 ){//dominio
            return "{\"id\":" + conFicEvaId+",\"nom\":\""+nom +
                    "\",\"tip\":\"" + tip + "\",\"cont\":" + hijosContenido + "}";
        }
        return "{\"id\":" + conFicEvaId+",\"nom\":\""+nom +
                "\",\"tip\":\"" + tip + "\",\"indicadores\":" + indicadoresToString() + "}";
    }

    @Override
    public boolean equals(Object o){
        if(this == o)return true;
        else if(o!= null && o instanceof ContenidoFichaEvaluacion){
            ContenidoFichaEvaluacion con = (ContenidoFichaEvaluacion)o;
            if(con.getConFicEvaId() == this.getConFicEvaId() && con.getNom().equals(this.getNom()) && this.getTip() == con.getTip()) return true;
            else return false;
        }
        else{
            return false;
        }
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (String.valueOf(conFicEvaId) == null ? 0 : String.valueOf(conFicEvaId).hashCode());
        return result;
    }
}


