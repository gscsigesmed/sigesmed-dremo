/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ObjetivosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Objetivos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarObjetivosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        JSONObject requestData = (JSONObject)wr.getData();
        int iteide = requestData.getInt("iteide");
        String tipo = requestData.getString("tipo");
      
        List<Objetivos> objetivos = null;
        ObjetivosDao objetivosDao = (ObjetivosDao)FactoryDao.buildDao("smdg.ObjetivosDao");
        
        try{
            objetivos = objetivosDao.listar(iteide, tipo.charAt(0));
            
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar los objetivos ", e.getMessage() );
        }
        //Fin

        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Objetivos i:objetivos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("objide",i.getObjId());
            oResponse.put("objdes",i.getObjDes());
            oResponse.put("objava",i.getObjAva());
            oResponse.put("objfec",i.getObjFec());
            oResponse.put("objdoc",i.getObjDoc());//nombre del doc. adjunto del objetivo
            oResponse.put("objusu",i.getUsuId());//usuario responsable del objetivo
            oResponse.put("objtip",i.getObjCua());//cualitativo o cuantitativo            
            
            if(new String(i.getObjCua()).equals("c")){
                oResponse.put("odeide",i.getDetalle().getOdeId());
                oResponse.put("indicador",i.getDetalle().getOdeInd());
                oResponse.put("meta",i.getDetalle().getOdeMet());
                oResponse.put("odeava",i.getDetalle().getOdeAva());
            }
                    
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }
}
