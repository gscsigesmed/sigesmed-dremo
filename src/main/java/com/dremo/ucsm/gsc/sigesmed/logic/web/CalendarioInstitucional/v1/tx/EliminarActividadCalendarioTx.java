package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EliminarActividadCalendarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EliminarActividadCalendarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
            ActividadCalendario activity = actividadDao.buscarActividadConPadre(data.getInt("cod"));
            int actCod;

            if (activity == null) {
                activity = actividadDao.buscarActividad(data.getInt("cod"));
                actCod = activity.getActCalId();
            } else {
                actCod = activity.getActividadPadre().getActCalId();
            }

            actividadDao.delete(activity);

            if (activity.getActividadPadre() != null) {
                ActividadCalendario father = activity.getActividadPadre();
                actividadDao.delete(father);
            }

            if (activity.getTipAct() != 'P') {
                remove(actCod);
            }

            return WebResponse.crearWebResponseExito("La actividad fue eliminada correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarActividad", e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la actividad", WebResponse.BAD_RESPONSE);
        }
    }

    private void remove(int actCod) {
        ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
        List<ActividadCalendario> activities = actividadDao.buscarActividades(actCod);

        for (ActividadCalendario activity : activities) {
            if (activity.getEstReg() == 'P') {
                remove(activity.getActCalId());
            }

            actividadDao.delete(activity);
        }
    }
}
