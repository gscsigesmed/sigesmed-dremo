package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
@Entity
@Table(name = "competencia_capacidad", schema = "pedagogico")
public class CompetenciaCapacidad implements java.io.Serializable{
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "com_id", nullable = false)
        protected int comId;

        @Column(name = "cap_id", nullable = false)
        protected int capId;

        public Id() {
        }

        public Id(int comId, int capId) {
            this.comId = comId;
            this.capId = capId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (comId != id.comId) return false;
            return capId == id.capId;

        }

        @Override
        public int hashCode() {
            int result = comId;
            result = 31 * result + capId;
            return result;
        }
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "comId",column = @Column(name = "com_id", nullable = false)),
            @AttributeOverride(name = "capId",column = @Column(name = "cap_id", nullable = false))
    })
    private Id id = new Id();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "com_id", insertable = false, updatable = false)
    private CompetenciaAprendizaje com;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cap_id", insertable = false, updatable = false)
    private CapacidadAprendizaje cap;

    @OneToMany(mappedBy = "competenciaCapacidad",fetch = FetchType.LAZY)
    private List<CompetenciasUnidadDidactica> competeciasUnidad = new ArrayList<>();
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public CompetenciaCapacidad() {
    }

    public CompetenciaCapacidad(CompetenciaAprendizaje com, CapacidadAprendizaje cap) {

        this.id.comId = com.getComId();
        this.id.capId = cap.getCapId();

        this.com = com;
        this.cap = cap;

        this.estReg = 'A';
        this.fecMod = new Date();

    }

    public CompetenciaAprendizaje getCom() {
        return com;
    }

    public CapacidadAprendizaje getCap() {
        return cap;
    }

    public List<CompetenciasUnidadDidactica> getCompeteciasUnidad() {
        return competeciasUnidad;
    }

    public void setCompeteciasUnidad(List<CompetenciasUnidadDidactica> competeciasUnidad) {
        this.competeciasUnidad = competeciasUnidad;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
