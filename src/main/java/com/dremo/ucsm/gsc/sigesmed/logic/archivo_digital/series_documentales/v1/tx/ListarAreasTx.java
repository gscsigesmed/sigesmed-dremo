/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public class ListarAreasTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr){
        try{
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipos de Area",e.getMessage());
            
        }
        
        // OPERACIONES CON LA BASE DE DATOS
        List<Area> areas = null;
        AreaDao areaDAO = (AreaDao)FactoryDao.buildDao("AreaDao");
        try{
            areas = areaDAO.buscarConOrganizacionYAreaPadre();
        } catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar tipos de Area ", e.getMessage() );
        }
        
        JSONArray miArray = new JSONArray();
        for(Area area : areas){
            JSONObject oResponse = new JSONObject();
            oResponse.put("areaID" , area.getAreId());
            oResponse.put("nombre_area", area.getNom());
            oResponse.put("abr" , area.getCod());
            
            miArray.put(oResponse);
        }
        return WebResponse.crearWebResponseExito("Se listo las Areas Correctamente",miArray);
        
    }
    
}
