package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSessionPersona;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.bandeja_evaluacion_escolar" )
public class BandejaEvaluacion  implements java.io.Serializable {

    @Id
    @Column(name="ban_eva_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_bandejaevaluacionescolar", sequenceName="pedagogico.bandeja_evaluacion_escolar_ban_eva_esc_id_seq" )
    @GeneratedValue(generator="secuencia_bandejaevaluacionescolar")
    private int banEvaId;
    
    @Column(name="not1")
    private int nota;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ent", nullable=false, length=29)
    private Date fecEnt;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_vis", nullable=false, length=29)
    private Date fecVis;    
    
    @Column(name="eva_esc_id")
    private int evaEscId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="eva_esc_id",insertable = false,updatable = false)
    private EvaluacionEscolar evaluacionEscolar;
    
    @Column(name="est_eva")
    private char estado;
    
    @Column(name="per_id")
    private Integer alumnoID;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="per_id",insertable = false,updatable = false)
    private Persona alumno;
    
    @OneToMany(mappedBy="bandejaEvaluacion",cascade=CascadeType.ALL)
    private List<RespuestaEvaluacion> respuestas;
    

    public BandejaEvaluacion() {
    }
    public BandejaEvaluacion(int banEvaId) {
        this.banEvaId = banEvaId;
    }
    public BandejaEvaluacion(int banEvaId, Date fecEnt,Date fecVis, int nota,char estado,int evaEscId,int alumnoID) {
       this.banEvaId = banEvaId;
       this.fecVis = fecVis;
       this.fecEnt = fecEnt;
       this.nota = nota;
       this.estado = estado;
       this.evaEscId = evaEscId;
       this.alumnoID = alumnoID;
    }
   
     
    public int getBanEvaId() {
        return this.banEvaId;
    }    
    public void setBanEvaId(int banEvaId) {
        this.banEvaId = banEvaId;
    }
    
    public Date getFecVis() {
        return this.fecVis;
    }
    public void setFecVis(Date fecVis) {
        this.fecVis = fecVis;
    }
    
    public Date getFecEnt() {
        return this.fecEnt;
    }
    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }
    
    public int getNota() {
        return this.nota;
    }
    public void setNota(int nota) {
        this.nota = nota;
    }
    
    public int getEvaEscId() {
        return this.evaEscId;
    }    
    public void setEvaEscId(int evaEscId) {
        this.evaEscId = evaEscId;
    }
    
    public EvaluacionEscolar getEvaluacionEscolar() {
        return this.evaluacionEscolar;
    }
    public void setEvaluacionEscolar(EvaluacionEscolar evaluacionEscolar) {
        this.evaluacionEscolar = evaluacionEscolar;
    }
    
    public char getEstado(){
        return this.estado;
    }
    public void setEstado(char estado){
        this.estado = estado;
    }
    
    public int getAlumnoId() {
        return this.alumnoID;
    }    
    public void setAlumnoId(int alumnoID) {
        this.alumnoID = alumnoID;
    }
    
    public Persona getAlumno() {
        return this.alumno;
    }    
    public void setAlumno(Persona alumno) {
        this.alumno = alumno;
    }
    
    public List<RespuestaEvaluacion> getRespuestas(){
        return this.respuestas;
    }
    public void setRespuestas(List<RespuestaEvaluacion> respuestas){
        this.respuestas = respuestas;
    }
}


