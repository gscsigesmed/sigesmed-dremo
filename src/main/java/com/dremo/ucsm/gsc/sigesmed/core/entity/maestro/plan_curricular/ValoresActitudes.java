package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 24/10/2016.
 */
@Entity
@Table(name = "valores_actitudes", schema = "pedagogico")
public class ValoresActitudes implements java.io.Serializable {
    @Id
    @Column(name = "val_act_id")
    @SequenceGenerator(name = "valores_actitudes_val_act_id_seq", sequenceName = "pedagogico.valores_actitudes_val_act_id_seq")
    @GeneratedValue(generator = "valores_actitudes_val_act_id_seq")
    private int valActId;
    @Column(name = "val", length = 30)
    private String val;
    @Column(name = "cont", length = 256)
    private String cont;
    @Column(name = "sit_sig")
    private String sitSig;
    @ManyToMany(mappedBy = "valores")
    List<ProgramacionAnual> programaciones = new ArrayList<>();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;


    public ValoresActitudes() {
    }

    public ValoresActitudes(String val, String cont, String sitSig) {
        this.val = val;
        this.cont = cont;
        this.sitSig = sitSig;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getValActId() {
        return valActId;
    }

    public void setValActId(int valActId) {
        this.valActId = valActId;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getCont() {
        return cont;
    }

    public void setCont(String cont) {
        this.cont = cont;
    }

    public String getSitSig() {
        return sitSig;
    }

    public void setSitSig(String sitSig) {
        this.sitSig = sitSig;
    }

    public List<ProgramacionAnual> getProgramaciones() {
        return programaciones;
    }

    public void setProgramaciones(List<ProgramacionAnual> programaciones) {
        this.programaciones = programaciones;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValoresActitudes)) return false;

        ValoresActitudes that = (ValoresActitudes) o;

        return valActId == that.valActId;

    }

    @Override
    public int hashCode() {
        int result = valActId;
        result = 31 * result + val.hashCode();
        result = 31 * result + cont.hashCode();
        result = 31 * result + sitSig.hashCode();
        return result;
    }
}
