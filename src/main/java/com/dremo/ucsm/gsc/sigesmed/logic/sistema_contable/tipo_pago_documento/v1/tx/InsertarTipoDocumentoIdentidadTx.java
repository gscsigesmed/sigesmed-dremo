/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.TipoDocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoDocumentoIdentidad;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class InsertarTipoDocumentoIdentidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoDocumentoIdentidad tipoDocumento = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            String nombre = requestData.getString("nombre");
        
            String estado = requestData.getString("estado");
            
            tipoDocumento = new TipoDocumentoIdentidad((short)0, nombre, new Date(),wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos" );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoDocumentoDao tipoDocumentoDao = (TipoDocumentoDao)FactoryDao.buildDao("sci.TipoDocumentoDao");
        try{
            tipoDocumentoDao.insert(tipoDocumento);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el tipo de documento\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el tipo de documento", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("tipoPagoID",tipoDocumento.getTipDocIdeId());
        oResponse.put("fecha",tipoDocumento.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de Tipo Documento se realizo correctamente", oResponse);
        //Fin
    }
    
}
