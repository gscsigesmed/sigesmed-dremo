/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.ver_documento.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.ver_documento.v1.tx.ListarDocumentosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.ver_documento.v1.tx.TraerContenidoDocumentoTx;



/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DOCUMENTOS_COMUNICACION);        
        
        //Registrnado el Nombre del componente
        component.setName("ver_documento");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
//        component.addTransactionPOST("editarPlantilla", EditarPlantillaTx.class);
        component.addTransactionGET("listarDocumentos", ListarDocumentosTx.class);
        component.addTransactionGET("traerContenidoDocumento", TraerContenidoDocumentoTx.class);
//        component.addTransactionPUT("registrarPlantilla", RegistrarPlantillaTx.class);
//        component.addTransactionPUT("eliminarPlantilla", EliminarPlantillaTx.class);
//        component.addTransactionDELETE("eliminarTipoTramite", EliminarTipoTramiteTx.class);
      
        
        return component;
    }
}
