package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;

import java.util.List;
import org.json.JSONArray;

public interface CursoCapacitacionDao extends GenericDao<CursoCapacitacion> {
    List<CursoCapacitacion> listarCapacitaciones();
    List<CursoCapacitacion> listarCapacitacionPorOrganizacion(int idOrg);
    List<CursoCapacitacion> listarCapacitacionPorOrganizacionYAno(int idOrg, int ano);
    List<CursoCapacitacion> listarCapacitacionPorTipo(String tipo);
    CursoCapacitacion buscarPorId(int idCur);
    List<Organizacion> listarOrganizaciones();    
    JSONArray listarCapacitacionesCapacitador(int usuCod);
    JSONArray listarCapacitacionesPersona(int usuCod);
    List<CursoCapacitacion> listarReportes();
    List<AsistenciaParticipante> asistenciaDocente(int docCod);
    List<SedeCapacitacion> listarDisponibles(int perId);
    CursoCapacitacion buscarDetalle(int curCod);
}
