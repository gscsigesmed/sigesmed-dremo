package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="asistencia_aula" ,schema="administrativo")
public class AsistenciaAula  implements java.io.Serializable {


    @Id
    @Column(name="asi_au_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_asistencia_aula", sequenceName="administrativo.asistencia_aula_asi_au_id_seq" )
    @GeneratedValue(generator="secuencia_asistencia_aula")
    private Long asistenciaAulaId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_ing")
    private Date horaIngreso;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_sal")
    private Date horaSalida;
    
    @Column(name="est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="doc_id", nullable=false )
    private Docente docente;    
    
    @Column(name="usu_mod")
    private Integer usuario;
    
    @Column(name="asi_au_hrs")
    private Integer horasPedagogicas;
    
    @Column(name="asi_au_rel_hrs")
    private String relacionHoras;
    
    public AsistenciaAula() {
    }

    public AsistenciaAula(Long asistenciaAulaId) {
        this.asistenciaAulaId = asistenciaAulaId;
    }

    public AsistenciaAula(Date horaIngreso, Date horaSalida, String estReg, Docente docente,Integer usuario, Integer horasPedagogicas,String relacionHoras) {
        this.horaIngreso = horaIngreso;
        this.horaSalida = horaSalida;
        this.estReg = estReg;
        this.docente = docente;
        this.usuario = usuario;
        this.horasPedagogicas = horasPedagogicas ;
        this.relacionHoras=relacionHoras;
    }
    
    public AsistenciaAula(Date horaIngreso, Date horaSalida, String estReg, Docente docente,Integer usuario) {
        this.horaIngreso = horaIngreso;
        this.horaSalida = horaSalida;
        this.estReg = estReg;
        this.docente = docente;
        this.usuario = usuario;
        
        
    }
    
    public Long getAsistenciaAulaId() {
        return asistenciaAulaId;
    }

    public void setAsistenciaAulaId(Long asistenciaAulaId) {
        this.asistenciaAulaId = asistenciaAulaId;
    }

    public Date getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(Date horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Integer getHorasPedagogicas() {
        return horasPedagogicas;
    }

    public void setHorasPedagogicas(Integer horasPedagogicas) {
        this.horasPedagogicas = horasPedagogicas;
    }

    public String getRelacionHoras() {
        return relacionHoras;
    }

    public void setRelacionHoras(String relacionHoras) {
        this.relacionHoras = relacionHoras;
    }

   

}


