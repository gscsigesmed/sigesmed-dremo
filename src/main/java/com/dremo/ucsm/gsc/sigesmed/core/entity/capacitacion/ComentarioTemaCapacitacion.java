package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comentario_tema_capacitacion", schema = "pedagogico")
public class ComentarioTemaCapacitacion implements Serializable {

    @Id
    @Column(name = "com_tem_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_comentario_tema_capacitacion", sequenceName = "pedagogico.comentario_tema_capacitacion_com_tem_cap_id_seq")
    @GeneratedValue(generator = "secuencia_comentario_tema_capacitacion")
    private int comTemCapId;

    @Column(name = "tem_cur_cap_id", nullable = false)
    private int temCurCapId;

    @Column(name = "sed_cap_id", nullable = false)
    private int sedCapId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id")
    private Persona persona;

    @Column(name = "com", nullable = false)
    private String com;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec", nullable = false)
    private Date fec;

    @Column(name = "usu_mod")
    private int usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "comentarioTema", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<AdjuntoComentarioTema> adjuntos = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_res_id")
    private ComentarioTemaCapacitacion comentarioRes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(updatable = false, insertable = false, name = "tem_cur_cap_id"),
        @JoinColumn(updatable = false, insertable = false, name = "sed_cap_id")})
    private TemarioCursoCapacitacion tema;

    public ComentarioTemaCapacitacion() {
    }

    public ComentarioTemaCapacitacion(int temCurCapId, int sedCapId, int docId, String com, int usuMod, Date fecMod, Character estReg) {
        this.temCurCapId = temCurCapId;
        this.sedCapId = sedCapId;
        this.persona = new Persona();
        this.persona.setPerId(docId);
        this.com = com;
        this.fec = new Date();
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public int getComTemCapId() {
        return comTemCapId;
    }

    public void setComTemCapId(int comTemCapId) {
        this.comTemCapId = comTemCapId;
    }

    public int getTemCurCapId() {
        return temCurCapId;
    }

    public void setTemCurCapId(int temCurCapId) {
        this.temCurCapId = temCurCapId;
    }

    public int getSedCapId() {
        return sedCapId;
    }

    public void setSedCapId(int sedCapId) {
        this.sedCapId = sedCapId;
    }

    public int getPerId() {
        return persona.getPerId();
    }

    public void setPerId(int docId) {
        this.persona.setPerId(docId);
    }

    public List<AdjuntoComentarioTema> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<AdjuntoComentarioTema> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public ComentarioTemaCapacitacion getComentarioRes() {
        return comentarioRes;
    }

    public void setComentarioRes(ComentarioTemaCapacitacion comentarioRes) {
        this.comentarioRes = comentarioRes;
    }

    public String getCom() {
        return com;
    }

    public void setCom(String com) {
        this.com = com;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public TemarioCursoCapacitacion getTema() {
        return tema;
    }

    public void setTema(TemarioCursoCapacitacion tema) {
        this.tema = tema;
    }
}
