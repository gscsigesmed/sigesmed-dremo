/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarAscensosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarAscensosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Ascenso> ascensos = null;
        AscensoDao ascensoDao = (AscensoDao)FactoryDao.buildDao("se.AscensoDao");
        
        try{
            ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar ascensos",e);
            System.out.println("No se pudo listar los ascensos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los ascensos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Ascenso a: ascensos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("ascId", a.getAscId());
            oResponse.put("numRes", a.getNumRes());
            oResponse.put("fecRes", a.getFecRes());
            oResponse.put("fecEfe", a.getFecEfe());
            oResponse.put("esc", a.getEsc());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los ascensos fueron listados exitosamente", miArray);
    }
    
}
