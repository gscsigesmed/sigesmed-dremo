package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.RespuestaEncuesta;
import java.util.List;

public interface RespuestaEncuestaDao extends GenericDao<RespuestaEncuesta> {
    List<RespuestaEncuesta> buscarPorPregunta(int codPre);
}
