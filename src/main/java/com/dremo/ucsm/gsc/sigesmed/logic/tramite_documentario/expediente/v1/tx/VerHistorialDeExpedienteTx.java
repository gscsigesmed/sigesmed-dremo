/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class VerHistorialDeExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int expedienteID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            expedienteID = requestData.getInt("expedienteID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial del expediente, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<HistorialExpediente> historial = null;
        //List<RutaTramite> rutas = null;
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            historial =historialDao.buscarPorExpediente(expedienteID);
            //rutas =historialDao.buscarRutas(expediente);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial ", e.getMessage() );
        }
        //Fin
        JSONObject oResponse = new JSONObject();
        
        JSONArray aHistoriales = new JSONArray();
        for(HistorialExpediente h: historial){
            JSONObject oHistorial = new JSONObject();
            oHistorial.put("historialID",h.getHisExpId());
            oHistorial.put("observacion",h.getObservacion() );
            oHistorial.put("areaID",h.getAreaId());
            oHistorial.put("area",h.getArea().getNom());
            
            oHistorial.put("estadoID",h.getEstado().getEstExpId());
            oHistorial.put("estado",h.getEstado().getNom());
            
            oHistorial.put("responsableID",h.getResId());
            oHistorial.put("responsable",h.getResponsable().getNombrePersona());
            aHistoriales.put(oHistorial);
        }
        oResponse.put("historial",aHistoriales);
        /*
        JSONArray aRutas = new JSONArray();
        //si es tupa llamamos a los tipos de area
        if(expediente.getEsTup())
            for(RutaTramite rt:rutas){
                JSONObject oRuta = new JSONObject();
                oRuta.put("rutaID",rt.getRutTraId() );
                oRuta.put("descripcion",rt.getDes() );

                oRuta.put("areaOriID",rt.getTipoAreaOrigen().getTipAreId());
                JSONObject areaOri = new JSONObject();                    
                areaOri.put("nombre",rt.getTipoAreaOrigen().getNom());                    
                oRuta.put("areaDesID",rt.getTipoAreaDestino().getTipAreId());
                JSONObject areaDes = new JSONObject();                    
                areaDes.put("nombre",rt.getTipoAreaDestino().getNom());

                oRuta.put("areaOri",areaOri );
                oRuta.put("areaDes",areaDes );

                aRutas.put(oRuta);
            }
        //si no lo es llamamos a las areas de la orgnizacion
        else
            for(RutaTramite rt:rutas){
                JSONObject oRuta = new JSONObject();
                oRuta.put("rutaID",rt.getRutTraId() );
                oRuta.put("descripcion",rt.getDes() );

                JSONObject areaOri = new JSONObject();
                areaOri.put("areaOriID",rt.getAreaOrigen().getAreId());
                areaOri.put("nombre",rt.getAreaOrigen().getNom());                    
                JSONObject areaDes = new JSONObject();
                areaDes.put("areaDesID",rt.getAreaDestino().getAreId());
                areaDes.put("nombre",rt.getAreaDestino().getNom());

                oRuta.put("areaOri",areaOri );
                oRuta.put("areaDes",areaDes );

                aRutas.put(oRuta);
            }
        
        
        oResponse.put("rutas",aRutas);*/
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);
    }
    
}

