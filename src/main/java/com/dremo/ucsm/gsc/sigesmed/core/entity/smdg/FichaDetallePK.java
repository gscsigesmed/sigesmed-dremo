/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Administrador
 */

public class FichaDetallePK implements Serializable {
        
    @Column(name = "fev_doc_id")
    private int fevDocId;
    
    @Column(name = "pin_id")
    private int pinId;

    public FichaDetallePK() {
    }

    public FichaDetallePK(int fevDocId, int pinId) {
        this.fevDocId = fevDocId;
        this.pinId = pinId;
    }

    public int getFevDocId() {
        return fevDocId;
    }

    public void setFevDocId(int fevDocId) {
        this.fevDocId = fevDocId;
    }

    public int getPinId() {
        return pinId;
    }

    public void setPinId(int pinId) {
        this.pinId = pinId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) fevDocId;
        hash += (int) pinId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaDetallePK)) {
            return false;
        }
        FichaDetallePK other = (FichaDetallePK) object;
        if (this.fevDocId != other.fevDocId) {
            return false;
        }
        if (this.pinId != other.pinId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.FichaDetallePK[ fevDocId=" + fevDocId + ", pinId=" + pinId + " ]";
    }
    
}
