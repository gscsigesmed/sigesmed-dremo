/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoItemFile")
@Table(name = "tipo_item_file", schema="institucional")

public class TipoItemFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tif_ide")
    private Integer tifIde;
    @JoinColumn(name = "tif_tip_ext", referencedColumnName = "tex_ide")
    @ManyToOne
    private TipoExtension tifTipExt;
    @JoinColumn(name = "tif_tes_ide", referencedColumnName = "tes_ide")
    @ManyToOne
    private TipoEspecializado tifTesIde;
    @OneToMany(mappedBy = "iteTifIde")
    private Collection<ItemFile> itemFileCollection;

    public TipoItemFile() {
    }

    public TipoItemFile(Integer tifIde) {
        this.tifIde = tifIde;
    }

    public Integer getTifIde() {
        return tifIde;
    }

    public void setTifIde(Integer tifIde) {
        this.tifIde = tifIde;
    }

    public TipoExtension getTifTipExt() {
        return tifTipExt;
    }

    public void setTifTipExt(TipoExtension tifTipExt) {
        this.tifTipExt = tifTipExt;
    }

    public TipoEspecializado getTifTesIde() {
        return tifTesIde;
    }

    public void setTifTesIde(TipoEspecializado tifTesIde) {
        this.tifTesIde = tifTesIde;
    }

    @XmlTransient
    public Collection<ItemFile> getItemFileCollection() {
        return itemFileCollection;
    }

    public void setItemFileCollection(Collection<ItemFile> itemFileCollection) {
        this.itemFileCollection = itemFileCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tifIde != null ? tifIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoItemFile)) {
            return false;
        }
        TipoItemFile other = (TipoItemFile) object;
        if ((this.tifIde == null && other.tifIde != null) || (this.tifIde != null && !this.tifIde.equals(other.tifIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.TipoItemFile[ tifIde=" + tifIde + " ]";
    }
    
}
