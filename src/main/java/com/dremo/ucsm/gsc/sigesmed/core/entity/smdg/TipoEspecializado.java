/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoEspecializado")
@Table(name = "tipo_especializado", schema="institucional")
public class TipoEspecializado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tes_ide")
    private Short tesIde;
    @Basic(optional = false)
    @Column(name = "tes_ali")
    private String tesAli;
    @Column(name = "tes_nom")
    private String tesNom;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
    @Column(name = "tes_des")
    private String tesDes;
//    @OneToMany(mappedBy = "tifTesIde")
//    private Collection<TipoItemFile> tipoItemFileCollection;

    public TipoEspecializado() {
    }

    public TipoEspecializado(Short tesIde) {
        this.tesIde = tesIde;
    }

    public TipoEspecializado(Short tesIde, String tesAli) {
        this.tesIde = tesIde;
        this.tesAli = tesAli;
    }

    public Short getTesIde() {
        return tesIde;
    }

    public void setTesIde(Short tesIde) {
        this.tesIde = tesIde;
    }

    public String getTesAli() {
        return tesAli;
    }

    public void setTesAli(String tesAli) {
        this.tesAli = tesAli;
    }

    public String getTesNom() {
        return tesNom;
    }

    public void setTesNom(String tesNom) {
        this.tesNom = tesNom;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getTesDes() {
        return tesDes;
    }

    public void setTesDes(String tesDes) {
        this.tesDes = tesDes;
    }

//    @XmlTransient
//    public Collection<TipoItemFile> getTipoItemFileCollection() {
//        return tipoItemFileCollection;
//    }
//
//    public void setTipoItemFileCollection(Collection<TipoItemFile> tipoItemFileCollection) {
//        this.tipoItemFileCollection = tipoItemFileCollection;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tesIde != null ? tesIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEspecializado)) {
            return false;
        }
        TipoEspecializado other = (TipoEspecializado) object;
        if ((this.tesIde == null && other.tesIde != null) || (this.tesIde != null && !this.tesIde.equals(other.tesIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.TipoEspecializado[ tesIde=" + tesIde + " ]";
    }
    
}
