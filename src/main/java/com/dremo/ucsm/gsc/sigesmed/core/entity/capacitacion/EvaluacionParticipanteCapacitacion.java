package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "evaluaciones_participantes_capacitacion", schema = "pedagogico")
public class EvaluacionParticipanteCapacitacion implements Serializable {
    @Id
    @Column(name = "eva_par_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_evaluaciones_participantes_capacitacion", sequenceName = "pedagogico.evaluaciones_participantes_capacitacion_eva_par_cap_id_seq")
    @GeneratedValue(generator = "secuencia_evaluaciones_participantes_capacitacion")
    private int evaParCapId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_apl")
    private Date fecApl;
    
    @Column(name = "not_opt")
    private double notObt;
    
    @Column(name = "sed_cap_id")
    private int sedCapId;
    
    @Column(name = "eva_cur_cap_id")
    private int evaCurCapId;
    
    @Column(name = "per_id")
    private int perId;
        
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "eva_cur_cap_id", referencedColumnName = "eva_cur_cap_id", updatable = false, insertable = false),
        @JoinColumn(name = "sed_cap_id",  referencedColumnName = "sed_cap_id" , updatable = false, insertable = false)
    })
    private EvaluacionCursoCapacitacion evaluacionCursoCapacitacion;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id", updatable = false, insertable = false)
    private Persona persona;
    
    @Column(name = "est_reg", nullable = false)
    private Character estReg;
    
    public EvaluacionParticipanteCapacitacion() {}

    public EvaluacionParticipanteCapacitacion(Date fecApl, double notObt, int docId, Character estReg, EvaluacionCursoCapacitacion evaluacionCursoCapacitacion) {
        this.fecApl = fecApl;
        this.notObt = notObt;
        this.perId = docId;
        this.estReg = estReg;
        this.evaCurCapId = evaluacionCursoCapacitacion.getEvaCurCapId();
        this.sedCapId = evaluacionCursoCapacitacion.getSede().getSedCapId();
    }

    public int getEvaParCapId() {
        return evaParCapId;
    }

    public void setEvaParCapId(int evaParCapId) {
        this.evaParCapId = evaParCapId;
    }
    
    public Date getFecApl() {
        return fecApl;
    }

    public void setFecApl(Date fecApl) {
        this.fecApl = fecApl;
    }

    public double getNotObt() {
        return notObt;
    }

    public void setNotObt(double notObt) {
        this.notObt = notObt;
    }

    public int getSedCapId() {
        return sedCapId;
    }

    public void setSedCapId(int sedCapId) {
        this.sedCapId = sedCapId;
    }

    public int getEvaCurCapId() {
        return evaCurCapId;
    }

    public void setEvaCurCapId(int evaCurCapId) {
        this.evaCurCapId = evaCurCapId;
    }

    public EvaluacionCursoCapacitacion getEvaluacionCursoCapacitacion() {
        return evaluacionCursoCapacitacion;
    }

    public void setEvaluacionCursoCapacitacion(EvaluacionCursoCapacitacion evaluacionCursoCapacitacion) {
        this.evaluacionCursoCapacitacion = evaluacionCursoCapacitacion;
        this.evaCurCapId = this.evaluacionCursoCapacitacion.getEvaCurCapId();
        this.sedCapId = this.evaluacionCursoCapacitacion.getSede().getSedCapId();
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public int getPerId() {
        return perId;
    }

    public void setPerId(int perId) {
        this.perId = perId;
    }
}
