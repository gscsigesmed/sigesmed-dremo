/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class DemeritoDaoHibernate extends GenericDaoHibernate<Demerito> implements DemeritoDao{
    private static final Logger logger = Logger.getLogger(DemeritoDaoHibernate.class.getName());
    
    @Override
    public List<Demerito> listarxFichaEscalafonaria(int ficEscId) {
        List<Demerito> demeritos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT dem from Demerito as dem "
                    + "join fetch dem.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND dem.estReg='A'";
            Query query = session.createQuery(hql);
            demeritos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los demeritos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los demeritos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return demeritos;
    }

    @Override
    public Demerito buscarPorId(Integer demId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Demerito dem = (Demerito)session.get(Demerito.class, demId);
        session.close();
        return dem;
    }

    @Override
    public JSONObject contarxOrganizacion(int orgId) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            
            String queryStr = "SELECT COUNT(dem.dem_id)\n" +
                                "  FROM administrativo.demerito AS dem, \n" +
                                "  administrativo.ficha_escalafonaria AS fic, \n" +
                                "  public.trabajador AS tra\n" +
                                "  WHERE dem.fic_esc_id = fic.fic_esc_id AND fic.tra_id = tra.tra_id AND tra.org_id = " + orgId;

            String respuesta = session.createSQLQuery(queryStr).uniqueResult().toString();
            
            JSONObject resultado = new JSONObject();
            resultado.put("totalDemeritos", Integer.parseInt(respuesta));
            System.out.println(resultado);
            return resultado;
            
        } catch (NumberFormatException | JSONException e){
            logger.log(Level.SEVERE,"totalDemeritosPorOrganizacion",e);
            throw e;
        } finally {
            session.close();
        } 
    }
   
    
}
