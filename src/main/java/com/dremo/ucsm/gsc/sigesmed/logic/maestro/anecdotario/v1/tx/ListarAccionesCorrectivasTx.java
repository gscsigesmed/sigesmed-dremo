package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.AccionesCorrectivas;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 23/12/2016.
 */
public class ListarAccionesCorrectivasTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarAccionesCorrectivasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idAnec = data.getInt("id");
        return listarAccionesCorrectivas(idAnec);
    }

    private WebResponse listarAccionesCorrectivas(int idAnec) {
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            List<AccionesCorrectivas> acciones = anecDao.listarAccionesAnecdotario(idAnec);
            JSONArray accionesJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"aneAcCorId","nom"},
                    new String[]{"id","nom"},
                    acciones
            ));
            return WebResponse.crearWebResponseExito("se listo correctamente",accionesJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAccionesCorrectivas",e);
            return WebResponse.crearWebResponseError("no se puede listar las acciones");
        }
    }
}
