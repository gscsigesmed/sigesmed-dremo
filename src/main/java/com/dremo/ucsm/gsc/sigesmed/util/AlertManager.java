/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaAlertaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaAlerta;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public final class AlertManager {
    
    //envio de alerta a un usuario en especifico
    public static boolean send(int alertaID,int usuarioID){
        
        try{
            BandejaAlerta nuevo = new BandejaAlerta(0,new Date(),null,alertaID,usuarioID);
            BandejaAlertaDao bandejaDao = (BandejaAlertaDao)FactoryDao.buildDao("web.BandejaAlertaDao");
            
            bandejaDao.insert(nuevo);
            
        }catch(Exception e){
            System.out.println("nose pudo enviar la alerta al usuario\n"+e);
            return false;
        }
        return true;
    }
    
    //envio de alerta a varios usuarios
    public static boolean sendAll(int alertaID,List<Integer> usuarios){
        
        try{            
            
            BandejaAlertaDao bandejaDao = (BandejaAlertaDao)FactoryDao.buildDao("web.BandejaAlertaDao");
            
            List<BandejaAlerta> varios = new ArrayList<BandejaAlerta>();
            Date fecha = new Date();
            for(Integer usuID: usuarios){
                
                BandejaAlerta nuevo = new BandejaAlerta(0,fecha,null,alertaID,usuID);                
                varios.add(nuevo);
            }
            bandejaDao.insertarVarios(varios);
            
        }catch(Exception e){
            System.out.println("nose pudo enviar la alerta a los usuarios\n"+e);
            return false;
        }
        return true;
    }
    //envio de alerta a varios usuarios
    public static boolean sendAll2(int alertaID,List<BigInteger> usuarios){
        
        try{            
            
            BandejaAlertaDao bandejaDao = (BandejaAlertaDao)FactoryDao.buildDao("web.BandejaAlertaDao");
            
            List<BandejaAlerta> varios = new ArrayList<BandejaAlerta>();
            Date fecha = new Date();
            for(BigInteger usuID: usuarios){
                
                BandejaAlerta nuevo = new BandejaAlerta(0,fecha,null,alertaID,usuID.shortValue());
                varios.add(nuevo);
            }
            bandejaDao.insertarVarios(varios);
            
        }catch(Exception e){
            System.out.println("nose pudo enviar la alerta a los usuarios\n"+e);
            return false;
        }
        return true;
    }
}
