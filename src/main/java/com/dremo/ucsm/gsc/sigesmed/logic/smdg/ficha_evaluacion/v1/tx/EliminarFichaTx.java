/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaEvaluacionDocumentosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaEvaluacionDocumentos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarFichaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int ficid = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ficid = requestData.getInt("ficid");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        FichaEvaluacionDocumentosDao ficDao = (FichaEvaluacionDocumentosDao)FactoryDao.buildDao("smdg.FichaEvaluacionDocumentosDao");
        try{
            ficDao.delete(new FichaEvaluacionDocumentos(ficid));        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la plantilla\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Ficha", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Ficha se elimino correctamente");
        //Fin
    }
}
