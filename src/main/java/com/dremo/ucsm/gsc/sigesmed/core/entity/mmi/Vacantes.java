package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;

public class Vacantes {

    int planNivId;
    int nivId;
    String nivNom;
    String nivNomCom;
    int jorEscId;
    String jorEscNom;
    char turId;
    String turNom;
    int graId;
    String graNom;
    int numSec;
    int numEstxSec;

    public Vacantes() {
    }

    public Vacantes(int planNivId, int nivId, String nivNom, String nivNomCom, int jorEscId, String jorEscNom, char turId, String turNom, int graId, String graNom, int numSec, int numEstxSec) {
        this.planNivId = planNivId;
        this.nivId = nivId;
        this.nivNom = nivNom;
        this.nivNomCom = nivNomCom;
        this.jorEscId = jorEscId;
        this.jorEscNom = jorEscNom;
        this.turId = turId;
        this.turNom = turNom;
        this.graId = graId;
        this.graNom = graNom;
        this.numSec = numSec;
        this.numEstxSec = numEstxSec;
    }

    public int getNivId() {
        return nivId;
    }

    public String getNivNom() {
        return nivNom;
    }

    public int getJorEscId() {
        return jorEscId;
    }

    public String getJorEscNom() {
        return jorEscNom;
    }

    public char getTurId() {
        return turId;
    }

    public String getTurNom() {
        return turNom;
    }

    public int getGraId() {
        return graId;
    }

    public String getGraNom() {
        return graNom;
    }

    public int getNumSec() {
        return numSec;
    }

    public int getNumEstxSec() {
        return numEstxSec;
    }

    public void setNivId(int nivId) {
        this.nivId = nivId;
    }

    public void setNivNom(String nivNom) {
        this.nivNom = nivNom;
    }

    public void setJorEscId(int jorEscId) {
        this.jorEscId = jorEscId;
    }

    public void setJorEscNom(String jorEscNom) {
        this.jorEscNom = jorEscNom;
    }

    public void setTurId(char turId) {
        this.turId = turId;
    }

    public void setTurNom(String turNom) {
        this.turNom = turNom;
    }

    public void setGraId(int graId) {
        this.graId = graId;
    }

    public void setGraNom(String graNom) {
        this.graNom = graNom;
    }

    public void setNumSec(int numSec) {
        this.numSec = numSec;
    }

    public void setNumEstxSec(int numEstxSec) {
        this.numEstxSec = numEstxSec;
    }

    public int getPlanNivId() {
        return planNivId;
    }

    public void setPlanNivId(int planNivId) {
        this.planNivId = planNivId;
    }

    public String getNivNomCom() {
        return nivNomCom;
    }

    public void setNivNomCom(String nivNomCom) {
        this.nivNomCom = nivNomCom;
    }

    @Override
    public String toString() {
        return "Vacantes{" + "planNivId=" + planNivId + ", nivId=" + nivId + ", nivNom=" + nivNom + ", nivNomCom=" + nivNomCom + ", jorEscId=" + jorEscId + ", jorEscNom=" + jorEscNom + ", turId=" + turId + ", turNom=" + turNom + ", graId=" + graId + ", graNom=" + graNom + ", numSec=" + numSec + ", numEstxSec=" + numEstxSec + '}';
    }

}
