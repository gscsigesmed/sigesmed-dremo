package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;

import java.util.List;

/**
 * Created by geank on 28/09/16.
 */
public interface ReunionComisionDao  extends GenericDao<ReunionComision> {
    ReunionComision buscarReunionPorId(int id);
    ReunionComision buscarActasyAsistenciaPorReunion(int id);
    ReunionComision buscarReunionConActas(int id);
    List<ReunionComision> buscarReunionesPorComision(int com);
    List<ReunionComision> buscarReunionesConAsistencias(int com);
}
