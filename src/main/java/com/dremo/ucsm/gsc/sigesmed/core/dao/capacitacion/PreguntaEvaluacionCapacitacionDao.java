package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import java.util.List;

public interface PreguntaEvaluacionCapacitacionDao extends GenericDao<PreguntaEvaluacionCapacitacion> {
    PreguntaEvaluacionCapacitacion buscarPorId(int codPre);
    List<PreguntaEvaluacionCapacitacion> buscarPorEvaluacion(int codEva);
    boolean existenPreguntas(int codEva);
}
