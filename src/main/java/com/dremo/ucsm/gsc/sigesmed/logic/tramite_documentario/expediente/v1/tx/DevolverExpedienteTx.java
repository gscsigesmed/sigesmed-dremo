/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.Expediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class DevolverExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        List<HistorialExpediente> historiales = new ArrayList<HistorialExpediente>();
        List<HistorialExpediente> historialesNuevos = new ArrayList<HistorialExpediente>();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray historialData = requestData.getJSONArray("historiales");
            int areaID = requestData.getInt("areaID");
            int usuarioID = requestData.getInt("usuarioID");
            String observacion = requestData.getString("observacion");
            
            Date hoy = new Date();
            
            for( int i = 0 ; i < historialData.length(); i++){
                JSONObject bo = historialData.getJSONObject(i);
                HistorialExpediente actual = new HistorialExpediente(bo.getInt("historialID"),bo.getInt("expedienteID"));
                actual.setObservacion(observacion);
                actual.setFecAte(hoy);
                actual.setEstadoId(EstadoExpediente.DEVUELTO);
                historiales.add(actual);                
                
                historialesNuevos.add( new HistorialExpediente(bo.getInt("historialID")+1,new Expediente(bo.getInt("expedienteID")),"",EstadoExpediente.RECHAZADO, areaID, usuarioID,hoy ) );
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo devolver el expediente datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            historialDao.actualizarEstadoVarios(historiales,EstadoExpediente.DEVUELTO);
            historialDao.insertarEstadoVarios(historialesNuevos);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo devolver el expediente", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Se devolvio correctamente el expediente al area anterior");
        //Fin
    }
    
}
