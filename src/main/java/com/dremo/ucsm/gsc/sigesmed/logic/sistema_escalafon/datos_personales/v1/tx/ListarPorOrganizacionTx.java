/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 *
 * @author gscadmin
 */
public class ListarPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("orgId");
                
        List<FichaEscalafonaria> fichas = new ArrayList<FichaEscalafonaria>();
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao");
        
        try{
            fichas = fichaEscalafonariaDao.ListarxOrganizacion(orgId);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los datos de escalafon  \n"+e);
            return WebResponse.crearWebResponseError("No se pudo los datpos de escalafon ", e.getMessage() );
        }
        
       
        JSONArray miArray = new JSONArray();
        for(FichaEscalafonaria datosFE:fichas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("ficEscId", datosFE.getFicEscId());
            oResponse.put("perApePat", datosFE.getTrabajador().getPersona().getApePat());
            oResponse.put("perApeMat", datosFE.getTrabajador().getPersona().getApeMat());
            oResponse.put("perNom", datosFE.getTrabajador().getPersona().getNom());
            oResponse.put("perDni", datosFE.getTrabajador().getPersona().getDni());
            oResponse.put("traId", datosFE.getTrabajador().getTraId());
            
            if(datosFE.getTrabajador().getTieServ() != null) 
                oResponse.put("traAnoSer", datosFE.getTrabajador().getTieServ());
            else
                oResponse.put("traAnoSer", "Años de servicio no definidos");

            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);
    }
    
}
