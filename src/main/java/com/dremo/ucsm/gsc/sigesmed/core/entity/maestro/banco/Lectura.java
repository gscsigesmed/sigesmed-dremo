package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity
@Table(name = "lecturas",schema = "pedagogico")
public class Lectura implements java.io.Serializable{
    @Id
    @Column(name = "lec_id",nullable = false,unique = true)
    @SequenceGenerator(name = "lecturas_lec_id_seq",sequenceName = "pedagogico.lecturas_lec_id_seq")
    @GeneratedValue(generator = "lecturas_lec_id_seq")
    private int lecId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ban_lec_id")
    private BancoLectura banco;

    @Column(name = "nom",length = 20)
    private String nom;
    @Column(name = "aut",length = 20)
    private String aut;
    @Column(name = "des")
    private String des;
    @Column(name = "ubi")
    private String ubi;
    @Column(name = "tip",length = 20)
    private String tip;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_cre")
    private Date fecCre;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_pub")
    private Date fecPub;

    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public Lectura() {
    }

    public Lectura(String nom, String ubi) {
        this.nom = nom;
        this.ubi = ubi;
        this.fecCre = new Date();
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public Lectura(String nom, String aut, String des, String ubi, String tip, Date fecCre, Date fecPub) {
        this.nom = nom;
        this.aut = aut;
        this.des = des;
        this.ubi = ubi;
        this.tip = tip;
        this.fecCre = fecCre;
        this.fecPub = fecPub;
        this.fecCre = new Date();
        this.estReg = 'A';
        this.fecMod = new Date();
    }
    public Lectura(String nom, String aut, String des, String tip, Date fecCre, Date fecPub) {
        this.nom = nom;
        this.aut = aut;
        this.des = des;
        this.tip = tip;
        this.fecCre = fecCre;
        this.fecPub = fecPub;
        this.fecCre = new Date();
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getLecId() {
        return lecId;
    }

    public void setLecId(int lecId) {
        this.lecId = lecId;
    }

    public BancoLectura getBanco() {
        return banco;
    }

    public void setBanco(BancoLectura banco) {
        this.banco = banco;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAut() {
        return aut;
    }

    public void setAut(String aut) {
        this.aut = aut;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getUbi() {
        return ubi;
    }

    public void setUbi(String ubi) {
        this.ubi = ubi;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Date getFecPub() {
        return fecPub;
    }

    public void setFecPub(Date fecPub) {
        this.fecPub = fecPub;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
