/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.TipoPariente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarParienteTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Parientes pariente = null;
        
        Long parId = 0L,
            perId = 0L;
        int tpaId = 0;
        String   parDni = "";
            
        String parPat = "",
               parMat = "",
               parNom = "",
               parDir = "",
               //parDes = "",
               parTel = "";
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            parId = requestData.getLong("parId");
            perId = requestData.getLong("perId");
            tpaId = requestData.getInt("tpaId");
            parMat = requestData.getString("parMat");
            parPat = requestData.getString("parPat");
            parNom = requestData.getString("parNom");
            parDir = requestData.getString("parDir");
            parTel = requestData.getString("parTel");
            parDni = requestData.getString("parDni");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        //si el pariente no existe en la tabla persona
        
        Persona pe = null;
        PersonaDao peDao = (PersonaDao)FactoryDao.buildDao("di.PersonaDao");        
        
        //si no existe el pariente en la tabla persona
        if(parId == 0L){            
            pe = new Persona(parMat, parPat, parNom, parDni, parTel, parDir);               
            try {
                peDao.insert(pe);
            } catch (Exception e) {
                System.out.println(e);
            }            
            parId = pe.getPerId();
        }
        else{            
            //pe = (Persona)session.load(Persona.class, parId);//new Parientes(3);
            pe = peDao.buscarPersonaxId(perId);
            pe.setApeMat(parMat);
            pe.setApePat(parPat);
            pe.setNom(parNom);
            pe.setPerDir(parDir);
            pe.setFij(parTel);
            pe.setDni(parDni);            
            
            try {
               peDao.update(pe);               
            }catch (Exception e) {               
               System.out.println(e);
            }
            
        }

        ParientesDao parienteDao = (ParientesDao)FactoryDao.buildDao("di.ParientesDao");
        Parientes pa = new Parientes(parId, perId, new TipoPariente(tpaId));

        try{
            parienteDao.insert(pa);   
        }catch(Exception e){   
            System.out.println("No se pudo registrar pariente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar pariente", e.getMessage() );
        }        
        //Fin       
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();            
        oResponse.put("parId",pa.getParId());
        oResponse.put("perId",pa.getPerId());
        oResponse.put("tpaDes",pa.getParentesco().getTpaDes());
        return WebResponse.crearWebResponseExito("El registro del Pariente se realizo correctamente", oResponse);
        //Fin
        
    }
    
}
