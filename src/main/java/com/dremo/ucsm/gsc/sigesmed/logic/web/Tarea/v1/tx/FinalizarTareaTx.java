/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
public class FinalizarTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        TareaEscolar tarea = new TareaEscolar();
        try{
            JSONObject requestData = (JSONObject)wr.getData();            
            
            tarea.setTarEscId( requestData.getInt("tareaID") );
            tarea.setEstado(Tarea.ESTADO_CALIFICADO);//actualizando el estado a calificado = C
            tarea.setFecMod(new Date());            
        
        }catch(Exception e){
            System.out.println("No se pudo dar por finalizado la tarea, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo dar por finalizado la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            List<BandejaTareaModel> alumnos = tareaDao.buscarAlumnosQueFaltaCalificarTarea(tarea.getTarEscId());
            //todavia hay tareas enviadas por revisar, no se puede dar por finalzado la tarea
            if(alumnos.size()>0){
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado",""+tarea.getEstado());
                return WebResponse.crearWebResponseError("Hay "+alumnos.size()+" tareas por revisar, no se puede finalizar la session de tarea");
            }
            tareaDao.finalizarTarea(tarea);
            
            
            /*
            * FALTA ACTUALIZAR REGISTRO AUXILIAR
            */
            
        }catch(Exception e){
            System.out.println("No se pudo dar por finalizado la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo dar por finalizado la tarea", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+tarea.getEstado());
        return WebResponse.crearWebResponseExito("Se finalizo y acutalizo el registro auxilar con las notas correctamente",oResponse);
        //Fin
    }    
    
    
}
