/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
/**
 *
 * @author abel
 */
public class PersistenciaPeriodoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Periodo nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            char codigo = requestData.optString("periodoID").charAt(0);
            String nombre = requestData.getString("nombre");
            int factor = requestData.getInt("factor");
            char estado = requestData.optString("estado").charAt(0);
            
            nuevo = new Periodo(codigo,factor,nombre,new Date(),wr.getIdUsuario(),estado);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("datos incorrectos", e.getMessage() );
        }
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            planDao.mergePeriodo(nuevo);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la accion sobre periodo", e.getMessage() );
        }
        //Fin
        
        JSONObject res = new JSONObject();
        res.put("periodoID", ""+nuevo.getPerId());
        
        return WebResponse.crearWebResponseExito("La accion sobre periodo se realizo correctamente",res);
    }    
    
    
}

