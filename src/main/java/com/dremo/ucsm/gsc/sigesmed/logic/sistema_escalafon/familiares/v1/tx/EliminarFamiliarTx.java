/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarFamiliarTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarFamiliarTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {        
         
        int parId = 0,
            perId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            parId = requestData.getInt("parId");
            perId = requestData.getInt("perId");            
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarPariente",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("se.ParientesDao");
        try{
            parientesDao.delete(new Parientes(parId, perId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el  pariente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el pariente", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El pariente se elimino correctamente");
    }

}
