/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.OperacionDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.OperacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaOperacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Operacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarOperacionTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        List<Operacion> operaciones=null;
      
        OperacionDao operacionesDao = (OperacionDao)FactoryDao.buildDao("sci.OperacionDao");
       
       
        try {
            operaciones= operacionesDao.buscarConCuentas();
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar las Operaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Operaciones", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
        for(Operacion ope:operaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("operacionID",ope.getOpeId());
            oResponse.put("descripcion",ope.getDesOpe());
            oResponse.put("tipo",ope.getTipOpe());
            oResponse.put("estado",""+ope.getEstReg());
           
            Set<CuentaOperacion> cuentaOperaciones = ope.getCuentaOperaciones();   
          
            if( cuentaOperaciones.size()> 0 ){
                JSONArray aDetalle = new JSONArray();
                for( CuentaOperacion cOpe:cuentaOperaciones ){
                    JSONObject oCuenta = new JSONObject();
                    oCuenta.put("cuentaContableID",cOpe.getCuentaContable().getCueConId());
                     oCuenta.put("numero",cOpe.getCuentaContable().getNumCue());
                    oCuenta.put("nombre",cOpe.getCuentaContable().getNomCue());
                    oCuenta.put("estado",""+cOpe.getCuentaContable().getEstReg());
                    oCuenta.put("naturaleza",""+cOpe.getNatCueOpe());
                    aDetalle.put(oCuenta);
                }
                oResponse.put("cuentaOperaciones",aDetalle);
            }
            else{
                JSONArray aDetalle = new JSONArray();
                oResponse.put("cuentaOperaciones",aDetalle);
            }
                
            
            
            miArray.put(oResponse);
        }
        System.out.println(miArray.toString());
        
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

