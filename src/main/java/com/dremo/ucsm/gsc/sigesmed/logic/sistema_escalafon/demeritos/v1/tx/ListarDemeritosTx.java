/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarDemeritosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarDemeritosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Demerito> demeritos = null;
        DemeritoDao demeritoDao = (DemeritoDao)FactoryDao.buildDao("se.DemeritoDao");
        
        try{
            demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar demeritos",e);
            System.out.println("No se pudo listar los demeritos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los demeritos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Demerito d:demeritos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("demId", d.getDemId());
            oResponse.put("entEmi", d.getEntEmi());
            oResponse.put("numRes", d.getNumRes());
            oResponse.put("fecRes", d.getFecRes());
            oResponse.put("sep", d.getSep());
            oResponse.put("fecIni", d.getFecIni());
            oResponse.put("fecFin", d.getFecFin());
            oResponse.put("mot", d.getMot());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los demeritos fueron listados exitosamente", miArray);
    }
    
}
