/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "ficha_evaluacion_detalle", schema = "pedagogico")

public class FichaEvaluacionDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FichaEvaluacionDetallePK fichaEvaluacionDetallePK;
    @Column(name = "pun")
    private Integer pun;
    
    

    public FichaEvaluacionDetalle() {
    }

    public FichaEvaluacionDetalle(FichaEvaluacionDetallePK fichaEvaluacionDetallePK) {
        this.fichaEvaluacionDetallePK = fichaEvaluacionDetallePK;
    }

    public FichaEvaluacionDetalle(int fevId, int iteId) {
        this.fichaEvaluacionDetallePK = new FichaEvaluacionDetallePK(fevId, iteId);
    }

    public FichaEvaluacionDetallePK getFichaEvaluacionDetallePK() {
        return fichaEvaluacionDetallePK;
    }

    public void setFichaEvaluacionDetallePK(FichaEvaluacionDetallePK fichaEvaluacionDetallePK) {
        this.fichaEvaluacionDetallePK = fichaEvaluacionDetallePK;
    }

    public Integer getPun() {
        return pun;
    }

    public void setPun(Integer pun) {
        this.pun = pun;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fichaEvaluacionDetallePK != null ? fichaEvaluacionDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaEvaluacionDetalle)) {
            return false;
        }
        FichaEvaluacionDetalle other = (FichaEvaluacionDetalle) object;
        if ((this.fichaEvaluacionDetallePK == null && other.fichaEvaluacionDetallePK != null) || (this.fichaEvaluacionDetallePK != null && !this.fichaEvaluacionDetallePK.equals(other.fichaEvaluacionDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.FichaEvaluacionDetalle[ fichaEvaluacionDetallePK=" + fichaEvaluacionDetallePK + " ]";
    }
    
}
