/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx.ActualizarCargoTx;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarArticuloTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarCargoTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject dataRequest = (JSONObject)wr.getData();
              
        JSONObject jsonTipo = dataRequest.getJSONObject("tip");
        int id = dataRequest.getInt("id");
        String tipArticulo = jsonTipo.optString("nomt","");
        String nomArticulo = dataRequest.optString("nom","");
        String desArticulo = dataRequest.optString("des","");
        String resMinArticulo = dataRequest.optString("res","");
        double preArticulo = dataRequest.optDouble("pre");

        return actualizar(id,nomArticulo,desArticulo,tipArticulo,resMinArticulo,preArticulo);
    }
    public WebResponse actualizar(int idArt,String nom, String des,String tip, String res, Double pre){
        ArticuloEscolarDao articuloEscolarDao = (ArticuloEscolarDao) FactoryDao.buildDao("ma.ArticuloEscolarDao");
        try{
            ArticuloEscolar articulo = articuloEscolarDao.buscarPorId(idArt);
            articulo.setNomArticulo(nom);
            articulo.setDesArticulo(des);
            articulo.setResMinArticulo(res);
            articulo.setTipoArticulo(tip);
            articulo.setPreArticulo(pre);
            articulo.setFecMod(new Date());
            articuloEscolarDao.update(articulo);
            return WebResponse.crearWebResponseExito("La actualizacion se realizo con exito", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo realizar la transaccion", WebResponse.BAD_RESPONSE);
        }
    }
}
