/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author gscadmin
 */
@Embeddable
public class FichaEvaluacionDetallePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "fev_id")
    private int fevId;
    @Basic(optional = false)
    @Column(name = "ite_id")
    private int iteId;

    public FichaEvaluacionDetallePK() {
    }

    public FichaEvaluacionDetallePK(int fevId, int iteId) {
        this.fevId = fevId;
        this.iteId = iteId;
    }

    public int getFevId() {
        return fevId;
    }

    public void setFevId(int fevId) {
        this.fevId = fevId;
    }

    public int getIteId() {
        return iteId;
    }

    public void setIteId(int iteId) {
        this.iteId = iteId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) fevId;
        hash += (int) iteId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaEvaluacionDetallePK)) {
            return false;
        }
        FichaEvaluacionDetallePK other = (FichaEvaluacionDetallePK) object;
        if (this.fevId != other.fevId) {
            return false;
        }
        if (this.iteId != other.iteId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.FichaEvaluacionDetallePK[ fevId=" + fevId + ", iteId=" + iteId + " ]";
    }
    
}
