/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.sub_modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.SubModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarSubModuloSistemaConFuncionesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<SubModuloSistema> subModulos = null;
        SubModuloSistemaDao subModuloDao = (SubModuloSistemaDao)FactoryDao.buildDao("SubModuloSistemaDao");
        try{
            subModulos = subModuloDao.buscarConFunciones();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Sub Modulos del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Sub Modulos del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(SubModuloSistema subModulo:subModulos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("subModuloID",subModulo.getSubModSisId() );
            oResponse.put("codigo",subModulo.getCod());
            oResponse.put("nombre",subModulo.getNom());
            
            
            if( subModulo.getFuncionSistemas().size() > 0 ){
                JSONArray aFunciones = new JSONArray();
                for(FuncionSistema funcion:subModulo.getFuncionSistemas()){
                    JSONObject oFuncion = new JSONObject();
                    oFuncion.put("funcionID",funcion.getFunSisId() );
                    oFuncion.put("nombre",funcion.getNom());
                    oFuncion.put("icono",funcion.getIco());
                    oFuncion.put("estado",""+funcion.getEstReg());
                    aFunciones.put(oFuncion);
                }
                oResponse.put("funciones",aFunciones);
            }
            
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

