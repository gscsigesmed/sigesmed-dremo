package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.RespuestaEncuestaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.RespuestaEncuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

public class RespuestaEncuestaDaoHibernate extends GenericDaoHibernate<RespuestaEncuesta> implements RespuestaEncuestaDao{
    
    private static final Logger logger = Logger.getLogger(RespuestaEncuestaDaoHibernate.class.getName());
    
    @Override
    public List<RespuestaEncuesta> buscarPorPregunta(int codPre) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(RespuestaEncuesta.class)
                    .add(Restrictions.eq("preEvaCapId", codPre))
                    .setProjection(Projections.projectionList()
                        .add(Projections.property("resEnc"), "resEnc"))
                    .setResultTransformer(Transformers.aliasToBean(RespuestaEncuesta.class));
            
            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorEvaluacion", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
