package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.DisenoCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 19/10/2016.
 */
public class RegistrarAreaCurricularTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarAreaCurricularTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        AreaCurricular area = new AreaCurricular(data.optString("abr"),data.getString("nom"),data.optString("des"),true);

        return registrarArea(area, data.getInt("curr"));
    }
    private WebResponse registrarArea(AreaCurricular area, int curr) {
        try{
            AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");
            DisenoCurricularDao disenoDao = (DisenoCurricularDao) FactoryDao.buildDao("maestro.plan.DisenoCurricularDao");

            DisenoCurricular diseno = disenoDao.buscarPorId(curr);
            area.setDisenoCurr(diseno);
            areaDao.insert(area);
            JSONObject obj = new JSONObject().put("cod",area.getAreCurId()).put("est",area.getEstReg());
            return WebResponse.crearWebResponseExito("Se inserto correctamente el area", obj);
        }catch (Exception e){
            logger.log(Level.SEVERE, "registrarArea", e);
            return WebResponse.crearWebResponseError("Error al realizar la insercion", e.getMessage());
        }
    }
}
