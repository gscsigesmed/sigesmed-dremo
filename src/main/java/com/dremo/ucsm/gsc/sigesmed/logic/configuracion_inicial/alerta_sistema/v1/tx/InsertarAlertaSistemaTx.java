/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.AlertaSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.AlertaSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class InsertarAlertaSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        AlertaSistema nuevoAlerta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int alertaID = requestData.getInt("alertaID");
            int funcionID = requestData.getInt("funcionID");
            String nombre = requestData.getString("nombre");
            //String codigo = requestData.getString("codigo");
            String descripcion = requestData.optString("descripcion");
            String accion = requestData.getString("accion");
            String tipo = requestData.getString("tipo");
            nuevoAlerta = new AlertaSistema(alertaID,"", nombre, accion, descripcion, tipo.charAt(0),funcionID);
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AlertaSistemaDao alertaDao = (AlertaSistemaDao)FactoryDao.buildDao("AlertaSistemaDao");
        try{
            alertaDao.insert(nuevoAlerta);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar\n", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("alertaID",nuevoAlerta.getAleId());
        return WebResponse.crearWebResponseExito("El registro de la Alerta se realizo correctamente", oResponse);
        //Fin
    }
    
}
