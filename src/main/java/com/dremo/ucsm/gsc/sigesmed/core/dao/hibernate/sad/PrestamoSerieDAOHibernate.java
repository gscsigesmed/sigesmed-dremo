/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sad;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.PrestamoSerieDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.PrestamoSerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author Jeferson
 */
public class PrestamoSerieDAOHibernate extends GenericDaoHibernate<PrestamoSerieDocumental> implements PrestamoSerieDAO{

    @Override
    public List<PrestamoSerieDocumental> buscarPorCodigo(int codigo) {
        
        List<PrestamoSerieDocumental> psd = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT  psd FROM PrestamoSerieDocumental psd WHERE psd.pre_ser_doc_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",codigo);
            psd = query.list();
        }catch(Exception e){
            System.out.println("No se pudo Obtener el Prestamo de Serie Documental \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Obtener el Prestamo de Serie Documental\\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return psd;
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PrestamoSerieDocumental> listarPrestamos() {
        
        List<PrestamoSerieDocumental> pres_series = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT ps FROM PrestamoSerieDocumental ps";
            Query query = session.createQuery(hql);
            pres_series = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo listar los Prestamos de Inventario \\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Prestamos de Inventario \\n "+ e.getMessage());           
        }
        finally{
            session.close();
        }
        return pres_series;
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
