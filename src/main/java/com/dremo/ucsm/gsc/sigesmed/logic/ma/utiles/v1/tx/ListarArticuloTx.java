/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author ucsm
 */
public class ListarArticuloTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarArticuloTx.class.getName());
       
    @Override
    public WebResponse execute(WebRequest wr) {
        return listarArticulo();
    }
    private WebResponse listarArticulo(){
        ArticuloEscolarDao articuloEscolarDao = (ArticuloEscolarDao) FactoryDao.buildDao("ma.ArticuloEscolarDao");
       try{
           List<ArticuloEscolar> articulos = articuloEscolarDao.buscarTodos(ArticuloEscolar.class);
           
           return WebResponse.crearWebResponseExito("Exito al listar los articulos",
                    new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"artEscId","tipoArticulo","preArticulo","nomArticulo","desArticulo","resMinArticulo"},
                            new String[]{"id","tip","pre","nom","des","res"},articulos)));
       }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar los articulos",WebResponse.BAD_RESPONSE);
        }      
    }
}
