/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sad;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.TablaRetencionDocumentosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.TablaRetencionDocumentos;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author admin
 */
public class TablaRetencionDocumentosDAOHibernate extends GenericDaoHibernate<TablaRetencionDocumentos> implements TablaRetencionDocumentosDAO{

    @Override
    public List<TablaRetencionDocumentos> buscarPorCodigo(int SerieID) {
        
        List<TablaRetencionDocumentos> trd = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT  trd FROM TablaRetencionDocumentos trd WHERE trd.ser_doc_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",SerieID);
            trd = query.list();
        }catch(Exception e){
            System.out.println("No se pudo Listar la Tabla de Retencion para dicha Serie Documental \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar la Tabla de Retencion para dicha Serie Documental\\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return trd;
    }
    
    
}
