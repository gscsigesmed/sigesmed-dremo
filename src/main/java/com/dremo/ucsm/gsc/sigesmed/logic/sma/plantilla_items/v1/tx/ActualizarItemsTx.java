/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_items.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ItemsDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Items;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarItemsTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Items grupoAct = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int indId = requestData.getInt("indId");
            int gruId = requestData.getInt("gruId");
            String indDes = requestData.optString("indNom");
            

            grupoAct = new Items(indId, new CompromisosGestion(gruId), indDes);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ItemsDao dexDao = (ItemsDao)FactoryDao.buildDao("sma.ItemsDao");
        try{
            dexDao.update(grupoAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Indicador\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Indicador", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Indicador se actualizo correctamente");
        //Fin
    }
    
}
