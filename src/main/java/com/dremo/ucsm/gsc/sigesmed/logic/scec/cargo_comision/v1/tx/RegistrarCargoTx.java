/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RegistrarCargoTx implements ITransaction{
    private final static Logger logger = Logger.getLogger(RegistrarCargoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject dataRequest = (JSONObject)wr.getData();
        String nomCargo = dataRequest.getString("nom");
        String desCargo = dataRequest.getString("des");
        CargoComision carcom = new CargoComision(nomCargo.toUpperCase(),desCargo);
        return registrarCargo(carcom);
    }
    private WebResponse registrarCargo(CargoComision com) {
        com.setEstReg('A');
        com.setFecMod(new Date());

        CargoComisionDao cargoComisionDao = (CargoComisionDao) FactoryDao.buildDao("scec.CargoComisionDao");
        try{
            cargoComisionDao.insert(com);
            String strjson = EntityUtil.objectToJSONString(new String[]{"carComId","nomCar","desCar"},new String[]{"cod","nom","des"},com);
            JSONObject respuesta = new JSONObject(strjson);
            
            return WebResponse.crearWebResponseExito("Se realizo el registro correctamente",WebResponse.OK_RESPONSE).setData(
                    respuesta
            );
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName()+":registrarCargo",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",WebResponse.BAD_RESPONSE);
        }
    }
}
