/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.funcion_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarFuncionSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<FuncionSistema> funciones = null;
        FuncionSistemaDao funcionDao = (FuncionSistemaDao)FactoryDao.buildDao("FuncionSistemaDao");
        try{
            funciones = funcionDao.buscarConSubModulo();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las funciones del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las funciones del Sistema ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(FuncionSistema funcion:funciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("funcionID",funcion.getFunSisId() );
            oResponse.put("subModuloID",funcion.getSubModuloSistema().getSubModSisId() );
            oResponse.put("nombre",funcion.getNom());
            oResponse.put("descripcion",funcion.getDes());
            oResponse.put("url",funcion.getUrl());
            oResponse.put("clave",funcion.getClaNav());
            oResponse.put("controlador",funcion.getNomCon());
            oResponse.put("interfaz",funcion.getNomInt());
            oResponse.put("icono",funcion.getIco());
            oResponse.put("fecha",funcion.getFecMod().toString());
            oResponse.put("estado",""+funcion.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

