/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
public class ListarElementosFichaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarElementosFichaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest request){
        String idFicha = request.getMetadataValue(MEP.CAMPO_ELEMENTO_FICHA);
        String strrol = request.getMetadataValue("rol");
        return getElementosFicha(Integer.valueOf(idFicha));
    }
    private WebResponse getElementosFicha(int idFicha){
        WebResponse wResponse = new WebResponse();
        
        try{
            JSONObject dataResponse = new JSONObject();
            dataResponse.put("ficha", idFicha);
            for (String TIPO_ELEMENTOS : MEP.TIPO_ELEMENTOS) {
                List<ElementoFicha> lElementos = getElemento(idFicha, TIPO_ELEMENTOS);
                JSONArray aescalas = new JSONArray();
                for(ElementoFicha elemento : lElementos){
                    aescalas.put(new JSONObject(elemento.toString()));
                }
                dataResponse.put(TIPO_ELEMENTOS, aescalas);
            }
            wResponse.setResponse(MEP.SUCC_RESPONSE_COD);
            wResponse.setResponseMsg(MEP.SUCC_RESPONSE_MESS_LIST);
            wResponse.setData(dataResponse);
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":getElementosFicha()",e);
            wResponse.setResponse(MEP.ERR_RESPONSE_COD);
            wResponse.setResponseMsg(MEP.ERR_RESPONSE_MESS_LIST);
        }
        return wResponse;
    }
    private List<ElementoFicha> getElemento(int idFicha,String tipo){
        Class elementoFichaClass = null;
        String nameId;
        try{
            switch(tipo){
                case MEP.TIPO_ELEMENTO_ESCALA: elementoFichaClass = 
                    EscalaValoracionFicha.class; 
                    nameId = "val";
                    break;
                case MEP.TIPO_ELEMENTO_RANGO: elementoFichaClass = 
                    RangoPorcentualFicha.class;
                    nameId = "min";
                    break;
                case MEP.TIPO_ELEMENTO_CONTENIDO: elementoFichaClass =
                    ContenidoFichaEvaluacion.class;
                    nameId = "tip";
                    break;
                default: return null;
            }
            FichaEvaPerslDaoHibernate fevpDao = new FichaEvaPerslDaoHibernate();
            return fevpDao.getElementos(idFicha, elementoFichaClass,nameId);
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":getElementos()",e);
            return null;
        }
    }
}
