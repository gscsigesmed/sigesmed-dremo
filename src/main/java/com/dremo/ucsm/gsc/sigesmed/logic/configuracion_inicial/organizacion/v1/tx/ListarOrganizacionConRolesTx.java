/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoOrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarOrganizacionConRolesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Organizacion> organizaciones = null;
        List<TipoOrganizacion> tipos = null;
        OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        TipoOrganizacionDao tipoDao = (TipoOrganizacionDao)FactoryDao.buildDao("TipoOrganizacionDao");
        try{
            organizaciones = organizacionDao.buscarConTipoOrganizacionYPadre();
            tipos = tipoDao.listarConRoles();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Organizaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Organizaciones", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        for(Organizacion o:organizaciones )
            for(TipoOrganizacion tipo:tipos )                
                if(o.getTipoOrganizacion().getTipOrgId() == tipo.getTipOrgId() ){
                    o.setTipoOrganizacion(tipo);
                    break;
                }
        
        
        JSONArray miArray = new JSONArray();
        for(Organizacion organizacion:organizaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("organizacionID",organizacion.getOrgId() );
            oResponse.put("tipoOrganizacionID",organizacion.getTipoOrganizacion().getTipOrgId() );
            oResponse.put("tipoOrganizacion",organizacion.getTipoOrganizacion().getNom() );
            oResponse.put("codigo",organizacion.getCod());
            oResponse.put("nombre",organizacion.getNom());
            oResponse.put("alias",organizacion.getAli());
            oResponse.put("descripcion",organizacion.getDes());
            
            List<Rol> roles = organizacion.getTipoOrganizacion().getRoles();            
            if( roles.size() > 0 ){
                JSONArray aRoles = new JSONArray();
                for( Rol r:roles ){
                    JSONObject oRol = new JSONObject();
                    oRol.put("rolID",r.getRolId() );
                    oRol.put("nombre",r.getNom() );
                    oRol.put("descripcion",r.getDes() );
                    oRol.put("estado",""+r.getEstReg() );
                    aRoles.put(oRol);
                }
                oResponse.put("roles",aRoles);
            }
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

