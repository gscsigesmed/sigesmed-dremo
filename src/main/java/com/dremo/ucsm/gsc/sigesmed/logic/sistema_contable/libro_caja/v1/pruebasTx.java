/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class pruebasTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        LibroCaja nuevoLibro = null;
    Object fa ;
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();                     
                          
           fa = requestData.get("fechaAperturaString");  
           
            System.out.println(fa);
            
           
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
         //Fin
        
        /*
        *   Parte de Logica de Negocio    
        *
        */
        
     
        Date b=new Date(fa.toString());
        
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
        String convertido = fechaHora.format(b);
        System.out.print(convertido);
        /*
        *  Repuesta Correcta
        */
       
        JSONObject oResponse = new JSONObject();
       
     //   JSONObject fa = new JSONObject(); fa.put("y",nuevoLibro.getFecApe().getYear());fa.put("m",nuevoLibro.getFecApe().getMonth());fa.put("d",nuevoLibro.getFecApe().getDate());
        oResponse.put("fechaEntrega",convertido); 
       

        return WebResponse.crearWebResponseExito("El registro del Libro Caja se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
