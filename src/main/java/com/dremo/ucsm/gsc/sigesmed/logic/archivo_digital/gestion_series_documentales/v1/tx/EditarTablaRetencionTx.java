/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.TablaRetencionDocumentosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.TablaRetencionDocumentos;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author admin
 */
public class EditarTablaRetencionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        TablaRetencionDocumentos trd = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int serieid = requestData.getInt("serie_id");
            int ag  = requestData.getInt("ag");
            int ap = requestData.getInt("ap");
            int oaa = requestData.getInt("oaa");
            
            trd = new TablaRetencionDocumentos(serieid,ag,ap,oaa);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Actualizar la Tabla de Retencion", e.getMessage() );
        }
           //Operaciones con la Base de Datos
       TablaRetencionDocumentosDAO trd_dao = (TablaRetencionDocumentosDAO)FactoryDao.buildDao("sad.TablaRetencionDocumentosDAO");
        try{
            trd_dao.update(trd);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Actualizar la Tabla de Retencion", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El registro de La Tabla de Retencion se realizo correctamente");
        
        
        
        
        
     //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
