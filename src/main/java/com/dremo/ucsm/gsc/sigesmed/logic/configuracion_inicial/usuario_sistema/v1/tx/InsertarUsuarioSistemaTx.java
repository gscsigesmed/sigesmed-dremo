/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author abel
 */
public class InsertarUsuarioSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Usuario nuevoUsuario = null;
        Persona nuevaPersona = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONObject rUsuario = requestData.getJSONObject("usuario");
            JSONObject rPersona = requestData.getJSONObject("persona");
            
            
            String dni = rPersona.getString("dni");
            String nombres = rPersona.getString("nombre");
            String materno = rPersona.getString("materno");
            String paterno = rPersona.getString("paterno");            
            String email = rPersona.optString("email");
            String numero1 = rPersona.optString("numero1");
            String numero2 = rPersona.optString("numero2");
            
            
            int rolID = rUsuario.getInt("rolID");
            int organizacionID = rUsuario.getInt("organizacionID");
            String nombre = rUsuario.getString("nombre");
            String password = rUsuario.optString("password");
            String estado = rUsuario.getString("estado");
            //nuevoUsuario = new Usuario(0, new Organizacion(organizacionID), new Rol(rolID), nombre, password, new Date(), new Date(), 1, estado.charAt(0));
            
            nuevaPersona = new Persona(0,dni,nombres,materno,paterno,new Date(),email,numero1,numero2);           
            nuevoUsuario = new Usuario(0, nombre, (password.length() >0? password :paterno.substring(0, 3)+materno.substring(0, 3)+nombres.substring(0, 3)), new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));
                        
            UsuarioSession session = new UsuarioSession(0, new Organizacion(organizacionID), new Rol(rolID), nuevoUsuario, new Date(), new Date(), wr.getIdUsuario(), 'A');
            nuevoUsuario.setSessiones(new ArrayList<UsuarioSession>());
            nuevoUsuario.getSessiones().add( session );
            
            int areaID = rUsuario.optInt("areaID");
            if(areaID>0){
                session.setAreId(areaID);
            }
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
        try{
            Persona persona = personaDao.buscarPorDNI(nuevaPersona.getDni());
            if(persona == null )
                personaDao.insert(nuevaPersona);
            else{
                nuevaPersona = persona;
            }
            
            nuevoUsuario.setUsuId(nuevaPersona.getPerId());
            usuarioDao.insert(nuevoUsuario);
            /*         
            nuevoUsuario.setUsuId(nuevaPersona.getPerId());
            usuarioDao.insert(nuevoUsuario);*/
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Usuario\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Usuario", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("usuarioID",nuevoUsuario.getUsuId());
        oResponse.put("sessionID",nuevoUsuario.getSessiones().get(0).getUsuSesId());
        oResponse.put("password",nuevoUsuario.getPas());
        return WebResponse.crearWebResponseExito("El registro del Usuario se realizo correctamente", oResponse);
        //Fin
    }
    
}
