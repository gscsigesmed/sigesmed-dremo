/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "compromisos_ficha", schema = "pedagogico")

public class CompromisosFicha implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cfi_id")
    private Integer cfiId;
    @Column(name = "cfi_fec")
    @Temporal(TemporalType.DATE)
    private Date cfiFec;
    @Column(name = "cfi_niv")
    private Short cfiNiv;
    @Column(name = "cfi_des")
    private String cfiDes;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
    @JoinColumn(name = "fev_id", referencedColumnName = "fev_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private FichaEvaluacion fevId;

    public CompromisosFicha() {
    }

    public CompromisosFicha(Integer cfiId) {
        this.cfiId = cfiId;
    }

    public Integer getCfiId() {
        return cfiId;
    }

    public void setCfiId(Integer cfiId) {
        this.cfiId = cfiId;
    }

    public Date getCfiFec() {
        return cfiFec;
    }

    public void setCfiFec(Date cfiFec) {
        this.cfiFec = cfiFec;
    }

    public Short getCfiNiv() {
        return cfiNiv;
    }

    public void setCfiNiv(Short cfiNiv) {
        this.cfiNiv = cfiNiv;
    }

    public String getCfiDes() {
        return cfiDes;
    }

    public void setCfiDes(String cfiDes) {
        this.cfiDes = cfiDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public FichaEvaluacion getFevId() {
        return fevId;
    }

    public void setFevId(FichaEvaluacion fevId) {
        this.fevId = fevId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cfiId != null ? cfiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompromisosFicha)) {
            return false;
        }
        CompromisosFicha other = (CompromisosFicha) object;
        if ((this.cfiId == null && other.cfiId != null) || (this.cfiId != null && !this.cfiId.equals(other.cfiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.CompromisosFicha[ cfiId=" + cfiId + " ]";
    }
    
}
