/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarItemTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int iteid = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            iteid = requestData.getInt("iteid");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ItemFileDao ficDao = (ItemFileDao)FactoryDao.buildDao("smdg.ItemFileDao");
        try{
            ficDao.delete(new ItemFile(iteid));        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la plantilla\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el documento", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El documento se elimino correctamente");
        //Fin
    }
}
