/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarDirectorioExternoTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int dexId = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            dexId = requestData.getInt("dexId");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DirectorioExternoDao dexDao = (DirectorioExternoDao)FactoryDao.buildDao("di.DirectorioExternoDao");
        try{
            dexDao.delete(new DirectorioExterno(dexId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Directorio Externo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Directorio Externo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Directorio Externo se elimino correctamente");
        //Fin
    }
    
}
