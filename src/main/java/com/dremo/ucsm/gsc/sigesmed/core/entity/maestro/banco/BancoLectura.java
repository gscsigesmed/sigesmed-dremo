package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity
@Table(name = "banco_lectura", schema = "pedagogico")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BancoLectura implements java.io.Serializable{

    @Id
    @Column(name = "ban_lec_id",nullable = false,unique = true)
    @SequenceGenerator(name = "banco_lectura_ban_lec_id_seq",sequenceName = "pedagogico.banco_lectura_ban_lec_id_seq")
    @GeneratedValue(generator = "banco_lectura_ban_lec_id_seq")
    private int banLecId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_cre")
    private Date fecCre;
    @Column(name = "des")
    private String des;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pro")
    private Usuario pro;

    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public BancoLectura() {
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public BancoLectura(Date fecCre, String des) {
        this.fecCre = fecCre;
        this.des = des;

        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public BancoLectura(Date fecCre) {
        this.fecCre = fecCre;
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getBanLecId() {
        return banLecId;
    }

    public void setBanLecId(int banLecId) {
        this.banLecId = banLecId;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Usuario getPro() {
        return pro;
    }

    public void setPro(Usuario pro) {
        this.pro = pro;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
