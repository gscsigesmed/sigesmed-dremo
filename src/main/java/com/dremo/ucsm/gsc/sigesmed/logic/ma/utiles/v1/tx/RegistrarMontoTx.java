/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.MontoListaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.MontoLista;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RegistrarMontoTx implements ITransaction{
    private final static Logger logger = Logger.getLogger(RegistrarMontoTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject dataRequest = (JSONObject)wr.getData();
        JSONObject jsonNivel = dataRequest.getJSONObject("nivel");
        JSONObject jsonAnio = dataRequest.getJSONObject("ani");
        int nivMonto = jsonNivel.getInt("id");
        String anioMonto = jsonAnio.getString("anio");
        double valMonto = dataRequest.getDouble("val");
        
        System.out.print(" anioMonto ... " + anioMonto);
        
        //MontoLista monto = new MontoLista(anioMonto,valMonto,jsonObject);
        //ArticuloEscolar articulo = new ArticuloEscolar("aseo","toalla n",5.00,"prena algodon","R.M. 1021");
        return registrarMonto(nivMonto,anioMonto,valMonto);
     
    }
    private WebResponse registrarMonto(int idNiv, String anio, Double valor){
        
        MontoListaDao montoListaDao = (MontoListaDao) FactoryDao.buildDao("ma.MontoListaDao");
        Nivel niv = montoListaDao.buscarNivelPorId(idNiv);
        
        MontoLista mon = new MontoLista(anio,valor);
        mon.setNivel(niv);
        mon.setEstReg('A');
        mon.setFecMod(new Date());
        
        try{
            montoListaDao.insert(mon);
            
            String strjson = EntityUtil.objectToJSONString(new String[]{"monId","anioMonto","valMonto"},new String[]{"id","ani","val"},mon);
            JSONObject respuesta = new JSONObject(strjson);
            JSONObject nivelJSON = new JSONObject(EntityUtil.objectToJSONString(new String[]{"nivId","nom"},new String[]{"id","nom"},niv));
            respuesta.put("niv", nivelJSON);
            
            return WebResponse.crearWebResponseExito("Se realizo el registro correctamente",WebResponse.OK_RESPONSE).setData(
                    respuesta);
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName()+":registrar monto",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",WebResponse.BAD_RESPONSE);
        }
    }
}
