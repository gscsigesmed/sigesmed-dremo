/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import java.util.Date;
import java.util.List;


/**
 *
 * @author carlos
 */
public interface InasistenciaDao extends GenericDao<Inasistencia>
{
   public List<DiasEspeciales> getDiasEspeciales(Organizacion org,Date inicio,Date fin,String estado);
   public DiasEspeciales verificarDiaEspecial(Organizacion org,Date fecha,String estado);
   public List<Organizacion> getOrganizaciones();
//       public Persona buscarPorDNI(Integer dni,Organizacion org);
   
}