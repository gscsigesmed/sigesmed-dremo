package com.dremo.ucsm.gsc.sigesmed.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="alerta_sistema" ,schema="pedagogico" )
public class AlertaSistema  implements java.io.Serializable {

    @Id
    @Column(name="ale_sis_id", unique=true, nullable=false)
    /*@SequenceGenerator(name = "secuencia_alertasistema", sequenceName="pedagogico.alerta_sistema_ale_sis_id_seq" )
    @GeneratedValue(generator="secuencia_alertasistema")*/
    private int aleId;
    @Column(name="cod",length=4)
    private String cod;
    @Column(name="nom",length=32)
    private String nom;
    @Column(name="acc",length=128)
    private String acc;
    @Column(name="des",length=256)
    private String des;
    
    @Column(name="tip_ale")
    private char tipAle;
    
    @Column(name="fun_sis_id")
    private int funSisId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fun_sis_id", nullable=false, insertable=false,updatable=false )
    private FuncionSistema funcionSistema;

    public AlertaSistema() {
    }
    public AlertaSistema(int aleId) {
        this.aleId = aleId;
    }
    public AlertaSistema(int aleId,String cod, String nom,String acc,String des,char tipAle, int funSisId) {
       this.aleId = aleId;
       this.tipAle = tipAle;
       this.cod = cod;
       this.nom = nom;
       this.acc = acc;
       this.des = des;
       this.funSisId = funSisId;
    }
   
     
    public int getAleId() {
        return this.aleId;
    }    
    public void setAleId(int aleId) {
        this.aleId = aleId;
    }
    public String getCod() {
        return this.cod;
    }
    public void setCod(String cod) {
        this.cod = cod;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getAcc() {
        return this.acc;
    }
    public void setAcc(String acc) {
        this.acc = acc;
    }
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    
    public char getTipAle() {
        return this.tipAle;
    }
    public void setTipAle(char tipAle) {
        this.tipAle = tipAle;
    }
    
    public int getFunSisId() {
        return this.funSisId;
    }    
    public void setFunSisId(int funSisId) {
        this.funSisId = funSisId;
    }
    
    public FuncionSistema getFuncionSistema() {
        return this.funcionSistema;
    }    
    public void setFuncionSistema(FuncionSistema funcionSistema) {
        this.funcionSistema = funcionSistema;
    }
}


