/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "estudiante", schema="pedagogico")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Estudiante.findAll", query = "SELECT e FROM Estudiante e"),
//    @NamedQuery(name = "Estudiante.findByEstId", query = "SELECT e FROM Estudiante e WHERE e.estudianteId.estId = :estId"),
//    @NamedQuery(name = "Estudiante.findByPerId", query = "SELECT e FROM Estudiante e WHERE e.estudianteId.perId = :perId"),
//    @NamedQuery(name = "Estudiante.findByGraAct", query = "SELECT e FROM Estudiante e WHERE e.graAct = :graAct"),
//    @NamedQuery(name = "Estudiante.findBySecAc", query = "SELECT e FROM Estudiante e WHERE e.secAc = :secAc"),
//    @NamedQuery(name = "Estudiante.findByFecMod", query = "SELECT e FROM Estudiante e WHERE e.fecMod = :fecMod"),
//    @NamedQuery(name = "Estudiante.findByUsuMod", query = "SELECT e FROM Estudiante e WHERE e.usuMod = :usuMod"),
//    @NamedQuery(name = "Estudiante.findByEstReg", query = "SELECT e FROM Estudiante e WHERE e.estReg = :estReg")})

public class Estudiante implements Serializable {
    private static final long serialVersionUID = 1L;    
    @Id
    @Basic(optional = false)
    @Column(name = "per_id")
    private Long perId;
//    @EmbeddedId
//    protected EstudianteId estudianteId;
    @Column(name="cod_est",length = 10,nullable = false,unique = true)
    private String codEst;
    @Column(name = "num_her",length = 2)
    private String numHer;
    @Column(name = "lug",length = 2)
    private String lug;
    @Column(name = "tip_par",length = 2)
    private String tipPar;
    @Column(name = "rel",length = 2)
    private String rel;
    @Column(name = "tip_dis",length = 2)
    private String tipDis;
    @Column(name = "obs",length = 256)
    private String obs;

//    protected EstudianteId estudianteId;   
//    @Column(name = "sec_ac")
//    private String secAc;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
    @JoinColumn(name = "per_id", referencedColumnName = "per_id", insertable = false, updatable = false)
//    @JoinColumns({
//        @JoinColumn(name="per_id", referencedColumnName="per_id"),
//        @JoinColumn(name="est_id", referencedColumnName="est_id")})
    @ManyToOne(fetch=FetchType.LAZY)
    private Persona persona;

    public Estudiante() {
    }

    public Estudiante(long perId,String codEst, String numHer, String lug, String tipPar, String rel, String tipDis, String obs) {
        this.codEst = codEst;
        this.numHer = numHer;
        this.lug = lug;
        this.tipPar = tipPar;
        this.rel = rel;
        this.tipDis = tipDis;
        this.obs = obs;
    }
    public Estudiante(long perId) {
        this.perId = perId;
    }
    public Estudiante(long perId, Persona persona) {
        this.perId = perId;
        this.persona = persona;        
    }

//    public Estudiante(EstudianteId estudianteId) {
//        this.estudianteId = estudianteId;
//    }
//
//    public Estudiante(long estId, long perId) {
//        this.estudianteId = new EstudianteId(estId, perId);
//    }

//    public EstudianteId getEstudianteId() {
//        return estudianteId;
//    }
//
//    public void setEstudianteId(EstudianteId estudianteId) {
//        this.estudianteId = estudianteId;
//    }


    public String getCodEst() {
        return codEst;
    }

    public void setCodEst(String codEst) {
        this.codEst = codEst;
    }

    public String getNumHer() {
        return numHer;
    }

    public void setNumHer(String numHer) {
        this.numHer = numHer;
    }

    public String getLug() {
        return lug;
    }

    public void setLug(String lug) {
        this.lug = lug;
    }

    public String getTipPar() {
        return tipPar;
    }

    public void setTipPar(String tipPar) {
        this.tipPar = tipPar;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getTipDis() {
        return tipDis;
    }

    public void setTipDis(String tipDis) {
        this.tipDis = tipDis;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (estudianteId != null ? estudianteId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Estudiante)) {
//            return false;
//        }
//        Estudiante other = (Estudiante) object;
//        if ((this.estudianteId == null && other.estudianteId != null) || (this.estudianteId != null && !this.estudianteId.equals(other.estudianteId))) {
//            return false;
//        }
//        return true;
//    }

//    @Override
//    public String toString() {
//        return "entidades.Estudiante[ estudianteId=" + estudianteId + " ]";
//    }
//    

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }
    
}
