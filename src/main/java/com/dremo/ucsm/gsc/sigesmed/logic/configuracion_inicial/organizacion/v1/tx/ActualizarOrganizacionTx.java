/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Organizacion organizacionAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoOrganizacionID = requestData.getInt("tipoOrganizacionID");
            int organizacionPadreID = requestData.getInt("organizacionPadreID");
            int organizacionID = requestData.getInt("organizacionID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String alias = requestData.getString("alias");
            String descripcion = requestData.getString("descripcion");
            String direccion = requestData.getString("direccion");
            String estado = requestData.getString("estado");
            
            String ubigeo = requestData.getString("ubigeo");
            String organizacionGestion = requestData.getString("organizacionGestion");
            String organizacionCaracteristica = requestData.getString("organizacionCaracteristica");
            String organizacionPrograma = requestData.getString("organizacionPrograma");
            String organizacionForma = requestData.getString("organizacionForma");
            String organizacionVariante = requestData.getString("organizacionVariante");
            String organizacionModalidad = requestData.getString("organizacionModalidad");
            
            if(organizacionPadreID == 0)
                organizacionAct = new Organizacion(organizacionID,new TipoOrganizacion(tipoOrganizacionID), codigo, nombre, alias, descripcion,direccion, new Date(), 1, estado.charAt(0));
            else            
                organizacionAct = new Organizacion(organizacionID,new TipoOrganizacion(tipoOrganizacionID),new Organizacion(organizacionPadreID), codigo, nombre, alias, descripcion, direccion, new Date(), 1, estado.charAt(0), null);
        
            organizacionAct.setUbiCod(ubigeo);
            organizacionAct.setOrgGes(organizacionGestion);
            organizacionAct.setOrgCar(organizacionCaracteristica);
            organizacionAct.setOrgPro(organizacionPrograma);
            organizacionAct.setOrgFor(organizacionForma);
            organizacionAct.setOrgVar(organizacionVariante);
            organizacionAct.setOrgMod(organizacionModalidad);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        OrganizacionDao moduloDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        try{
            moduloDao.update(organizacionAct);
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Organizacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Organizacion se actualizo correctamente");
        //Fin
    }
    
}
