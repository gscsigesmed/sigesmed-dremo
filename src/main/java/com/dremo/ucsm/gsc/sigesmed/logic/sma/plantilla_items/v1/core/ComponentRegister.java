/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_items.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_compromisos.v1.tx.ActualizarCompromisoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_compromisos.v1.tx.EliminarCompromisoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_compromisos.v1.tx.InsertarCompromisoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_items.v1.tx.ActualizarItemsTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_items.v1.tx.EliminarItemsTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_items.v1.tx.InsertarItemsTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_items.v1.tx.ListarItemsTx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.SISTEMA_MONITOREO_ACOMPANIAMIENTO);        
        
        //Registrando el Nombre del componente
        component.setName("plantilla_items");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarItems", InsertarItemsTx.class);
        component.addTransactionPUT("actualizarItems", ActualizarItemsTx.class);        
        component.addTransactionPUT("eliminarItems", EliminarItemsTx.class);
        component.addTransactionGET("listarItems", ListarItemsTx.class);
        
        return component;
    }
}
