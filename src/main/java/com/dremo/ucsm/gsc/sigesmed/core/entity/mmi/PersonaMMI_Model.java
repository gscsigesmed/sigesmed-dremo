package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "persona", schema = "pedagogico", uniqueConstraints = @UniqueConstraint(columnNames = "dni")
)
public class PersonaMMI_Model implements java.io.Serializable {

    private long perId;
    private Integer usuMod;
    private Date fecMod;
    private Character estReg;
    private Set matriculasForApoId = new HashSet(0);
    private Set parientesesForPerId = new HashSet(0);
    private Set documentosVarioses = new HashSet(0);
    private Set parientesesForParId = new HashSet(0);
    private Set trasladoSalidas = new HashSet(0);
    private Set matriculasForRegId = new HashSet(0);
    private Set domicilios = new HashSet(0);

    public PersonaMMI_Model() {
    }

    public PersonaMMI_Model(long perId) {
        this.perId = perId;
    }

    public PersonaMMI_Model(long perId, Integer usuMod, Date fecMod, Character estReg, String depNac, String proNac, String disNac, Set matriculasForApoId, Set parientesesForPerId, Set documentosVarioses, Set parientesesForParId, Set trasladoSalidas, Set matriculasForRegId, EstudianteMMI estudiante, Set domicilios) {
        this.perId = perId;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.matriculasForApoId = matriculasForApoId;
        this.parientesesForPerId = parientesesForPerId;
        this.documentosVarioses = documentosVarioses;
        this.parientesesForParId = parientesesForParId;
        this.trasladoSalidas = trasladoSalidas;
        this.matriculasForRegId = matriculasForRegId;
        this.domicilios = domicilios;
    }

    @Id
    @Column(name = "per_id", unique = true, nullable = false)
    public long getPerId() {
        return this.perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    @Column(name = "usu_mod")
    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    @Column(name = "est_reg", length = 1)
    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "personaByApoId")
    public Set getMatriculasForApoId() {
        return this.matriculasForApoId;
    }

    public void setMatriculasForApoId(Set matriculasForApoId) {
        this.matriculasForApoId = matriculasForApoId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "personaByPerId")
    public Set getParientesesForPerId() {
        return this.parientesesForPerId;
    }

    public void setParientesesForPerId(Set parientesesForPerId) {
        this.parientesesForPerId = parientesesForPerId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
    public Set getDocumentosVarioses() {
        return this.documentosVarioses;
    }

    public void setDocumentosVarioses(Set documentosVarioses) {
        this.documentosVarioses = documentosVarioses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "personaByParId")
    public Set getParientesesForParId() {
        return this.parientesesForParId;
    }

    public void setParientesesForParId(Set parientesesForParId) {
        this.parientesesForParId = parientesesForParId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
    public Set getTrasladoSalidas() {
        return this.trasladoSalidas;
    }

    public void setTrasladoSalidas(Set trasladoSalidas) {
        this.trasladoSalidas = trasladoSalidas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "personaByRegId")
    public Set getMatriculasForRegId() {
        return this.matriculasForRegId;
    }

    public void setMatriculasForRegId(Set matriculasForRegId) {
        this.matriculasForRegId = matriculasForRegId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
    public Set getDomicilios() {
        return this.domicilios;
    }

    public void setDomicilios(Set domicilios) {
        this.domicilios = domicilios;
    }

}
