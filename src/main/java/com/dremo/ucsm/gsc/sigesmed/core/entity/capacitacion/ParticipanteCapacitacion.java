package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "participantes_capacitacion", schema = "pedagogico")
public class ParticipanteCapacitacion implements Serializable {
    @EmbeddedId
    @AttributeOverrides( {
            @AttributeOverride(name = "sedCapId", column = @Column(name = "sed_cap_id", nullable = false)),
            @AttributeOverride(name = "perId", column = @Column(name = "per_id", nullable = false))})
    private ParticipanteCapacitacionId id = new ParticipanteCapacitacionId();
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sed_cap_id", updatable = false, insertable = false)
    private SedeCapacitacion sedCap;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rol_id", updatable = false, insertable = false)
    private Rol rol;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id", updatable = false, insertable = false)
    private Persona persona;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_reg",nullable = false)
    private Date fecReg;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg",nullable = false,length = 1)
    private Character estReg;

    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name="rol_id")
    private int rol_id;
    
    
    public ParticipanteCapacitacion() {}

    public ParticipanteCapacitacion(SedeCapacitacion sedCap, Persona persona, Date fecReg, Date fecMod, Character estReg, Integer usuMod) {
        this.sedCap = sedCap;
        this.persona = persona;
        this.fecReg = fecReg;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.usuMod = usuMod;
    }
    
    public ParticipanteCapacitacion(ParticipanteCapacitacionId id, Date fecReg, Date fecMod, Character estReg, Integer usuMod) {
        this.id = id;
        this.fecReg = fecReg;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.usuMod = usuMod;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public Rol getRol() {
        return rol;
    }

    public int getRol_id() {
        return rol_id;
    }
    
    

    public ParticipanteCapacitacionId getId() {
        return id;
    }

    public void setId(ParticipanteCapacitacionId id) {
        this.id = id;
    }

    public SedeCapacitacion getSedCap() {
        return sedCap;
    }

    public void setSedCap(SedeCapacitacion sedCap) {
        this.sedCap = sedCap;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    @Override
    public String toString() {
        return "ParticipanteCapacitacion{" + "sedCap=" + sedCap + ", perId="+ persona.getPerId() + '}';
    }
    
    
}
