package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

public interface UsuarioSessionDao extends GenericDao<UsuarioSession> {

    List<Integer> listarUsuariosCalendario(int orgCod, int funCod);
    
    List<Integer> listarAutoridadesCalendario(int usuCod, char tipAct);
}
