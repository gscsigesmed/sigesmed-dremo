/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;

/**
 *
 * @author gscadmin
 */

@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador")
@Table(name = "trabajador")

public class Trabajador implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tra_id")
    private Integer traId;
    
    @Column(name = "hor_cab_id")
    private Integer horCabId;
    
    @Column(name = "tie_serv")
    private Short tieServ;
    
    @Column(name = "fec_ing")
    @Temporal(TemporalType.DATE)
    private Date fecIng;
    
    @Column(name = "sal")
    private BigDecimal sal;
    
    @Column(name = "tra_tip")
    private String traTip;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Organizacion organizacion;
    
    @JoinColumn(name = "tra_car", referencedColumnName = "crg_tra_ide")
    @ManyToOne(fetch = FetchType.LAZY)
    private TrabajadorCargo trabajadorCargo;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id", referencedColumnName = "per_id")
    private Persona persona; 

    public Trabajador() {
    }

    public Trabajador(Integer traId) {
        this.traId = traId;
    }

    public Integer getTraId() {
        return traId;
    }

    public void setTraId(Integer traId) {
        this.traId = traId;
    }

    public Integer getHorCabId() {
        return horCabId;
    }

    public void setHorCabId(Integer horCabId) {
        this.horCabId = horCabId;
    }

    public Short getTieServ() {
        return tieServ;
    }

    public void setTieServ(Short tieServ) {
        this.tieServ = tieServ;
    }

    public Date getFecIng() {
        return fecIng;
    }

    public void setFecIng(Date fecIng) {
        this.fecIng = fecIng;
    }

    public BigDecimal getSal() {
        return sal;
    }

    public void setSal(BigDecimal sal) {
        this.sal = sal;
    }

    public String getTraTip() {
        return traTip;
    }

    public void setTraTip(String traTip) {
        this.traTip = traTip;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public TrabajadorCargo getTrabajadorCargo() {
        return trabajadorCargo;
    }

    public void setTrabajadorCargo(TrabajadorCargo trabajadorCargo) {
        this.trabajadorCargo = trabajadorCargo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (traId != null ? traId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trabajador)) {
            return false;
        }
        Trabajador other = (Trabajador) object;
        if ((this.traId == null && other.traId != null) || (this.traId != null && !this.traId.equals(other.traId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.Trabajador[ traId=" + traId + " ]";
    }
    
}
