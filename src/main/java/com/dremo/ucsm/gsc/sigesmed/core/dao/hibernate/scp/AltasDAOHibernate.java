/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Altas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BajasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
/**
 *
 * @author Administrador
 */
public class AltasDAOHibernate extends GenericDaoHibernate<Altas> implements AltasDAO  {

    @Override
    public List<Altas> listar_bienes_altas(int org_id) {
        
        List<Altas> altas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT al FROM Altas al JOIN FETCH al.causal_alta JOIN FETCH al.aldet detalle JOIN FETCH detalle.bm bienes JOIN FETCH bienes.dtm JOIN FETCH bienes.ambiente JOIN FETCH bienes.anexo WHERE al.est_reg!='E' and al.org_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",org_id);
            altas = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Mostrar la lista de Bienes de Alta \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes de Alta \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return altas;  
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BajasDetalle baja_alta(int cod_bie) {
         Session session = HibernateUtil.getSessionFactory().openSession();
         BajasDetalle baj_det = null;
        try{
            String hql = "SELECT baj_det FROM BajasDetalle baj_det WHERE baj_det.cod_bie=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",cod_bie);
            baj_det = (BajasDetalle)(query.uniqueResult());
            
            
            
        }catch(Exception e){
            System.out.println("No se pudo Eliminar el Bien de Baja \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes de Alta \\n "+ e.getMessage());

        } finally{
            session.close();
        }
        return baj_det;  
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int contar_altas(int org_id) {
        
         Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
        }catch(Exception e){
            
        }
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
