package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class EditarCapacidadBancoTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EditarCapacidadBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return editarCapacitacion(data);
    }

    private WebResponse editarCapacitacion(JSONObject data) {
        try{
            CapacidadAprendizajeDao capacidadDao = (CapacidadAprendizajeDao) FactoryDao.buildDao("maestro.plan.CapacidadAprendizajeDao");
            CapacidadAprendizaje capacidad = capacidadDao.buscarPorId(data.getInt("cod"));
            capacidad.setNom(data.optString("nom",capacidad.getNom()));
            capacidad.setDes(data.optString("des",capacidad.getDes()));

            capacidad.setFecMod(new Date());

            capacidadDao.update(capacidad);
            return WebResponse.crearWebResponseExito("Se actualizo correctamente la capacidad", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarCapacitacion",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",e.getMessage());
        }
    }
}
