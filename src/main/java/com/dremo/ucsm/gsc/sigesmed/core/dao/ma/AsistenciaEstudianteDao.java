package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.AsistenciaEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.EstudianteAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.JustificacionInasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 11/01/2017.
 */
public interface AsistenciaEstudianteDao extends GenericDao<AsistenciaEstudiante> {
    List<EstudianteAsistencia> listarAsistenciasEstudiantes(int idOrg,int idGrado,char idSeccion,Date fecAsi);
    List<EstudianteAsistencia> listarAsistenciasEstudiantesArea(int idOrg,int idGrado,char idSeccion,int idArea,Date fecAsi);
    Matricula buscarMatricula(int idMatricula);
    AsistenciaEstudiante buscarAsistenciaEstudiante(int id);
    AsistenciaEstudiante buscarAsistenciaEstudiante(int idEst,int idOrg,int idGra, char idSec,int idAre,Date fec);
    AsistenciaEstudiante buscarAsistenciaEstudiante(int idEst,int idOrg,int idGra, char idSec,Date fec);
    void registrarJustificacion(JustificacionInasistencia justificacion);
}
