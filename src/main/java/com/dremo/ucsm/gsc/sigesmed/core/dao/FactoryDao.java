/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.integration.GeneradorClases;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;

/**
 *
 * @author Administrador
 */
public class FactoryDao {
    //nombre de la implementacion actual
    private static final String implementacion = "Hibernate";
    //contexto de la implementacion actual
    private static final String contextoDaoHibernate = "core.dao.hibernate";
    
    public static Object buildDao(String nombreDao){
        try {
            return GeneradorClases.generarClase(Sigesmed.COXTEXTO_PROYECTO+"."+contextoDaoHibernate+"."+nombreDao+implementacion).newInstance();
            
        } catch (InstantiationException | IllegalAccessException  ex) {
            throw new RuntimeException("Error instanciando Clase Dao : "+ex.getMessage());//erro
        }
    }
}
