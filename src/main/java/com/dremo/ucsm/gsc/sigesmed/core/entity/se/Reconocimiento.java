/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "reconocimiento", schema="administrativo")
public class Reconocimiento implements Serializable {
    
    @Id
    @Column(name = "rec_id")
    @SequenceGenerator(name = "secuencia_reconocimiento", sequenceName="administrativo.reconocimiento_rec_id_seq" )
    @GeneratedValue(generator="secuencia_reconocimiento")
    private Integer recId;
    
    @Column(name = "num_res", length=20)
    private String numRes;
    
    @Column(name = "fec_res")
    @Temporal(TemporalType.DATE)
    private Date fecRes;
    
    @Column(name = "tip_mot")
    private Character mot;
    
    @Column(name = "des_mot")
    private Character desMot;
    
    @Column(name = "ent_emi")
    private String entEmi;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public Reconocimiento() {
    }

    public Reconocimiento(Integer recId) {
        this.recId = recId;
    }
    
    public Reconocimiento(FichaEscalafonaria fichaEscalafonaria, Character mot, String numRes, Date fecRes, String entEmi, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.mot = mot;
        this.numRes = numRes;
        this.fecRes = fecRes;
        this.entEmi = entEmi;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getRecId() {
        return recId;
    }

    public void setRecId(Integer recId) {
        this.recId = recId;
    }

    public String getNumRes() {
        return numRes;
    }

    public void setNumRes(String numRes) {
        this.numRes = numRes;
    }

    public Date getFecRes() {
        return fecRes;
    }

    public void setFecRes(Date fecRes) {
        this.fecRes = fecRes;
    }

    public Character getMot() {
        return mot;
    }

    public void setMot(Character mot) {
        this.mot = mot;
    }
    
    public Character getDesMot() {
        return desMot;
    }

    public void setDesMot(Character desMot) {
        this.desMot = desMot;
    }

    public String getEntEmi() {
        return entEmi;
    }

    public void setEntEmi(String entEmi) {
        this.entEmi = entEmi;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recId != null ? recId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reconocimiento)) {
            return false;
        }
        Reconocimiento other = (Reconocimiento) object;
        if ((this.recId == null && other.recId != null) || (this.recId != null && !this.recId.equals(other.recId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.Meritos[ merId=" + recId + " ]";
    }
    
}
