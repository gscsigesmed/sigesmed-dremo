/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sdc;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Documento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface DocumentoDao extends GenericDao<Documento>{
   
   
    public List<ContenidoPlantilla> listarContenidosPlantilla(Plantilla plantillaId);
    public List<Documento> listarDocumentos(Organizacion organizacion);
    public List<ContenidoDocumento> listarContenidosDocumento(Documento documentoId);
   
}