package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrolloId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class EvaluacionDesarrolloDaoHibernate extends GenericDaoHibernate<EvaluacionDesarrollo> implements EvaluacionDesarrolloDao {

    private static final Logger logger = Logger.getLogger(EvaluacionDesarrolloDaoHibernate.class.getName());

    @Override
    public EvaluacionDesarrollo buscarPorId(int desCod, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (EvaluacionDesarrollo) session.get(EvaluacionDesarrollo.class, new EvaluacionDesarrolloId(desCod, perId));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<EvaluacionDesarrollo> buscarParticipantes(int desCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(EvaluacionDesarrollo.class)
                    .add(Restrictions.eq("id.desTemCapId", desCod))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarParticipantes", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Double> obtenerParaCertificacion(int codSed, int perId, double notMin, Character tipDes) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT e.notPar FROM EvaluacionDesarrollo e "
                    + "JOIN e.desarrollo d "
                    + "JOIN d.tema t "
                    + "JOIN e.persona p "
                    + "WHERE e.estReg = :estReg AND e.notPar >= :notPar AND "
                    + "d.tip = :tip AND d.sedCapId = :sedCapId AND "
                    + "p.perId = :perId AND t.estReg != :temEstReg";

            Query query = session.createQuery(hql);
            query.setParameter("estReg", 'C');
            query.setParameter("notPar", notMin);
            query.setParameter("tip", tipDes);
            query.setParameter("sedCapId", codSed);
            query.setParameter("perId", perId);
            query.setParameter("temEstReg", 'E');

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerParaCertificacion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Map<Character, Double> contarReporte(int codSed, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT d.tip, AVG(e.notPar) FROM EvaluacionDesarrollo e "
                    + "JOIN e.desarrollo d "
                    + "JOIN d.tema t "
                    + "JOIN e.persona p "
                    + "WHERE d.sedCapId = :sedCapId AND p.perId = :perId AND t.estReg != :estReg "
                    + "GROUP BY d.tip";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);
            query.setParameter("perId", perId);
            query.setParameter("estReg", 'E');

            List<Object[]> rows = query.list();
            Map<Character, Double> result = new HashMap<>();

            for (Object[] row : rows) {
                result.put(row[0].toString().charAt(0), Double.parseDouble(row[1].toString()));
            }

            return result;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "contarReporte", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Object[]> reporteParticipante(int codSed, int perId, int temCurCapId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT e.notPar, d.nom, d.tip FROM EvaluacionDesarrollo e "
                    + "JOIN e.desarrollo d "
                    + "JOIN d.tema t "
                    + "JOIN e.persona p "
                    + "WHERE d.sedCapId = :sedCapId AND d.temCurCapId = :temCurCapId AND p.perId = :perId AND d.tip != :tip AND t.estReg != :estReg "
                    + "ORDER BY d.tip";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);
            query.setParameter("temCurCapId", temCurCapId);
            query.setParameter("perId", perId);
            query.setParameter("tip", 'C');
            query.setParameter("estReg", 'E');

            List<Object[]> rows = query.list();
            return rows;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "reporteParticipante", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
