package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by geank on 30/09/16.
 */
public class ListarCargosComisionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarCargosComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String com = wr.getMetadataValue("cod");
        return listarCargosDeComision(Integer.parseInt(com));
    }
    public WebResponse listarCargosDeComision(int idCom){
        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            List<CargoDeComision> cargosDeComision = comisionDao.buscarCargosDeComision(idCom);
            JSONArray jsonCargos = new JSONArray();
            for(CargoDeComision cdc : cargosDeComision){
                JSONObject jsonCargo = new JSONObject(EntityUtil.objectToJSONString(new String[]{"carComId","nomCar"},
                        new String[]{"cod","nom"},cdc.getCargoComision()));
                jsonCargo.put("des",cdc.getDesCar());
                jsonCargos.put(jsonCargo);
            }
            return WebResponse.crearWebResponseExito("Exito al listar los cargos",jsonCargos);
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar los cargos",WebResponse.BAD_RESPONSE);
        }
    }

}
