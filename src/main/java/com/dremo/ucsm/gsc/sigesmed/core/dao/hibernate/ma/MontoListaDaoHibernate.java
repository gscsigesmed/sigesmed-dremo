/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import static com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.MepReportDaoHibernate.logger;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.MontoListaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.MontoLista;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.List;
import java.util.logging.Level;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author ucsm
 */
public class MontoListaDaoHibernate extends GenericDaoHibernate<MontoLista> implements MontoListaDao{
    
    @Override
    public MontoLista buscarPorAnio(String anio) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT c FROM MontoLista c WHERE c.anioMonto LIKE upper(:anio)";
            //SQLQuery query = session.createSQLQuery(sql);
  
            Query query = session.createQuery(sql);
            query.setString("anio", anio);
            query.setMaxResults(1);
            return (MontoLista) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public MontoLista buscarPorId(int idMonto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query  = session.createCriteria(MontoLista.class);
            
            query.add(Restrictions.eq("monId",idMonto));
            //Comision comision= (Comision) session.get(Comision.class,idComision);
            return (MontoLista) query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Nivel buscarNivelPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT c FROM Nivel c WHERE c.nivId =:nivel";
            //SQLQuery query = session.createSQLQuery(sql);
  
            Query query = session.createQuery(sql);
            query.setInteger("nivel", id);
            query.setMaxResults(1);
            return (Nivel) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Nivel> listarNiveles() {
        List<Nivel> niveles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try{
            //String hql = "SELECT o FROM MontoLista o WHERE o.estReg!='E'";
            String hql = "SELECT o FROM Nivel o WHERE o.estReg!='E'";
            Query query = session.createQuery(hql);
            niveles = query.list();
        }catch(Exception e){
            System.out.println("No se pudo Listar los niveles" );
            throw e;            
        }
        finally{
            session.close();
        }
        
        return niveles;
    }
    
   
    
}
