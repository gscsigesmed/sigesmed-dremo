/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;


import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;


/**
 *
 * @author Administrador
 */
public class Reporte_Bienes_RecuperadosTx {

    public Reporte_Bienes_RecuperadosTx() {
    }

    public Mitext generar_reporte_recuperados(List<BienesMuebles> bm ,Organizacion org){
      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try{
            
        /* DATOS DE LA ORGANIZACION*/
        
        String nombre_inst = org.getNom();
      //  String ugel = org.getOrganizacionPadre().getNom();
        String denominacion = org.getAli();
        String ubicacion = org.getDir();
        

        /*TITULO*/  
        String titulo = "INVENTARIO FISICO DE BIENES MATERIALES SERVIBLES RECUPERADOS";
        
        
        /*CABECERA DE TABLA*/
        String [] cab = {"Nro","Fecha","Descripcion del Bien","Dimensiones","Ubicacion","Estado"};
        
         /*LLENAMOS LOS DATOS A LA CABECERA DEL REPORTE*/
          Mitext m = null; 
           m = new Mitext(true,titulo);
           m.newLine(2);
           m.agregarParrafo("Nombre de la Institucion : " +nombre_inst);
           m.newLine(3);
           m.agregarParrafo("Ubicacion : " +ubicacion);
           m.newLine(4);
           m.agregarParrafo("Denominacion : " + denominacion);
           m.newLine(5);
      //     m.agregarParrafo("UGEL : " + ugel);
           
            /*CONSTRUIMOS EL OBJETO REPORTE*/
            float columnWidths_1 []= {1,1,2,1,2,1};
            GTabla tabla_1 = new GTabla(columnWidths_1);
            tabla_1.setWidthPercent(100);
            tabla_1.build(cab);
            
             /*INIZIALIZAMOS LA DATA A VACIO*/ 
            int data_length = cab.length;
            String[] archivos_data = new String[data_length];
            for(int i=0;i<data_length;i++){
                archivos_data[i]=" ";
            }
            
            /*LLENAMOS LA DATA A LA TABLA*/  
             GCell[] cell ={tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)
            ,tabla_1.createCellCenter(1,1)};
          
            int size = bm.size();
  
            for(int i=0 ; i<size ; i++){
                BienesMuebles bien_mueble = bm.get(i);
                
                String bien_id = Integer.toString(i);
                String fec_ing = formatter.format(bien_mueble.getFec_reg());
                String des = bien_mueble.getDes_bie();
                String dim = Integer.toString(bien_mueble.getDtm().getDim());
                String ubicac = bien_mueble.getAmbiente().getDes();
                String est_bien = bien_mueble.getEstado_bie();
                
                
                archivos_data[0] = bien_id;
                archivos_data[1] = fec_ing;
                archivos_data[2] = des;
                archivos_data[3] = dim;
                archivos_data[4] = ubicac;
                archivos_data[5] = est_bien;
                        
                 tabla_1.processLineCell(archivos_data,cell);  
            }
           m.agregarTabla(tabla_1);
           
                /*PIE DE PAGINA*/
                        m.newLine(6);
                        m.agregarParrafo("NOTA : * El Presente Formato sera utilizado para Bienes que nos se encuentren en el"
                                + "Catalogo SBN ");
                        m.newLine(7);
                        m.agregarParrafo("** Estado de Conservacion : B=Bueno , M= Malo , R=Regular ,M= Malo , MM = Muy Malo");
           
           return m;
        }catch(Exception e){
              System.out.println(e);
              return null;
        }
        
        
    
    }
}
