/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoRecursosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoRecursos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarRecursosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int recid = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            recid = requestData.getInt("recid");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ProyectoRecursosDao recDao = (ProyectoRecursosDao)FactoryDao.buildDao("smdg.ProyectoRecursosDao");
        try{
            recDao.delete(new ProyectoRecursos(recid));        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Recurso\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Recurso", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Recurso se elimino correctamente");
        //Fin
    }
}
