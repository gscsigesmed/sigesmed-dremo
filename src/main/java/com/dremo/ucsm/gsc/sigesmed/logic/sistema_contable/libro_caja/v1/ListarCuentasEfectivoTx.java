/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarCuentasEfectivoTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
       
        
        List<CuentaContable> cueEfe=null;       
        LibroCajaDao cuentas = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        
        try {
            
                cueEfe= cuentas.listarCuentasEfectivo();
            

            
        } catch (Exception e) {
            System.out.println("No se pudo Listar \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar ", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        int i=0;
        JSONArray miArray = new JSONArray();
        
        for(CuentaContable c:cueEfe ){
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("cuentaContableID",c.getCueConId());
            oResponse.put("numero",c.getNumCue());
            oResponse.put("nombre",c.getNomCue());
            oResponse.put("subClase",c.getSubCla());
            oResponse.put("i",i);

          i++;
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
        
        
        
    }
    
}
