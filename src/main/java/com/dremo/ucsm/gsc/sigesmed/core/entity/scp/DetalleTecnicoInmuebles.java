/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
/**
 *
 * @author Jeferson
 */

@Entity
@Table(name="detalle_tecnico_inmuebles", schema="administrativo")
public class DetalleTecnicoInmuebles {
    
    @Id
    @Column(name="bie_inm_id_")
    private int bie_inm_id_;
    
    
    /*
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="bie_inm_id_", insertable = false , updatable = false)
    private BienesInmuebles bien_inmueble;
    */
    
    @Column(name="det_tec_prop")
    private String det_tec_prop;
    
    @Column(name="tip_terr")
    private String tip_terr;
    
    @Column(name="est_sanea")
    private char est_sanea;
    
    @Column(name="area")
    private int area;
    
    @Column(name="uni_area")
    private String uni_area;
    
    @Column(name="part_elec")
    private String part_elec;
    
    @Column(name="reg_sinabip")
    private String reg_sinabip;
    
    @Column(name="nro_fic_reg")
    private String nro_fic_reg;
    
   /*
    public void setBien_inmueble(BienesInmuebles bien_inmueble) {
        this.bien_inmueble = bien_inmueble;
    }
    */
    public void setDet_tec_prop(String det_tec_prop) {
        this.det_tec_prop = det_tec_prop;
    }

    public void setTip_terr(String tip_terr) {
        this.tip_terr = tip_terr;
    }

    public void setEst_sanea(char est_sanea) {
        this.est_sanea = est_sanea;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setUni_area(String uni_area) {
        this.uni_area = uni_area;
    }

    public void setPart_elec(String part_elec) {
        this.part_elec = part_elec;
    }

    public void setReg_sinabip(String reg_sinabip) {
        this.reg_sinabip = reg_sinabip;
    }

    public void setNro_fic_reg(String nro_fic_reg) {
        this.nro_fic_reg = nro_fic_reg;
    }

   /*
    public BienesInmuebles getBien_inmueble() {
        return bien_inmueble;
    }
    */

    public String getDet_tec_prop() {
        return det_tec_prop;
    }

    public String getTip_terr() {
        return tip_terr;
    }

    public char getEst_sanea() {
        return est_sanea;
    }

    public int getArea() {
        return area;
    }

    public String getUni_area() {
        return uni_area;
    }

    public String getPart_elec() {
        return part_elec;
    }

    public String getReg_sinabip() {
        return reg_sinabip;
    }

    public String getNro_fic_reg() {
        return nro_fic_reg;
    }

    public void setBie_inm_id_(int bie_inm_id_) {
        this.bie_inm_id_ = bie_inm_id_;
    }

    public int getBie_inm_id_() {
        return bie_inm_id_;
    }

    public DetalleTecnicoInmuebles(int bie_inm_id_, String det_tec_prop, String tip_terr, char est_sanea, int area, String uni_area, String part_elec, String reg_sinabip, String nro_fic_reg) {
        this.bie_inm_id_ = bie_inm_id_;
        this.det_tec_prop = det_tec_prop;
        this.tip_terr = tip_terr;
        this.est_sanea = est_sanea;
        this.area = area;
        this.uni_area = uni_area;
        this.part_elec = part_elec;
        this.reg_sinabip = reg_sinabip;
        this.nro_fic_reg = nro_fic_reg;
    }

    public DetalleTecnicoInmuebles() {
    }
    
    
    
}
