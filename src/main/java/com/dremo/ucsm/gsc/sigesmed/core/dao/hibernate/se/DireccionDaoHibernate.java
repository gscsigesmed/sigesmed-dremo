/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class DireccionDaoHibernate extends GenericDaoHibernate<Direccion> implements DireccionDao{

    @Override
    public Direccion buscarPorId(Integer dirId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Direccion dir = (Direccion)session.get(Direccion.class, dirId);
        session.close();
        return dir;
    }

    @Override
    public List<Direccion> listarxPersona(int perId) {
        List<Direccion> direcciones = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT dir from Direccion as dir "
                    + "join fetch dir.persona as per "
                    + "WHERE per.perId=" + perId + " AND dir.estReg='A'";
            Query query = session.createQuery(hql);
            direcciones = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las direcciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las direcciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return direcciones;
    }
    
}
