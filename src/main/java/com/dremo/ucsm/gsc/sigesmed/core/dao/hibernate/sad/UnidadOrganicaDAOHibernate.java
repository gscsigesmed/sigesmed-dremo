/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sad;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author Jeferson
 */
public class UnidadOrganicaDAOHibernate extends GenericDaoHibernate<UnidadOrganica> implements UnidadOrganicaDAO {

    @Override
    public List<UnidadOrganica> buscarPorOrganizacion(int tipoOrganizacionID) {
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UnidadOrganica> buscarPorCodigo(int codigo) {
        List<UnidadOrganica> uni_organicas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT  uo FROM UnidadOrganica uo JOIN FETCH uo.area WHERE uo.uni_org_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", codigo);
            uni_organicas = query.list();
        }catch(Exception e){
             System.out.println("No se pudo Listar las Unidades Organicas buscadas por el codigo \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Unidades Organicas buscadas por el codigo \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        
        return uni_organicas;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Area buscarAreaPorCodigo(int codigo) {
        Area area = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT uo.area FROM UnidadOrganica uo WHERE uo.area.areId=:p1 AND uo.area.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1",codigo);
            area = ((Area)query.uniqueResult());
        }
        catch(Exception e){
               System.out.println("No se pudo Listar las Series Documentales perteneciente a dicha unidad Organica\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Series Documentales perteneciente a dicha unidad Organica \\n "+ e.getMessage()); 
        }
        finally{
            session.close();
        }
        return area;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UnidadOrganica> buscarPorArea(int cod_area) {
        
     
        List<UnidadOrganica> uni_organicas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
         try{
            String hql = "SELECT  uo FROM UnidadOrganica uo WHERE uo.area.areId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", cod_area);
            uni_organicas = query.list();
        }catch(Exception e){
             System.out.println("No se pudo Listar las Unidades Organicas buscaadas por el Area \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Unidades Organicas buscadas por el ArEEea \\n "+ e.getMessage() + "CODIGO DE AREA :" + cod_area );
        }
        finally{
            session.close();
        }
        
        return uni_organicas;
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
