package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class RegistrarIndicadorBancoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarIndicadorBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        IndicadorAprendizaje indicador = new IndicadorAprendizaje(data.getString("nom"),true,null);
        return registrarIndicador(indicador, data.getInt("cod"));
    }

    private WebResponse registrarIndicador(IndicadorAprendizaje indicador, int cod) {
        try{
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            CapacidadAprendizajeDao capacidadDao = (CapacidadAprendizajeDao) FactoryDao.buildDao("maestro.plan.CapacidadAprendizajeDao");

            CapacidadAprendizaje capacidad = capacidadDao.buscarPorId(cod);
            indicador.setCap(capacidad);
            indicadorDao.insert(indicador);
            JSONObject obj = new JSONObject().put("cod",indicador.getIndAprId());
            return WebResponse.crearWebResponseExito("Se inserto correctamente el indicador", obj);
        }catch (Exception e){
            logger.log(Level.SEVERE, "registrarIndicador", e);
            return WebResponse.crearWebResponseError("Error al realizar la insercion", e.getMessage());
        }
    }
}
