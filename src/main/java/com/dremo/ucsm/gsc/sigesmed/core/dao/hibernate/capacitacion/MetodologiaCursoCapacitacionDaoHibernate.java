package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.MetodologiaCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.MetodologiaCursoCapacitacion;

public class MetodologiaCursoCapacitacionDaoHibernate extends GenericDaoHibernate<MetodologiaCursoCapacitacion> implements MetodologiaCursoCapacitacionDao {
    
}
