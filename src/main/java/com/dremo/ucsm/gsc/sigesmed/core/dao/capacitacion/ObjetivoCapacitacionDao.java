package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ObjetivoCapacitacion;
import java.util.List;

public interface ObjetivoCapacitacionDao extends GenericDao<ObjetivoCapacitacion> {
    List<ObjetivoCapacitacion> listarObjetivosCursoCapacitacion(int curCapId);
    ObjetivoCapacitacion buscarPorId(int idObj);
}
