/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.MontoLista;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface MontoListaDao extends GenericDao<MontoLista>{
    MontoLista buscarPorAnio(String anio);
    MontoLista buscarPorId(int idMonto);
    Nivel buscarNivelPorId(int id);
    List<Nivel> listarNiveles();
}
