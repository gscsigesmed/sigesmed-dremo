package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 30/09/16.
 */
public class ActasReunionComisionDaoHibernate extends GenericDaoHibernate<ActasReunionComision> implements ActasReunionComisionDao {
    private static final Logger logger = Logger.getLogger(ActasReunionComisionDaoHibernate.class.getName());
    @Override
    public List<ActasReunionComision> buscarPorReunion(int idReu) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ActasReunionComision.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("reunionComision").add(Restrictions.eq("reuId",idReu));
            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorReunion",e);
            throw e;
        }finally {
            session.close();
        }

    }
    @Override
    public List<ActasReunionComision> buscarPorReunionConAcuerdos(int idReu) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ActasReunionComision.class)
                    .setFetchMode("acuerdos",FetchMode.JOIN)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("reunionComision").add(Restrictions.eq("reuId",idReu))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorReunion",e);
            throw e;
        }finally {
            session.close();
        }

    }

    @Override
    public void registrarAcuerdos(ActasReunionComision acta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.saveOrUpdate(acta);
            tx.commit();

        }catch(Exception e){
            tx.rollback();
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public ActasReunionComision buscarPorId(int idAct) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            ActasReunionComision acta  = (ActasReunionComision)session.get(ActasReunionComision.class, idAct);

            return acta;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ActasReunionComision buscarConAcuerdos(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ActasReunionComision.class)
                    .setFetchMode("acuerdos", FetchMode.JOIN)
                    .add(Restrictions.eq("actComId", id));
            return (ActasReunionComision)query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarConAcuerdos",e);
            throw e;
        }finally {
            session.close();
        }
    }
}
