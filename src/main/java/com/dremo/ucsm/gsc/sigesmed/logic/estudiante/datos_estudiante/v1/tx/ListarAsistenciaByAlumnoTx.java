package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.AsistenciaEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.EstudianteAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.EstudianteAsistenciaModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * @author carlos
 */
public class ListarAsistenciaByAlumnoTx implements ITransaction {
    @Override
    public WebResponse execute(WebRequest wr) {
        
        Long matriculaID;
        Integer areaId;
        Date desde;
        Date hasta;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sfa = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            areaId = requestData.getInt("area");
            matriculaID = requestData.getLong("matriculaID");
            String fecDesde=requestData.getString("desde");
            String fecHasta=requestData.getString("hasta");
            
            desde=sf.parse(fecDesde);
            hasta=sf.parse(fecHasta);
           
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las assitencias, datos incorrectos", e.getMessage() );
        }
        
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        List<EstudianteAsistenciaModel> asistencias=null;
        try{
            asistencias=estudianteDao.buscarAsistenciaByAlumno(matriculaID, areaId, desde, hasta);

            
        }catch (Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listas las Asistencias", e.getMessage() );
        }
        JSONArray aAre = new JSONArray();
        JSONObject oResponse = new JSONObject();
        int i=0;
        int nAsis=0;
        int nTjus=0;
        int nTinj=0;
        int nFjus=0;
        int nFinj=0;
        
        try{
            
            for(EstudianteAsistenciaModel a : asistencias){
                JSONObject oAre = new JSONObject();
                oAre.put("i", ++i );
                oAre.put("fecha", sfa.format(a.getFecAsi() ));
                
                Integer dayOfWeek;
                Calendar c = Calendar.getInstance();
                c.setTime(a.getFecAsi());
                dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                
                switch (dayOfWeek) {
                    case 1:
                        oAre.put("dia", "Domingo" );
                        break;
                    case 2:
                        oAre.put("dia", "Lunes" );
                        break;
                    case 3:
                        oAre.put("dia", "Martes" );
                        break;
                    case 4:
                        oAre.put("dia", "Miercoles" );
                        break;
                    case 5:
                        oAre.put("dia", "Jueves" );
                        break;
                    case 6:
                        oAre.put("dia", "Viernes" );
                        break;
                    case 7:
                        oAre.put("dia", "Sabado" );
                        break;

                }
                
                switch (a.getEstAsi()){
                    case 0:
                        oAre.put("estado","A" );
                        nAsis++;
                        break;
                    case 1:
                        oAre.put("estado", "F");
                        if (a.getJusId() != null) {
                            nFinj++;
                        } else {
                            nFjus++;
                        }

                        break;
                    case 2:  
                        oAre.put("estado","T" );
                        if (a.getJusId() != null) {
                            nTinj++;
                        } else {
                            nTjus++;
                        }
                        break;
                    default: 
                        oAre.put("estado","" );
                        break;
                }
                oAre.put("jus",a.getDesJus() );
                
                aAre.put(oAre);
            }
            oResponse.put("asistencias", aAre);
            JSONObject oResumen = new JSONObject();
            oResumen.put("nAsis", nAsis);
            oResumen.put("nTjus", nTjus);
            oResumen.put("Tinj", nTinj);
            oResumen.put("nFjus", nFjus);
            oResumen.put("Finj", nFinj);
            oResponse.put("resumen", oResumen);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listas las Areas", e.getMessage() );
        }    
           

        return WebResponse.crearWebResponseExito("Se listo correctamente",oResponse);
    
    }
}
