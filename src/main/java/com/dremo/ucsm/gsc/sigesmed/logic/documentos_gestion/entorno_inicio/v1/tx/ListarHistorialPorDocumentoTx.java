/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarHistorialPorDocumentoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int docID=0;
//        int usuarioID=0;
//        int orgID=0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            docID=requestData.getInt("docID");
//            usuarioID=requestData.getInt("usuarioID");
//            orgID=requestData.getInt("orgID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer los datos");
        }
//        
        ItemFileDao itemFileDao = (ItemFileDao) FactoryDao.buildDao("rdg.ItemFileDao");
        ItemFile item= itemFileDao.buscarPorID(docID);
        
        List<ItemFile> historial=itemFileDao.buscarHistorialPorDocumento(item);
        
        JSONArray lista=new JSONArray();
        
        for(ItemFile elem: historial){
            JSONObject temp=new JSONObject();
            temp.put("docID",elem.getIteIde());
            temp.put("verDoc", elem.getIteVer());
            temp.put("nomDoc",elem.getIteNom());
            temp.put("tipoDoc",elem.getTesIde().getTesDes());
            temp.put("tipoDocAli",elem.getTesIde().getTesAli());
            temp.put("urlDoc","archivos/"+elem.getIteUrlDes());
            temp.put("tamDoc",elem.getIteTam());
            temp.put("fecCre",elem.getIteFecCre());
            lista.put(temp);
                    
        }
        return WebResponse.crearWebResponseExito("Se listo el historial correctamente", lista);
    }
    
}
