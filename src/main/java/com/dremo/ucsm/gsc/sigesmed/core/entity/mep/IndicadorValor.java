package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

/**
 * @author geank
 */
public class IndicadorValor {
    private int indicadorId;
    private int valor;

    public IndicadorValor(int indicadorId, int valor) {
        this.indicadorId = indicadorId;
        this.valor = valor;
    }

    public int getIndicadorId() {
        return indicadorId;
    }

    public void setIndicadorId(int indicadorId) {
        this.indicadorId = indicadorId;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
