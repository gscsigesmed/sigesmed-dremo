/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.MontoListaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.MontoLista;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarMontoTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ActualizarMontoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject dataRequest = (JSONObject)wr.getData();
        JSONObject jsonNivel = dataRequest.getJSONObject("niv");
        JSONObject jsonAnio = dataRequest.getJSONObject("ani");
        int nivMonto = jsonNivel.getInt("id");
        String anioMonto = jsonAnio.getString("anio");
        double valMonto = dataRequest.getDouble("val");
        return editarMonto(nivMonto,anioMonto,valMonto);
    }
    public WebResponse editarMonto(int idNiv, String anio, Double valor){
        try{
            MontoListaDao montoListaDao = (MontoListaDao) FactoryDao.buildDao("ma.MontoListaDao");
            MontoLista montoLista = montoListaDao.buscarPorId(idNiv);
            Nivel niv = montoListaDao.buscarNivelPorId(idNiv);
            
            montoLista.setNivel(niv);
            montoLista.setAnioMonto(anio);
            montoLista.setValMonto(valor);
            montoLista.setFecMod(new Date());
            
            montoListaDao.update(montoLista);
            return WebResponse.crearWebResponseExito("La actualizacion se realizo exitosamente",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizar",e);
            return WebResponse.crearWebResponseError("error al realizar la operacion", WebResponse.BAD_RESPONSE);
        }
    }
}
