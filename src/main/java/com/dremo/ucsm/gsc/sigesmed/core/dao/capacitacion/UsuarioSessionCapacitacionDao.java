package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.UsuarioSession;

public interface UsuarioSessionCapacitacionDao extends GenericDao<UsuarioSession> {
    UsuarioSession buscarPorPersona(int perId);
}
