package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class EvaluacionCursoCapacitacionDaoHibernate extends GenericDaoHibernate<EvaluacionCursoCapacitacion> implements EvaluacionCursoCapacitacionDao {

    private static final Logger logger = Logger.getLogger(EvaluacionCursoCapacitacionDaoHibernate.class.getName());

    @Override
    public EvaluacionCursoCapacitacion buscarPorId(int codEva) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionCursoCapacitacion.class)
                    .add(Restrictions.ne("estReg", 'E'))
                    .add(Restrictions.eq("evaCurCapId", codEva));

            return (EvaluacionCursoCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<EvaluacionCursoCapacitacion> buscarPorSede(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionCursoCapacitacion.class)
                    .add(Restrictions.ne("estReg", 'E'))
                    .createCriteria("sede")
                    .add(Restrictions.eq("sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorSede", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<EvaluacionCursoCapacitacion> buscarIniciados(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionCursoCapacitacion.class)
                    .add(Restrictions.ne("estReg", 'E'))
                    .createCriteria("sede")
                    .add(Restrictions.eq("sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorSede", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public int totalEvaluaciones(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionCursoCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'F'))
                    .add(Restrictions.eq("tip", 'P'))
                    .createCriteria("sede")
                    .add(Restrictions.eq("sedCapId", codSed))
                    .setProjection(Projections.rowCount())
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return ((Long) query.uniqueResult()).intValue();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "totalEvaluaciones", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<EvaluacionCursoCapacitacion> listarEncuestas(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionCursoCapacitacion.class)
                    .add(Restrictions.eq("tip", 'E'))
                    .add(Restrictions.ne("estReg", 'E'))
                    .createCriteria("sede")
                    .add(Restrictions.eq("sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarEncuestas", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
