/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;


import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarAmbientesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarBienesInmueblesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarBienesMueblesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarInventarioFisicoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarInventarioInicialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarTipoMovimientoIngresosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarAltasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarAmbientesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarBienInmuebleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarBienMuebleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarCausalAltaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarInventarioFisicoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarInventarioInicialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.RegistrarTipoMovimientoIngresoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ReporteBienesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ReporteInventarioInicialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarAltasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarBajasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarCausalAltaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarCausalBajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ObtenerBienTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarTipoMovimientoIngresosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ReporteBienesCPTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarArchivosBienesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarBienesInmueblesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.EliminarBienInmuebleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ObtenerBienInmuebleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ListarInventariosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ObtenerInventarioInicialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ObtenerInventarioFisicoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.EliminarBienInmuebleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.EliminarBienMuebleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.EliminarInventarioFisicoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.EliminarInventarioInicialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.ObtenerCorrelativoTx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister {

    @Override
    public WebComponent createComponent() {
        
         // Asignando al modulo al cual Pertenece
         WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTROL_PATRIMONIAL);
         
        //Registrando el Nombre del Componente
        component.setName("ingresos");
        //version del componente
        component.setVersion(1);
        
        component.addTransactionDELETE("eliminar_bien_inmueble",EliminarBienInmuebleTx.class);
        component.addTransactionGET("listar_ambientes",ListarAmbientesTx.class);
        component.addTransactionGET("listar_bienes_inmuebles",ListarBienesInmueblesTx.class);
        component.addTransactionGET("listar_bienes_muebles",ListarBienesMueblesTx.class);
        component.addTransactionGET("listar_inventario_fisico",ListarInventarioFisicoTx.class);
        component.addTransactionGET("listar_inventario_inicial",ListarInventarioInicialTx.class);
        component.addTransactionGET("listar_tipo_movimiento_ingresos",ListarTipoMovimientoIngresosTx.class);
        component.addTransactionPOST("registrar_altas",RegistrarAltasTx.class);
        component.addTransactionPOST("registrar_ambientes",RegistrarAmbientesTx.class);
        component.addTransactionPOST("registrar_bien_inmueble",RegistrarBienInmuebleTx.class);
        component.addTransactionPOST("registrar_bien_mueble",RegistrarBienMuebleTx.class);
        component.addTransactionPOST("registrar_causal_alta",RegistrarCausalAltaTx.class);
        component.addTransactionPOST("registrar_inventario_fisico",RegistrarInventarioFisicoTx.class);
        component.addTransactionPOST("registrar_inventario_incial",RegistrarInventarioInicialTx.class);
        component.addTransactionPOST("registrar_tipo_movimiento_ingresos",RegistrarTipoMovimientoIngresoTx.class);
        component.addTransactionGET("reporte_bienes", ReporteBienesTx.class);
        component.addTransactionGET("reporte_inventario_inicial",ReporteInventarioInicialTx.class);
        component.addTransactionGET("listar_altas",ListarAltasTx.class);
        component.addTransactionGET("listar_bajas",ListarBajasTx.class);
        component.addTransactionGET("listar_causal_alta",ListarCausalAltaTx.class);
        component.addTransactionGET("listar_causal_baja",ListarCausalBajaTx.class);
        component.addTransactionGET("obtener_bien",ObtenerBienTx.class);
        component.addTransactionGET("listar_tipo_movimiento",ListarTipoMovimientoIngresosTx.class);
        component.addTransactionGET("reporte_bienes_cp", ReporteBienesCPTx.class);
        component.addTransactionGET("listar_archivos_bienes",ListarArchivosBienesTx.class);
        component.addTransactionGET("obtener_bien_inmueble",ObtenerBienInmuebleTx.class);
        component.addTransactionGET("listar_inventarios",ListarInventariosTx.class);
        component.addTransactionGET("obtener_inventario_inicial",ObtenerInventarioInicialTx.class);
        component.addTransactionGET("obtener_inventario_fisico",ObtenerInventarioFisicoTx.class);
        component.addTransactionDELETE("eliminar_bien_inmueble",EliminarBienInmuebleTx.class);
        component.addTransactionDELETE("eliminar_bien_mueble",EliminarBienMuebleTx.class);
        component.addTransactionDELETE("eliminar_inventario_fisico",EliminarInventarioFisicoTx.class);
        component.addTransactionDELETE("eliminar_inventario_inicial",EliminarInventarioInicialTx.class);
        component.addTransactionGET("obtener_correlativo",ObtenerCorrelativoTx.class);
        
        return component;
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
