/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.generar_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.DocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class TraerPlantilla2EditarTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer plantillaId;
        Plantilla plant=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            plantillaId = requestData.getInt("plantillaID");        
            plant=new Plantilla(plantillaId);
//            plantilla = new Plantilla(plantillaId);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Contenidos de la Plantilla", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<ContenidoPlantilla> contenidosPla = null;
        DocumentoDao documentoDao = (DocumentoDao)FactoryDao.buildDao("sdc.DocumentoDao");
        try{
           
            contenidosPla =documentoDao.listarContenidosPlantilla(plant);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se obtener contenidos de la  Plantilla ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(ContenidoPlantilla contenidos:contenidosPla ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("contenidoPlantillaId",contenidos.getConplaId());;
            oResponse.put("contenidoTipo",contenidos.getConPlaTip());
            oResponse.put("contenido", contenidos.getConPlaCon());
            oResponse.put("letraId", contenidos.getPropiedadLetra().getProLetId());
            oResponse.put("alineacion", contenidos.getAlineacion());
            oResponse.put("isBold", contenidos.getIsBold());
            oResponse.put("isCursiva", contenidos.getIsCursiva());
            oResponse.put("isSubrayado", contenidos.getIsSubrayado());
            oResponse.put("tamanho", contenidos.getTamanho());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

