/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abel
 */
public class RegistrarConfiguracionNotasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        try{
        
        String mensaje= "Se realizo  la Configuracion de Nota Correctamente" ;
        JSONObject data = (JSONObject) wr.getData();
        
        String not_lit = data.getString("lit");
        String not_min = data.getString("min");
        String not_max = data.getString("max");
        String des = data.getString("des");
        int usu_mod = data.getInt("usu_mod");
      
        String isupdate = data.optString("act");
        
        
          ConfiguracionNotaDao conf_not = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
        
        if(isupdate.equals("R")){
            ConfiguracionNota conf_nota = new ConfiguracionNota(not_lit,not_min,not_max,new Date(),usu_mod,'A',des );
            conf_not.insert(conf_nota);
        }
        if(isupdate.equals("A")){
            ConfiguracionNota conf_nota = new ConfiguracionNota(not_lit,not_min,not_max,new Date(),usu_mod,'A',des );
            conf_not.update(conf_nota);
        }
        if(isupdate.equals("E")){
            ConfiguracionNota conf_nota = new ConfiguracionNota(not_lit,not_min,not_max,new Date(),usu_mod,'E',des );
            conf_not.update(conf_nota);
        }
        return WebResponse.crearWebResponseExito(mensaje);
        
        
        } catch(Exception e){
            System.out.println("No se pudo registrar la configuracion de notas\n"+e);
            return WebResponse.crearWebResponseError("ERROR con la solicitud de operacion para la configuracion de notas", e.getMessage() );  
        }
        
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
