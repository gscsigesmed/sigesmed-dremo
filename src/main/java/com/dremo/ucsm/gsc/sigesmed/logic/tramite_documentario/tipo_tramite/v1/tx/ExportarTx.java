/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.tipo_tramite.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public class ExportarTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String nombre = "";
        String organizacion = "";
        String descripcion = "";
        double costo = 0;
        int duracion = 0;
        String codigo = "";
        
        List<String> requisitos = new ArrayList<String>();
        
        try{
            JSONObject rD = (JSONObject)wr.getData();
            
            nombre = rD.getString("nombre");
            organizacion = rD.getString("tipoOrganizacion");
            descripcion = rD.getString("descripcion");
            duracion = rD.getInt("duracion");
            costo = rD.getDouble("costo");
            codigo = rD.getString("codigo");
            
            JSONArray listaRequisitos = rD.getJSONArray("requisitos");
            
            if(listaRequisitos.length() > 0){
                for(int i = 0; i < listaRequisitos.length();i++){
                    JSONObject bo =listaRequisitos.getJSONObject(i);
                    requisitos.add(bo.getString("descripcion"));
                }
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo exportar tramite, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
            float alto = 810f;
            
            r.agregarTitulo("TRAMITE : " +nombre );
            
            r.agregarSubtitulos(response);
            
            JSONObject or = new JSONObject();
            
            or.put("Codigo", codigo);
            or.put("Organizacion", organizacion);
            or.put("Costo", ""+costo);
            or.put("Duracino", ""+duracion);
            or.put("Descripcion", descripcion);
            or.put("", "");
            or.put("Requisitos", "");            
            r.agregarSubtitulos(or);
            
            float[] cols = {1f,6f};
            GTabla tablaReq = new GTabla(cols);
            
            String[] labels = {"N°","Descripcion"};
            tablaReq.build(labels);
            int i=1;
            for(String d : requisitos){                
                String[] fila = new String [2];
                fila[0] = ""+ i++;
                fila[1] = d;
                tablaReq.processLine(fila);
            }            
            r.agregarTabla(tablaReq);
            
            r.cerrarDocumento();
            response.append("tramite", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ExportarTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("Se pudo exportar tramite",response);
        //Fin
    }
    
}

