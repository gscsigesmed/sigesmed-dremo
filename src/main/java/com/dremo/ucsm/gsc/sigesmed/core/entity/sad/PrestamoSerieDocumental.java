/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="prestamo_serie_documental" , schema="administrativo")

public class PrestamoSerieDocumental implements java.io.Serializable {
    
    @Id
    @Column(name="pre_ser_doc_id" , unique=true , nullable=false)
    @SequenceGenerator(name="secuencia_pres_ser_doc" , sequenceName="administrativo.prestamo_serie_documental_pre_ser_doc_id_seq")
    @GeneratedValue(generator="secuencia_pres_ser_doc")
    private int pre_ser_doc_id;
    
    @Id
    @Column(name="inv_tra_id" , unique=true , nullable=false)
    private int inv_tra_id;
    @Column(name="ser_doc_id" , unique=true , nullable=false)
    private int ser_doc_id;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="inv_tra_id",referencedColumnName="inv_tra_id",insertable=false,updatable=false),
        @JoinColumn(name="ser_doc_id",referencedColumnName="ser_doc_id",insertable=false,updatable=false)
    })
    private InventarioTransferencia inv_tra;
  //  private SerieDocumental serie_documental;
    
    
    @Column(name="obs")
    private String obs;
    
    @Column(name="dni")
    private String dni;
    
    @Column(name="nom")
    private String nom;
    
    /*
    @Column(name="org")
    private String org;
    */
    
    @Column(name="fec_dev")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fec_dev;
    
    @Column(name="fec_pre")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fec_pre;
    
    public PrestamoSerieDocumental(){
        
    }
    public PrestamoSerieDocumental(int pres_ser_doc_id){
        this.pre_ser_doc_id = pres_ser_doc_id;
    }
    public PrestamoSerieDocumental(int pres_ser_doc_id,int inv_tran_id){
        this.pre_ser_doc_id = pres_ser_doc_id;
        this.inv_tra_id = inv_tran_id;
        
        inv_tra = new InventarioTransferencia(inv_tran_id);
    }
    public PrestamoSerieDocumental(int pres_ser_doc_id,int inv_tran_id,int ser_doc_id){
        this.pre_ser_doc_id = pres_ser_doc_id;
        this.inv_tra_id = inv_tran_id;
        this.ser_doc_id = ser_doc_id;
        
        inv_tra = new InventarioTransferencia(inv_tran_id);
       // serie_documental = new SerieDocumental(ser_doc_id);
    }
    public PrestamoSerieDocumental(int pres_ser_doc_id,int inv_tran_id,int ser_doc_id,String obs,String dni,String nom ,Date fec_dev,Date fec_pre){
        this.pre_ser_doc_id = pres_ser_doc_id;
        this.inv_tra_id = inv_tran_id;
        this.ser_doc_id = ser_doc_id;
        
        this.obs = obs;
        this.dni = dni;
        this.nom = nom;
    //    this.org = org;
        this.fec_dev = fec_dev;
        this.fec_pre = fec_pre;
        
        inv_tra = new InventarioTransferencia(inv_tran_id);
      //  serie_documental = new SerieDocumental(ser_doc_id);
        
    }
    public void setPrestamoSerieDocumentalId(int pre_inv_ser_doc_id){
        this.pre_ser_doc_id = pre_inv_ser_doc_id;
    }
    public int getPrestamoSerieDocumentalId(){
        return this.pre_ser_doc_id;
    }
    public void setInvTrans(InventarioTransferencia inv_trans){
        this.inv_tra=inv_trans;
    }
    public InventarioTransferencia getInvTrans(){
        return this.inv_tra;
    }
    /*
    public void setSerieDoc(SerieDocumental serie){
        this.serie_documental=serie;
    }
    public SerieDocumental getSerieDoc(){
        return this.serie_documental;
    }
    */
    public void setObservacion(String obs){
        this.obs=obs;
    }
    public String getObservacion(){
        return this.obs;
    }
    public void setdni(String dni){
        this.dni = dni;
    }
    public String getdni(){
        return this.dni;
    }
    public void setnombre(String nom){
        this.nom = nom;
    }
    public String getnombre(){
        return this.nom;
    }
    public void setFecDev(Date fec_dev){
        this.fec_dev = fec_dev;
    }
    public Date getFecDev(){
        return this.fec_dev;
    }
    public void setFecPres(Date fec_pres){
        this.fec_pre = fec_pres;
    }
    public Date getFecPres(){
        return this.fec_pre;
    }
    public int getInvTransId(){
        return this.inv_tra_id;
    }
    public void setInvTransId(int inv_id){
        this.inv_tra_id = inv_id;
    }
    public int getSerDocId(){
        return this.ser_doc_id;
    }
    public void setSerDodcId(int ser_doc_id){
        this.ser_doc_id = ser_doc_id;
    }
    
    
    public void setPresSerDocId(int pre_ser_id){
        this.pre_ser_doc_id = pre_ser_id;
    }
            
    
}
