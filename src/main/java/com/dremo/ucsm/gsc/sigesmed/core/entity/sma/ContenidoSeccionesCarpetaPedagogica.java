/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "contenido_secciones_carpeta_pedagogica", schema = "pedagogico")

public class ContenidoSeccionesCarpetaPedagogica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "con_sec_car_ped_id")
    private Integer conSecCarPedId;
    @Column(name = "nom")
    private String nom;
    @Column(name = "tip")
    private String tip;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contenidoSeccionesCarpetaPedagogica", fetch = FetchType.LAZY)
    private List<DocenteCarpetaPedagogica> docenteCarpetaPedagogicaList;
    @JoinColumn(name = "sec_car_ped_id", referencedColumnName = "sec_car_ped_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SeccionCarpetaPedagogica secCarPedId;

    public ContenidoSeccionesCarpetaPedagogica() {
    }

    public ContenidoSeccionesCarpetaPedagogica(Integer conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public Integer getConSecCarPedId() {
        return conSecCarPedId;
    }

    public void setConSecCarPedId(Integer conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    @XmlTransient
    public List<DocenteCarpetaPedagogica> getDocenteCarpetaPedagogicaList() {
        return docenteCarpetaPedagogicaList;
    }

    public void setDocenteCarpetaPedagogicaList(List<DocenteCarpetaPedagogica> docenteCarpetaPedagogicaList) {
        this.docenteCarpetaPedagogicaList = docenteCarpetaPedagogicaList;
    }

    public SeccionCarpetaPedagogica getSecCarPedId() {
        return secCarPedId;
    }

    public void setSecCarPedId(SeccionCarpetaPedagogica secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conSecCarPedId != null ? conSecCarPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContenidoSeccionesCarpetaPedagogica)) {
            return false;
        }
        ContenidoSeccionesCarpetaPedagogica other = (ContenidoSeccionesCarpetaPedagogica) object;
        if ((this.conSecCarPedId == null && other.conSecCarPedId != null) || (this.conSecCarPedId != null && !this.conSecCarPedId.equals(other.conSecCarPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.ContenidoSeccionesCarpetaPedagogica[ conSecCarPedId=" + conSecCarPedId + " ]";
    }
    
}
