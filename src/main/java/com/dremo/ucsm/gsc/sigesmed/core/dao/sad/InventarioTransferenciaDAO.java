/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.DetalleInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public interface InventarioTransferenciaDAO extends GenericDao<InventarioTransferencia> {
    
    public List<InventarioTransferencia> buscarPorOrganizacion();
    public List<InventarioTransferencia> buscarPorInventarioTransferencia();
    public List<InventarioTransferencia> buscarPorInventarioTransferencia(int inv_tra_id);
    public List<InventarioTransferencia> buscarPorSerieDoc(int ser_doc_id);
    public void registrarDocumentos(List documentos);
    /*Agregado */
    public void registrarDetalleIventarioTrans(DetalleInventarioTransferencia dit);
    public InventarioTransferencia buscarUltimoInventario(String nom_ult_inv);
 //   public void registrarArchivosInventarioTrans(DetalleInventarioTransferencia dit);
    
}
