/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.EvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Administrador
 */
public class EvaluacionPersonalDaoHibernate extends GenericDaoHibernate<ResumenEvaluacionPersonal> implements EvaluacionPersonalDao{
     private static Logger  logger = Logger.getLogger(EvaluacionPersonalDaoHibernate.class.getName());
    @Override
    public List<ResumenEvaluacionPersonal> getResumenEvaluacion(int idTrab){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ResumenEvaluacionPersonal.class);
            Criteria fCriteria = criteria.createCriteria("trabajador");
            fCriteria.add(Restrictions.eq("traId", idTrab));
            List<ResumenEvaluacionPersonal> result = criteria.list();
            return result;
        }catch(HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getResumenEvaluacion()",e);
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": getResumenEvaluacion()",e);
        }finally{
            session.close();
        }
        return null;
    }
    @Override
    public List<EvaluacionesPersonal> getCantidadEvaluacionesTrabajador(int idOrganizacion){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            Criteria criteria = session.createCriteria(Trabajador.class);
            //
            criteria.createAlias("persona", "p", JoinType.INNER_JOIN);
            criteria.createAlias("detallesEvaluacionPersonal", "d",JoinType.FULL_JOIN);
            criteria.createAlias("traCar", "car",JoinType.INNER_JOIN);
            criteria.setProjection(Projections.projectionList()
                    //.add(Projections.property("organizacion"))
                    //.add(Projections.property("p.nom"), "nom")
                    //.add(Projections.property("p.apeMat"), "apeMat")
                    //.add(Projections.property("p.apePat"), "apePat")
                    //.add(Projections.property("tieServ"), "tieServ")
                    .add(Projections.groupProperty("organizacion"), "organizacion")
                    .add(Projections.groupProperty("traId"), "traId")
                    .add(Projections.groupProperty("p.perId"), "perId")
                    .add(Projections.groupProperty("p.nom"), "nom")
                    .add(Projections.groupProperty("p.apeMat"), "apeMat")
                    .add(Projections.groupProperty("p.apePat"), "apePat")
                    //.add(Projections.groupProperty("tieServ"), "tieServ")
                    .add(Projections.groupProperty("car.crgTraNom"), "traCar")
                    .add(Projections.count("d.trabajador"), "evaluaciones")
            );
            Organizacion org = (Organizacion) session.get(Organizacion.class,idOrganizacion);
            Criteria fCriteria = criteria.createCriteria("organizacion");
            switch (org.getTipoOrganizacion().getTipOrgId()){
                case 3:
                    fCriteria.createCriteria("organizacionPadre").add(Restrictions.eq("orgId",idOrganizacion));break;
                case 4:
                    fCriteria.add(Restrictions.eq("orgId", idOrganizacion));break;

            }
            criteria.addOrder(Order.asc("traId"));
            criteria.setResultTransformer(Transformers.aliasToBean(EvaluacionesPersonal.class));
            List<EvaluacionesPersonal> result = criteria.list();
            return result;
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": getCantidadEvaluacionesTrabajador()",e);
        }finally{
            session.close();
        }
        return null;
    }
    @Override
    public ResumenEvaluacionPersonal getResumentEvaluacionById(int idResumen){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ResumenEvaluacionPersonal.class);
            criteria.add(Restrictions.eq("resEvaPerId",idResumen));
            criteria.setFetchMode("trabajador.traCar",FetchMode.JOIN);
            criteria.setFetchMode("trabajador",FetchMode.JOIN);
            criteria.setFetchMode("trabajador.persona",FetchMode.JOIN);
            criteria.setFetchMode("trabajador.organizacion",FetchMode.JOIN);
            ResumenEvaluacionPersonal resumen = (ResumenEvaluacionPersonal)criteria.uniqueResult();
            return resumen;
        }catch (HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getResumentEvaluacionById()",e);
            return null;
        }finally {
            session.close();
        }
    }
    @Override
    public List<Object[]> getDominiosEvaluacionDocente(int idResumen){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List result = null;
        try{
            String sql = "select c.con_fic_eva_id,d.sub_tot,d.pun from pedagogico.contenido_ficha_evaluacion c inner join pedagogico.detalle_resume_evaluacion_personal d on d.id_con = c.con_fic_eva_id where d.det_eva_per_id = :resumen_id and c.tip = 'd';";
            SQLQuery query = session.createSQLQuery(sql);
            query.setParameter("resumen_id",idResumen);
            result = query.list();
            logger.log(Level.INFO,"size: {0}",result.size());
        }catch (Exception e){

        }finally {
            session.close();
        }
        return result;

    }
    @Override
    public Object[] getDetalleEvaluacionByContenido(int idResumen,int idCont){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object[] result = null;
        try{
            String sql = "select * from pedagogico.detalle_resume_evaluacion_personal d where d.det_eva_per_id = ? and d.id_con = ?;";
            SQLQuery query = session.createSQLQuery(sql);
            query.setParameter(0,idResumen);
            query.setParameter(1,idCont);
            result = (Object[])query.uniqueResult();
        }catch(Exception e){
            return null;
        }finally {
            session.close();
        }
        return result;
    }
    @Override
    public List<Object> getEscalasByRol(String tipoTra){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(EscalaValoracionFicha.class);
            Criteria auxCriteria = criteria.createCriteria("fichaEvaluacionPersonal");
            auxCriteria.createCriteria("tipTra").add(Restrictions.like("carNom", tipoTra));
            criteria.add(Restrictions.eq("estReg",'A'));
            criteria.addOrder(Order.asc("val"));

            List<Object> escalas = criteria.list();
            return escalas;
        }catch (HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getResumentEvaluacionById()",e);
            return null;
        }finally {
            session.close();
        }
    }
    @Override
    public List<Object> getDetalleFichaEvaluacion(int idResumen){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            Criteria criteria = session.createCriteria(DetalleResumenEvaluacionPersonal.class);
            Criteria rcriteria = criteria.createCriteria("resEvaPer");
            rcriteria.add(Restrictions.eq("resEvaPerId",idResumen));
            criteria.addOrder(Order.desc("tip"));
            List<Object> ldetalle = criteria.list();
            return ldetalle;
        }catch(Exception e){
             logger.log(Level.SEVERE,logger.getName() + ": getDetalleFichaEvaluacion()",e);
        }finally{
            session.close();
        }
        return null;
    }
    @Override
    public List<IndicadoresEvaluarPersonal> getIndicadoresEvaluadosResumen(int idResumen){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            Criteria criteria = session.createCriteria(IndicadoresEvaluarPersonal.class);
            Criteria rcriteria = criteria.createCriteria("resumenEvaluacionPersonal");
            rcriteria.add(Restrictions.eq("resEvaPerId",idResumen));
            criteria.setFetchMode("indicadorFichaEvaluacion",FetchMode.JOIN);
            criteria.createAlias("indicadorFichaEvaluacion","in",JoinType.INNER_JOIN);
            criteria.createAlias("in.contenidoFichaEvaluacion","con",JoinType.INNER_JOIN);
            List<IndicadoresEvaluarPersonal> lindicadores = criteria.list();
            return lindicadores;
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": getIndicadoresEvaluadosResumen()",e);
        }finally{
            session.close();
        }
        return null;
    }
    @Override
    public Trabajador getTrabajadorById(int id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Trabajador tra = (Trabajador)session.get(Trabajador.class, id);
            return  tra;
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":getTrabajadorById()",e);
            return null;
        }finally {
            session.close();
        }

    }
    @Override
    public List<PuntajesEvaluacionTrabajador> getPuntajesEvaluacionTrabajador(int trabajadorId){
        List<PuntajesEvaluacionTrabajador> puntajes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(ResumenEvaluacionPersonal.class);
            criteria.createCriteria("trabajador").add(Restrictions.eq("traId",trabajadorId));
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.property("pun"),"pun")
                    .add(Projections.property("fecEva"),"fecEva"))
                    .addOrder(Order.asc("fecEva"))
                    .setResultTransformer(Transformers.aliasToBean(PuntajesEvaluacionTrabajador.class));
            //criteria.setResultTransformer(Transformers.aliasToBean(EvaluacionesPersonal.class));
            puntajes = criteria.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":getPuntajesEvaluacionTrabajador()",e);
        }finally {
            session.close();
        }
        return puntajes;
    }
    @Override
    public List<ResultadoEstadistica> getPuntajeAgrupadoTrabajadores(int idOrg){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ResumenEvaluacionPersonal.class);
            criteria.createAlias("trabajador","t",JoinType.INNER_JOIN);
            criteria.createAlias("trabajador.traCar","car",JoinType.INNER_JOIN);
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("car.crgTraNom"), "clave")
                    .add(Projections.avg("pun"), "valor"));
            Organizacion org = (Organizacion)session.get(Organizacion.class,idOrg);
            switch (org.getTipoOrganizacion().getTipOrgId()){
                case 3:
                    criteria.createCriteria("t.organizacion").createCriteria("organizacionPadre").add(Restrictions.eq("orgId",idOrg));
                    break;
                case 4:
                    criteria.createCriteria("t.organizacion").add(Restrictions.eq("orgId",idOrg));
                    break;
            }
            criteria.setResultTransformer(Transformers.aliasToBean(ResultadoEstadistica.class));
            List<ResultadoEstadistica> result = criteria.list();
            return result;
            /*.add(Projections.groupProperty("traId"),"traId")
                    .add(Projections.groupProperty("p.perId"),"perId")
                    .add(Projections.count("d.trabajador"),"evaluaciones")*/
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":getPuntajeAgrupadoTrabajadores()",e);
        }finally {
            session.close();
        }
        return null;
    }
    @Override
    public List<ResumenEvaluacionPersonal> getEvaluacionesCompartidas(int idTrabajador,int idOrg){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ResumenEvaluacionPersonal.class)
                   .add(Restrictions.eq("esCom", true));

            Criteria ctrTrab = criteria.createCriteria("trabajador");
            ctrTrab.createCriteria("organizacion").add(Restrictions.eq("orgId",idOrg));
            ctrTrab.createCriteria("persona").add(Restrictions.eq("perId",idTrabajador));

            List<ResumenEvaluacionPersonal> evaluaciones = criteria.list();
            return evaluaciones;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }

    }
    @Override
    public boolean registrarEvaluacionFicha(ResumenEvaluacionPersonal resumen, Set<IndicadorValor> indicadores, Set<DetalleResumenEvaluacionPersonal> detalleResumen){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            Iterator<DetalleResumenEvaluacionPersonal> dIter = detalleResumen.iterator();
            while(dIter.hasNext()){
                DetalleResumenEvaluacionPersonal dre = dIter.next();
                dre.setResEvaPer(resumen);
                resumen.getDetallesResumen().add(dre);
            }
            session.persist(resumen);
            Iterator<IndicadorValor> iter = indicadores.iterator();
            while(iter.hasNext()){
                IndicadorValor ind = iter.next();
                IndicadorFichaEvaluacion indicador = (IndicadorFichaEvaluacion )session.get(IndicadorFichaEvaluacion.class,ind.getIndicadorId());
                IndicadoresEvaluarPersonal iep = new IndicadoresEvaluarPersonal(resumen,indicador,ind.getValor());
                session.persist(iep);
            }
            tx.commit();
            return true;
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":registrarEvaluacionFicha()",e);
            tx.rollback();
            return false;
        }finally {
            session.close();
        }
    }
    @Override
    public boolean compartirResultado(int idResumen){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try {

            ResumenEvaluacionPersonal resumen = (ResumenEvaluacionPersonal)session.get(ResumenEvaluacionPersonal.class,idResumen);
            resumen.setEsCom(true);
            session.update(resumen);
            tx.commit();
            return true;
        }catch (HibernateException e){
            tx.rollback();
        }finally {
            session.close();
        }
        return false;
    }

    @Override
    public Trabajador buscarTrabajadorPorPersona(int idPersona) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(Trabajador.class)
                    .setFetchMode("traCar",FetchMode.JOIN)
                    .createCriteria("persona").add(Restrictions.eq("perId",idPersona))
                    .setMaxResults(1);

            return (Trabajador)criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
