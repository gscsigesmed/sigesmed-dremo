package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class EditarActaReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EditarActaReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        return editarActa((JSONObject)wr.getData());
    }
    private WebResponse editarActa(JSONObject data){
        try{
            ActasReunionComisionDao actasReunionComisionDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
            ActasReunionComision acta = actasReunionComisionDao.buscarPorId(data.getInt("cod"));
            acta.setDes(data.getString("des"));
            acta.setFecMod(new Date());
            actasReunionComisionDao.update(acta);
            return WebResponse.crearWebResponseExito("Exito al editar el acta",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE, "editarActa",e);
            return WebResponse.crearWebResponseError("Error al editar el acta",WebResponse.BAD_RESPONSE);
        }
    }
}