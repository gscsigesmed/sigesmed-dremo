/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface DirectorioExternoDao extends GenericDao<DirectorioExterno>{
    public String buscarUltimoCodigo();    
    public List ListarDirectorioExterno(String s);            
    //public List<DirectorioExterno> listarDirectorioExterno();
    //public boolean eliminarxId(Integer dexId);
}
