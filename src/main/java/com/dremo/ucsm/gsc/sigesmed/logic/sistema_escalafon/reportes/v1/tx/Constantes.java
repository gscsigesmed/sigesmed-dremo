/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

/**
 *
 * @author Felipe
 */
public class Constantes {
    public static final int indice_datosPersonales = 0;
    public static final int datos_familiares = 1;
    public static final int formaciones_educativas= 2;
    public static final int info_colegiaturas = 3;
    public static final int estudios_especializacion = 4;
    public static final int conocimientos_informaticos = 5;
    public static final int info_idiomas = 6;
    public static final int exposiciones_ponencias = 7;
    public static final int info_publicaciones = 8;
    public static final int cargos_desempenados = 9;
    public static final int meritos_felicitaciones_reconocimientos = 10;
    public static final int info_bonificaciones = 11;
    public static final int info_demeritos = 12;
    public static final int anios_estudio_reconocimiento = 13;
    public static final int info_ascensos = 14;

}
