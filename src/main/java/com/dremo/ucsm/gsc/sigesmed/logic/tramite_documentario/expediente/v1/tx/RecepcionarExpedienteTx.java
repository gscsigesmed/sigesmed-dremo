/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;
import org.json.JSONArray;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class RecepcionarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        List<HistorialExpediente> historiales = new ArrayList<HistorialExpediente>();
        try{
            JSONArray requestData = (JSONArray)wr.getData();            
            Date hoy = new Date();
            
            for( int i = 0 ; i < requestData.length(); i++){
                JSONObject bo = requestData.getJSONObject(i);
                HistorialExpediente nuevo = new HistorialExpediente(bo.getInt("historialID"),bo.getInt("expedienteID"));
                nuevo.setFecRec(hoy);
                nuevo.setResId(bo.getInt("responsableID"));
                nuevo.setEstadoId(EstadoExpediente.RECIBIDO);
                historiales.add(nuevo);
            }            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar los historiales, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            historialDao.actualizarEstadoVarios(historiales,EstadoExpediente.NUEVO);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar los historiales", e.getMessage() );
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        
        JSONArray aHistoriales = new JSONArray();
        for(HistorialExpediente h: historiales){
            JSONObject oHistorial = new JSONObject();
            oHistorial.put("historialID",h.getHisExpId());
            //oHistorial.put("observacion",h.getObservacion() );
            //oHistorial.put("areaID",h.getAreaId());
            //oHistorial.put("area",h.getArea().getNom());
            
            //oHistorial.put("expedienteID",h.getExpediente().getExpId());
            //oHistorial.put("codigo",h.getExpediente().getCodigo());
            
            oHistorial.put("estadoID",h.getEstadoId());
            oHistorial.put("estado",EstadoExpediente.toString(h.getEstadoId()));
            
            //oHistorial.put("tipoTramiteID",h.getExpediente().getTipoTramiteId());
            
            oHistorial.put("fechaRecepcion",sdf.format(h.getFecRec()));
            
            //oHistorial.put("responsableID",h.getResId());
            aHistoriales.put(oHistorial);
        }
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Se actualizo correctamente el estado de los historias",aHistoriales);
        //Fin
    }
    
}
