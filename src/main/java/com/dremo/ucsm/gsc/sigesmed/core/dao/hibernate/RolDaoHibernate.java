/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.RolDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class RolDaoHibernate extends GenericDaoHibernate<Rol> implements RolDao {


    @Override
    public List<Rol> buscarConFunciones() {
        List<Rol> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Roles
            String hql = "SELECT DISTINCT r FROM Rol r LEFT JOIN FETCH r.rolFunciones rf WHERE r.estReg!='E' ORDER BY rf.num" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Rolos \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Roles \n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<Rol> buscarPorOrganizacion(int orgID) {
        List<Rol> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Roles
            String hql = "SELECT to.roles FROM TipoOrganizacion to, Organizacion o WHERE to = o.tipoOrganizacion and o.orgId=:p1" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Rolos \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Roles \n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void eliminarFunciones(int rolID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "DELETE FROM rol_funcion WHERE rol_id="+rolID;
            SQLQuery query = session.createSQLQuery(hql);
            query.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo Eliminar las Funciones \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Eliminar las Funciones \n "+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
}
