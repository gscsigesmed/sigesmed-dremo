/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class EstadisticaTipoTramiteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<EntidadCantidadModel> expedientes = null;
        List<EntidadCantidadModel> finalizados = null;
        List<EntidadCantidadModel> entregados = null;
        ExpedienteDao expDao = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        try{
            expedientes = expDao.cantidadTipoExpedientesPorOrganizacionYFecha(organizacionID, desde, hasta);
            finalizados = expDao.cantidadTipoExpedientesFinalizadosPorOrganizacionYFecha(organizacionID, desde, hasta);
            entregados = expDao.cantidadTipoExpedientesEntregadosPorOrganizacionYFecha(organizacionID, desde, hasta);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray aAreas = new JSONArray();
        JSONArray aExpedientes = new JSONArray();
        JSONArray aFinalizados = new JSONArray();
        JSONArray aEntregados = new JSONArray();
        
        boolean encontro = false;
        for(EntidadCantidadModel a:expedientes){
            aAreas.put(a.nombre);
            aExpedientes.put(a.num1);
            
            for(EntidadCantidadModel e :finalizados)
                if( e.ID == a.ID ){
                    aFinalizados.put(e.num1);
                    encontro = true;
                    break;
                }
            if(!encontro)
                aFinalizados.put(0);
            encontro = false;
            
            for(EntidadCantidadModel e :entregados)
                if( e.ID == a.ID ){
                    aEntregados.put(e.num1);
                    encontro = true;
                    break;
                }
            if(!encontro)
                aEntregados.put(0);
            encontro = false;
            
        }
        
        JSONObject res = new JSONObject();
        res.put("labels", aAreas);
        res.put("expedientes", aExpedientes);
        res.put("finalizados", aFinalizados);
        res.put("entregados", aEntregados);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",res);        
        //Fin
    }
    
}

