/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Administrador
 */

public class ParientesPK implements Serializable {
    
    @Column(name = "par_id")
    private long parId;
    
    @Column(name = "per_id")
    private long perId;

    public ParientesPK() {
    }

    public ParientesPK(long parId, long perId) {
        this.parId = parId;
        this.perId = perId;
    }

    public long getParId() {
        return parId;
    }

    public void setParId(long parId) {
        this.parId = parId;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) parId;
        hash += (int) perId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParientesPK)) {
            return false;
        }
        ParientesPK other = (ParientesPK) object;
        if (this.parId != other.parId) {
            return false;
        }
        if (this.perId != other.perId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.ParientePK[ parId=" + parId + ", perId=" + perId + " ]";
    }
    
}
