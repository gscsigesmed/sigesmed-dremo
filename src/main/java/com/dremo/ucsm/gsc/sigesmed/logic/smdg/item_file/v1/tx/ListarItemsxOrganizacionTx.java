/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarItemsxOrganizacionTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("orgId");
                
        List<ItemFile> items = null;
        ItemFileDao itemsDao = (ItemFileDao)FactoryDao.buildDao("smdg.ItemFileDao");
        
        try{
            items = itemsDao.ListarxOrganizacion(orgId);
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar el los items ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(ItemFile i:items ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("iteide",i.getIteIde());
            oResponse.put("itenom",i.getIteNom());
            oResponse.put("itetam",i.getIteTam());
            oResponse.put("itever",i.getIteVer());
            oResponse.put("itefec",i.getIteFecCre());
            
            String nombres = "";
            nombres = i.getTrabajador().getPersona().getNom() +" "+ i.getTrabajador().getPersona().getApePat() +" "+ i.getTrabajador().getPersona().getApeMat();
            oResponse.put("iteusu",nombres);
            oResponse.put("iteusuide",i.getTrabajador().getTraId());            
            oResponse.put("iteusucar",i.getTrabajador().getTraCar().getCrgTraNom());
            oResponse.put("iteorgnom",i.getOrganizacion().getNom());
            oResponse.put("iteorgid",i.getOrganizacion().getOrgId());
            oResponse.put("iteava",i.getPgeAva());
            oResponse.put("iteurl",i.getIteUrlDes());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }
}
