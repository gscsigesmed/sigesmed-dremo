/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity(name="com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoExtension")
@Table(name = "tipo_extension", schema="institucional")

public class TipoExtension implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tex_ide")
    private Short texIde;
    @Column(name = "tex_nom")
    private String texNom;
    @Column(name = "tex_des")
    private String texDes;
//    @OneToMany(mappedBy = "tifTipExt")
//    private Collection<TipoItemFile> tipoItemFileCollection;

    public TipoExtension() {
    }

    public TipoExtension(Short texIde) {
        this.texIde = texIde;
    }

    public Short getTexIde() {
        return texIde;
    }

    public void setTexIde(Short texIde) {
        this.texIde = texIde;
    }

    public String getTexNom() {
        return texNom;
    }

    public void setTexNom(String texNom) {
        this.texNom = texNom;
    }

    public String getTexDes() {
        return texDes;
    }

    public void setTexDes(String texDes) {
        this.texDes = texDes;
    }
//
//    @XmlTransient
//    public Collection<TipoItemFile> getTipoItemFileCollection() {
//        return tipoItemFileCollection;
//    }
//
//    public void setTipoItemFileCollection(Collection<TipoItemFile> tipoItemFileCollection) {
//        this.tipoItemFileCollection = tipoItemFileCollection;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (texIde != null ? texIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoExtension)) {
            return false;
        }
        TipoExtension other = (TipoExtension) object;
        if ((this.texIde == null && other.texIde != null) || (this.texIde != null && !this.texIde.equals(other.texIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.TipoExtension[ texIde=" + texIde + " ]";
    }
    
}
