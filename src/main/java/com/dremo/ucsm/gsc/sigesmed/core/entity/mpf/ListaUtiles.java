/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author Carlos
 */
@Entity(name="ListaUtilesMpf")
@Table(name="lista_utiles", schema="pedagogico")
@DynamicUpdate(value = true)
public class ListaUtiles implements java.io.Serializable{
    @Id
    @Column(name="list_uti_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_lista_utiles", sequenceName="pedagogico.lista_utiles_list_uti_id_seq")
    @GeneratedValue(generator="secuencia_lista_utiles")
    private int listUtiId;
    
    @Column(name="ani_lis", length = 4)
    private String anioLista;
        
    @Column(name="tit_lis_uti", length = 60)
    private String tituloLista;
    
    @Column(name="tip_lis", length = 1)
    private Character tipoLista;
       
    @Column(name="obs_lis_uti", length = 200)
    private String observacionLista;
    
    @Column(name="rec_lis_uti", length = 200)
    private String recomendacionLista;
    
    @Column(name = "pre_tot_lis")
    private Double precioTotal;  
    
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    private Usuario usuId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "niv_id")
    private Nivel nivel;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_id")
    private Grado grado;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular area;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_id")
    private Seccion seccion;
    
    @OneToMany (mappedBy = "listaUtiles" , fetch = FetchType.LAZY)
    private List<ListaArticulo> listas = new ArrayList();
    
    
    /*@OneToMany(mappedBy = "lista_utiles", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<ListaArticulo> listaArticulo = new ArrayList<>();*/
    
    public ListaUtiles() {
    }

    public ListaUtiles(String anioLista, String tituloLista, Character tipoLista, String recomendacionLista, Double precioTotal,Organizacion organizacion, Usuario usuId, Nivel nivel, Grado grado, AreaCurricular area, Seccion seccion) {
        this.anioLista = anioLista;
        this.tituloLista = tituloLista;
        this.tipoLista = tipoLista;
        this.recomendacionLista = recomendacionLista;
        this.precioTotal = precioTotal;
        this.organizacion = organizacion;
        this.usuId = usuId;
        this.nivel = nivel;
        this.grado = grado;
        this.area = area;
        this.seccion = seccion;
    }
  
    

    public int getListUtiId() {
        return listUtiId;
    }

    public void setListUtiId(int listUtiId) {
        this.listUtiId = listUtiId;
    }

    public String getTituloLista() {
        return tituloLista;
    }

    public void setTituloLista(String tituloLista) {
        this.tituloLista = tituloLista;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal){
        this.precioTotal = precioTotal;
    }

    public String getAnioLista() {
        return anioLista;
    }

    public void setAnioLista(String anioLista) {
        this.anioLista = anioLista;
    }

    public Character getTipoLista() {
        return tipoLista;
    }

    public void setTipoLista(Character tipoLista) {
        this.tipoLista = tipoLista;
    }
    
    public String getObservacionLista() {
        return observacionLista;
    }

    public void setObservacionLista(String observacionLista) {
        this.observacionLista = observacionLista;
    }

    public String getRecomendacionLista() {
        return recomendacionLista;
    }

    public void setRecomendacionLista(String recomendacionLista) {
        this.recomendacionLista = recomendacionLista;
    }
    
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Usuario getUsuario() {
        return usuId;
    }

    public void setUsuario(Usuario usuario) {
        this.usuId = usuario;
    }
    
    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }
    
    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public List<com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaArticulo> getListas() {
        return listas;
    }

    public void setListas(List<com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaArticulo> listas) {
        this.listas = listas;
    }
    
    

    public Usuario getUsuId() {
        return usuId;
    }

    public void setUsuId(Usuario usuId) {
        this.usuId = usuId;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }
}
