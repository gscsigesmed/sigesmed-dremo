/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarDatosPersonalesTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(AgregarDatosPersonalesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        Persona persona = null;
        Direccion direccion = null;
        Organizacion organizacion = null;
        Trabajador trabajador = null;
        FichaEscalafonaria fe = null;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            
            JSONObject rPersona = requestData.getJSONObject("persona");
            JSONObject rTrabajador = requestData.getJSONObject("trabajador");
            JSONObject rFichaEscalafonaria = requestData.getJSONObject("fichaEscalafonaria");
            JSONArray rDirecciones = requestData.getJSONArray("direcciones");
            
            String apePat = rPersona.getString("apePat");
            String apeMat = rPersona.getString("apeMat");
            String nom = rPersona.getString("nom");
            String dni = rPersona.getString("dni");
            String fecNac = rPersona.getString("fecNac");
            String num1 = rPersona.getString("num1");
            String num2 = rPersona.getString("num2");
            String fij = rPersona.getString("fij");
            String email = rPersona.getString("email");
            Character sex = rPersona.getString("sex").charAt(0);
            Character estCiv = rPersona.getString("estCiv").charAt(0);
            String depNac = rPersona.getString("depNac");
            String proNac = rPersona.getString("proNac");
            String disNac = rPersona.getString("disNac");
            persona = new Persona(apePat, apeMat, nom, dni, sdi.parse(fecNac.substring(0, 10)), num1, num2, fij, email, sex, estCiv, depNac, proNac, disNac, wr.getIdUsuario(), new Date(), 'A');

            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");
            personaDao.insert(persona);

            for(int i = 0;i < rDirecciones.length();i++) {
                String tipDir = rDirecciones.getJSONObject(i).getString("tipDir");
                String nomDir = rDirecciones.getJSONObject(i).getString("nomDir");
                String depDir = rDirecciones.getJSONObject(i).getString("depDir");
                String proDir = rDirecciones.getJSONObject(i).getString("proDir");
                String disDir = rDirecciones.getJSONObject(i).getString("disDir");
                String num = rDirecciones.getJSONObject(i).getString("num");
                String dpto = rDirecciones.getJSONObject(i).getString("dpto");
                String interior = rDirecciones.getJSONObject(i).getString("int");
                String mz = rDirecciones.getJSONObject(i).getString("mz");
                String lote = rDirecciones.getJSONObject(i).getString("lote");
                String km = rDirecciones.getJSONObject(i).getString("km");
                String bloque = rDirecciones.getJSONObject(i).getString("bloque");
                String etapa = rDirecciones.getJSONObject(i).getString("etapa");
                String nomZon = rDirecciones.getJSONObject(i).getString("nomZon");
                String desRef = rDirecciones.getJSONObject(i).getString("desRef");
                String fulDir = nomDir + " Nº " + num + " Dpto. " + dpto + " Int. " + interior + " Mz. " + mz + " Lt." + lote + " Km. " + km + " Bloque " + bloque + " Etapa " + etapa; 
                direccion = new Direccion(persona, tipDir.charAt(0), fulDir, depDir, proDir, disDir, nomZon, desRef, wr.getIdUsuario(), new Date(), 'A');

                DireccionDao direccionDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
                direccionDao.insert(direccion);   
            }

            Integer orgId = requestData.getInt("orgId"); 
            organizacion = new Organizacion(orgId);
            Character traCon = rTrabajador.getString("traCon").charAt(0);
            trabajador = new Trabajador(persona, organizacion, traCon, wr.getIdUsuario(), new Date(), 'A');
            
            String autEss = rFichaEscalafonaria.getString("autEss");
            String sisPen = rFichaEscalafonaria.getString("sisPen");
            String nomAfp = rFichaEscalafonaria.getString("nomAfp");
            String codCuspp = rFichaEscalafonaria.getString("codCuspp");
            String fecIngAfp = rFichaEscalafonaria.getString("fecIngAfp");
            Boolean perDis = rFichaEscalafonaria.getBoolean("perDis");
            String regCon = rFichaEscalafonaria.getString("regCon");
            Character gruOcu = rFichaEscalafonaria.getString("gruOcu").charAt(0);
            
            TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
            trabajadorDao.insert(trabajador);
            
            if (!fecIngAfp.equals("")){
                fe = new FichaEscalafonaria(trabajador, autEss, sisPen, nomAfp, codCuspp, sdi.parse(fecIngAfp), perDis, regCon, gruOcu, wr.getIdUsuario(), new Date(), 'A');
            } else {
                fe = new FichaEscalafonaria(trabajador, autEss, sisPen, perDis, regCon, gruOcu, wr.getIdUsuario(), new Date(), 'A');
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar datos generales de trabajador",e);
            System.out.println(e);
            return WebResponse.crearWebResponseError("Datos incorrectos");
        }

        FichaEscalafonariaDao feDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        try {
            feDao.insert(fe);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar ficha escalafonaria",e);
            System.out.println("No se pudo registrar la ficha escalafonaria\n" + e);
            return WebResponse.crearWebResponseError("No se pudo registrar la ficha escalafonaria", e.getMessage());
        }

        JSONObject oResponse = new JSONObject();  
        oResponse.put("perId", persona.getPerId());
        oResponse.put("perApePat", persona.getApePat());
        oResponse.put("perApeMat", persona.getApeMat());
        oResponse.put("perNom", persona.getNom());
        oResponse.put("perDni", persona.getDni());
        oResponse.put("traId", trabajador.getTraId());
        oResponse.put("ficEscId", fe.getFicEscId());
        
        return WebResponse.crearWebResponseExito("El registro de la ficha escalafonaria se realizo correctamente", oResponse);
        //Fin
    }

}
