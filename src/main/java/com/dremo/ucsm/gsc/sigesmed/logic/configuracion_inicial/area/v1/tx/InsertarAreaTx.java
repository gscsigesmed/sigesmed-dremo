/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class InsertarAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Area nuevaArea = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int organizacionID = requestData.getInt("organizacionID");
            int tipoAreaID = requestData.getInt("tipoAreaID");
            int areaPadreID = requestData.getInt("areaPadreID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String estado = requestData.getString("estado");
            
            if(areaPadreID == 0)
                nuevaArea = new Area(0,new Organizacion(organizacionID),new TipoArea(tipoAreaID), codigo, nombre, new Date(), wr.getIdUsuario(), estado.charAt(0));            
            else
                nuevaArea = new Area(0,new Organizacion(organizacionID),new Area(areaPadreID), new TipoArea(tipoAreaID), codigo, nombre, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AreaDao areaDao = (AreaDao)FactoryDao.buildDao("AreaDao");
        try{
            areaDao.insert(nuevaArea);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("areaID",nuevaArea.getAreId());
        oResponse.put("fecha",nuevaArea.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Area se realizo correctamente", oResponse);
        //Fin
    }
    
}
