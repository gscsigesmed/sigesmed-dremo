package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "evaluacion_desarrollo", schema = "pedagogico")
public class EvaluacionDesarrollo implements Serializable {
    @EmbeddedId
    @AttributeOverrides( {
            @AttributeOverride(name = "desTemCapId", column = @Column(name = "des_tem_cap_id", nullable = false)),
            @AttributeOverride(name = "perId", column = @Column(name = "per_id", nullable = false))})
    private EvaluacionDesarrolloId id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "des_tem_cap_id", updatable = false, insertable = false)
    private DesarrolloTemaCapacitacion desarrollo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id", updatable = false, insertable = false)
    private Persona persona;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ent")
    private Date fecEnt;

    @Column(name = "est_reg", nullable = false, length = 1)
    private Character estReg;

    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "not_par", nullable = false)
    private double notPar;
    
    @OneToMany(mappedBy = "evaluacionDesarrollo", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<AdjuntoEvaluacionDesarrollo> adjuntos = new ArrayList<>();

    public EvaluacionDesarrollo() {}
    
    public EvaluacionDesarrollo(EvaluacionDesarrolloId id, Date fecEnt, Character estReg, Integer usuMod, double notPar) {
        this.id = id;
        this.fecEnt = fecEnt;
        this.estReg = estReg;
        this.usuMod = usuMod;
        this.notPar = notPar;
    }

    public EvaluacionDesarrolloId getId() {
        return id;
    }

    public void setId(EvaluacionDesarrolloId id) {
        this.id = id;
    }

    public DesarrolloTemaCapacitacion getDesarrollo() {
        return desarrollo;
    }

    public void setDesarrollo(DesarrolloTemaCapacitacion desarrollo) {
        this.desarrollo = desarrollo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public Date getFecEnt() {
        return fecEnt;
    }

    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public double getNotPar() {
        return notPar;
    }

    public void setNotPar(double notPar) {
        this.notPar = notPar;
    }

    public List<AdjuntoEvaluacionDesarrollo> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<AdjuntoEvaluacionDesarrollo> adjuntos) {
        this.adjuntos = adjuntos;
    }
}
