package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 26/01/2017.
 */
@Entity
@Table(name = "historico_notas_estudiante", schema = "pedagogico")
public class HistoricoNotasEstudiante implements java.io.Serializable{
    @Id
    @Column(name = "his_not_est_id",nullable = false, unique = true)
    @SequenceGenerator(name = "historico_notas_estudiante_his_not_est_id_seq",sequenceName = "pedagogico.historico_notas_estudiante_his_not_est_id_seq")
    @GeneratedValue(generator = "historico_notas_estudiante_his_not_est_id_seq")
    private int hisNotEstId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_est_id")
    private GradoIEEstudiante gradoEst;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular areaCurricular;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_eva_id")
    private PeriodosPlanEstudios periodos;
    
    @Column(name = "not_are", length = 2)
    String nota;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public HistoricoNotasEstudiante() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public HistoricoNotasEstudiante(String nota) {
        this.nota = nota;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getHisNotEstId() {
        return hisNotEstId;
    }

    public void setHisNotEstId(int hisNotEstId) {
        this.hisNotEstId = hisNotEstId;
    }

    public GradoIEEstudiante getGradoEst() {
        return gradoEst;
    }

    public void setGradoEst(GradoIEEstudiante gradoEst) {
        this.gradoEst = gradoEst;
    }

    public AreaCurricular getAreaCurricular() {
        return areaCurricular;
    }

    public void setAreaCurricular(AreaCurricular areaCurricular) {
        this.areaCurricular = areaCurricular;
    }

    public PeriodosPlanEstudios getPeriodos() {
        return periodos;
    }

    public void setPeriodos(PeriodosPlanEstudios periodos) {
        this.periodos = periodos;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
