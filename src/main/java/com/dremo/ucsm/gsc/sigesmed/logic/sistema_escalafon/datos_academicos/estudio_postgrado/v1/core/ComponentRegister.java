/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx.ActualizarEstudioPostgradoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx.AgregarEstudioPostgradoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx.ListarEstudiosPostgradoTx;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("estudio_postgrado");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarEstudiosPostgrado", ListarEstudiosPostgradoTx.class);
        seComponent.addTransactionPOST("agregarEstudioPostgrado", AgregarEstudioPostgradoTx.class);
        seComponent.addTransactionPOST("actualizarEstudioPostgrado", ActualizarEstudioPostgradoTx.class);
        
        
        return seComponent;
    }
    
}
