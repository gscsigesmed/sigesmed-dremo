/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.ActualizarArticuloTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.ActualizarMontoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.BuscarArticuloParaListaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.EliminarArticuloTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.EliminarMontoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.ListarArticuloTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.ListarListaUtilesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.ListarMontoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.ListarNivelesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.RegistrarArticuloTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.RegistrarListaUtilesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.RegistrarMontoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx.RegistrarPlantillaListaTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_ACADEMICO);
        maComponent.setName("utiles");
        maComponent.setVersion(1);

        maComponent.addTransactionPOST("registrarArticulo",RegistrarArticuloTx.class);
        maComponent.addTransactionGET("listarArticulo",ListarArticuloTx.class);
        maComponent.addTransactionPUT("actualizarArticulo", ActualizarArticuloTx.class);
        maComponent.addTransactionDELETE("eliminarArticulo", EliminarArticuloTx.class);
        
        maComponent.addTransactionPOST("registrarMonto",RegistrarMontoTx.class);
        maComponent.addTransactionGET("listarMonto",ListarMontoTx.class);
        maComponent.addTransactionPUT("actualizarMonto", ActualizarMontoTx.class);
        maComponent.addTransactionDELETE("eliminarMonto", EliminarMontoTx.class);
        
        maComponent.addTransactionGET("listarNivel",ListarNivelesTx.class);
        
        maComponent.addTransactionPOST("registrarListaUtiles",RegistrarListaUtilesTx.class);
        maComponent.addTransactionGET("listarListaUtiles",ListarListaUtilesTx.class);
        maComponent.addTransactionGET("buscarArticuloParaLista",BuscarArticuloParaListaTx.class);
        
        maComponent.addTransactionPOST("registrarPlantillaLista",RegistrarPlantillaListaTx.class);
        
        return maComponent;
    }
}
