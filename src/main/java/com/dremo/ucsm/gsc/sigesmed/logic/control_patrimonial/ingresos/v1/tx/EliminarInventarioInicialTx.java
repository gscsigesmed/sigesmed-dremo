/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioFisicoDAO;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;

/**
 *
 * @author Administrador
 */
public class EliminarInventarioInicialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        InventarioInicial inv_ini = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int inv_ini_id = requestData.getInt("inv_ini_id");
            inv_ini = new InventarioInicial();
            inv_ini.setInv_ini_id(inv_ini_id);
            InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
            inv_ini_dao.eliminar_inventario(inv_ini_id);
            
        }catch(Exception e){
            System.out.println(e);
            return  WebResponse.crearWebResponseError("No se pudo eliminar el Inventario Inicial, datos incorrectos", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El Inventario Inicial se elimino correctamente");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
