/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.DetalleInventarioTransferencia;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Jeferson
 */
public class RegistrarInventarioTransferenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha ;
        InventarioTransferencia it = null;
        JSONObject requestData = (JSONObject)wr.getData();
        
        
        
        InventarioTransferenciaDAO inv_tra_dao = (InventarioTransferenciaDAO)FactoryDao.buildDao("sad.InventarioTransferenciaDAO");
        DetalleInventarioTransferencia dit = null;    
          int ser_doc_id = requestData.getInt("serie_id");
          String cod = requestData.getString("cod_serie");
        try{
            fecha = formatter.parse(requestData.getString("fec"));
           
            /*DATA INVENTARIO TRANSFERENCIA CABECERA*/
        //    String res = requestData.getString("res_con");
            char est = 'A';
            it = new InventarioTransferencia(0,ser_doc_id,cod,fecha,est); 
            it.setInvTransDet(new ArrayList<DetalleInventarioTransferencia>());
            // Insertamos la cabecera del Inventario
           
        }   
        catch(Exception e){
            System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Inventario de Transferencia, datos incorrectos", e.getMessage() );
        }   
       
        try{
                    
           
                    /*DATA INVENTARIO TRANSFERENCIA DETALLE*/
                    String asunto = requestData.getString("asu");
                    int met_lin = requestData.getInt("met_lin");
                  //  int res_con =0; 

                 //   int loc = requestData.getInt("loc");
                    String alm = requestData.getString("alm");
                    int est = requestData.getInt("est");
                    int bal = requestData.getInt("bal");
                    int fil = requestData.getInt("fil");
                    int cue = requestData.getInt("cue");
                    int caj = requestData.getInt("caj");
                      
                    
                    dit = new DetalleInventarioTransferencia(0,it ,ser_doc_id,fecha,asunto,met_lin,alm,est,bal,fil,cue,caj);
                    it.getInvTransDet().add(dit);
                      
                      
                    inv_tra_dao.insert(it);
                
        }catch(Exception e){
              System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Inventario de Transferencia Detalle, datos incorrectos", e.getMessage() );
        }
        JSONObject oResponse = new JSONObject();
        int prueba = dit.getdet_inv_tra();
        oResponse.put("det_inv_trans",dit.getdet_inv_tra());
        oResponse.put("fec_cre",dit.getFecha());
         return WebResponse.crearWebResponseExito("El registro Inventario de Transferencia  y Detalle  se realizo correctamente",oResponse);
    
    }
    
}
