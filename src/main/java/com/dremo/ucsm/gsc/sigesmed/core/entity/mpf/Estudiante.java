package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1

import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Persona;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * @author :Carlos
 */
@Entity
(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Estudiante")
@Table(name = "estudiante", schema = "pedagogico",  uniqueConstraints = @UniqueConstraint(columnNames = "cod_est"))
public class Estudiante implements java.io.Serializable {
    
    @Id
    @Column(name = "per_id", unique = true, nullable = false)
    private Long perId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private Persona persona;
    
    @Column(name = "cod_est", unique = true, nullable = false, length = 10)
    private String codEst;
    
    @Column(name = "num_her")
    private Short numHer;
    
    @Column(name = "lug")
    private Short lug;
    
    @Column(name = "tip_par", length = 2)
    private String tipPar;
    
    @Column(name = "rel", length = 2)
    private String rel;
    
    @Column(name = "tip_dis", length = 2)
    private String tipDis;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg", length = 1)
    private String estReg;
    
    @Column(name = "obs", length = 256)
    private String obs;
    
    @Column(name = "ale")
    private String ale;
    
    @Column(name = "exp_tra")
    private String expTra;
    
    @Column(name = "tip_san", length = 2)
    private String tipSan;
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "estudiante")
    private DatosNacimiento datosNacimiento;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "estudiante")
    private List<SaludControles> saludControles;

    public Estudiante() {
    }

    public Estudiante(Persona persona, String codEst) {
        this.persona = persona;
        this.codEst = codEst;
    }

    public Estudiante(Persona persona, String codEst, Short numHer, Short lug,
            String tipPar, String rel, String tipDis, Date fecMod, Integer usuMod,
            String estReg, String obs, String ale, String expTra,
            String tipSan, DatosNacimiento datosNacimiento) {
        this.persona = persona;
        this.codEst = codEst;
        this.numHer = numHer;
        this.lug = lug;
        this.tipPar = tipPar;
        this.rel = rel;
        this.tipDis = tipDis;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
        this.obs = obs;
        this.ale = ale;
        this.expTra = expTra;
        this.tipSan = tipSan;
        this.datosNacimiento = datosNacimiento;
    }

   
   
    public Long getPerId() {
        return this.perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    
    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    
    public String getCodEst() {
        return this.codEst;
    }

    public void setCodEst(String codEst) {
        this.codEst = codEst;
    }

   
    public Short getNumHer() {
        return this.numHer;
    }

    public void setNumHer(Short numHer) {
        this.numHer = numHer;
    }

    
    public Short getLug() {
        return this.lug;
    }

    public void setLug(Short lug) {
        this.lug = lug;
    }

    
    public String getTipPar() {
        return this.tipPar;
    }

    public void setTipPar(String tipPar) {
        this.tipPar = tipPar;
    }

    
    public String getRel() {
        return this.rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    
    public String getTipDis() {
        return this.tipDis;
    }

    public void setTipDis(String tipDis) {
        this.tipDis = tipDis;
    }

    
    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public String getEstReg() {
        return this.estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    
    public String getObs() {
        return this.obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    
    public String getAle() {
        return this.ale;
    }

    public void setAle(String ale) {
        this.ale = ale;
    }

    
    public String getExpTra() {
        return this.expTra;
    }

    public void setExpTra(String expTra) {
        this.expTra = expTra;
    }

    
    public String getTipSan() {
        return this.tipSan;
    }

    public void setTipSan(String tipSan) {
        this.tipSan = tipSan;
    }

    
    public DatosNacimiento getDatosNacimiento() {
        return this.datosNacimiento;
    }

    public void setDatosNacimiento(DatosNacimiento datosNacimiento) {
        this.datosNacimiento = datosNacimiento;
    }

    public List<SaludControles> getSaludControles() {
        return saludControles;
    }

    public void setSaludControles(List<SaludControles> saludControles) {
        this.saludControles = saludControles;
    }

   
}
