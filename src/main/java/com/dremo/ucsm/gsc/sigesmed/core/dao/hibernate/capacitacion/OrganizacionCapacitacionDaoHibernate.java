package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.OrganizacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


public class OrganizacionCapacitacionDaoHibernate extends GenericDaoHibernate<Organizacion> implements OrganizacionCapacitacionDao {

    private static final Logger logger = Logger.getLogger(OrganizacionCapacitacionDaoHibernate.class.getName());
    
    @Override
    public Organizacion buscarPorId(int orgID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Organizacion organizacion = (Organizacion)session.get(Organizacion.class,orgID);

            return organizacion;
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public String buscarCapacitacionOrg(int codCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(Organizacion.class)
                    .setProjection(Projections.property("nom"))
                    .createAlias("capacitacionesOrg", "cap")
                    .add(Restrictions.eq("cap.curCapId", codCap))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return (String) query.uniqueResult();
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarCapacitacionOrg",e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public String buscarCapacitacionAut(int codCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(Organizacion.class)
                    .setProjection(Projections.property("nom"))
                    .createAlias("capacitacionesAut", "cap")
                    .add(Restrictions.eq("cap.curCapId", codCap))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return (String) query.uniqueResult();
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarCapacitacionAut",e);
            throw e;
        } finally {
            session.close();
        }
    }
}
