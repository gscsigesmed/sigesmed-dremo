/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface EstudioPostgradoDao extends GenericDao<EstudioPostgrado>{
    public List<EstudioPostgrado> listarxFichaEscalafonaria(int ficEscId);
    public EstudioPostgrado buscarPorId(Integer estPosId);
}
