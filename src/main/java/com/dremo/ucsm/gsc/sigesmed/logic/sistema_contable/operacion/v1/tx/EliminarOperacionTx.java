/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.OperacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Operacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarOperacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        short operacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
           operacionID = (short)requestData.getInt("operacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        OperacionDao operacionDao = (OperacionDao)FactoryDao.buildDao("sci.OperacionDao");
        try{
            operacionDao.delete(new Operacion(operacionID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la operacion del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Operacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Operacion se elimino correctamente");
        //Fin
    }
}

    

