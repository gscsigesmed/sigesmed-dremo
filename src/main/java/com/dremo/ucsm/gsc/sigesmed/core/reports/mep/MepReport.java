package com.dremo.ucsm.gsc.sigesmed.core.reports.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.MepReportDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.math3.util.Pair;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 31/08/2016.
 */
public class MepReport{
    private  final static Logger logger = Logger.getLogger(MepReport.class.getName());
    private  ResumenEvaluacionPersonal resumen;
    private ByteArrayOutputStream baos = null;
    protected  class PageEventHandler implements IEventHandler{
        Document document;
        protected PageEventHandler (Document document){

            super();
            this.document = document;
        }
        @Override
        public void handleEvent(Event event){
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdfDoc = docEvent.getDocument();

            PdfPage page = docEvent.getPage();
            int pageNumber = pdfDoc.getPageNumber(page);
            if(pageNumber == 1){
                PdfCanvas pdfCanvas = new PdfCanvas(page);
                createTitle(pdfCanvas,pdfDoc,resumen.getTrabajador().getTraCar().getCrgTraNom());
                try {
                    createHeader(pdfCanvas,pdfDoc,resumen.getTrabajador());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                document.setTopMargin(200);
            }else{
                document.setTopMargin(20);
            }
        }
        private void createTitle(PdfCanvas pdfCanvas,PdfDocument pdf,String titulo) {
            pdfCanvas.setFillColor(Color.BLACK);
            Rectangle rectangle = new Rectangle(10, 720, 580, 60);
            pdfCanvas.roundRectangle(10, 720, 580, 100,10);
            pdfCanvas.fillStroke();

            //Canvas
            Canvas canvas = new Canvas(pdfCanvas,pdf,rectangle);
            Text text = new Text("Ficha de Evaluacion para el : " + titulo).setFontColor(Color.WHITE);
            Paragraph paragraph = new Paragraph().add(text);
            canvas.setTextAlignment(TextAlignment.CENTER);
            canvas.add(paragraph);
        }
        private void createHeader(PdfCanvas pdfCanvas,PdfDocument pdf, Trabajador trabajador) throws IOException {
            Rectangle rectangle = new Rectangle(10, 650, 580, 60);
            pdfCanvas.rectangle(rectangle);
            pdfCanvas.stroke();
            //Canvas
            Persona persona = trabajador.getPersona();
            Canvas canvas = new Canvas(pdfCanvas,pdf,rectangle);
            canvas.setTextAlignment(TextAlignment.JUSTIFIED);

            PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
            Text name = new Text("Nombres: ").setFont(bold);
            Text surname = new Text("Apellidos: ").setFont(bold);

            Paragraph paragraph = new Paragraph().add(name)
                    .add(persona.getNom()).add(" ")
                    .add(surname).add(persona.getApePat() + " " + persona.getApeMat());
            canvas.add(paragraph);

            Text cargo = new Text("Cargo: ").setFont(bold);
            Text institucion = new Text("Institucion: ").setFont(bold);
            Paragraph paragraph2 = new Paragraph().add(cargo)
                    .add(trabajador.getTraCar().getCrgTraNom());
            canvas.add(paragraph2);

            Paragraph paragraph3 = new Paragraph().add(institucion)
                    .add(trabajador.getOrganizacion().getNom());
            canvas.add(paragraph3);
        }
    }
    public void createReport(int idResumen) throws IOException,HibernateException {


        baos = new ByteArrayOutputStream();
        PdfWriter writer = new PdfWriter(baos);
        EvaluacionPersonalDaoHibernate eva = new EvaluacionPersonalDaoHibernate();
        resumen = eva.getResumentEvaluacionById(idResumen);

        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf);
        //document.setTopMargin(200);
        pdf.addEventHandler(PdfDocumentEvent.START_PAGE,new PageEventHandler(document));
        String cargo = resumen.getTrabajador().getTraCar().getCrgTraNom();

        if(cargo.toUpperCase().equals("DOCENTE"))document.add(createContentDocente(resumen));
        else document.add(createContent(resumen));
        createFooter(document,resumen);
        document.close();

        pdf.close();
    }

    private void createFooter(Document document, ResumenEvaluacionPersonal resumen){
        document.add(new Paragraph("Puntaje Final: ").add(resumen.getPun().toString()));
        document.add(new Paragraph("Resultado: ").add(resumen.getRes()));
    }

    private Table createContentDocente(ResumenEvaluacionPersonal resumen) throws IOException,HibernateException {
        FichaEvaPerslDaoHibernate feph = new FichaEvaPerslDaoHibernate();

        MepReportDaoHibernate meprh = new MepReportDaoHibernate();
        Pair<Integer,Map<String,List<IndicadorFichaEvaluacion>>> pair = meprh.getContenidoResumenEvaluacion(resumen.getResEvaPerId(),'c');
        int idFicha = pair.getFirst();
        List<ElementoFicha> escalas = feph.getElementos(idFicha, EscalaValoracionFicha.class,"val");
        Table tableReport = new Table(new float[]{3.5f,4.5f,0.5f,0.5f,0.5f});
        tableReport.setWidthPercent(100);
        tableReport.addHeaderCell(new Cell(2, 1)
                .add(new Paragraph("COMPETENCIAS").setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9))
        );
        tableReport.addHeaderCell(new Cell(2,1).add(new Paragraph("Indicadores").setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9)));
        tableReport.addHeaderCell(new Cell(1,escalas.size()).add(new Paragraph("Valoración")));
        for(ElementoFicha er : escalas){
            EscalaValoracionFicha e = (EscalaValoracionFicha)er;
            tableReport.addHeaderCell(new Paragraph(String.valueOf(e.getVal())).setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9));
        }

        Map<String,List<IndicadorFichaEvaluacion>> contenidos =  pair.getSecond();
        for (Map.Entry<String,List<IndicadorFichaEvaluacion>> entry : contenidos.entrySet()){
            logger.log(Level.INFO,"cantidad de indi:{0}",entry.getValue().size());
            Cell cellReport = new Cell(entry.getValue().size(),1).add(new Paragraph(entry.getKey()));
            tableReport.addCell(cellReport);
            for(IndicadorFichaEvaluacion ind: entry.getValue()){
                tableReport.addCell(new Cell().add(new Paragraph(ind.getNom())));
                insertEscalasTable(tableReport,escalas,ind);
            }
        }
        return tableReport;
    }
    private Table createContent(ResumenEvaluacionPersonal resumen) throws IOException,HibernateException {
        FichaEvaPerslDaoHibernate feph = new FichaEvaPerslDaoHibernate();
        MepReportDaoHibernate meprh = new MepReportDaoHibernate();

        Pair<Integer,Map<String,List<IndicadorFichaEvaluacion>>> pair = meprh.getContenidoResumenEvaluacion(resumen.getResEvaPerId(),'f');

        int idFicha = pair.getFirst();
        Map<String,List<IndicadorFichaEvaluacion>> contenidos =  pair.getSecond();
        List<ElementoFicha> escalas = feph.getElementos(idFicha, EscalaValoracionFicha.class,"val");

        Table tableReport = new Table(new float[]{3.5f,4,0.5f,0.5f,0.5f,0.5f,0.5f});

        tableReport.setWidthPercent(100);
        //insertamos el titulo
        tableReport.addHeaderCell(new Cell(2, 1)
                .add(new Paragraph("I. Funciones").setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9))
        );
        tableReport.addHeaderCell(new Cell(2,1).add(new Paragraph("Indicadores").setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9)));
        tableReport.addHeaderCell(new Cell(1,5).add(new Paragraph("Valoración")));
        for(ElementoFicha er : escalas){
            EscalaValoracionFicha e = (EscalaValoracionFicha)er;
            tableReport.addHeaderCell(new Paragraph(String.valueOf(e.getVal())).setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9));
        }

        for (Map.Entry<String,List<IndicadorFichaEvaluacion>> entry : contenidos.entrySet()){
            Cell cellReport = new Cell(entry.getValue().size(),1).add(new Paragraph(entry.getKey()));
            tableReport.addCell(cellReport);
            for(IndicadorFichaEvaluacion ind: entry.getValue()){
                tableReport.addCell(new Cell().add(new Paragraph(ind.getNom())));
                insertEscalasTable(tableReport,escalas,ind);
            }
        }
        //ahora insertamos las competencias
        Pair<Integer,Map<String,List<IndicadorFichaEvaluacion>>> pair2 = meprh.getContenidoResumenEvaluacion(resumen.getResEvaPerId(),'c');
        Map<String,List<IndicadorFichaEvaluacion>> competencias = pair2.getSecond();
        for(Map.Entry<String,List<IndicadorFichaEvaluacion>> entry : competencias.entrySet()){
            tableReport.addCell(new Cell(1,2).add(new Paragraph(entry.getKey())));
            tableReport.addCell(new Cell(1,5).add(new Paragraph("Valoracion")));
            tableReport.addCell(new Cell(1,2).add(new Paragraph("Indicadores Conductuales")));
            for(ElementoFicha er : escalas){
                EscalaValoracionFicha e = (EscalaValoracionFicha)er;
                tableReport.addCell(new Cell().add(new Paragraph(String.valueOf(e.getVal()))));
                //tableReport.addHeaderCell(new Paragraph(String.valueOf(e.getVal())).setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD)).setFontSize(9));
            }
            for(IndicadorFichaEvaluacion ind: entry.getValue()){
                tableReport.addCell(new Cell(1,2).add(new Paragraph(ind.getNom())));
                insertEscalasTable(tableReport,escalas,ind);
            }

        }

        return tableReport;
    }
    private double insertEscalasTable(Table tableReport, List<ElementoFicha> escalas,IndicadorFichaEvaluacion ind){
        double sum = 0;
        for(ElementoFicha er : escalas){
            EscalaValoracionFicha escala = (EscalaValoracionFicha)er;
            if(escala.getVal() == ind.getValue()){
                sum += ind.getValue();
                tableReport.addCell(new Paragraph("X"));
            }
            else tableReport.addCell(new Paragraph(""));

        }
        return sum;
    }
    public String encodeToBase64(){
        return "data:application/pdf;base64,"+ Base64.encodeBase64String(baos.toByteArray());
    }
}
