/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class FinalizarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        HistorialExpediente actual = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONObject oHistorial = requestData.getJSONObject("historial");
            JSONArray listaDocumentos = requestData.getJSONArray("documentos");
            String codigo = requestData.getString("codigo");
            String observacion = requestData.optString("observacion");
            
            
            Date hoy = new Date();
            
            actual = new HistorialExpediente(oHistorial.getInt("historialID"),oHistorial.getInt("expedienteID"));
            actual.setFecAte(hoy);
            actual.setEstadoId(EstadoExpediente.FINALIZADO);
            actual.setObservacion(observacion);
            
            
            
            //leendo los documentos           
            if(listaDocumentos.length() > 0){
                int numDoc = historialDao.numeroDocumentos(actual.getExpediente().getExpId()) + 1;
                actual.setDocumentos(new ArrayList<DocumentoExpediente>());
                for(int i = 0; i < listaDocumentos.length();i++){
                    JSONObject bo =listaDocumentos.getJSONObject(i);

                    String nombreArchivo = "";
                    String documentoDescripcion = bo.getString("descripcion");
                    int tipoDocumentoId = bo.getInt("tipoDocumentoID");
                    
                    //verificamos si existe un archivo adjunto al requisito
                    JSONObject jsonArchivo = bo.optJSONObject("archivo");
                    if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                        FileJsonObject miF = new FileJsonObject( jsonArchivo ,codigo+"_doc_res_"+BuildCodigo.cerosIzquierda(numDoc + i,2));
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    actual.getDocumentos().add( new DocumentoExpediente(numDoc + i, actual.getExpediente(),documentoDescripcion,nombreArchivo,tipoDocumentoId,actual.getHisExpId()) );
                }
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo finalizar el expediente, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        try{
            historialDao.actualizarEstado(actual,EstadoExpediente.FINALIZADO);
            historialDao.insertarDocumentos(actual.getDocumentos());
            ((ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao")).finalizarExpediente(actual.getExpediente().getExpId(),wr.getIdUsuario());
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo finalizar el expediente", e.getMessage() );
        }
        
        //si ya se registro el tipo de tramite 
        //ahora creamos los archivos que se desean subir
        for(FileJsonObject archivo : listaArchivos){
            BuildFile.buildFromBase64("expediente/salientes", archivo.getName(), archivo.getData());
        }
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Se finalizo correctamente el estado de los historias");
        //Fin
    }
    
}
