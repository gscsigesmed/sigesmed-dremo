package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import java.util.List;

public interface EvaluacionCursoCapacitacionDao extends GenericDao<EvaluacionCursoCapacitacion> {
    EvaluacionCursoCapacitacion buscarPorId(int codEva);
    List<EvaluacionCursoCapacitacion> buscarPorSede(int codSed);
    List<EvaluacionCursoCapacitacion> buscarIniciados(int codSed);
    int totalEvaluaciones(int codSed);
    List<EvaluacionCursoCapacitacion> listarEncuestas(int codSed);
}
