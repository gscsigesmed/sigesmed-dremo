/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionInasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Justificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author carlos
 */
public class RegistrarHoraAdicionalTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
          
        FileJsonObject miF=null;
        String nombreArchivo="";
        JSONArray registros=null;
        String descripcion="";
        String nroDoc="";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            
            
            JSONObject registro=requestData.getJSONObject("registro");
            nroDoc=registro.getString("documento");
            descripcion=registro.getString("descripcion");
            JSONObject doc = registro.getJSONObject("documentoAdj");
            String _url=doc.getString("url");
            int num = 0;
            JSONObject jsonArchivo = doc.optJSONObject("archivo");
            if (jsonArchivo != null && jsonArchivo.length() > 0) {
                miF = new FileJsonObject(jsonArchivo);
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String s = formatter.format(new Date());
                nombreArchivo = s+" "+ miF.getName();
            } 
            registros = (JSONArray) requestData.getJSONArray("registros");
            
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }

        
        try{
            AsistenciaDao asistenciaDao = (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
            
            for(int i=0;i<registros.length();i++)
            {
                JSONObject obj=registros.getJSONObject(i);
                Integer idRa=obj.getInt("id");
                Integer minAd=obj.getInt("adicional");
                asistenciaDao.registrarHoraAdicional(idRa, descripcion, nroDoc, nombreArchivo, minAd);
                if(nombreArchivo.length()>0)
                BuildFile.buildFromBase64("control_personal", nombreArchivo, miF.getData());
            }
          
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar ", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se registro correctamente");        
        //Fin
    }
    
}

