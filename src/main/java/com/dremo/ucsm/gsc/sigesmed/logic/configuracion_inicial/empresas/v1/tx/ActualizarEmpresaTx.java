/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.EmpresaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author carlos
 */
public class ActualizarEmpresaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Empresa empresaToUpdate=null;
        Empresa empresaNueva=null;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            int empresaLast=requestData.getInt("empresaID");
            empresaToUpdate=new Empresa(empresaLast);
            
            String razon_social=requestData.getString("razonSocial");
            String ruc_=requestData.getString("ruc");
            String web=requestData.getString("pagWeb");
            String num=requestData.getString("telefono");
            char estado=requestData.getString("estado").charAt(0);
            
            empresaNueva=new Empresa(ruc_, razon_social, num, web, new Date(), wr.getIdUsuario(), estado);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar la Organizacion", e.getMessage());
        }

        EmpresaDao empresaDao = (EmpresaDao) FactoryDao.buildDao("EmpresaDao");

        try {
            empresaDao.delete(empresaToUpdate);
            empresaDao.insert(empresaNueva);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo Actualizar ", e.getMessage());
        }

        JSONObject oRes=new JSONObject();
        oRes.put("empresaID", empresaNueva.getEmpId());

        return WebResponse.crearWebResponseExito("Se Actualizo correctamente", oRes);
        //Fin
    }

}
