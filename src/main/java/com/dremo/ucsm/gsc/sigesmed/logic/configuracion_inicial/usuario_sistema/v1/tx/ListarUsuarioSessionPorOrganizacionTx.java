/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarUsuarioSessionPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo listar usuarios por organizacion, datos incorrectos", e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Usuario> usuarios = null;
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        try{
            usuarios = usuarioDao.buscarConRolPorOrganizacion(organizacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los usuarios sessiones del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los usuarios sessiones del Sistema ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        
        JSONArray miArray = new JSONArray();
        int i =0;
        for(Usuario usuario:usuarios ){            
            //datos de session
            for( UsuarioSession session: usuario.getSessiones() ){
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("usuarioID",usuario.getUsuId());
                oResponse.put("nombreUsuario",usuario.getNom());            
                oResponse.put("password",usuario.getPas());
                oResponse.put("estado",""+usuario.getEstReg());

                //datos de persona
                oResponse.put("DNI", usuario.getPersona().getDni());
                oResponse.put("nombres", usuario.getPersona().getNombrePersona());
                oResponse.put("nombre", usuario.getPersona().getNom());
                oResponse.put("paterno", usuario.getPersona().getApePat());
                oResponse.put("materno", usuario.getPersona().getApeMat());
                oResponse.put("materno", usuario.getPersona().getEmail());
                oResponse.put("numero1", usuario.getPersona().getNum1());
                oResponse.put("numero2", usuario.getPersona().getNum2());
                
                oResponse.put("rolID",session.getRol().getRolId() );
                oResponse.put("rol",session.getRol().getNom() );
                oResponse.put("sessionID",session.getUsuSesId() );
                
                if(session.getArea()!=null){
                    oResponse.put("areaID",session.getArea().getAreId());
                    oResponse.put("area",session.getArea().getNom());
                }
                oResponse.put("estado",""+session.getEstReg());
                oResponse.put("i",i++);

                miArray.put(oResponse);
            }           
            
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los usuarios sessiones",miArray);        
        //Fin
    }
    
}



