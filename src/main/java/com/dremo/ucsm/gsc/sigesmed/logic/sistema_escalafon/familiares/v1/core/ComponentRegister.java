/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx.*;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("familiares");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarTipoParentesco", ListarTipoParentescoTx.class);
        seComponent.addTransactionGET("listarParientes", ListarParientesTx.class);
        
        seComponent.addTransactionPOST("agregarPariente", AgregarParienteTx.class);
        seComponent.addTransactionPOST("actualizarFamiliar", ActualizarFamiliarTx.class);
        seComponent.addTransactionDELETE("eliminarFamiliar", EliminarFamiliarTx.class);
        return seComponent;
    }
    
}
