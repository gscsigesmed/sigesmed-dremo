/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarDetallesComisionTx  implements ITransaction
{
    private static Logger logger = Logger.getLogger(ListarDetallesComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject js = (JSONObject)wr.getData();
            int idCom = js.getInt("com");
            List<CargoDeComision> cargos = listarCargosDeComision(idCom);
            Persona persona = buscarPresidente(idCom).getPersona();
            JSONObject jsonIntegrante = null;
            JSONObject result = new JSONObject();
            if(persona != null){
                jsonIntegrante = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"dni","nom","apeMat","apePat"},
                    new String[]{"dni","nom","apem","apep"},
                    persona));
            }
            
            JSONArray jsonCargos = new JSONArray();
            for(CargoDeComision cdc : cargos){
                JSONObject jsonCargo = new JSONObject(EntityUtil.objectToJSONString(new String[]{"carComId","nomCar"},
                        new String[]{"cod","nom"},cdc.getCargoComision()));
                jsonCargo.put("des",cdc.getDesCar());
                jsonCargos.put(jsonCargo);
            }
            
            result.put("pres", jsonIntegrante);
            result.put("cargos", jsonCargos);
            
            return WebResponse.crearWebResponseExito("se encontro presidente",result);
            
        }catch(Exception e){
            logger.log(Level.SEVERE,"listarDetalles",e);
            return WebResponse.crearWebResponseError("Error al listar los datos");
        }
    }
    private IntegranteComision buscarPresidente(int idCom){
        try{
            IntegranteComisionDao intComDao = (IntegranteComisionDao) FactoryDao.buildDao("scec.IntegranteComisionDao");
            IntegranteComision integrante = intComDao.buscarPresidenteComision(idCom);
            return integrante;
           
        }catch (Exception e){
            throw e;
           
        }
    }
    private List<CargoDeComision> listarCargosDeComision(int idCom){
        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            List<CargoDeComision> cargosDeComision = comisionDao.buscarCargosDeComision(idCom);
            return cargosDeComision;
            /*JSONArray jsonCargos = new JSONArray();
            for(CargoDeComision cdc : cargosDeComision){
                JSONObject jsonCargo = new JSONObject(EntityUtil.objectToJSONString(new String[]{"carComId","nomCar"},
                        new String[]{"cod","nom"},cdc.getCargoComision()));
                jsonCargo.put("des",cdc.getDesCar());
                jsonCargos.put(jsonCargo);
            }
            return WebResponse.crearWebResponseExito("Exito al listar los cargos",jsonCargos);*/
        }catch (Exception e){
            throw e;
            //return WebResponse.crearWebResponseError("Error al listar los cargos",WebResponse.BAD_RESPONSE);
        }
    }
    
}
