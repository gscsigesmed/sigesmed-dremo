/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Matricula;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface MatriculaDao extends GenericDao<Matricula>{
    public String buscarUltimoCodigo();
    public List<Matricula> MatriculasxApoderado(int orgid);    
    public List<Matricula> MatriculasxEstudiante(int orgid);    
    //public List<Area> buscarConOrganizacionYAreaPadre();
    //public List<Area> buscarPorOrganizacion(int organizacionID);
}
