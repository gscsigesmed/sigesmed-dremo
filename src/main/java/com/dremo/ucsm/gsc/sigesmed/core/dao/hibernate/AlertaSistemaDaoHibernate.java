/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AlertaSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.AlertaSistema;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author abel
 */
public class AlertaSistemaDaoHibernate extends GenericDaoHibernate<AlertaSistema> implements AlertaSistemaDao {

    @Override
    public List<AlertaSistema> buscarTodos() {
        List<AlertaSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Alertas
            String hql = "SELECT a FROM AlertaSistema a ORDER BY a.aleId" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Alertas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Alertas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<AlertaSistema> buscarConFuncion() {
        List<AlertaSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Alertas
            String hql = "SELECT a FROM AlertaSistema a JOIN FETCH a.funcionSistema ORDER BY a.aleId" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Alertas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Alertas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
}
