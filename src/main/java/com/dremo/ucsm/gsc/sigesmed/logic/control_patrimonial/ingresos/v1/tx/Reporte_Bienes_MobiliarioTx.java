/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class Reporte_Bienes_MobiliarioTx {
    
       SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
      public Mitext generar_reporte_mobiliario(List<BienesMuebles> bm , Organizacion org){
         
        try{
            
             /* DATOS DE LA ORGANIZACION*/
            String nombre_inst = org.getNom();
        //    String ugel = org.getOrganizacionPadre().getNom();
            String denominacion = org.getAli();
            String ubicacion = org.getDir();
            
            
                /*TITULO*/  
               String titulo = "INVENTARIO FISICO GENERAL DE MOBILIARIO ";

        
                /*CABECERA DE TABLA*/
                String [] cab = {"Codigo Patrimonial","Descripcion del Bien","Marca","Modelo","Serie","Dimensiones","Color","Valor en Libros","Fecha de Ingreso","Dcoumentos Ingreso","Estado del Bien","Ubicacion","Procedencia","Usuario a Cargo Bien","Tiempo de Duracion"};

                 
                /*LLENAMOS LOS DATOS A LA CABECERA DEL REPORTE*/
                  Mitext m = null; 
                   m = new Mitext(true,titulo);
                   m.newLine(2);
                   m.agregarParrafo("Nombre de la Institucion : " +nombre_inst);
                   m.newLine(3);
                   m.agregarParrafo("Ubicacion : " +ubicacion);
                   m.newLine(4);
                   m.agregarParrafo("Denominacion : " + denominacion);
                   m.newLine(5);
            //       m.agregarParrafo("UGEL : " + ugel);

                            /*CONSTRUIMOS EL OBJETO REPORTE*/
                     float columnWidths_1 []= {1,2,1,1,1,1,1,1,1,2,1,2,1,1,1};
                     GTabla tabla_1 = new GTabla(columnWidths_1);
                     tabla_1.setWidthPercent(100);
                     tabla_1.build(cab);


                   /*INIZIALIZAMOS LA DATA A VACIO*/ 
                     int data_length = cab.length;
                     String[] archivos_data = new String[data_length];
                     for(int i=0;i<data_length;i++){
                         archivos_data[i]=" ";
                     }
    
                    /*LLENAMOS LA DATA A LA TABLA*/  
                     GCell[] cell ={tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)
                    ,tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)
                     ,tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)};
     
                    int size = bm.size();

                    for(int i=0 ; i<size ; i++){
                        BienesMuebles bien_mueble = bm.get(i);  
                        String cod_pat = Integer.toString(bien_mueble.getCon_pat_id());
                        String des_bie = bien_mueble.getDes_bie();
                        String marca = bien_mueble.getDtm().getMarc();
                        String modelo = bien_mueble.getDtm().getMod();
                        String serie = bien_mueble.getDtm().getSer();
                        String dim = Integer.toString(bien_mueble.getDtm().getDim());
                        String color = bien_mueble.getDtm().getCol();
                        String val_lib = "";
                        String fec_ing = formatter.format(bien_mueble.getFec_reg());
                        String doc_ing = bien_mueble.getRut_doc_bie();
                        String est_bie = bien_mueble.getEstado_bie();
                        String ubicac = bien_mueble.getAmbiente().getDes();
                        String proc = "";
                        String user = Integer.toString(bien_mueble.getUsu_mod());
                        String tiem_dur = "";
                        
                         archivos_data[0] = cod_pat;
                         archivos_data[1] = des_bie;
                         archivos_data[2] = marca;
                         archivos_data[3] = modelo;
                         archivos_data[4] = serie;
                         archivos_data[5] = dim;
                         archivos_data[6] = color;
                         archivos_data[7] = val_lib;
                         archivos_data[8] = fec_ing;
                         archivos_data[9] = doc_ing;
                         archivos_data[10] = est_bie;
                         archivos_data[11] = ubicac;
                         archivos_data[12] = proc;
                         archivos_data[13] = user;
                         archivos_data[14] = tiem_dur;
                        
                          tabla_1.processLineCell(archivos_data,cell);  
                    }
  
                      m.agregarTabla(tabla_1);
                      
                        /*PIE DE PAGINA*/
                        m.newLine(6);
                        m.agregarParrafo("NOTA : * El Presente Formato sera utilizado para Bienes que nos se encuentren en el"
                                + "Catalogo SBN ");
                        m.newLine(7);
                        m.agregarParrafo("** Estado de Conservacion : B=Bueno , M= Malo , R=Regular ,M= Malo , MM = Muy Malo");
                        
                      
                      
                      
                      
                      return m;        
        }catch(Exception e){
              System.out.println(e);
              return null;
 
        }
    
    
      }  
}
