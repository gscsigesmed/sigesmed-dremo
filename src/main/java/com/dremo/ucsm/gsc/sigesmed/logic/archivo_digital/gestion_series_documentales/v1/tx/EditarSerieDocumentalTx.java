/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jeferson
 */
public class EditarSerieDocumentalTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        SerieDocumental serie_doc = null;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int serie_id = requestData.getInt("serie_id");
            int areaID = requestData.getInt("areaID");
            int uni_org_id = requestData.getInt("uni_org_id");
            String cod_serie = requestData.getString("cod_serie");
            String nombre = requestData.getString("nom_serie");
            int valor = requestData.getInt("val_serie");
            
            serie_doc = new SerieDocumental(serie_id,areaID,uni_org_id,cod_serie,nombre,valor);
       
        }catch(Exception e){
              System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar la Serie Documental, datos incorrectos", e.getMessage() ); 
        }
       
        try{
            SerieDocumentalDAO serieDAO = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");  
            serieDAO.update(serie_doc);
            
        }catch(Exception e){
            
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar La Serie Documental", e.getMessage() );
        }
          return  WebResponse.crearWebResponseExito("La Serie Documental se pudo editar correctamente");
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
