/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.TablaRetencionDocumentosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.TablaRetencionDocumentos;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class ObtenerTablaRetencionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int ser_doc_id = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            ser_doc_id = requestData.getInt("serie_id");
        
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Obtener la Tabla de Retencion ", e.getMessage() );
        }
        List<TablaRetencionDocumentos> tabla_retencion = null;
        TablaRetencionDocumentosDAO trd_dao = (TablaRetencionDocumentosDAO)FactoryDao.buildDao("sad.TablaRetencionDocumentosDAO");
        
        try{
            tabla_retencion = trd_dao.buscarPorCodigo(ser_doc_id);
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Obtener la Tabla de Retencion ", e.getMessage() );
        }
        JSONArray miArray = new JSONArray();
        for(TablaRetencionDocumentos trdoc : tabla_retencion){
            JSONObject oResponse = new JSONObject();
            oResponse.put("serie_id",ser_doc_id);
            oResponse.put("ag",trdoc.getag());
            oResponse.put("ap",trdoc.getap());
            oResponse.put("oaa",trdoc.getoaa());
            miArray.put(oResponse);
        }
         return WebResponse.crearWebResponseExito("Se Obtuvo la Tabla de Retencion Correctamente",miArray); 
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
