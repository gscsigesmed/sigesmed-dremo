/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Administrador
 */
public class ProyectoActividadesDaoHibernate extends GenericDaoHibernate<ProyectoActividades> implements ProyectoActividadesDao{
    @Override
    public String buscarUltimoCodigo() {
        String actividades = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo actividades de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            actividades = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(actividades==null)
//                actividades = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo actividades \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo actividades \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return actividades;
    }
    public List<ProyectoActividades> listarActividadesxProyecto(int proid){
        List<ProyectoActividades> actividades = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo actividades de tramite
            String hql = "SELECT a FROM ProyectoActividades a "
                    + "WHERE a.proId:=p1"
                    + "ORDER BY a.pacId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", proid);            
            actividades = (List<ProyectoActividades>)query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo listar las actividades \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las actividades \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return actividades;
    }
}
