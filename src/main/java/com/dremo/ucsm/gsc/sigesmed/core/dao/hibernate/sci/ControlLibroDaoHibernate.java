/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.ControlLibroDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;


/**
 *
 * @author Administrador
 */
public class ControlLibroDaoHibernate  extends GenericDaoHibernate<ControlLibro> implements ControlLibroDao{
     public List<ControlLibro> listarControles(int organizacionID,int year){
        List<ControlLibro> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT cl FROM ControlLibro cl  WHERE cl.estReg!='E' AND cl.organizacion.orgId=:p1 AND YEAR(cl.fecCie)=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", year);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
     }
}
