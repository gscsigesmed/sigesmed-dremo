/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ClienteProveedor;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarClienteProveedorTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        List<ClienteProveedor> cliPro=null;       
        LibroCajaDao cliProDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        
        try {
            cliPro= cliProDao.listarClienteProveedor();
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar ", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
        for(ClienteProveedor cP:cliPro ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipoDocumento",cP.getTipoDocumentoIdentidad().getNom());
            oResponse.put("clienteProveedorID",cP.getCliProId());
            oResponse.put("datos",cP.getDat());    
            oResponse.put("tipo",cP.getTip());    
            oResponse.put("estado",""+cP.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
        
        
        
    }
    
}
