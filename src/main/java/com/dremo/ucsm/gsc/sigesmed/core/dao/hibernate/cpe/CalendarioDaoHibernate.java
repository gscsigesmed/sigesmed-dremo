    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.CalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Carlos
 */
public class CalendarioDaoHibernate extends GenericDaoHibernate<DiasEspeciales> implements CalendarioDao {

    

    @Override
    public Integer getLastIdDiaEspecial() {
        Integer id=-1;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT MAX(de.diaEspId) FROM DiasEspeciales de WHERE de.estReg<>'E'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            id = (Integer)query.uniqueResult();
            
        } catch (Exception e) {
            System.out.println("No se pudo hallar el dia Origen \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo hallar el dia Origen \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return id;
    }

    
}
