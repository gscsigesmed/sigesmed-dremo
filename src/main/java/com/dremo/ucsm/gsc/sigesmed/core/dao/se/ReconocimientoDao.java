/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public interface ReconocimientoDao extends GenericDao<Reconocimiento>{
    public List<Reconocimiento> listarxFichaEscalafonaria(int ficEscId);
    public Reconocimiento buscarPorId(Integer recId);
    public JSONObject contarxOrganizacion(int orgId);
}
