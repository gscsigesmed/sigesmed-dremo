/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarDocumentosDeTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int bandejaTarea = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            bandejaTarea = requestData.getInt("bandejaTareaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los documentos de la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TareaDocumento> documentos = null;
        TareaEscolarDao documentoDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            documentos = documentoDao.verDocumentosTarea(bandejaTarea);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documentos de la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los documentos de la tarea", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(TareaDocumento documento:documentos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tareaDocumentoID",documento.getTarDocId() );
            oResponse.put("documento",documento.getNomDoc());
            oResponse.put("url",Sigesmed.UBI_ARCHIVOS+Tarea.BANDEJA_TAREA_PATH);
            
            oResponse.put("i",i++);
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listaron los documentos de la tarea correctamente",miArray);        
        //Fin
    }
    
}

