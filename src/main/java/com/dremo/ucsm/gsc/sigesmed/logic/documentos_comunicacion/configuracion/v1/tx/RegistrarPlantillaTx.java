/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.PlantillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


/**
 *
 * @author Administrador
 */
public class RegistrarPlantillaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer plantillaId;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            plantillaId = requestData.getInt("plantillaID");        
       
//            plantilla = new Plantilla(plantillaId);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener identificador de Plantilla a registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Boolean estadoOperacion=Boolean.FALSE;
        PlantillaDao plantillaDao = (PlantillaDao)FactoryDao.buildDao("sdc.PlantillaDao");
        try{
            estadoOperacion =plantillaDao.registrarPlantilla(plantillaId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Registrar la Plantillas ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
       
        
        return WebResponse.crearWebResponseExito("Se Registro correctamente");        
        //Fin
    }
    
}

