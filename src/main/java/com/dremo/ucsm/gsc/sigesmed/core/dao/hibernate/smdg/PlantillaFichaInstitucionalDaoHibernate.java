/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import java.util.List;
import org.hibernate.Filter;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class PlantillaFichaInstitucionalDaoHibernate extends GenericDaoHibernate<PlantillaFichaInstitucional> implements PlantillaFichaInstitucionalDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    @Override
    public List<Object[]> listarPlantillas(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Object[]> plantillas = null;
        Transaction t = session.beginTransaction();
        Query query = null;
        try{
            
            query = session.createSQLQuery("SELECT p.pfi_ins_id, p.pfi_cod, p.pfi_nom, pt.pti_nom, p.pfi_fec\n" +
                "  FROM institucional.plantilla_ficha_institucional p\n" +
                "  JOIN institucional.plantilla_tipo pt ON p.pti_id = pt.pti_id ORDER BY p.pfi_ins_id;");  
                        
//            String hql = "SELECT p FROM PlantillaFichaInstitucional p join fetch p.tipo t";
            plantillas = query.list();
            t.commit();
            
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las plantillas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las plantillas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return plantillas;
    }
    
    @Override
    public PlantillaFichaInstitucional obtenerPlantilla(int plaId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaFichaInstitucional plantilla = null;
        Transaction t = session.beginTransaction();
        
        session.enableFilter("filtroplantilla");
        session.enableFilter("filtrogrupo");
        
        try{                                        
            String hql = "SELECT p FROM PlantillaFichaInstitucional p " 
                    + "join fetch p.tipo tp "
                    + "left join fetch p.grupos gru "
//                    + "left join fetch gru.indicadores ind "
//                    + "left join fetch ind.items it "
                    + "left join fetch gru.tipoGrupo tg "
                    + "WHERE p.pfiInsId = " + plaId + " AND p.estReg = 'A' " 
                    + "ORDER BY p.pfiInsId";
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            plantilla = (PlantillaFichaInstitucional)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener la plantilla \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la plantilla \\n "+ e.getMessage());            
        }
        finally{
            session.disableFilter("filtroplantilla");
            session.disableFilter("filtrogrupo");
            session.close();
        }
        
        return plantilla;
    }
    
    @Override
    public PlantillaFichaInstitucional getPlantilla(int plaId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaFichaInstitucional plantilla = null;
        Transaction t = session.beginTransaction();
        try{                                        
            plantilla = (PlantillaFichaInstitucional)session.get(PlantillaFichaInstitucional.class, plaId);
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener la plantilla \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la plantilla \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return plantilla;
    }
    
}
