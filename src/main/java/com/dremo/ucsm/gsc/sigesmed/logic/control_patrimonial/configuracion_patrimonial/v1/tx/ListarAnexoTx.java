/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AnexoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AnexoBienesDAO;
import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AnexoBienesDAO;
/**
 *
 * @author Administrador
 */
public class ListarAnexoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
         JSONArray miArray = new JSONArray(); 
         List<AnexoBienes> anexos = null;
         
         try{
             
            JSONObject requestData = (JSONObject)wr.getData();
             
            AnexoBienesDAO anexo_dao = (AnexoBienesDAO)FactoryDao.buildDao("scp.AnexoBienesDAO");
            
            anexos = anexo_dao.listarAnexoBienes();
            
            for(AnexoBienes anexo : anexos){
               JSONObject oResponse = new JSONObject();
               oResponse.put("ane_id",anexo.getAn_id());
               oResponse.put("ane_nom",anexo.getAn_des());
               miArray.put(oResponse);
           } 
          
         }catch(Exception e){
            System.out.println("No se pudo Listar el catalogo de anexo de bienes\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el catalogo de anexo de bienes", e.getMessage() );        
 
         }
         
            return WebResponse.crearWebResponseExito("Se Listo el catalogo de anexo correctamente",miArray); 
       
         
     }
    
}
