/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarUsuarioPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo listar usuarios por organizacion, datos incorrectos", e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Usuario> usuarios = null;
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        try{
            usuarios = usuarioDao.buscarConRolPorOrganizacion(organizacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los usuarios del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los usuarios del Sistema ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        
        JSONArray miArray = new JSONArray();
        for(Usuario usuario:usuarios ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("usuarioID",usuario.getUsuId());
            oResponse.put("nombreUsuario",usuario.getNom());            
            oResponse.put("password",usuario.getPas());
            oResponse.put("estado",""+usuario.getEstReg());
            
            //datos de persona
            oResponse.put("DNI", usuario.getPersona().getDni());
            oResponse.put("nombres", usuario.getPersona().getNombrePersona());
            oResponse.put("nombre", usuario.getPersona().getNom());
            oResponse.put("paterno", usuario.getPersona().getApePat());
            oResponse.put("materno", usuario.getPersona().getApeMat());
            oResponse.put("materno", usuario.getPersona().getEmail());
            oResponse.put("numero1", usuario.getPersona().getNum1());
            oResponse.put("numero2", usuario.getPersona().getNum2());
            
            //datos de session
            
            JSONArray aSessiones = new JSONArray();
            for( UsuarioSession session: usuario.getSessiones() ){
                JSONObject oSession = new JSONObject();
                oSession.put("rolID",session.getRol().getRolId() );
                oSession.put("rol",session.getRol().getNom() );
                oSession.put("sessionID",session.getUsuSesId() );
                
                if(session.getArea()!=null){
                    oSession.put("areaID",session.getArea().getAreId());
                    oSession.put("area",session.getArea().getNom());
                }
                oSession.put("estado",""+session.getEstReg());

                aSessiones.put(oSession);
            }
            oResponse.put("sessiones", aSessiones);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}



