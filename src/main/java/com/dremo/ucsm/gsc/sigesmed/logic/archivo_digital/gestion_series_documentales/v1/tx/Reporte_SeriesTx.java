

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;



/**
 *
 * @author Jeferson
 */
public class Reporte_SeriesTx  implements ITransaction {
    
    @Override
    public WebResponse execute(WebRequest wr) {
        int area_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            area_id = requestData.getInt("area_id");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("Error al Elegir Area , ERROR AL GENERAR REPORTE ", e.getMessage() );
        }
        
        
        /*Listamos todas las series perteneciente a dicha area*/
        List<SerieDocumental> todas_series ;
        try{
            SerieDocumentalDAO serie_dao = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");
            todas_series = serie_dao.buscarPorArea(area_id);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar Todas Las Series Documentales , ERROR AL GENERAR REPORTE  ", e.getMessage() );
        }
        
        /*Listamos las Unidades Organicas perteneciente a dicha area*/
        List<UnidadOrganica> unidades_organicas ;
        try{
            UnidadOrganicaDAO unidad_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("sad.UnidadOrganicaDAO");
            unidades_organicas=unidad_dao.buscarPorArea(area_id);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Unidades Organicas , ERROR AL GENERAR REPORTE  ", e.getMessage() );
        }
        
        /*Listamos Las Series Asociadas a dicha unidad organica */
         List<List<SerieDocumental>> total_series_unidad = new ArrayList<>();
        try{
            List<SerieDocumental> series_por_unidad;
            for(int i= 0 ; i<unidades_organicas.size();i++){
                SerieDocumentalDAO serie_dao = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");
                series_por_unidad =  serie_dao.buscarPorUnidadOrganica(unidades_organicas.get(i).getUniOrgId());
                total_series_unidad.add(series_por_unidad);
            }
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las series por unidad organica , ERROR REPORTE  ", e.getMessage() );
        }
        
        /*Preparamos el Reporte*/
         Mitext m = null; 
        try{
            int COLUMNAS = 3;
            int cantidad = 0;
            cantidad = todas_series.size() / 3; /*Cantidad de SERIES*/
             m = new Mitext(true,"REPORTE DE SERIES DOCUMENTALES");
             m.newLine(2);
             m.agregarParrafo("AREA : Archivo Central");
             
            /* Preparamos la primera tabla (SERIES DOCUMENTALES GENERALES)*/
            float[] columnWidths={4,4,4};
            GTabla t_general = new GTabla(columnWidths);
            t_general.build();
            t_general.addHeaderCell(new Cell(1,12).setBorder(Border.NO_BORDER).add(new Paragraph("  SERIES DOCUMENTALES GENERALES ")).setFontSize(10).setBorder(Border.NO_BORDER));
            GCell[] cell ={t_general.createCellCenter(1,1),t_general.createCellCenter(1,1),t_general.createCellCenter(1,1)};
           
            
            /*Preparamos nuestro arreglo de Serie Generales para que imprima en filas */
            
            int tamanio_series = todas_series.size();
            int i_series = 0;
            for(int i = 0;i<cantidad;i++){
                String[] serie_fila = new String[COLUMNAS];
                for(int j=0;j<COLUMNAS;j++){
                    if(todas_series.get(i_series).getNombre() == "" | i_series>tamanio_series){break;}
                    serie_fila[j]=todas_series.get(i_series).getNombre();
                    i_series++;
                    
                }
                t_general.processLineCell(serie_fila,cell);
              
            }
            m.agregarTabla(t_general); /*Insertamos nuestra primera tabla al reporte */
            
            
            /*Preparamos la segunda tabla del Reporte (SERIES DOCUMENTALES ESPECIFICAS)*/
            
            float[] columnWidths2={6,6};
            
            GTabla t_especifica = new GTabla(columnWidths2);
            t_especifica.build();
            t_especifica.addHeaderCell(new Cell(1,12).setBorder(Border.NO_BORDER).add(new Paragraph("  SERIES DOCUMENTALES ESPECIFICAS ")).setFontSize(10).setBorder(Border.NO_BORDER));
            
            
              for(int i=0;i<total_series_unidad.size();i++){
                List<GCell> cell2 = new ArrayList<>();
                List<String> data_series_esp = new ArrayList<>();
                /*Unidad Organica*/
                if(total_series_unidad.get(i).size()== 0){
                   cell2.add(t_especifica.createCellCenter(1,1));
                   data_series_esp.add(unidades_organicas.get(i).getnombre());
                   cell2.add(t_especifica.createCellCenter(1,1));
                   data_series_esp.add("-");
                }else{
                    cell2.add(t_especifica.createCellCenter(total_series_unidad.get(i).size(),1));
                    data_series_esp.add(unidades_organicas.get(i).getnombre());
                } 
                for(int j=0;j<total_series_unidad.get(i).size();j++){
                    /* Series */
                    cell2.add(t_especifica.createCellCenter(1,1));
                    data_series_esp.add(total_series_unidad.get(i).get(j).getNombre());
                }
                String[] data_series = new String[data_series_esp.size()];
                data_series = data_series_esp.toArray(data_series);
                
                GCell[] cell2_ = new GCell[cell2.size()]; 
                cell2_ = cell2.toArray(cell2_);
                t_especifica.processLineCell(data_series,cell2_);

            }
            m.agregarTabla(t_especifica);
               
            
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError(" ERROR AL PREPARAR REPORTE  ", e.getMessage() );
        }
        
        
        /*Mostramos el reporte*/
        JSONArray miArray = new JSONArray();
        try{
            
            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();        
            oResponse.put("datareporte",m.encodeToBase64());
            miArray.put(oResponse); 
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError(" ERROR AL GENERAR REPORTE  ", e.getMessage() );
        }
        
        
        return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...",miArray); 
    }
    
    
}
