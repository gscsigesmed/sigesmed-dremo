/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.tipo_tramite.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RequisitoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RutaTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */

public class ListarRequisitoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoTramite tipoTramite = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int idTipoTramite = requestData.getInt("tipoTramiteID");
            boolean tupa = requestData.getBoolean("tupa");
            
            tipoTramite = new TipoTramite(idTipoTramite);
            tipoTramite.setEsTup(tupa);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipos Requisitos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<RequisitoTramite> requisitos = null;
        List<RutaTramite> rutas = null;
        TipoTramiteDao tipoTramiteDao = (TipoTramiteDao)FactoryDao.buildDao("std.TipoTramiteDao");
        try{
            requisitos =tipoTramiteDao.buscarRequisitos(tipoTramite);
            rutas =tipoTramiteDao.buscarRutas(tipoTramite);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar tipos de requisitos ", e.getMessage() );
        }
        //Fin
        JSONObject oResponse = new JSONObject();
        
        JSONArray aRequisitos = new JSONArray();
        for(RequisitoTramite rt: requisitos){
            JSONObject oRequisito = new JSONObject();
            oRequisito.put("requisitoID",rt.getReqTraId() );
            oRequisito.put("descripcion",rt.getDes() );
            oRequisito.put("nombreArchivo",rt.getNomArcAdj());
            oRequisito.put("url",Sigesmed.UBI_ARCHIVOS+"/tramite/");
            aRequisitos.put(oRequisito);
        }
        oResponse.put("requisitos",aRequisitos);
        
        JSONArray aRutas = new JSONArray();
        //si es tupa llamamos a los tipos de area
        if(tipoTramite.getEsTup())
            for(RutaTramite rt:rutas){
                JSONObject oRuta = new JSONObject();
                oRuta.put("rutaID",rt.getRutTraId() );
                oRuta.put("descripcion",rt.getDes() );
                
                JSONObject areaOri = new JSONObject();                    
                areaOri.put("nombre",rt.getTipoAreaOrigen().getNom());
                areaOri.put("areaOriID",rt.getTipoAreaOrigen().getTipAreId());                
                
                JSONObject areaDes = new JSONObject();                    
                areaDes.put("nombre",rt.getTipoAreaDestino().getNom());
                areaDes.put("areaDesID",rt.getTipoAreaDestino().getTipAreId());

                oRuta.put("areaOri",areaOri );
                oRuta.put("areaOriID",rt.getTipoAreaOrigen().getTipAreId());
                oRuta.put("areaDes",areaDes );
                oRuta.put("areaDesID",rt.getTipoAreaDestino().getTipAreId());

                aRutas.put(oRuta);
            }
        //si no lo es llamamos a las areas de la orgnizacion
        else
            for(RutaTramite rt:rutas){
                JSONObject oRuta = new JSONObject();
                oRuta.put("rutaID",rt.getRutTraId() );
                oRuta.put("descripcion",rt.getDes() );

                JSONObject areaOri = new JSONObject();
                areaOri.put("areaOriID",rt.getAreaOrigen().getAreId());
                areaOri.put("nombre",rt.getAreaOrigen().getNom());                    
                JSONObject areaDes = new JSONObject();
                areaDes.put("areaDesID",rt.getAreaDestino().getAreId());
                areaDes.put("nombre",rt.getAreaDestino().getNom());

                oRuta.put("areaOri",areaOri );
                oRuta.put("areaOriID",rt.getAreaOrigen().getAreId());
                oRuta.put("areaDes",areaDes );
                oRuta.put("areaDesID",rt.getAreaDestino().getAreId());

                aRutas.put(oRuta);
            }                
        oResponse.put("rutas",aRutas);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);
    }
    
}

