/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicialDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import java.text.SimpleDateFormat;


/**
 *
 * @author Administrador
 */


public class ObtenerInventarioInicialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        JSONArray miArray = new JSONArray();
        JSONObject inv_ini = null;
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        InventarioInicial ii = null;
        MovimientoIngresos mi = null;
        List<InventarioInicialDetalle> iidet = null;
        JSONObject requestData = (JSONObject)wr.getData();        
        try{
         //   JSONObject fil = requestData.getJSONObject("filtro");
            int id_inv = requestData.getInt("id_inv");
            int org_id = requestData.getInt("org_id");
            
            /*Obtenemos la Cabecera del Inventario*/
            InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
            ii = inv_ini_dao.obtenerInventarioInicial(id_inv,org_id);
            
            /*Obtenemos el Movimiento*/
            MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO");
            mi = mov_ing_dao.obtener_movimiento(ii.getMov_ing_id());
            
            /*Obtenemos el Detalle del Inventario Inicial*/
            iidet=inv_ini_dao.obtenerDetalleInventarioInicial(id_inv, org_id);
            
            JSONObject oResponse1 = new JSONObject();
            
            /*MOVIMIENTO DE INGRESOS*/
            oResponse1.put("mov_ing_id",mi.getMov_ing_id());
            oResponse1.put("tip_mov_ing",mi.getTip_mov_ing_id());
            oResponse1.put("cont_pat_id",mi.getCon_pat_id());
            oResponse1.put("num_res",mi.getNum_res());
            String fec_res = formatter.format(mi.getFec_res());
            oResponse1.put("fec_res",fec_res);
            oResponse1.put("obs",mi.getObs());
            miArray.put(oResponse1);

            JSONObject oResponse2 = new JSONObject();
            /*INVENTARIO INICIAL CABECERA*/
            oResponse2.put("inv_id",ii.getInv_ini_id());
            oResponse2.put("con_pat_id",ii.getCon_pat_id());
            oResponse2.put("fla_cie",String.valueOf(ii.getFla_cie()));
            miArray.put(oResponse2);
            
            /*INVENTARIO INICIAL DETALLE*/
            
            for(InventarioInicialDetalle iidd : iidet){
                JSONObject oResponse3 = new JSONObject();
                oResponse3.put("cod_inv_det",iidd.getInv_ini_det());
                oResponse3.put("cod_pat",iidd.getBien_inmueble().getCat_bie_id());
                oResponse3.put("cod_bie",iidd.getBien_inmueble().getCod_bie());
                oResponse3.put("des_bie",iidd.getBien_inmueble().getDes_bie());
                oResponse3.put("fec_reg",iidd.getBien_inmueble().getFec_reg());
                oResponse3.put("est_bien",iidd.getBien_inmueble().getEstado_bie());
                oResponse3.put("ubi_bien",iidd.getBien_inmueble().getAmbiente().getDes());
                oResponse3.put("an_bien",iidd.getBien_inmueble().getAnexo().getAn_des());
                miArray.put(oResponse3);
            }
        } 
    
        catch(Exception e){
            System.out.println("No se pudo Obtener el Inventario Inicial\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Obtener el Inventario Inicial", e.getMessage() );
        }
        
          return WebResponse.crearWebResponseExito("Se obtuvo el Inventario Inicial Correctamente",miArray); 
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 
}
