/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class ColegiaturaDaoHibernate extends GenericDaoHibernate<Colegiatura> implements ColegiaturaDao{

    @Override
    public List<Colegiatura> listarxFichaEscalafonaria(int ficEscId) {
        List<Colegiatura> colegiaturas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT col from Colegiatura as col "
                    + "join fetch col.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND col.estReg='A'";
            Query query = session.createQuery(hql);
            colegiaturas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las colegiaturas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las colegiaturas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return colegiaturas;
    }

    @Override
    public Colegiatura buscarPorId(Integer colId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Colegiatura col = (Colegiatura)session.get(Colegiatura.class, colId);
        session.close();
        return col;
    }
}
