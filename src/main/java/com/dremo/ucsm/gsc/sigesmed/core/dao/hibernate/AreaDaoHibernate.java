/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */

public class AreaDaoHibernate extends GenericDaoHibernate<Area> implements AreaDao {

    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT a.cod FROM Area a WHERE a.estReg!='E' ORDER BY a.cod DESC ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            codigo = ((String)query.uniqueResult());
            
            //solo cuando no hay ningun registro aun
            if(codigo==null)
                codigo = "0000";
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return codigo;
    }

    @Override
    public List<Area> buscarPorOrganizacion(int organizacionID) {
        List<Area> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //listar Areas por Organizacion
            String hql = "SELECT a FROM Area a JOIN FETCH a.organizacion JOIN FETCH a.tipoArea LEFT JOIN FETCH a.areaPadre WHERE a.estReg!='E' and a.organizacion.orgId=:p1" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            objetos = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar las Areas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Areas \\n "+ e.getMessage());            
        }
        finally{
            
            session.close();
        }
        return objetos;        
    }

    @Override
    //solo los que no han sido elimados
    public List<Area> buscarConOrganizacionYAreaPadre() {
        List<Area> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Areas
            String hql = "SELECT a FROM Area a JOIN FETCH a.organizacion JOIN FETCH a.tipoArea LEFT JOIN FETCH a.areaPadre WHERE a.estReg!='E'" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Areas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Areas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void cambiarResponsable(Integer resID, Date fecha, int usuID, int areID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE administrativo.area SET res_id=:p1 , fec_mod=:p2, usu_mod=:p3 WHERE are_id=:p4";
            SQLQuery query = session.createSQLQuery(hql);
            query.setParameter("p1", resID );
            query.setParameter("p2", fecha );
            query.setParameter("p3", usuID );
            query.setParameter("p4", areID );
            query.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo cambiar de responsable de area \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo cambiar de responsable de area \n "+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
}
