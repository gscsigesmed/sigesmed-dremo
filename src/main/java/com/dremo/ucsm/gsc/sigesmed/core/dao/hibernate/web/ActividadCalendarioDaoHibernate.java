package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

public class ActividadCalendarioDaoHibernate extends GenericDaoHibernate<ActividadCalendario> implements ActividadCalendarioDao {

    private static final Logger logger = Logger.getLogger(ActividadCalendarioDaoHibernate.class.getName());

    @Override
    public List<ActividadCalendario> listarActividades(int usuCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(ActividadCalendario.class)
                    .add(Restrictions.ne("estReg", 'E'))
                    .add(Restrictions.eq("usuario.usuSesId", usuCod))
                    .addOrder(Order.asc("fecIni"))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarActividades", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public ActividadCalendario buscarActividad(int actCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (ActividadCalendario) session.get(ActividadCalendario.class, actCod);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarActividad", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<ActividadCalendario> buscarInstitucionalesInicio(int usuCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT a FROM ActividadCalendario a "
                    + "JOIN FETCH a.funcion "
                    + "WHERE a.estReg = :estReg AND a.usuario.usuSesId = :usuSesId "
                    + "AND a.tipAct != :tipAct AND a.fecIni < current_timestamp "
                    + "AND a.notIni = :notIni";

            Query query = session.createQuery(hql);
            query.setParameter("estReg", 'A');
            query.setParameter("usuSesId", usuCod);
            query.setParameter("tipAct", 'P');
            query.setParameter("notIni", false);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscar", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<ActividadCalendario> buscarInstitucionalesFin(int usuCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT a FROM ActividadCalendario a "
                    + "JOIN FETCH a.funcion "
                    + "WHERE a.estReg = :estReg AND a.usuario.usuSesId = :usuSesId "
                    + "AND a.tipAct != :tipAct AND a.fecFin < current_timestamp "
                    + "AND a.notFin = :notFin";

            Query query = session.createQuery(hql);
            query.setParameter("estReg", 'I');
            query.setParameter("usuSesId", usuCod);
            query.setParameter("tipAct", 'P');
            query.setParameter("notFin", false);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscar", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<Integer> buscarFuncionalidades(int usuCod, int rolCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT(a.funcion.funSisId) FROM ActividadCalendario a "
                    + "WHERE a.usuario.usuSesId = :usuSesId "
                    + "AND a.tipAct != :tipAct AND a.estReg != :estRegAct "
                    + "AND a.funcion IN "
                        + "(SELECT r.claveRolFuncion.funcionSistema FROM RolFuncion r "
                            + "WHERE r.claveRolFuncion.rol.rolId = :rolId AND "
                            + "r.claveRolFuncion.funcionSistema.estReg = :estRegFun)";

            Query query = session.createQuery(hql);
            query.setParameter("usuSesId", usuCod);
            query.setParameter("estRegAct", 'E');
            query.setParameter("tipAct", 'P');
            query.setParameter("rolId", rolCod);
            query.setParameter("estRegFun", 'A');

            List<Integer> codes = query.list();
            List<Integer> result = new ArrayList<>();

            for(Integer code: codes) {
                hql = "SELECT a FROM ActividadCalendario a "
                    + "WHERE a.usuario.usuSesId = :usuSesId AND a.tipAct != :tipAct AND a.estReg != :estReg "
                    + "AND a.funcion.funSisId = :funSisId AND a.fecIni <= current_timestamp AND current_timestamp <= a.fecFin";
                
                query = session.createQuery(hql);
                query.setParameter("usuSesId", usuCod);
                query.setParameter("estReg", 'E');
                query.setParameter("tipAct", 'P');
                query.setParameter("funSisId", code);
                
                if(query.list().size() == 0)
                    result.add(code);
            }
            
            return result;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscar", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public ActividadCalendario buscarActividadConPadre(int actCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT a FROM ActividadCalendario a "
                    + "JOIN FETCH a.actividadPadre "
                    + "WHERE a.estReg != :estReg AND a.actCalId = :actCalId";

            Query query = session.createQuery(hql);
            query.setParameter("estReg", 'E');
            query.setParameter("actCalId", actCod);

            return (ActividadCalendario) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarActividad", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<ActividadCalendario> buscarActividades(int actCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(ActividadCalendario.class)
                    .add(Restrictions.eq("actividadPadre.actCalId", actCod))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarActividades", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public JSONArray buscarActividadesFecha(int usuCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT DISTINCT(to_char(a.fecIni, 'yyyy-mm-dd')) FROM ActividadCalendario a "
                    + "WHERE a.estReg != :estReg AND a.usuario.usuSesId = :usuSesId";

            Query query = session.createQuery(hql);
            query.setParameter("estReg", 'E');
            query.setParameter("usuSesId", usuCod);
            
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            List<String> rows = query.list();
            JSONArray array = new JSONArray();
            
            try { 
                for (String row : rows) {
                    JSONObject object = new JSONObject();
                    object.put("date", format.parse(row).getTime());
                    object.put("status", "full");
                    array.put(object);
                }
            } catch (ParseException e) {
                logger.log(Level.SEVERE, "buscarActividadesFecha", e);    
            }
            
            return array;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarActividadesFecha", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
