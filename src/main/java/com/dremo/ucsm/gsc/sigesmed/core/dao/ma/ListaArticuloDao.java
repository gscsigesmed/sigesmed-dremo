/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface ListaArticuloDao extends GenericDao<ListaArticulo>{
    void eliminarArticulosDeLista(int idList);
    List <ArticuloEscolar> listarArticulosDeLista (int idLista);
    ListaArticulo buscarArticulo (int idLista, int idArticulo);
    ListaArticulo buscarPorId(int idListaArticulo);
    
}
