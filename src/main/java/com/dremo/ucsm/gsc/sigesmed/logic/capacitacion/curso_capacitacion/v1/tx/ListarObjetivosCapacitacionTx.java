package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ObjetivoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ObjetivoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarObjetivosCapacitacionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarObjetivosCapacitacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data =  (JSONObject) wr.getData();        
            int curCapId = data.getInt("cod");
            
            ObjetivoCapacitacionDao objetivoDao = (ObjetivoCapacitacionDao) FactoryDao.buildDao("capacitacion.ObjetivoCapacitacionDao");
            List<ObjetivoCapacitacion> objetivos = objetivoDao.listarObjetivosCursoCapacitacion(curCapId);

            JSONObject objCapacitacion = new JSONObject();
            
            if(objetivos.size() > 0) {
                JSONArray generales = new JSONArray();
                JSONArray especificos = new JSONArray();
                JSONArray metas = new JSONArray();
                
                for(ObjetivoCapacitacion objetivo : objetivos) {
                    JSONObject obj = new JSONObject();
                    obj.put("id", objetivo.getObjCapId());
                    obj.put("des", objetivo.getDes());
                    obj.put("sel", false);

                    switch(objetivo.getTip()) {
                        case 'G':   generales.put(obj);
                                    break;
                        
                        case 'E':   especificos.put(obj);
                                    break;
                        
                        case 'M':   metas.put(obj);
                                    break;
                    }
                }
                
                objCapacitacion.put("generales", generales);
                objCapacitacion.put("especificos", especificos);
                objCapacitacion.put("metas", metas);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente",WebResponse.OK_RESPONSE)
                    .setData(objCapacitacion);
        } catch (Exception e ){
            logger.log(Level.SEVERE,"ListarObjetivosCapacitacionTx",e);
            return WebResponse.crearWebResponseError("Error al listar los objetivos",WebResponse.BAD_RESPONSE);
        }
    }        
}
