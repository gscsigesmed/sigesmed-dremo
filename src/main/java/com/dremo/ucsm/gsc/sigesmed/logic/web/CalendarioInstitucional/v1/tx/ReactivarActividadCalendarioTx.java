package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ReactivarActividadCalendarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ReactivarActividadCalendarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            UsuarioSessionDao usuarioSessionDao = (UsuarioSessionDao) FactoryDao.buildDao("UsuarioSessionDao");
            List<Integer> authorities = usuarioSessionDao.listarAutoridadesCalendario(data.getInt("usu"), data.getString("tip").charAt(0));

            MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao) FactoryDao.buildDao("web.MensajeElectronicoDao");
            String msm = "Saludos, necesito la reactivación de la actividad: " + data.getString("tit") + " , lo más antes posible. El periodo de tiempo requerido será de 1 semana.";
            MensajeElectronico message = new MensajeElectronico(0, "REACTIVAR ACTIVIDAD", msm, "", "", new Date(), data.getInt("usu"));
            System.out.println("A: " + authorities);
            mensajeDao.enviarMensaje(message, authorities);

            return WebResponse.crearWebResponseExito("Los mensaje fueron enviados correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "reactivarActividad", e);
            return WebResponse.crearWebResponseError("No se pudo enviar los mensajes", WebResponse.BAD_RESPONSE);
        }
    }
}
