

package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.ArchivoInventarioDAO;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Jeferson
 */
public class RegistrarArchivoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        ArchivosInventarioTransferencia archivo = null;
        JSONObject requestData = (JSONObject)wr.getData();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        ArchivoInventarioDAO archivo_dao = (ArchivoInventarioDAO)FactoryDao.buildDao("sad.ArchivoInventarioDAO");
        
         try{
             int det_inv_tra = requestData.getInt("det_inv_trans");
             int num_fol = requestData.getInt("num_folio");
             String titulo = requestData.getString("titulo");
             Date fecha = formatter.parse(requestData.getString("fecha"));
             String cod_expediente = requestData.getString("cod_expediente");
             
             String nombre_archivo = ""; 
            FileJsonObject miF = new FileJsonObject( requestData.getJSONObject("archivo") );
            nombre_archivo = "sad_"+miF.getName();
            
            //archivo.setNombre_archivo(nombre_archivo);
             
            BuildFile.buildFromBase64("sad", nombre_archivo, miF.getData());
             
            archivo = new ArchivosInventarioTransferencia(0,det_inv_tra,num_fol,titulo,fecha,cod_expediente,nombre_archivo);
            archivo_dao.insert(archivo);
            
         }catch(Exception e){
              System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Archivo de Inventario, datos incorrectos", e.getMessage() );
             
         }
         
         //Ahora registramos el Archivo 
         /*
         try{
            String nombre_archivo = ""; 
            FileJsonObject miF = new FileJsonObject( requestData.getJSONObject("archivo") );
            nombre_archivo = "sad_"+miF.getName();
            
            archivo.setNombre_archivo(nombre_archivo);
             
            BuildFile.buildFromBase64("sad", nombre_archivo, miF.getData());
             
         }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Archivo Adjunto", e.getMessage() );
             
         }
                 */
             
            JSONObject oResponse = new JSONObject();
            oResponse.put("arc_inv_tra_id",archivo.getarcinvtraID());
            oResponse.put("det_inv_trans",archivo.getdetinvtrans());
            oResponse.put("num_fol",archivo.getnumfol());
            oResponse.put("titulo",archivo.gettitulo());
            oResponse.put("fecha",archivo.getfecha());
            oResponse.put("cod_expediente",archivo.getcod_exp());
            oResponse.put("nombre_archivo","archivos/"+"sad"+"/"+archivo.getNombre_archivo());
         
         
           return WebResponse.crearWebResponseExito("El registro de Archivo de Inventario se realizo correctamente",oResponse);
      
    }
 
}
